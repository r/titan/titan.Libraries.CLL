#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
echo "ttcn3cov_compiler version 1.0"

#load config:
TTCN3COV_USEDMODULES_DEFAULT="*Definitions *Functions"

# used modules can be set in the file cov2ttcn3.cfg to override the default setting
if [ -r "./cov2ttcn3.cfg" ]
then
  source ./cov2ttcn3.cfg
  # if no modules are set: modules set to invalid value:
  if [ "${TTCN3COV_USEDMODULES}" == "" ]
  then
    TTCN3COV_USEDMODULES="*no-modules.cc"
  fi
  #remove cfg file if setting in it is the same as the default:
  if [ "${TTCN3COV_USEDMODULES}" == "${TTCN3COV_USEDMODULES_DEFAULT}" ]
  then
    rm ./cov2ttcn3.cfg
  fi
else
TTCN3COV_USEDMODULES="${TTCN3COV_USEDMODULES_DEFAULT}"
fi

#config loaded into TTCN3COV_USEDMODULES

if [ ! "$*" ]
then
echo "Modified TITAN compiler that adds coverage detection to TTCN"
echo "Usage: Use instead of TTCN3_DIR/bin/compiler"
exit
fi

if [ "$*" == "-v" ]
then
exit
fi

#add .cc extension to the used modules, remove any previous extension:
echo "ttcn3cov_compiler: modules to process: ${TTCN3COV_USEDMODULES}"
TTCN3COV_USEDMODULES=$(for i in ${TTCN3COV_USEDMODULES}; do out="${out} ${i%.*}.cc"; done; echo "$out"; unset out;)
echo "ttcn3cov_compiler: .cc files to process: ${TTCN3COV_USEDMODULES}"

FILESTOPROCESS=$(ls ${TTCN3COV_USEDMODULES})
FILESTOPATCH=""

#backup originals:
for f in ${FILESTOPROCESS}
do
 if [ ! -w $f ]
 then
  #echo "ttcn3cov_compiler: ${f} is readonly. Ignored..."
  continue;
 fi
 FILESTOPATCH="${FILESTOPATCH} $f"
 #make a backup:
 mv $f ${f}.backup
 cp ${f}.backup $f
done

echo "ttcn3cov_compiler: Invoking the TTCN3 compiler..."
# Call the original compiler:
#COMPILER="$(cd ..;which compiler)"
COMPILER=${TTCN3_DIR}"/bin/compiler"
#echo "Calling TTCN3 compiler: "${COMPILER}" "$*
${COMPILER} $*

if [ "$?" != "0" ]
then
#restore backups:
for f in ${FILESTOPATCH}
do
 rm $f
 mv ${f}.backup $f
done
exit 1 # compiler exited with an error code
fi

#Add files created by compiler, that did not exist before it was called.
#No backup needed this time:
FILESTOPROCESS=$(ls ${TTCN3COV_USEDMODULES})
for f in ${FILESTOPROCESS}
do
 if [ ! -w $f -o -r ${f}.backup ]
 then
  #echo "ttcn3cov_compiler: ${f} is readonly or already added. Ignored..."
  continue;
 fi
 FILESTOPATCH="${FILESTOPATCH} $f"
done

echo "ttcn3cov_compiler: Invoking the TTCN3 coverage patcher..."
$(dirname $0)/cov2ttcn3 ${FILESTOPATCH}
$(dirname $0)/prof2ttcn3 ${FILESTOPATCH}

# restore original if not changed:
UPDATE=""
for f in ${FILESTOPATCH}
do
 if [ ! -r ${f}.backup ]
 then
  difference="Yes"
 else
   # (if not changed only one or none line is changed: 3c3 )
   difference=`diff ${f}.backup $f | grep "^[0-9]*[acd][0-9]*" | wc -w | sed 's/[ ]*[01]//g'`
 fi
 #echo ${difference}
 if [ "${difference}" ]
 then
  echo "ttcn3cov_compiler: ${f} updated."
  if [ -r ${f}.backup ]
  then
    rm ${f}.backup
  fi
  UPDATE="yes"
 else
  #echo "ttcn3cov_compiler: "$f" unchanged."
  #restore backup:
  rm $f
  mv ${f}.backup $f  
 fi

 #break
done

if [ ! "${UPDATE}" ]
then
  echo "ttcn3cov_compiler: None of the patched files needed update."
fi
