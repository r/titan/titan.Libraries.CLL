#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
# This processes the output produced by the executable test suite.
# It merges them into a single aggregated output.

# example format of the input:
# 12345:<TAB>filename:123
#  ^ nofCalls in ^file ^line
#
# The code lines for a file:
#Code lines     in file EPTF_CLL_Variable_Functions.ttcn: 1541


BEGIN {
  FS=":[ \t]*|[ \t]+"
  executedLineCount=0
  codeLineCount=0
  totalCalls=0
  usedFnsCounter=0
  allFunctions=0
  totalFnCalls=0
  print "------------------------------"
  print "-   Code Lines in files      -"
  print "------------------------------"
}

#Example line:
#33:	EPTF_CLL_LoadRegulator_Functions.ttcn:102 f_EPTF_LoadRegulator_init_CT
#$1     $2                                     $3   $4
# $4 can be empty
/^[[:digit:]]+:/ {
  if (executed[$2":"$3]==0) {
    lineno = sprintf("%07d", $3); # this is the line number
    linesSort[++executedLineCount] = $2":"lineno  # executedLineCount: total number of executed code lines
    lines[$2":"lineno] =  $2":"$3
  }
  if (fns[$2":"$3]!="") {
    if (usedFns[$2":"$3]==0) {
      usedFns[$2":"$3]=1;         # marks the function as used
      usedFnsInFileCounter[$2]+=1 # number of functions used in a file
      usedFnsCounter++            # total number of functions used
    }
    fnCalls[$2] += $1           # total function calls in a file
    totalFnCalls += $1          # total function calls
  }
  executed[$2":"$3]+=$1 # how many times a code line is executed
  calls[$2] += $1       # how many calls were executed in a file
  totalCalls += $1      # total number of calls
  
  #for the top 10:
  executedSort[$2":"$3]=sprintf(" %12d: %s",executed[$2":"$3],$2":"$3" "fns[$2":"$3])
}

# Example line:
#Code lines in file EPTF_CLL_Variable_Functions.ttcn: 1541
#  $1  $2    $3  $4   $5                               $6  
/Code lines/ {
  if (Files[$5]==0) {
    Files[$5]=$5        # file database to ignore multiple entries for the same file. Only the first is taken into account.
    CodeLines[$5]=$6    # number of code lines in a file
    codeLineCount+=$6;  # total number of code lines
    if (fileList[$5]=="") {
      fileList[$5]=$5;
      calls[$5] += 0;
      fnCalls[$5] += 0;
      ExecutedLines[$5] += 0;
      usedFnsInFileCounter[$5] += 0;
      allFnsInFileCounter[$5] += 0;
    }
    print "Code lines in file "$5": " $6
  }
}

#function references from TTCN3Coverage.fns:
# Example line:
#f: f_EPTF_Var_getContent EPTF_CLL_Variable_Functions.ttcn:1828
#$1   $2                    $3                             $4
/^f:/ {
  #print "functions: " $2" at "$3":"$4
  if (fns[$3":"$4]==0) {
    fns[$3":"$4]=$2                     # maps code lines to functions names
    fnLocation[++allFunctions]=$3":"$4  # allFunctions: total number of functions
    allFnsInFileCounter[$3]+=1          # total number of functions in a file
  }
}

END {
  print "Total code lines: "codeLineCount
  print ""
  print "------------------------------"
  print "-     Calls per lines        -"
  print "------------------------------"
  n=asort(linesSort)
  for(i = 1; i <= n; i++) {
    line = lines[linesSort[i]];
    print executed[line]":\t"line" "fns[line];
  }
  print ""
  print "------------------------------"
  print "-   Calls per lines sorted   -"
  print "------------------------------"
  n=asort(executedSort);
  for(i=1; i<=n; i++) {
    print executedSort[i]
  }

  print ""
  print "------------------------------"
  print "-   Top 10  function calls   -"
  print "------------------------------"
  m=10;
  top10Fns=""
  for(i=0; i<n; i++) {
    split(executedSort[n-i],spl);
    #if function line:
    if (spl[5]!="") {
      top10Fns = spl[3]":"spl[4]":\t`"spl[5]"' was called "spl[2]" times.\n"top10Fns
      if (prevCounter!=spl[2]) {
        m--
        prevCounter=spl[2]
      }
    }
    if (m==0) {
      break
    }
  }
  printf top10Fns;

  print ""
  print "------------------------------"
  print "-   Top 10 code line calls   -"
  print "------------------------------"
  m=10;
  top10Fns=""
  for(i=0; i<n; i++) {
    split(executedSort[n-i],spl);
    #if function line:
    if (spl[5]=="") {
      top10CodeLines = spl[3]":"spl[4]": called "spl[2]" times.\n"top10CodeLines
      if (prevCounter!=spl[2]) {
        m--
        prevCounter=spl[2]
      }
    }
    if (m==0) {
      break
    }
  }
  printf top10CodeLines;

  #collect files and calculate ExecutedLines
  n=asort(linesSort)
  for(i = 1; i <= n; i++) {
    line = lines[linesSort[i]];
    split(line,lineSplit,":");
    fileName=lineSplit[1]
    ExecutedLines[fileName]++;
    if (fileList[fileName]=="") {
      fileList[fileName]=fileName;
    }
  }
  n=asort(fileList);

  print ""
  print "------------------------------"
  print "-   Total calls per files    -"
  print "------------------------------"
  for(i=1; i<=n; i++) {
    fileName=fileList[i];
    callsInFile[fileName] =  sprintf( "Total calls: %12d in  "fileName, calls[fileName])
    
    if (ExecutedLines[fileName]==0) {
      callsPerExecuted = 0;
    } else {
      callsPerExecuted = calls[fileName]/ExecutedLines[fileName];
    }
    avgCallsPerExecuted[fileName] =  sprintf( "Average callsPerExecuted: %10.2f =%12d/"ExecutedLines[fileName]" \tin  "fileName, callsPerExecuted,calls[fileName])
  }
  m=asort(callsInFile);
  for(i=1; i<=m; i++) {
    print callsInFile[i];
  }
  print ""
  print "------------------------------"
  print "-  Total calls in all files  -"
  print "------------------------------"
  printf( "All calls: %d\n",totalCalls)
  print ""
  print "------------------------------------"
  print "- Average calls per executed lines -"
  print "------------------------------------"
  m=asort(avgCallsPerExecuted);
  for(i=1; i<=m; i++) {
    print avgCallsPerExecuted[i];
  }
  if(executedLineCount==0) {
    cov=0
  } else {
    cov=totalCalls/executedLineCount
  }
  print ""
  print "----------------------------------"
  print "- Total calls per executed lines -"
  print "----------------------------------"
  print "Total callsPerExecuted: "cov" ("totalCalls"/"executedLineCount")"
#  print ""
#  print "----------------------------------------------"
#  print "-  Code Coverage per files (alphabetic list) -"
#  print "----------------------------------------------"
  for(i = 1; i <= n; i++) {
    fileName=fileList[i];
    #print "Total code lines     in "fileName": "CodeLines[fileName]
    #print "Total executed lines in "fileName": "ExecutedLines[fileName]
    if (CodeLines[fileName]=="") {
#      print "Invalid filename: "fileName
      continue
    }
    if (CodeLines[fileName]==0) {
      cov=0.0
    } else {
      cov=ExecutedLines[fileName]/CodeLines[fileName];
    }
    coverage[fileName] =  sprintf( "Code Coverage: %6f ("ExecutedLines[fileName]"/"CodeLines[fileName]") \tof "fileName, cov)
#    print coverage[fileName];
  }  
  print ""
  print "------------------------------"
  print "-  Code Coverage per files   -"
  print "------------------------------"
  #sort coverage by coverage size:
  n=asort(coverage)
  for(i = 1; i <= n; i++) {
    print coverage[i];
  }

  print ""
  print "------------------------------"
  print "-     Total Code Coverage    -"
  print "------------------------------"
  if (codeLineCount==0) {
    cov=0.0
  } else {
    cov=executedLineCount/codeLineCount
  }
  printf( "Total code coverage: %6f ("executedLineCount"/"codeLineCount")\n",cov)

  print ""
  print "-------------------------------"
  print "-  Functions called in files  -"
  print "-------------------------------"
  n=asort(fileList);
  for(i = 1; i <= n; i++) {
    fnsUsedInFile[i] = sprintf( "Functions called: %6d in file "fileList[i],usedFnsInFileCounter[fileList[i]]);
  }
  m=asort(fnsUsedInFile);
  for(i=1; i<=m; i++) {
    print fnsUsedInFile[i];
  }

  print ""
  print "---------------------------------"
  print "- Total function calls in files -"
  print "---------------------------------"
  n=asort(fileList);
  for(i = 1; i <= n; i++) {
    fnCallsInFile[i] = sprintf( "Total function calls: %12d in file "fileList[i],fnCalls[fileList[i]]);
  }
  m=asort(fnCallsInFile);
  for(i=1; i<=m; i++) {
    print fnCallsInFile[i];
  }
  print ""
  print "-------------------------------------"
  print "- Total function calls in all files -"
  print "-------------------------------------"
  print "All function calls: "totalFnCalls

  print ""
  print "-------------------------------"
  print "- Function Coverage per files -"
  print "-------------------------------"
  n=asort(fileList);
  for(i = 1; i <= n; i++) {
    fileName = fileList[i];
    if (allFnsInFileCounter[fileName]==0) {
      cov=0;
    } else {
      cov = usedFnsInFileCounter[fileName]/allFnsInFileCounter[fileName]
    }
    fnCovInFile[i] = sprintf( "Function Coverage: %6f ("usedFnsInFileCounter[fileName]"/"allFnsInFileCounter[fileName]") \tin "fileName, cov)
  }
  m=asort(fnCovInFile);
  for(i=1; i<=m; i++) {
    print fnCovInFile[i];
  }

  print ""
  print "------------------------------"
  print "-  Total Function Coverage   -"
  print "------------------------------"
  if (allFunctions==0) {
    cov=0.0
  } else {
    cov=usedFnsCounter/allFunctions
  }
  print "Total function coverage: "cov" (" usedFnsCounter "/" allFunctions ")"
}
