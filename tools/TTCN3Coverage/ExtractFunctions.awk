#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN {
FS="\"|,[ \t]*|[ \t]+|[()]+|:[ \t]*"
}

#TTCN_Location current_location("EPTF_CLL_Base_Functions.ttcn", 97, TTCN_Location::LOCATION_FUNCTION, "f_EPTF_Base_init_CT");
#  $1              $2           $3     $4                  $5  $6      $7        $8    $9           $10       $11
/TTCN_Location current_location/ {
  #ignore pre_init_module (lineno==0), because the modulepar of the TTCN3Coverage was not created there:
  if ($6==0) {
    next;
  }
#  print "x"$1,"x"$2,"x"$3,"x"$4,"x5"$5,"x"$6,"x"$7,"x"$8,"x"$9,"x10"$10,"x"$11
  if (fns[$4":"$6]==0 && modulefns[$11" "$4]==0) {
    fns[$4":"$6]=$11
    modulefns[$11" "$4]=$6
    print ("f: "$11" "$4":"$6)
  }
}

#function references from TTCN3Coverage.fns:
# Example line:
#f: f_EPTF_Var_getContent EPTF_CLL_Variable_Functions.ttcn:1828
#$1   $2                    $3                             $4
/^f:/ {
  #print "functions: " $2" at "$3":"$4
  if (fns[$3":"$4]==0 && modulefns[$2" "$3]==0) {
    fns[$3":"$4]=$2                     # maps code lines to functions names
    modulefns[$2" "$3]=$4
    print ("f: "$2" "$3":"$4)
  }
}

END {
}
