#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN {
FS=",[ \t]*|[ \t]+|[()]+"
currentFile=""
currentVar="ttcn3cov"
ignore=0
}

// {
  print $0
}

/TTCN_Location current_location/ {
  #print $0
  currentFile=$3
  #ignore pre_init_module (lineno==0), because the modulepar of the TTCN3Coverage was not created there:
  if ($4==0) {
    ignore=1
  } else {
    ignore=0
    print currentVar".enterFunction(",$3,",",$4,",",$6,");"
  }
}

/current_location.update_lineno/ {
  #print $0
  if (ignore==1) {
    next
  }
  #ignore duplicated lines
  if(data[$0]++ == 0) {
    print (currentVar".executeLine(",currentFile,",",$2,");")
  }
}

/Including header files/ {
  sourceFileName=""
  command="gawk -f GetSourceFileName.awk "FILENAME
  command  | getline sourceFileName
  close(command)
  
  if (sourceFileName=="") {
    next
  }
  
  print "#include \"ttcn3cov.hh\""

  #to increase speed create local variable in each file:
  #codelines can be determined in this case only
  split(FILENAME, file,".")
  currentVar2="ttcn3cov_"file[1];
  #determine the number of code lines:
  getCodeLines="gawk -f CodeLineCount.awk "FILENAME" | grep CodeLines | gawk '{print $3}'";
  #print codeLines
  print "TTCN3COV_SetCodeLines "currentVar2"(\""file[1]"\","
  system(getCodeLines)
  print");"
}


END {
}
