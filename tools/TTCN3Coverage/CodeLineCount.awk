#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN {
FS=",[ \t]*|[ \t]+|[()]+"
codeLines=0
ignore=0
}

#duplicated lines are removed

/TTCN_Location current_location/ {
  #ignore pre_init_module (lineno==0), because the modulepar of the TTCN3Coverage was not created there:
  if ($4==0) {
    ignore=1
    next;
  } else {
    ignore=0
  }
  if (data[$0]++ == 0) {
#    lines[++count] = $0
    codeLines++
  }
}

/current_location.update_lineno/ {
  if (ignore==1) {
    next
  }
  if (data[$0]++ == 0) {
#    lines[++count] = $0
    codeLines++
  }
}


END {
  print "CodeLines : "codeLines
}
