#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
# this file is used by the nedit macro: ShowCoverage.pats
# needed to generate the file with coverage info at the beginning of the lines
# nedit highlights the lines according to this number

BEGIN {
  FS=":[ \t]*|[ \t]+"
  readInfo=0
}

/Calls per lines/ {
  readInfo=1
}

/Calls per lines sorted/ {
  readInfo=0
}

#33:	EPTF_CLL_LoadRegulator_Functions.ttcn:102 f_EPTF_LoadRegulator_init_CT
#$1     $2                                     $3   $4
# $4 can be empty
/^[[:digit:]]+:/ {
  if (readInfo==1) {
    executed[$2":"$3]=$1
    knownFiles[$2]=1
  }
}

// {
  lineCount[FILENAME]++
  if (knownFiles[FILENAME]==1) {
    printf("%12d: %s\n", executed[FILENAME":"lineCount[FILENAME]]+0, $0)
    #print executed[FILENAME":"lineCount[FILENAME]]+0 " : " $0
  }
}


END {

}
