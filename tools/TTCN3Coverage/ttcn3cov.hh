///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////


#ifndef TTCN3COV_HH
#define TTCN3COV_HH


#include <TTCN3.hh>
#include "EPTF_CLL_TTCN3Coverage.hh"

class TTCN3COV_GLOBAL {
public:
  TTCN3COV_GLOBAL() {};
  ~TTCN3COV_GLOBAL();
};

class TTCN3COV {
public:
  TTCN3COV(const char* moduleName=NULL, int codeLines=-1):
    currentLocationIdx(-1),currentLocationFile(NULL)
    ,coverageDb(NULL_VALUE)
    ,hashmapId(-1),moduleName(moduleName)
    ,codeLines(codeLines) {
    // this is needed to force the ttcn3cov_global object to be constructed before any TTCN3COV => it will be destructed last:
    static TTCN3COV_GLOBAL ttcn3cov_global=TTCN3COV_GLOBAL(); // this var generates the coverage info when destructed on the HC
    //fprintf(stderr,"TTCN3COV constructed\n");
    if (moduleName!=NULL) {
      setLocation((const char*)(CHARSTRING(moduleName)+".ttcn")); // to set it even if no code line is called in the file
    }
    //fprintf(stderr,"TTCN3COV constructed done\n");
  };
  ~TTCN3COV() {
    printStat();
    //fprintf(stderr,"TTCN3COV destroyed\n");
  };
  
  void enterFunction(const char* filename,const int& lineno,const char* functionName);
  void executeLine(const char* filename,const int& lineno);
  
  void printStat(); // prints the coverage statistics
  void writeToFile(const char* data); // writes data to file
  static void log(const char *fmt, ...);
  void setCodeLines(const char* moduleName, int codeLines);

  static TTCN3COV& getInstance() {
    static TTCN3COV ttcn3cov;
    return ttcn3cov;
  }
  
private:
  void setLocation(const char* filename); // sets the current location 
  void incCov(const int& element, const int& lineno); // increments coverage counter at the current location
  int findElement(const char* filename);
  int newElement(const char* filename);

  // location database
  int currentLocationIdx;
  char* currentLocationFile;
  EPTF__CLL__TTCN3Coverage::EPTF__TTCN3CoverageDb coverageDb;
  INTEGER hashmapId;
  CHARSTRING moduleName;
  int codeLines;
};

extern TTCN3COV& ttcn3cov;

class TTCN3COV_SetCodeLines {
public:
  TTCN3COV_SetCodeLines(const char* moduleName=NULL, int codeLines=-1) {
    if (codeLines!=-1 && moduleName!=NULL) {
      TTCN3COV::getInstance().setCodeLines(moduleName,codeLines);
    }
  }
};


#endif
