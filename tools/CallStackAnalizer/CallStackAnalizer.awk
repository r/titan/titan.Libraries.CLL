#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
# Analizes the callstacks in the given Titan logfiles
# and prints the callstack ordered by how many functions or altstep calls it has
#
# Logging settings in Titan cfg file for FileMask should include PORTEVENT and TIMEROP
# Also the following should be set also:
# SourceInfoFormat := Stack
# LogEventTypes := Yes
# LogEntityName := Yes

#
# usage:
#awk -f CallStackAnalizer.awk *.log > stack.txt

BEGIN {
  if (PROCINFO["version"] < "4.1.0") {
    print "gawk version " PROCINFO["version"] " is not supported. Use version 4.1.0 or above."
    exit
  }
}
// {
  if (index($4, "ttcn") != 0) {
    stackStr = $4
  } else if (index($3, "ttcn") != 0) {
    stackStr = $3
  } else if (index($5, "ttcn") != 0) {
    stackStr = $5
  }
 
  target = stackStr;
  n = gsub(/->/, "===", target);
  target = stackStr;
  stack[stackStr]=n #" : " stackStr;
  as = gsub(/altstep:/, "as:", target);
  altstep[stackStr]=as #" :" stackStr ;
  #print n  ": " stackStr
  #print as ": " stackStr
}

END {
#  n=asort(stack, tmp);
#  for(i=1; i<=n; i++) {
#    print tmp[i];
#  }
#  print "---------------"
#  n=asort(altstep, tmp);
#  for(i=1; i<=n; i++) {
#    print tmp[i];
#  }
  print "---------- LIST BASED ON THE LENGHT OF THE STACK ---------------"
  print "StackLenght: (AltstepNum) Call-stack"
  PROCINFO["sorted_in"]="@val_num_desc"
  for (s in stack) {
    print stack[s] ": (" altstep[s] ") " s
  }
  print ""
  print "---------- LIST BASED ON THE NUMBER OF ALTSTEPS IN THE STACK ---------------"
  print "AltstepNum: (StackLenght) Call-stack"
  for (a in altstep) {
    print altstep[a] ": (" stack[a] ") " a
  }
}
