///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
function CView(aViewModel)
{
	/** private members */
    var mViewModel = aViewModel;
    var mAutoGuiView = new CAutoGuiView();
    var mThis = this;

	/** public functions */
    this.applicaionCreated = function()
    {
        setupCallbacks();
        this.getHelp();
    }

    this.refreshView = function(aData, aFullRebuild)
    {
        if (aFullRebuild)
        {
            var lMainDivObj = document.getElementById("titan_main");
            lMainDivObj.innerHTML = null;
            mAutoGuiView.buildElementFromData(lMainDivObj, aData, 1, '');
        }
        else
        {
            mAutoGuiView.buildElementFromData(null, aData, 1, '', true);
        }
    }

    this.getHelp = function()
    {
        var lMainDivObj = document.getElementById("titan_main");
        lMainDivObj.innerHTML = null;
        mViewModel.getHelp(refreshJSTree);
    }

    this.exit = function()
    {
        mViewModel.exit();
    }

    this.configure = function()
    {
        function cfgGot(aCfgText) {
            var lMainDivObj = document.getElementById("titan_main");
            lMainDivObj.innerHTML = '<textarea id="CfgEditor" spellcheck="false" class="boxsizingBorder" rows="200" cols="200">' + aCfgText + '</textarea>';
        }    
        mViewModel.getCfg(cfgGot);
    }
    
	/** private functions */
    function refreshJSTree(aRqJSONStr)
    {
        $('#jstree').jstree("destroy");
        $('#jstree').jstree({'core':{'data': JSON.parse(aRqJSONStr) }});
        $('button').on('click', function () {
            $('#jstree').jstree(true).select_node('child_node_1');
            $('#jstree').jstree('select_node', 'child_node_1');
            $.jstree.reference('#jstree').select_node('child_node_1');
        });
        $('#jstree').bind("hover_node.jstree", function (e, data) 
        {
            $("#"+data.node.id).prop('title', data.node.data);
        })
        $('#jstree').bind("select_node.jstree", function (e, data) 
        {
            $('#helpInfo').html(data.node.data);
        })
    }

    function action(e)
    {
        var lCklickedGD = e.target.getData;
        if (lCklickedGD != null)
        {
            var lOrigGDRef = lCklickedGD.origRq;
            var lIsContainer = e.target.id.search("_elem") > 0;
            var lIsText = e.target.id.search("_txt") > 0;
            mViewModel.command(lCklickedGD, lOrigGDRef, e.target.innerHTML, lIsContainer, lIsText);
        }
    }

    function setupCallbacks() 
    {
        $("#titan_main").on('click', action);
        $("#titan_header_btnExit").on('click', mThis.exit);
        $("#titan_header_btnHelp").on('click', mThis.getHelp);
        $("#titan_middle_startTest").on('click', mThis.startAll);
        $("#titan_middle_stopTest").on('click', mThis.stopAll);
        $("#titan_header_btnConfigure").on('click', mThis.configure);
        $("#titan_header_btnSave").on('click', mThis.saveCfg);
        $("#titan_header_btnTitanSim").on('click', mThis.home);
    }
}
