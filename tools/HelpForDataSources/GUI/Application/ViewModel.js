///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
/** App specific runtime behaviour */

function CViewModel(aDsRestAPI)
{
    /** private members */
    var mDsRestAPI = aDsRestAPI;
    var mAppMain;
    var mRequest = null;

    /** constructor */

    /** public functions */
    this.setAppMain = function(aAppMain)
    {
        mAppMain = aAppMain;
    }
        
    this.getRequest = function()
    {
        return mRequest;
    }

    this.getChangedData = function(aHandler)
    {
        if (mRequest != null)
            mDsRestAPI.getList(mRequest, aHandler);
    }

    this.getHelp = function(aHandler)
    {
        var lHandler = aHandler; // not thread-safe
        function l_got_help(aData)
        {
            var lData = JSON.parse(hex2a(aData.node.val));
            var lRqJSONStr = convertHelpToJsTreeData(lData.sources);
            lHandler(lRqJSONStr);
        }
        mDsRestAPI.getData("DataSource", "", "help", [{"paramName": "Format", "paramValue": "JSON"}], l_got_help);
    }

    this.exit = function()
    {
        function l_set_exitInfo(aData)
        {}
        function l_got_exitInfo(aData)
        {
            mDsRestAPI.setData("ExecCtrl", undefined, "Exit", undefined, "" + (parseInt(aData.node.val) + 1), [], aData.node.tp, l_set_exitInfo);
        }
        mDsRestAPI.getData("ExecCtrl", "", "Exit", [], l_got_exitInfo);
    }

    /** private functions */
    function hex2a(hex) 
    {
        var str = '';
        for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }

    function replaceAll(str, str1, str2)
    {
       var index = str.indexOf( str1 );     
       while (index != -1)
       {
          str = str.replace(str1, str2);
          index = str.indexOf(str1);
       }
       return str;
    }

    function convertHelpToJsTreeData(aData)
    {
        var result = '[';
        for (var i = 0; i < aData.length; i++)
        {
            var lChildrenStr = '';

            if (aData[i].children != undefined && aData[i].children.length > 0)
                lChildrenStr = convertHelpToJsTreeData(aData[i].children);
            else if (aData[i].dataElements != undefined && aData[i].dataElements.length > 0)
                lChildrenStr = convertHelpToJsTreeData(aData[i].dataElements);

            if (aData[i].dataElement != undefined)
                result += '{"text":"' + aData[i].dataElement.name + '"';
            else
                result += '{"text":"' + aData[i].source + '"';

            var lInfo = "No description";
            if (aData[i].dataElement != undefined)
            {
                lInfo = JSON.stringify(aData[i].dataElement, null, 4);
                lInfo = replaceAll(lInfo , '\n', "\\n");
                lInfo = replaceAll(lInfo , '\\"', "'");
                lInfo = replaceAll(lInfo , '"', "'");
            }
            result += ',"data":"' + lInfo + '"';
            if (lChildrenStr != '')
                result += ',"children":' + lChildrenStr;
            result += "}";
            if (i < aData.length - 1)
                result += ",";
        }
        result[result.length - 1] = " ";
        result += "]";
        return result;
    }

    // function convertRequestToJsTreeData(aData)
    // {
        // var result = '[';
        // for (var i = 0; i < aData.length; i++)
        // {
            // var lChildrenStr = '';
            // if (aData[i].getData.children != undefined && aData[i].getData.children.length > 0)
                // lChildrenStr = convertRequestToJsTreeData(aData[i].getData.children);
            // result += '{"text":"' + aData[i].getData.element + '"';
            // if (lChildrenStr != '')
                // result += ',"children":' + lChildrenStr;
            // result += "}";
            // if (i < aData.length - 1)
                // result += ",";
        // }
        // result[result.length - 1] = " ";
        // result += "]";
        // return result;
    // }
}
