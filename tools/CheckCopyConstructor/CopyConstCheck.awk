#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN {
  copyConstString = "_shadow";
  currentFunction = ""
}
/[[:alpha:]]+\([^\)]+\)$/ {
  currentFunction = $0
}
/_shadow\(/ {
  Type = $1;
  match($0, /_shadow\(([^\)]+)\)/, myarray) 
  argument = myarray[1];
  match(FILENAME, /([^.]+).cc/, myarray) 
  module = myarray[1];
  
  print("WARNING: In module ",module,": Copy constructor found in:",
    "\n  Function: ",currentFunction,
    "\n  Type of copied argument: ", Type,
    "\n  Name of copied argument: ", argument)
}

END {
}
