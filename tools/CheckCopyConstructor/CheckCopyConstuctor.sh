#/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////


gawk -f /vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/tools/CheckCopyConstructor/CopyConstCheck.awk *.cc
