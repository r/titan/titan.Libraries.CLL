#!/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

if ([$1 == ""]) then
echo '// usage:
createPool.sh <poolType>

// The generated output is in the file generatedCode.ttcn
// It contains the ttcn code that should be inserted into the ttcn module where
// the type <poolType> is defined. And also some instructions.
'
exit
fi

POOLTYPE=$1

echo '
module tmp {
// define POOL for '$POOLTYPE':
#define EPTF_POOLTYPE '$POOLTYPE'
#include "EPTF_CLL_PoolMgmt_Functions.ttcnin"
#undef EPTF_POOLTYPE

} // end of module' > tmp.ttcnpp

ttcn3_makefilegen -p tmp.ttcnpp

make tmp.ttcn

echo '///////////////////////////////////////////////////////////////////
// Insert the following code into the module where the type '$POOLTYPE' is defined:
///////////////////////////////////////////////////////////////////

/*
// THE GENERATED CODE BELOW IS EQUIVALENT WITH THE FOLLOWING MACRO:
// define POOL for '$POOLTYPE'
#define EPTF_POOLTYPE '$POOLTYPE'
#include "EPTF_CLL_PoolMgmt_Functions.ttcnin"
#undef EPTF_POOLTYPE
*/

//////////////////////// BEGINING OF CODE /////////////////////////
// GENERATED CODE, DO NOT EDIT BELOW!
' > generatedCode.ttcn

cat tmp.ttcn |
sed 's/module tmp {//g' |
sed 's/^}//g' >> generatedCode.ttcn
echo '
// DO NOT EDIT ABOVE THIS LINE!
//////////////////////////////////////////////////////////////

// NOTE!
// Define the following constant:
const '$POOLTYPE' c_empty'$POOLTYPE' := <YOU SHOULD FILL THIS>;
modulepar boolean tsp_debugVerbose_PoolMgmt := false;

//////////////////////// END OF CODE /////////////////////////



//////////////////////////////////////////////////////////////
// insert the following line into your component definition:
//////////////////////////////////////////////////////////////
var '$POOLTYPE'Pool v_'$POOLTYPE'Pool; // pool for type '$POOLTYPE'

//////////////////////////////////////////////////////////////
// do not forget to initialize your pool with:
//////////////////////////////////////////////////////////////
f_EPTF_init'$POOLTYPE'Pool(v_'$POOLTYPE'Pool);

// add a new data element to the pool with the function:
f_EPTF_setNew'$POOLTYPE'

// set the value of an element in the pool with the function:
f_EPTF_setValue'$POOLTYPE'

// get the value of an element from the pool with the function:
f_EPTF_getValue'$POOLTYPE'

// remove an element from the pool with:
f_EPTF_delete'$POOLTYPE'

// get the first used element index:
f_EPTF_getFirst'$POOLTYPE'Index

// get the next used element:
f_EPTF_getNext'$POOLTYPE'Index

// Do not use the other functions!!!
// if you would like to change the user data in the pool at given pl_idx index
// the preferred way is to use the f_EPTF_setValue'$POOLTYPE' function, but
// you may use the following structure to avoid unnecessary copying:
v_'$POOLTYPE'Pool.userData[pl_idx]
 
' >> generatedCode.ttcn

make clean
rm Makefile
rm tmp.ttcn*
