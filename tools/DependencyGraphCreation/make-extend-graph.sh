#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

if [ -z $1 ] 
then
  echo "usage: $0 <ttcnmodules>"
else
    echo digraph modules{
    echo size=\"16\"\;
    echo rankdir=RL\;
    echo ratio=compress\;
    
#     echo $* 
#     for file in $* 
#     do
#     echo   \"`basename $file| cut -d '.' -f1`\"\;
#     done
    
    for file in `fgrep -l extends $*`
    do 
	#echo file: $file
	#echo extender lines:
	for extenders  in "`fgrep extends $file| sed -e 's/\/\/.*//'| tr -s ' ' | sed -e 's/^ //g'| cut -d ' ' -f 3 `"
	do
	    #echo extenders: $extenders
	    for extender in $extenders
	    do
		extended="`fgrep extends $file| sed -e 's/\/\/.*//'| fgrep $extender|  tr -s ' ' |sed -e 's/^ //g' |  cut -d ' ' -f 5| tr -d "," | tr -d "{"`"
		#echo extender: $extender
		#echo extended: $extended
		echo "\"$extender\" -> \"$extended\""
	    done
	done
    done
    echo }
fi
