#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
#//
#//  File:     feature_graph.sh
#//  Rev:      
#//  Prodnr:   
#//  Updated:  2008-10-09
#//  Contact:  http://ttcn.ericsson.se
#///////////////////////////////////////////////////////////////////////////////

if [ -z $1 ] 
then
  echo "usage: $0 <src-directory> [-t]"
  exit 1
fi

if [ "$2" != "-t" ]
then
  echo "digraph features{"
  echo size=\"16\"\;
  echo rankdir=RL\;
  echo ratio=compress\;
fi

features=`ls -p $1 | grep /`
#echo $features

filelists[0]=""
modulelist[0]=""
importlist[0]=""
feature_index=0

# PASS1: for all feature, get the filelist, module list and import/include list
for feature in $features
do
  filelist[$feature_index]=`find ${1}/${feature} -type f -name \*ttcn\*`
  #echo "Files in feature " $feature ":"
  #echo ${filelist[$feature_index]}
  modules=
  imports=
  if [ "$2" != "-t" ]
  then
    for file in ${filelist[$feature_index]}
    do
      modules=${modules}" "`basename $file |cut -d '.' -f1`
      importsinfile="`fgrep import $file| sed -e 's/\/\/.*//'| tr -s ' ' | sed -e 's/ import/import/g' | cut -d ' ' -f 3`"
      includesinfile=`fgrep \#include $file| sed -e 's/\/\/.*//'| cut -d ' ' -f 2 |tr -d '"' | cut -d '.' -f1`
      imports=$imports" "$importsinfile
      imports=$imports" "$includesinfile
      #echo "Imports/includes in file" $file ":"
      #echo $importsinfile
    done
  fi
  #echo "Modules in feature " $feature ":"
  #echo $modules
  modulelist[$feature_index]=$modules
  #echo "Imports in feature " $feature ":"
  #echo $imports
  importlist[$feature_index]=$imports
  let "feature_index += 1"
done
#echo ${filelists[@]}

# test module names
if [ "$2" = "-t" ]
then
  for file in ${filelist[@]}
  do
    tmp=`echo $file | grep -v ttcnin`
    if [ -n "$tmp" ]
    then
      filename=`basename $file |cut -d '.' -f1`
      modulename=`grep -w module $file | grep -v "//" | grep -v "modulepar"`
      tmp=`echo $modulename | grep $filename`
      if [ -z "$tmp" ]
      then
        echo "ERROR: file "$filename" has incorrect module name definition \""$modulename"\""
      fi
    fi
  done
  exit
fi

#PASS2: for all features' import list, find matches in all other features' module list
feature_index=0
for feature in $features
do
  noimport=1
  module_index=0
  for feature2 in $features
  do
    if [ $module_index != $feature_index ]
    then
      #loop through the 'imports' of 'feature' and
      #check if it imports from 'modules' of 'feature2'
      found=0
      for import in ${importlist[$feature_index]}
      do
        if [ $found -eq 0 ]
        then
          tmp=`echo ${modulelist[$module_index]} | fgrep $import`
          if [ -n "$tmp" ]
          then
            f1=`echo $feature |cut -d '/' -f1`
            f2=`echo $feature2 |cut -d '/' -f1`
            echo "\"$f1\" -> \"$f2\";"
            #echo "Feature " $f1 " imports from feature " $f2
            found=1
            noimport=0
          fi
        fi
      done
    fi
    let "module_index += 1"
  done
  if [ $noimport -ne 0 ]
  then
    f1=`echo $feature |cut -d '/' -f1`
    echo "\"$f1\";"
  fi
  let "feature_index += 1"
done

echo "}"
