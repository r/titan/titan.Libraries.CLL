#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
rm   deps.extend.dot deps.import.dot  deps.extend.png deps.import.png
./make-extend-graph.sh $* > deps.extend.dot; 
./make-import-graphs.sh  $* >deps.import.dot
dot -Tpng  -o deps.extend.png deps.extend.dot 
dot -Tpng  -o deps.import.png deps.import.dot 
