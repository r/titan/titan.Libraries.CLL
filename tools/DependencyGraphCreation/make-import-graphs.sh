#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

if [ -z $1 ] 
then
  echo "usage: $0 <ttcnmodules>"
else
    echo "digraph modules{"
    echo size=\"16\"\;
    echo rankdir=RL\;
    echo ratio=compress\;
    
    #echo $*
#     node="" 
#     for file in $* 
#     do
#      node="\"`basename $file | cut -d '.' -f1`\""
#      #node="'`basename $file | cut -d '.' -f1`'"
#      if [ -n "`echo $node | fgrep EPTF_CLL`" ]
#      then 
#          node=$node" [style=filled, color=gold1]"
#      elif [ -n "`echo $node | fgrep EPTF_CS`" ]
#      then 
#          node=$node" [style=filled, color=green]"
#      fi
#      echo node $node\;
#     done
    
    for file in $* 
    do
    ##debug stuff starts
    #echo "==>Doing file: $file"
    #echo "====>imports:" `fgrep import $file|sed -e 's/\/\/.*//'`
    #echo "====>#includes:" `fgrep \#include $file| sed -e 's/\/\/.*//'`
    #echo 
    ##debug stuff ends
    #obtain ttcn imports
    imports="`fgrep import $file| sed -e 's/\/\/.*//'| tr -s ' ' | sed -e 's/ import/import/g' | cut -d ' ' -f 3`"
    #obtain #includes
    includes=`fgrep \#include $file| sed -e 's/\/\/.*//'| cut -d ' ' -f 2 |tr -d '"' | cut -d '.' -f1`

    for import in $imports $includes 
    do 
	#echo file: `basename $file`
	#echo import: $import
	if [ -n $import ]
	then
	echo "\"$import\" -> \"`basename $file |cut -d '.' -f1`\";"
	fi
    done
    done
    echo "}"
fi

