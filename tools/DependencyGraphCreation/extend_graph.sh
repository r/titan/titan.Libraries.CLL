#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
#//
#//  File:     extend_graph.sh
#//  Rev:      
#//  Prodnr:   
#//  Updated:  2008-10-09
#//  Contact:  http://ttcn.ericsson.se
#///////////////////////////////////////////////////////////////////////////////

if [ -z $1 ] 
then
  echo "usage: $0 <src-directory>"
else

  filelist=`find ${1} -type f -name \*ttcn\*`

  echo "digraph components{"
  echo size=\"16\"\;
  echo rankdir=RL\;
  echo ratio=compress\;

  for file in $filelist
  do
    # 1. cat file
    # 2. replace '{' to ' { ' - this is needed in case "type component myCT{" would be tokenized to "myCT{"
    # 3. remove // comments and string literals, replace ',' with ' '
    # 4. replace '/*' to ' /* ' and '*/' to ' */ '
    # 5. remove duplicated spaces
    tokens=`cat $file | sed -e 's/{/ { /g' | sed -e 's/\/\/.*//g' -e 's/\".*\"//g' -e 's/,/ /g' | sed -e 's/\/\*/ \/\* /' -e 's/\*\// \*\/ /' | tr -s ' '`

    #0:check for keyword
    #1:found keyword 'component'
    #2: found keyword 'extends'
    state=0
    comment=0
    component=
    extends=
    for token in $tokens
    do
      if [ $comment -ne 0 ]
      then
        if [ "$token" = "*/" ]
        then
          comment=0
        fi
      elif [ "$token" = "/*" ]
      then
        comment=1
      else
        if [ "$token" = "component" ]
        then
          state=1
        elif [ "$token" = "extends" ]
        then
          state=2
        elif [ "$token" = "{" ]
        then
          state=0
          if [ -n "$extends" ]
          then
            for ext in $extends
            do
              echo "\"$component\" -> \"$ext\";"
            done
          elif [ -n "$component" ]
          then
            echo "\"$component\";"
          fi
          component=
          extends=
        else
          if [ $state -eq 1 ]
          then
            #echo component $token
            component=$token
          elif [ $state -eq 2 ]
          then
            #echo extends $token
            extends=$extends" "$token
          fi
        fi
      fi
    done
  done
  echo "}"
fi
