///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
The scripts
  * make-import-graphs.sh
  * make-extend-graph.sh
create dependency graphs for "import"/"include" and "extends" dependencies, respectively.

The graphs are specified to the std. outout in the DOT language, see http://www.graphviz.org/

The arrowheads point respectively
  * from the importing/including module to the imported/included module
  * from the extending component type to the extended component type


