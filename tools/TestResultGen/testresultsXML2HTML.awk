#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN{
  FS="[<>]"
}
/:name/{
  print "<h2>Test Results of " $3"</h2>"
}
/purpose/{
  print "<h3>Purpose: "$3 "</h3>\n<table class=\"ExecutionTime\">"
}
/start_time/{
  print "<tbody><tr><td>Start Time:</td><td>" $3 "</td></tr>"
}
/end_time/{
  print "<tr><td>End Time:</td><td>" $3 "</td></tr></tbody></table>"
}
/<perftest:environment/{
  print "<div class=\"EnvironmentTable\"><p class=\"tableTitle\">Environment settings</p><table><tbody><tr class=\"Header\"><th>Environment setting</th><th>Value</th></tr>";
  env=1;
  next;
}
/<\/perftest:environment/{
  env=0;
}
//{
  if(env==1){
    split($2,tmp,"[:]");
    print "<tr class=\"EnvItems\"><td>"tmp[2]"</td><td>"$3 "</td></tr>"
  }
}
/<perftest:parameter/{
  if (par==0) {
    print "</tbody></table></div><div class=\"ParamTable\"><p class=\"tableTitle\">Parameters</p><table><tbody><tr class=\"Header\"><th>Parameter</th><th>Value [Unit]</th></tr>";
  }
  par=1;
  split($0,tmp,"[<>=']");
  print "<tr class=\"Params\"><td>"tmp[4]"</td><td>"tmp[9]" ["tmp[7] "]</td></tr>"
}
/:result/{
  if (res==0) {
    print "</tbody></table></div><div class=\"ResultTable\"><p class=\"tableTitle\">Test Results</p><table><tbody><tr class=\"Header\"><th>Result</th><th>Value [Unit]</th></tr>";
  }
  res=1;
  split($0,tmp,"[<>=']");
  print "<tr class=\"ResItems\"><td>"tmp[4]"</td><td>"tmp[9]" ["tmp[7] "]</td></tr>"
}
/<\/perftest:testresult>/{
  print "</tbody></table></div>"
  par=0;
  res=0;
}
