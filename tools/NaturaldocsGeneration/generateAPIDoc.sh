#!/usr/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

# USAGE:
#Tool location:
#/vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/tools/NaturaldocsGeneration
#
#Step by step:
#
#   - Execute the generateAPIDoc.sh script with the appropriate options: giving the source directory 
#   of your CLL repository, and a directory where the project build directory is going to be created.
#   - Check the generated apidoc file "EPTF_CLL_DataSource_Apidoc.ttcn" located in your CLL repo /src/DataSource

#Depend on user - optional arguments
#Source of the product
ttcn3="" #CLL source directory
#Destination folder
destfolder=~/apidoc

#Constants for the present - but optional arguments
##scripts, files used for generation
if [ "." = `dirname $0` ]
then
  natdocvobdir=`pwd`
else
  natdocvobdir=`dirname $0`
fi

showUsage()
{
echo >&2 "Usage: $0 [-s Source] [-d Destination]"
echo >&2 " -s Source folder of the product. Default: $ttcn3"
echo >&2 " -d Destination folder. Default: ~/apidoc" #$destfolder
}

# Get parameters

while getopts s:d:? o
do	case "$o" in
	s)  ttcn3=$OPTARG;;
	d)  destfolder=$OPTARG;;
	[?]) showUsage; exit 1;;
	esac
done

#Check destination folder
if [ ! -d $destfolder ]
then
  mkdir $destfolder
fi

#echo "ttcn3: "$ttcn3
#echo "destfolder: "$destfolder

currentDir=`pwd`;
cd $destfolder
mkdir build
cd build

$natdocvobdir/../../../../../tools/nightly-test/make_run.sh $ttcn3/../tools/HelpForDataSources/EPTF_DSHelp.prj
cp EPTF_CLL_DataSource_Apidoc.ttcn $ttcn3/DataSource/
cd ..
\rm -rf build
cd $currentDir




