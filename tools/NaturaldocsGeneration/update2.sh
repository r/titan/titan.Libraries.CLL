#!/usr/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////


src_path=~/apidoc
des_path="/vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc"

 if [ "$1" != "" ]
  then
    src_path="$1"
  fi

 if [ "$2" != "" ]
  then
    des_path="$2"
  fi

echo "Using source path: $src_path"
echo "Using destination path: $des_path"

if [ "$#" != "2" -a "$#" != "0" ]
then
    echo "Invalid number of arguments."
    echo "Usage: update2.sh version <source dir> <destination dir>"
    echo "Example: update2.sh /home/ebalbar/projects /vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc"
    exit
fi

if [ ! -d $src_path -o ! -d $des_path ]
then
    echo "Source or destination dir does not exist."
    echo "Usage: update2.sh version <source dir> <destination dir>"
    echo "Example: update2.sh /home/ebalbar/projects /vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc"
    exit
fi

#checks the pathes
cd $src_path
if [ $? != "0" ]
  then
    exit
  fi

cd $des_path
if [ $? != "0" ]
  then
    exit
  fi

des_length=`echo "$des_path" | wc -m`
src_length=`echo "$src_path" | wc -m`

diff_vob=`diff -r $src_path $des_path | grep "Only in $des_path" | cut -d ' ' -f3,4 | sed 's/: /\//g'| cut -c$des_length-200`

diff_loc=`diff -r $src_path $des_path | grep "Only in $src_path" | cut -d ' ' -f3,4 | sed 's/: /\//g'| cut -c$src_length-200`

diff_common=`diff -r $src_path $des_path | grep 'diff' | cut -d ' ' -f4 | cut -c$des_length-200`

if [  ! -z "$diff_common" ]
  then
    echo "\nfound different file(s) in vob listed below\n$diff_common\nupdate all in vob?(y/n)"
    read answer
    if [ "$answer" = "y" ]
      then
        for object in $diff_common
          do
            cleartool co -nc $des_path$object
            cp  $src_path$object $des_path$object
            echo "copied $src_path$object to $des_path$object"
            if [ -z "$3" ]
            #checks comment
              then
                cleartool ci -nc $des_path$object
              else
                cleartool ci -c $3 $des_path$object
            fi
          done
      fi
  fi    

#checks old file(s)
if [  ! -z "$diff_vob" ]
  then
    echo "\nfound file(s) just in vob listed below\n$diff_vob\nremove all from vob?(y/n)"
    read answer
    if [ "$answer" = "y" ]
      then
        for object in $diff_vob
          do
            if [ -d "$des_path$object" ]
              then
                echo "$des_path$object is a directory..."
            fi
            if [ -z "`cleartool ls $des_path$object | grep '@'`" ]
              then
                 echo "delete private file: $des_path$object"
                rm -rf $des_path$object
              else
               cleartool co -nc `dirname $des_path$object`
               cleartool rm -force -nc $des_path$object
               cleartool ci -nc `dirname $des_path$object`
            fi
        done
      fi
  fi

#checks new file(s)
if [ ! -z "$diff_loc" ]
  then
    echo "\nfound new file(s) in source listed below\n$diff_loc\ncreate vob object?(y/n)"
    read answer
    if [ "$answer" = "y" ]
      then
        for object in $diff_loc
          do
            cp  -r $src_path$object $des_path$object
            echo "copied $src_path$object to $des_path$object"
            if [ -d "$des_path$object" ]
              then
                files=`find $des_path$object -print`
              else
                files="$des_path$object"    
            fi
            
            if [ -z "$3" ]
            #checks comment
              then
                cleartool mkelem -mkpath -ci -nc $files
              else
                cleartool mkelem -mkpath -ci -c $3 $files
              fi
            if [ -d "$des_path$object" ]
              then
                echo "change $des_path$object directory permitions to 775"
                clearcase chmod -protect 775 $des_path$object
            fi
            
        done
      fi
  fi

