#!/usr/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

# USAGE:
#Tool location:
#/vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/tools/NaturaldocsGeneration
#
#Step by step:
#
#    Set the rigth config spec into your clearcase view to be able to see the right version of the product.
#    The generate.sh script is prepared to generate the apidoc of CLL in the HUB. If you want to generate apidoc for different product or in different environment, see generate.sh -?.
#    Modify the project descriptor files (Menu.txt) according the current version of the project. In CLL they are located in the doc/apidocSource folder.
#    These files have to be changed in case of:
#        new release
#        new files to be processed
#    The generate.sh script by default assumes the ~/apidoc as the destination folder. If you want to specify different folder, use the -d option. You do not have to create the folder, the script creates it.
#    Execute the generate.sh script with the appropriate options. Generally you do not need to use any option.
#    Check the generated apidoc.
#    If everything OK, run update2.sh <generated apidoc destination folder> <apidoc folder in CC>. In case of CLL, the CC apidoc folder is /vobs/ttcn/TCC_Common/Libraries/EPTF_Core_Library_CNL113512/doc/apidoc


#ct=/usr/atria/bin/cleartool

#Depend on user - optional arguments
#Source of the product
ttcn3=~/CLLRepo/src/Libraries/EPTF_Core_Library_CNL113512/src
#Folder of the files describing the generated apidoc. Parts of the product. The files must be updated periodically
natdocsources=~/CLLRepo/src/Libraries/EPTF_Core_Library_CNL113512/doc/apidocSource
#Destination folder
destfolder=~/apidoc
headerHtml=TitanSim.html
generateDSAPIDoc=false

#Constants for the present - but optional arguments
##scripts, files used for generation
if [ "." = `dirname $0` ]
then
  natdocvobdir=`pwd`
else
  natdocvobdir=`dirname $0`
fi

#Executable of the NaturalDocs
natdoc=/app/naturaldocs/1.4/NaturalDocs

showUsage()
{
echo >&2 "Usage: $0 [-n NaturalDocs executable] [-s Source] [-d Destination] [-p ProjectDescription] [-n NaturalDocs]" #[-a Additional files]"
echo >&2 " -s Source folder of the product. Default: $ttcn3"
echo >&2 " -d Destination folder. Default: ~/apidoc" #$destfolder
echo >&2 " -p Folder of the product descriptor files. (Menu, etc.) By default is set to CLL." #$natdocsources
echo >&2 " -n NaturalDocs executable path. Default: $natdoc"
echo >&2 " -h Header HTML of the help. Default: $headerHtml"
echo >&2 " -g Generate APIDoc for DataSource data elements. Default: $generateDSAPIDoc"
echo >&2 " -? Displays this message."
}

# Get parameters

while getopts s:d:p:n:h:g:? o
do	case "$o" in
	s)  ttcn3=$OPTARG;;
	d)  destfolder=$OPTARG;;
	p)  natdocsources=$OPTARG;;
	n)  natdoc=$OPTARG;;
	h)  headerHtml=$OPTARG;;
	g)  generateDSAPIDoc=$OPTARG;;
	[?]) showUsage; exit 1;;
	esac
done

#Check destination folder
if [ ! -d $destfolder ]
then
  mkdir $destfolder
fi

#Html destination folder:
html_dir=$destfolder/html

if [ ! -d $html_dir ]
then
  mkdir $html_dir
fi

#echo "ttcn3: "$ttcn3
#echo "destfolder: "$destfolder
#echo "natdocsources: "$natdocsources
#echo "natdoc: "$natdoc
#echo "headerHtml: "$headerHtml

# Generate APIDoc for DataSource data elements by building and running its generator project
if [ "true" = "$generateDSAPIDoc" ]
then
  $natdocvobdir/generateAPIDoc.sh -s $ttcn3 -d $destfolder
fi


# Prepare Files
prepare_file()
{
  f=$1
  if [ -w $f ]
  then
    $ct lscheckout -cview $f | grep unreserved 
    if [ $? ]
    then
      $ct unco -rm $f
    else
      echo "Warning - $f is checked out reserved!"
    fi
  fi
  if [ ! -w $f ]
  then
    $ct co -c "Checked out by generate.sh" -unreserved $f
  fi
  if [ ! -w $f ]
  then
    echo "Error - unable to check out $f - check your config spec"
    $ct ls $f
    exit
  fi
}

if [ ! -x $natdoc ]
then
  echo "Error - Can't execute $natdoc"
  exit
fi

# Check out those files that need to be modified, unreserved
#prepare_file "$destfolder/Topics.txt"
#prepare_file "$destfolder/Languages.txt"
cp $natdocvobdir/Topics.txt $destfolder/Topics.txt
cp $natdocvobdir/Languages.txt $destfolder/Languages.txt
cp $natdocvobdir/load.css $destfolder/load.css
cp $natdocsources/Menu.txt $destfolder/Menu.txt
chmod 664 $destfolder/Topics.txt $destfolder/Languages.txt $destfolder/load.css #$destfolder/Menu.txt

# Run from the top level directory
# NaturalDocs doesn't seem to update the menu properly without the hacking below
echo "Deleting cached info..."
#rm -f $destfolder/Menu.txt
rm -f $destfolder/Data/*
echo "Rebuilding everything..."
$natdoc --input $ttcn3 --output framedhtml $html_dir --project $destfolder -s Default load -r --exclude-input $natdocsources

# Ensure that any new files created are writable by all, but ignore
# the errors that will probably come
chmod 775 `find $html_dir -type d` > /dev/null 2>&1
chmod 664 `find $html_dir -type f` > /dev/null 2>&1
chmod 664 `find $destfolder/Data -type f` > /dev/null 2>&1
chmod 664 $destfolder/Menu.txt > /dev/null 2>&1

rm -f $destfolder/Topics.txt
rm -f $destfolder/load.css
rm -f $destfolder/Languages.txt

# copy externally modified html files
cp $natdocsources/$headerHtml $html_dir/files/$headerHtml
cp $natdocsources/index.html $html_dir/index.html

chmod 775 $html_dir/files/$headerHtml
chmod 775 $html_dir/index.html

# close group items for default
#/bin/sh closegroups.sh $html_dir/menu.html $html_dir/menu2.html
#rm -f $html_dir/menu.html
#mv $html_dir/menu2.html $html_dir/menu.html

# repairs unnatural html files generated by NaturalDocs
perl $natdocvobdir/repairunnaturalhtml.pl $html_dir




