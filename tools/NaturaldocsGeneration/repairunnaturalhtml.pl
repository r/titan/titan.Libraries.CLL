#!/usr/bin/perl -w
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

use strict;

# in:  file path
# out: extension
sub getFileExtension
{
  my $path = shift;
  my @lst = split( /\./ , $path );
  return( (@lst<2) ? '' : $lst[@lst-1] );
}

#in : reference to string containing source code line, extension
#out: number of inserted line breaks
sub insertLineBreaks
{
	my $r_line = shift;
  my $extension = shift; # currently not used
	my $subst_cnt = $$r_line =~ s/>\s*<([^\/])/>\n<$1/g;
	return $subst_cnt;
}

sub isHtmldocFile
{
	my $fname = shift;
	my $htmldocexts = "html|htm";
	return ($fname =~/\.($htmldocexts)$/);
}

# recursive scan of the given path: insert line breaks for all html files
# input : path
# output: number of changed html files
sub repairHtmlDoc
{
	my $source_path = shift;
	my %my_cnt = ( 'html' => 0, 'changed' => 0 );

	opendir(DIRH, $source_path) or die("Could not open the directory: $source_path\n");
	my @dirContent = readdir DIRH;
	closedir(DIRH);
	
	my $file;
	foreach $file (@dirContent)
	{
		if (($file ne '.') and ($file ne '..'))
		{
			my $path = $source_path.'/'.$file;
			if (-l $path)
			{
				# nothing to do	
			}
			elsif (-d $path)
			{
				# recursive call
				my %cnt = repairHtmlDoc($path);
				$my_cnt{'html'} += $cnt{'html'};
				$my_cnt{'changed'} += $cnt{'changed'};
			}
			else # file
			{
				if (isHtmldocFile($path))
				{
					my $inserted = 0;
          my $extension = getFileExtension($path);
					# read content
					open(HTML_FILE, $path) or die("Could not open file '$path' for reading.\n");
					my @html_content = <HTML_FILE>;
					close HTML_FILE;
					# overwrite file with inserted line breaks
					open(HTML_FILE, "+>$path") or die("Could not open file '$path' for overwriting.\n");
					my $line;
					foreach $line (@html_content)
					{
						$inserted += insertLineBreaks(\$line, $extension);
						print HTML_FILE $line;
					}
					close HTML_FILE;
					$my_cnt{'html'}++;
					$my_cnt{'changed'}++ if ($inserted);
					print "Inserted $inserted line breaks into '$path' file.\n";
				}
			}
		}
	}
	return %my_cnt;
}

#START:
# get directory name
die "Usage: $0 <path_to_naturaldocs_documentation_directory>\n" if (@ARGV!=1);
my $dirname = $ARGV[0];
# remove eventual ending '/' from directory names
$dirname =~ s/\/$//;
die "Directory '$dirname' does not exist.\n" unless (-e $dirname);
# insert line breaks in place
my %cnt = repairHtmlDoc($dirname);
# print statistics
print "*** Processed ".$cnt{'html'}." html files.\n";
