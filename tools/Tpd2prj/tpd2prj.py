#!/usr/bin/env python
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

#import sys
import re
from os import path

__referencedProjectExpr=re.compile(r'<ReferencedProject.+?\s+projectLocationURI="(.+?)"')
__fileResourceExpr=re.compile(r'<FileResource.+?\s+relativeURI="(.+?)"')
__excludedFilesExpr=re.compile(r'<FilePath>(.+?)</FilePath>')
__configurationExpr=re.compile(r'<Configuration.*?\s+name="(.+?)"')
__configurationFinishedExpr=re.compile(r'</Configuration>')

__pathSimplifyExpr=re.compile(r'\w+/../')

__excludedFiles=set()
__usedConfiguration=''

def __readTpd(tpdFile):
    grpFile=path.basename(tpdFile)+'.grp'
    grpAlreadyGenerated=path.exists(grpFile)
    projectName = None
    referencedProjects=set()
    fileResources=set()
    if not grpAlreadyGenerated or __usedConfiguration != '':
        configurationStarted=False
        configurationFinished=False
        with open(tpdFile) as f:
             for line in f:
                 if __usedConfiguration != '':
                     if not configurationStarted:
                         result = __configurationExpr.search(line)
                         if result:
                            configurationName=result.group(1)
                            if __usedConfiguration==configurationName:
                                configurationStarted=True

                     if configurationStarted:
                         result = __configurationFinishedExpr.search(line)
                         if result:
                            configurationFinished=True

                         if not configurationFinished:
                             result = __excludedFilesExpr.search(line)
                             if result:
                                excludedFile=__simplifyFileName(__dirNameWithDot(tpdFile)+'/'+result.group(1))
                                __excludedFiles.add(excludedFile)

                 if not grpAlreadyGenerated or __usedConfiguration != '':
                     # referenced projects needs to be collected even if file is already generated
                     # when project file is requested
                     result = __referencedProjectExpr.search(line)
                     if result:
                        referencedProject=__simplifyFileName(result.group(1))
                        referencedProjects.add(referencedProject)

                 if grpAlreadyGenerated:
                     continue

                 projectNameExpr=re.search(r'<ProjectName>(.+?)</', line)
                 if projectNameExpr:
                     projectName=projectNameExpr.group(1)

                 result = __fileResourceExpr.search(line)
                 if result:
                    fileResource=__simplifyFileName(result.group(1))
                    fileResources.add(fileResource)

    return projectName, referencedProjects,fileResources

def __dirNameWithDot(fileName):
    dirName=path.dirname(fileName)
    return dirName if dirName else '.'
    
def __simplifyFileName(fileName):
    '''
    Removes .. from file name
    '''
    #print 'Simplifying: ', fileName
    while True:
        newfileName = __pathSimplifyExpr.sub('',fileName)
        if newfileName == fileName:
            break
        else:
            fileName=newfileName

    return fileName

def __getGrp(tpdFileName, projectName,referencedProjects,fileResources):
    s = '<!DOCTYPE TITAN_GUI_FileGroup_file>'
    s += '\n<File_Group name="' + projectName +'">'
    for fileResource in fileResources:
        s += '\n    <File path="'+ __dirNameWithDot(tpdFileName)+'/'+fileResource+ '" />'

    s += '\n    <File_Groups>'
    for referencedProject in referencedProjects:
        s += '\n        <File_Group path="'+path.basename(referencedProject)+'.grp" />'

    s += '\n    </File_Groups>'
    s += '\n</File_Group>'
    s += '\n'

    return s

def __getPrj(mainTpdFileName, excludedFiles=''):
    s = '<!DOCTYPE TITAN_GUI_project_file>'
    s += '\n<Project TITAN_version="1.8.pl7" >'
    s += '\n    <General>'
    s += '\n        <Project_Name>'+path.basename(mainTpdFileName)+'</Project_Name>'
    s += '\n        <Executable_Path>./build/'+path.basename(mainTpdFileName)+'</Executable_Path>'
    s += '\n        <Working_Dir>./build/</Working_Dir>'
    s += '\n        <Build_Host>alpha</Build_Host>'
    s += '\n        <Execution_Mode>Parallel</Execution_Mode>'
    s += '\n        <Code_Splitting_Mode>None</Code_Splitting_Mode>'
    s += '\n        <Log_Format>yes</Log_Format>'
    s += '\n        <Update_Symlinks>yes</Update_Symlinks>'
    s += '\n        <Create_Absolute_Symlinks>no</Create_Absolute_Symlinks>'
    s += '\n        <Update_Makefile>yes</Update_Makefile>'
    s += '\n        <Localhost_Execute>yes</Localhost_Execute>'
    s += '\n        <Execute_Command>rsh %host &quot;cd %project_working_dir ; &quot;%executable&quot; %localhost %mctr_port&quot;</Execute_Command>'
    s += '\n        <Execute_Hosts>alfa, beta, gamma</Execute_Hosts>'
    s += '\n        <UnUsed_List>'+(','.join(s for s in list(__excludedFiles)))+'</UnUsed_List>'
    s += '\n    </General>'
    s += '\n    <Others/>'
    s += '\n    <File_Group name="MainFileGroup" >'
    s += '\n        <File_Groups>'
    s += '\n            <File_Group path="'+path.basename(mainTpdFileName)+'.grp" />'
    s += '\n        </File_Groups>'
    s += '\n    </File_Group>'
    s += '\n </Project>'
    s += '\n'
    return s

def __writeGrpFile(tpdFileName):
    '''
    Processes the given tpd file and generates a .grp file from it
    Returns the remaining tpd files referenced in the current one
    which should be processed also
    If the given tpd was already processed (.grp exists) it returns empty list
    '''
    #print 'Processing TPD: ', tpdFileName
    projectName, referencedProjects,fileResources = __readTpd(tpdFileName)
    #print 'projectName: ', projectName
    #print 'referencedProjects: ', referencedProjects
    #print 'fileResources: ', fileResources

    grpFile=path.basename(tpdFileName)+'.grp'
    if path.exists(grpFile) and __usedConfiguration == '':
        #print 'File already generated: ',grpFile,' ...'
        return set() # tpd was already processed, return empty set

    if not path.exists(grpFile):
        print('Writing file ',grpFile,' ...')
        with open(grpFile,'w') as f:
            f.write(__getGrp(tpdFileName, projectName,referencedProjects,fileResources))

    return [__simplifyFileName(__dirNameWithDot(tpdFileName)+'/'+fileName) for fileName in referencedProjects]

def generatePrj(tpdFileName):
    '''
    Generates the prj file for the given tpd file
    The grp files are not generated!
    '''
    #print 'Generating PRJ for TPD: ', tpdFileName
    #print 'Excluded files: ', __excludedFiles

    prjFileName=path.basename(tpdFileName)+'.prj'
    if path.exists(prjFileName):
        #print 'File already generated: ',prjFileName,' ...'
        return # prj was already generated, nothing to do

    print('Writing file ',prjFileName+' ...')
    with open(prjFileName,'w') as f:
        f.write(__getPrj(tpdFileName))

def generateGrp(tpdFileName):
    referencedTPDs=set()
    while tpdFileName:
        referencedTPDs.update(__writeGrpFile(tpdFileName))

        #print "Remaining TPDs: ", len(referencedTPDs)
        tpdFileName=referencedTPDs.pop() if len(referencedTPDs)>0 else None


if __name__ == '__main__':
    #tpdfile=sys.argv[1]

    from argparse import ArgumentParser

    parser=ArgumentParser(description='Tpd to prj converter')
    parser.add_argument('tpdfile',nargs=1, type=file, help='Tpd file to convert')
    parser.add_argument('-p', '--prj', action='store_true', help='generate prj')
    parser.add_argument('-c', '--configuration', type=str, default='Default', help='prj is generated with the configuration specified (default "Default")')

    try:
        args=parser.parse_args()
        tpdfile=args.tpdfile[0].name
        generatePrjRequired=args.prj
        configuration=args.configuration
        prjFile=path.basename(tpdfile)+'.prj'
        prjAlreadyGenerated=path.exists(prjFile)

        if generatePrjRequired and not prjAlreadyGenerated:
            __usedConfiguration=configuration

        generateGrp(tpdfile)
        if generatePrjRequired:
            generatePrj(tpdfile)
    except Exception as e:
        parser.print_help()
        print(e)
        exit(1)
