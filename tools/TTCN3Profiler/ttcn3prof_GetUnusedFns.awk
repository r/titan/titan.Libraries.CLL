#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////
BEGIN {
  FS=":[ \t]*|[ \t]+"
  unusedCounter=0
  allFunctions=0
}

#function references from TTCN3Coverage.fns:
/^f:/ {
  if (fns[$3":"$4]==0) {
    fns[$3":"$4]=$2
    fnLocation[++allFunctions]=$3":"$4
  }
}


/^([[:digit:]]+)|([[:digit:]]+\.[[:digit:]]+)([]|[eE][+-][[:digit:]]+):/ {
  if (fns[$2":"$3]!="") {
    usedFns[$2":"$3]=1;
    #print "used: "fns[$2":"$3]"\t"$2":"$3
  }
}

END {
  print "------------------------------"
  print "-     Unused functions       -"
  print "------------------------------"
  n=asort(fnLocation);
  for(i = 1; i <= n; i++) {
    if(usedFns[fnLocation[i]]==1) {
      continue
    }
    unusedCounter++;
    print fnLocation[i]":\t`"fns[fnLocation[i]]"' unused."
  }
  if (allFunctions==0) {
    unusedRatio=0
    print "No functions found!"
  } else {
    unusedRatio=unusedCounter/allFunctions
    if (unusedCounter==0) {
      print "All functions are used!"
    }
  }
  print ("Functions not used: "unusedCounter " out of "allFunctions,
    "("unusedRatio*100"% of all)")
}
