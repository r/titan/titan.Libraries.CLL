///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef TTCN3PROF_HH
#define TTCN3PROF_HH
// file: ttcn3prof.hh

#include "EPTF_CLL_TTCN3Profiler.hh"

class TTCN3PROF_GLOBAL {
public:
  TTCN3PROF_GLOBAL() {};
  ~TTCN3PROF_GLOBAL();
};

class TTCN3PROF {
public:
  TTCN3PROF(const char* moduleName=NULL, int codeLines=-1): moduleName(moduleName), codeLines(codeLines), prevTime(0.0), prevFile(NULL), prevLine(-1), profilerDb(NULL_VALUE),
    prevStackLen(0) {
    // this is needed to force the ttcn3prof_global object to be constructed before any TTCN3PROF => it will be destructed last:
    static TTCN3PROF_GLOBAL ttcn3prof_global=TTCN3PROF_GLOBAL(); // this var generates the profiler info when destructed on the HC
    //f__EPTF__TTCN3Profiler__init(profilerDb);
    if (this->moduleName==NULL) {
      this->moduleName ="EPTF_CLL_TTCN3Profiler";
    }
  };
  ~TTCN3PROF() {
    printStat();
    //fprintf(stderr,"TTCN3COV destroyed\n");
  };
  static double getTime();
  void enterFunction(const char* filename,const int& lineno,const char* functionName);
  void executeLine(const char* filename, const int& lineno);
  void printStat(); // prints the profile statistics
  void writeToFile(const char* data); // writes data to file
  static void log(const char *fmt, ...);
  int findElement(const char* filename);
  int newElement(const char* filename);
  void addTime(double elapsed, const int& element, const int& lineno);
  void updateLast();
  void setPrev(const int& stackLen,const char* filename, const int& lineno);

private:
  CHARSTRING moduleName;
  int codeLines;
  double prevTime;
  const char* prevFile;
  int prevLine;
  EPTF__CLL__TTCN3Profiler::EPTF__TTCN3ProfilerDb profilerDb;
  int prevStackLen;
};

extern TTCN3PROF ttcn3prof;

class StackDepth {
  public:
    StackDepth() {
      if (EPTF__CLL__TTCN3Profiler::tsp__EPTF__TTCN3Profiler__disableStack || EPTF__CLL__TTCN3Profiler::tsp__EPTF__TTCN3Profiler__disabled) {
        return;
      }
      ++currentDepth;
    };
    ~StackDepth() {
      if (EPTF__CLL__TTCN3Profiler::tsp__EPTF__TTCN3Profiler__disableStack || EPTF__CLL__TTCN3Profiler::tsp__EPTF__TTCN3Profiler__disabled) {
        return;
      }
      ttcn3prof.updateLast();
      removeStack();
      --currentDepth;
    };
    static int depth() {return currentDepth;}
    //static const EPTF__CLL__TTCN3Profiler::EPTF__TTCN3Profiler__CallStackTimerDb& get_callStackTimerDb() const { return callStackTimerDb; }
    static void addStack(const int& stackLen,const char* filename,const int& lineno);
    static void removeStack();
    static void updateStackElapsed(const double& elapsed,const int& stackLen);
  private:
    static int currentDepth;
    static EPTF__CLL__TTCN3Profiler::EPTF__TTCN3Profiler__CallStackTimerDb callStackTimerDb;
};

#endif
