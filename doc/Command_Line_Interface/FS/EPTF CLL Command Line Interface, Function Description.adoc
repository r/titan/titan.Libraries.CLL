---
Author: Balázs Lugossy
Version: 29/155 16-CNL 113 512, Rev. B
Date: 2012-12-01

---
= EPTF CLL Command Line Interface, Function Description
:author: Balázs Lugossy
:revnumber: 29/155 16-CNL 113 512, Rev. B
:revdate: 2012-12-01
:toc:

== How to Read This Document

This is the Function Description for the Command Line Interface of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, ‎[1]>> Toolset with TITAN <<_2, ‎[2]>>.

== Scope

This document is to specify the content and functionality of the Command Line Interface (CLI) feature of the TitanSim CLL.

== Recommended Way of Reading

The readers are supposed to get familiar with the concept and functionalities of TitanSim CLL <<_3, ‎[3]>>. They should get familiar with the list of <<terminology, acronyms>> and the <<abbreviations, glossary>>.

= General Description

The EPTF `CommandLineInterface` feature makes it possible that user defined commands in a user terminal can be used to execute custom functions implemented in TTCN3. The terminal is accessible by the Telnet protocol.

The EPTF `CommandLineInterface` feature provides two terminals:

1.	A normal terminal where commands can be entered and their result is shown
2.	A display terminal that can be used to show messages periodically. The display terminal can be used to execute the same commands as on the normal terminal as well.

The EPTF `CommandLineInterface` contains two components: The CLI and the CLI Client.

The CLI component

*	opens the telnet server port for the user terminal to connect to
*	manages command registration
*	routes user commands coming from the terminal to the corresponding PTC where they should be executed
*	prints out the command result on the terminal for commands executed on a PTC

The CLI Client component

*	registers new commands
*	executes the commands when the user enters them on the terminal by automatically invoking the registered handler function with the command arguments
*	sends messages to the display terminal

To be able to use EPTF CLI, at least one user component should extend the `EPTF_CLI_CT` component, and the component where the commands are handled (i.e. executed) should extend the `EPTF_CLI_Client_CT`.

= Terminal Interface

The telnet terminals provided by the EPTF CLI feature can be accessed via the telnet command using the configured ports.

== Built-in Commands

This section lists the commands that are available by default on the telnet command line interface terminal:name: value

*	`alias` - creates an alias to a command
*	`unalias` - removes an alias
*	`lsalias` - prints information about an alias
*	`allalias` - lists all aliases
*	`help` - prints a general help message
*	`exit` - closes the terminal
*	`quit` - closes the terminal
*	`stop` - terminates execution, stop all components
*	`stopAll` - terminates execution, stop all components

The help command can be used to get a general help which lists all available command. The command also gives help about a specific command by calling help with the given command name: `"help <commandName>"`.
Lines entered on the CLI terminals are ignored if they start with '#' or '//'. These lines are treated as comments.

== User-Defined Commands

It is possible to extend the list of commands available on the terminal by user defined commands. The commands together with their handler can be registered in the CLI Client. The user defined commands will appear after the list of built-in commands when a general help is requested with the help command.

= Functional Interface

Apart from this description a cross-linked reference guide for the TitanSim CLL Functions can be reached for on-line reading ‎<<_4, [4]>>.

== Naming Conventions

All functions have the prefix `f_EPTF_CLI_` that belongs to the `EPTF_CLI_CT` component, and `f_EPTF_CLI_Client_` that belongs to the `EPTF_CLI_Client_CT` component.

== Public Functions in the CLI

The detailed description of functions and there arguments are available in the source code, or in the reference guide <<_4, [4]>>. Only a basic description is given here.

=== Initialization

Before using any of the EPTF CLI functions the

`f_EPTF_CLI_init_CT`

function should be called. This initializes the EPTF CLI feature.

=== Get Formatted Help of a Command

The function

`f_EPTF_CLI_getHelpTxt`

can be used to get a formatted and detailed help message of a given command. The same message is printed to the terminal when the help is requested for a specific command using the built-in help command: `help <commandName>`

=== Executing a Command

To execute a command and get its result the function

`f_EPTF_CLI_executeCommand`

has to be called.

It executes the given command and returns the result. The command is executed as if it was started on the terminal.

=== Closing the Terminals

To close the terminal from TTCN3, call the function

`f_EPTF_CLI_closeTerminal`

Similar function to close the display terminal is:

`f_EPTF_CLI_closeDisplayTerminal`

== Public Functions in the CLI Client

=== Initialization

Before using any of the EPTF CLI Client functions the

`f_EPTF_CLI_Client_init_CT`

function should be called. This initializes the EPTF CLI Client feature.

=== Registering New Command to the Terminal

The function

`f_EPTF_CLI_Client_registerCommand`

can be used to register a new command to a given terminal. The command name, a short description of it and the handler function has to be specified in its arguments. Optionally the CLI component and the case sensitivity can also be set. By default the CLI given in the init function will be used as CLI, and the command name will be case insensitivity if these parameters are omitted. The case sensitivity refers to the command name only, not to its arguments! On one PTC commands with unique name can be registered only. Spaces are not allowed in command names.
This `registerCommand` function will return the status of the registration (success or failed).

=== Sending Messages to the Display Terminal

The function

`f_EPTF_CLI_Client_sendCommandDisplay`

can be used send messages to the display terminal that belongs to a given command. These messages will not appear on the normal terminal.

=== Setting the Tag That Signs Aliases in Commands

The function

`f_EPTF_CLI_setAliasTag`

can be used to set the tag that signs aliases in a command.

== General Functions

There are some function in the CLI that can be useful.

=== Splitting a String

The

`f_EPTF_CLI_splitString`

function can be used to split a string at given separator characters. It is possible to split so, that  multiple separator characters are treated as one separator character, or separate separators.

[[terminology]]
= Terminology

*TitanSim Core (Load) Library(CLL):*	+
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson.

*Command Line Interface:*	+
It is a user interface accessible via telnet protocol where the behavior of TitanSim can be changed or information can be requested using commands.

[[abbreviations]]
= Abbreviations

CLI::	Command Line Interface

CLL::	Core Load Library

EPTF::	Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim::	Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: 	Testing and Test Control Notation version 3 <<_1, [1]>>.

MTC::	Main Test Component

= References

[[_1]]
[1]	ETSI ES 201 873-1 v4.1.2 (2009-07) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2]	User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3]	TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4]	TitanSim CLL  for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
