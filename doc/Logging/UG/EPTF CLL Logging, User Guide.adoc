---
Author: László Skumát
Version: 8/198 17-CNL 113 512, Rev. C
Date: 2011-09-06

---
= EPTF CLL Logging, User Guide
:author: László Skumát
:revnumber: 8/198 17-CNL 113 512, Rev. C
:revdate: 2011-09-06
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the EPTF Logging of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the EPTF Logging feature <<_5, [5]>>. For more information on the TitanSim CLL consult the Users Guide <<_4, [4]>> and the Function Specification <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the EPTF Logging feature the system requirements listed in TitanSim CLL User Guide <<_4, [4]>> should be fulfilled.

= EPTF Logging

== Overview

The EPTF CLL Logging component is a fundamental component providing an implementation for logging features in a load test environment. It consists of sub-features Logging, LoggingUI and LoggingUIClient.

The Logging feature provides the possibility to define component masks and logging classes per component types. Logging can be enabled or disabled per component mask (locally or globally per component type), component instance (all component types) and logging classes.

Enabling or disabling log can be performed from TTCN via the public functions of the Logging feature, or through the user interface if the LoggingUI/LoggingUIClient sub-feature is used. For more details, see <<_5, [5]>>.

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL Logging API includes the following files:

* EPTF Logging:
** __EPTF_CLL_Logging_Definitions.ttcn__: +
This TTCN-3 module contains common type definitions that should be used in all EPTF Logging Components.
** __EPTF_CLL_Logging_Functions.ttcn__: +
This TTCN-3 module contains the implementation of EPTF Logging functions.
** __EPTF_CLL_Logging_ExternalFunctions.cc__: +
This C++ source file contains the implementation of external functions used by EPTF Logging.
* EPTF LoggingServer/LoggingClient:
** __EPTF_CLL_LoggingServer_Definitions.ttcn__: +
Contains component type, data and message type definitions for both LoggingServer and LoggingClient
** __EPTF_CLL_LoggingServer_Functions.ttcn__: +
Contains the functions of the LoggingServer.
** __EPTF_CLL_LoggingClient_Functions.ttcn__: +
Contains the functions of the LoggingClient.
* EPTF LoggingUI/LoggingUIClient:
** __EPTF_CLL_LoggingUI_Definitions.ttcn__: +
This TTCN-3 module contains common type definitions that should be used in all LoggingUI and LoggingUIClient components.
** __EPTF_CLL_LoggingUI_Functions.ttcn__: +
This TTCN-3 module contains the implementation of LoggingUI functions.
** __EPTF_CLL_LoggingUIClient_Functions.ttcn__: +
This TTCN-3 module contains the implementation of LoggingUIClient functions.
** __EPTF_CLL_LoggingUI_Private_Functions.ttcn__: +
This TTCN-3 module contains implementation of private functions used in LoggingUI and LoggingUIClient components.

[[description_of_required_files_from_other_feature]]
== Description of Required Files from Other Features

The EPTF Logging feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. To use the Logging sub-feature of EPTF Logging, the user has to obtain the respective files from the following features:

* Base
* Common
* DataSource

Additionally, the following EPTF features are used by the LoggingUI/LoggingUIClient sub-feature:

* HashMap
* UIHandler
* Variable

== Installation

Since `EPTF_CLL_Logging` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_Logging`:

* Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>>. and <<description_of_required_files_from_other_feature, Description of Required Files from Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the Logging demo or write your own application using EPTF Logging
* Create _Makefile_ or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs, see section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

This EPTF Logging feature does not define module parameters.

= Error Messages

NOTE: Error messages shown in <<_2, [2]>> or those of other used features or product may also appear.

= Warning Messages

NOTE: Warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_Logging_test.ttcn__
* __EPTF_Logging_test.cfg__
* __LoggingUI_Demo.ttcn__
* _LoggingUI.cfg_

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL Logging, Function Description

[[_6]]
[6] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
