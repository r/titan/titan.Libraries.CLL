---
Author: Zoltán János Sváner
Version: 27/198 17-CNL 113 512, Rev. B
Date: 2011-09-06

---
= EPTF CLL StatHandler, User Guide
:author: Zoltán János Sváner
:revnumber: 27/198 17-CNL 113 512, Rev. B
:revdate: 2011-09-06
:toc:

== About This Document

=== How to Read This Document

This is the User Guide for the StatHandler of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL). TitanSim CLL is developed for the TTCN-3 <<_1, [1]>> Toolset with TITAN <<_2, [2]>>. This document should be read together with the Function Description of the `StatHandler` feature <<_5, [5]>>. For more information on the TitanSim CLL consult the User's Guide <<_4, [4]>> and the Function Specification <<_3, [3]>> of the TitanSim.

== System Requirements

In order to use the StatHandler feature the system requirements listed in TitanSim CLL User's Guide <<_4, [4]>> should be fulfilled.

= StatHandler

== Overview

The EPTF `StatHandler` feature makes it possible to collect global, aggregated statistics via the existing EPTF Variable (see <<_6, [6]>>) interface.

The `StatHandler` feature consists of master and client components. Users of the `StatHandler` feature can declare aggregated statistics with a certain statistics method (for example,` Sum`), and register local or third party data sources (EPTF Variables).

[[description_of_files_in_this_feature]]
== Description of Files in This Feature

The EPTF CLL `StatHandler` API includes the following files:

* __EPTF_CLL_StatHandlerClient_Definitions.ttcn__ - This TTCN-3 module contains common type definitions that should be used in all `StatHandlerClient` Components.
* __EPTF_CLL_StatHandlerClient_Functions.ttcn__ - This TTCN-3 module contains the implementation of the `StatHandlerClient` functions.
* __EPTF_CLL_StatHandlerLogging_Definitions.ttcn__ - This TTCN-3 module contains common type definitions used for logging in the `StatHandler` feature.
* __EPTF_CLL_StatHandlerLogging_Functions.ttcn__ - This TTCN-3 module contains the implementation of the common logging functions used in the `StatHandler` feature.
* __EPTF_CLL_StatHandlerUI_Definitions.ttcn__ - This TTCN-3 module contains common type definitions used by the UI handling part of the `StatHandler` feature.
* __EPTF_CLL_StatHandlerUI_Functions.ttcn__ - This TTCN-3 module contains the implementation of the UI handling part of the `StatHandler` feature.
* __EPTF_CLL_StatHandler_Definitions.ttcn__ - This TTCN-3 module contains common type definitions that should be used in all `StatHandler` master components.
* __EPTF_CLL_StatHandler_Functions.ttcn__ - This TTCN-3 module contains the implementation of the `StatHandler` functions.
* __EPTF_CLL_StatHandler_DSFunctions.ttcn__ - This TTCN-3 module contains the implementation of the `StatHandler` DataSource functionality.

[[description_of_required_files_from_other_features]]
== Description of Required Files From Other Features

The `StatHandler` feature is part of the TitanSim EPTF Core Load Library (CLL). It relies on several features of the CLL. To use the `StatHandler`, the user has to obtain the respective files from the following features:

* `Base`
* `Common`
* `FreeBusyQueue`
* `HashMap`
* `Logging`
* `Semaphore`
* `StatMeasure`
* `UIHandler`
* `Variable`
* `DataSource`

== Installation

Since `EPTF_CLL_StatHandler` is used as a part of the TTCN-3 test environment this requires TTCN-3 Test Executor to be installed before any operation of these functions. For more details on the installation of TTCN-3 Test Executor see the relevant section of <<_2, [2]>>.

If not otherwise noted in the respective sections, the following are needed to use `EPTF_CLL_StatHandler`:

* Copy the files listed in section <<description_of_files_in_this_feature, Description of Files in This Feature>> and <<description_of_required_files_from_other_features, Description of Required Files From Other Features>> to the directory of the test suite or create symbolic links to them.
* Import the StatHandler demo or write your own application using StatHandler.
* Create Makefile or modify the existing one. For more details see the relevant section of <<_2, [2]>>.
* Edit the config file according to your needs, see following section <<configuration, Configuration>>.

[[configuration]]
== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

This `StatHandler` feature defines TTCN-3 module parameters as defined in <<_2, [2]>>, clause 4. Actual values of these parameters – when no default value or a different from the default actual value wished to be used – shall be given in the `[MODULE_PARAMETERS]` section of the configuration file.

=== Module Parameters of the StatHandler Feature

* `tsp_EPTF_StatHandler_nameSeparator`
+
This charstring type module parameter is defined in module `EPTF_CLL_StatHandler_Definitions`. It is used to define the separator string used for name creation in the feature.
+
Its default value is `_"."_`.

* `tsp_debug_EPTF_StatHandler`
+
This boolean type module parameter is defined in module `EPTF_CLL_StatHandler_Functions`. It can be used for debugging purposes.
+
Its default value is `_false_`.

* `tsp_debug_StatHandlerClient_autoDisconnectTimeout`
+
This float type module parameter is defined in module `EPTF_CLL_StatHandlerClient_Definitions`. It defines the inactivity time after which the `StatHandlerClient` will disconnect from all connected `StatHandler` master components to free port resources after `StatHandler` has been set up. Its default value is `_5.0_` for 5 seconds. The disconnect operation is only performed on the `StatHandler` management port, the collection of statistics will continue and the reset button will be handled via the EPTF Variable interface. The client re-connects to the server if it needs to send a new management message to it. It is recommended to set this value to the same as `tsp_EPTF_maxRunningTime` (which has a default value of `_2147483.0_`) if an automatic disconnection is not preferred.

* `tsp_debug_EPTF_StatHandlerClient`
+
This boolean type module parameter is defined in module `EPTF_CLL_StatHandlerClient_Functions`. It can be used for debugging purposes. Its default value is `_false_`.

= Error Messages

NOTE: Besides the below described error messages, error messages shown in <<_2, [2]>> or those of other used features or product may also appear.

`*f_EPTF_StatHandler_setMethodForStat: stat method <statMethod> for integer stat <statName> is not supported.*`

The method `<statMethod>` that was set for statistics `<statName>` is not supported for integer statistics.

`*f_EPTF_StatHandler_setMethodForStat: Cannot set <statMethod> stat method for stat <statName> := <statResetValue>*`

The method `<statMethod>` cannot be set for statistics <statName> as the type of the reset value `<statResetValue>` is not supported by `statHandler`.

`*f_EPTF_StatHandler_createAuxData_Percentile95: Could not create aux stat <auxStatName> for Percentile95.*`

The auxiliary statistics with name `<auxStatName>` could not be declared.

`*f_EPTF_StatHandler_createAuxData_PercentileP: Could not create aux stat <auxStatName> for PercentileP.*`

The auxiliary statistics with name `<auxStatName>` could not be declared.

`*as_EPTF_StatHandlerClient_msgHandler: unexpected message received from <sender>*`

The `<sender>` component sent an unexpected message to the `StatHandlerClient`.

`*f_EPTF_StatHandlerUI_initGUI: cannot add element to gui (<returnValue>): <xul>*`

The element described by the `<xul>` data structure could not be added to the GUI. The result of the operation is `<returnValue>`.

= Warning Messages

NOTE: Besides the below described warning messages, warning messages shown in <<_2, [2]>> or those of other used features or product may also appear.

`*f_EPTF_StatHandler_declareStat: redeclaration of <statName> with different properties.*`

The statistics with name `<statName>` was re-declared with a different statistics method or reset value.

`*f_EPTF_StatHandler_registerStat: called while reset is in progress*`

A data source was attempted to be registered during reset progress.

`*f_EPTF_StatHandler_registerStat: provider variable list is empty*`

No provider variables were specified for data source registration.

`*f_EPTF_StatHandler_registerStat: no such statistics: <statName>*`

The statistics with name `<statName>` does not exist.

`*f_EPTF_StatHandler_registerStat_Single: provider variable list for `"statMethod>" statistics "<statName>" has multiple elements, expected single numerical variable.*`

The data source registered should consist of a single element.

`*f_EPTF_StatHandler_registerStat_Single: invalid source type for stat <statName>*`

The type of the data source variable differs from the type of the statistics.

`*f_EPTF_StatHandler_registerStat_Mean: provider variable list for "<statMethod>" statistics "<statName>" needs two elements: local mean and number of samples.*`

The data source registered should consist of a local mean and a number of samples variable.

`*f_EPTF_StatHandler_registerStat_Mean: invalid source types for stat <statName>*`

Provider variable "local mean" should be float, "number of samples" should be integer type.

`*f_EPTF_StatHandler_registerStat_StandardDev: provider variable list for "<statMethod>" statistics "<statName>" needs three elements: local mean, number of samples and S.*`

The data source registered should consist of a local mean, a number of samples and a "S" variable.

`*f_EPTF_StatHandler_registerStat_StandardDev: invalid source type for stat <statName>*`

Provider variable "local mean" should be float, "number of samples" should be integer and "S" should be float type.

`*f_EPTF_StatHandler_registerStat_GlobalAverage: provider variable list for <statMethod> statistics <statName> needs two elements: sum and element number.*`

The data source registered should consist of a sum and a element number variable.

`*f_EPTF_StatHandler_registerStat_GlobalAverage: invalid source type for stat <statName>*`

Provider variable sum should be float, "number of samples" should be integer type.

`*f_EPTF_StatHandler_registerStat_Density: provider variable list for "<statMethod>" statistics "<statName>" needs two elements: density and boundaries.*`

The data source registered should consist of a density and a boundaries variable.

`*f_EPTF_StatHandler_registerStat_Density: invalid source type for stat <statName>*`

Provider variable "density" should be integer list, "boundaries" should be float list type.

`*f_EPTF_StatHandler_registerStat_Density: boundaries <sourceBoundaries> of source differs from boundaries <statBoundaries> stored for the stat <statName>*`

The boundaries part of the data source is not the same as the boundaries of the statistics. The latter is assigned the value of the first registered data source.

`*f_EPTF_StatHandler_registerStat_Percentile95: provider variable list for "<statMethod>" statistics "<statName>" needs three elements: density, boundaries and max.*`

The data source registered should consist of a density, a boundaries and a max variable.

`*f_EPTF_StatHandler_registerStat_Percentile95: could not register source for aux stat <auxStatName>*`

The source could not be registered for the auxiliary statistics.

`*f_EPTF_StatHandler_registerStat_PercentileP: provider variable list for "<statMethod>" statistics "<statName>" needs four elements: density, boundaries, max and pValue.*`

The data source registered should consist of a density, a boundaries, a max and a `pValue` variable.

`*f_EPTF_StatHandler_registerStat_PercentileP: could not register source for aux stat <auxStatName>*`

The source could not be registered for the auxiliary statistics.

`*f_EPTF_StatHandler_registerStat_PercentileP: pValue <pValue> of source differs from pValue <pValue>* *stored for the stat*`

The `pValue` of the data source variable differs from the `pValue` of the statistics.

`*f_EPTF_StatHandler_boundariesPostproc: ``boundaries'' part of source <sourceIndex> changed for stat <statName>*`

The boundaries part of the data source was changed by the providing component.

`*f_EPTF_StatHandler_boundariesPostproc: source providing Variable with index <index> not found in stat <statName>*`

The source which boundaries was changed is not in the source list of statistics `<statName>`.

`*as_EPTF_StatHandler_behavior: unexpected message received from <client>*`

Client sent an unexpected message to the `StatHandler`.

`*f_EPTF_StatHandler_setMethodForStat: Stat method "<method>" for integer stat "<statName>" is unsupported or not yet supported.*`

Stat method passed to `declareStat` is undefined.

`*f_EPTF_StatHandler_setMethodForStat: Stat method "<method>" for float stat "<statName>" is unsupported or not yet supported.*`

Stat method passed to `declareStat` is undefined.

`*f_EPTF_StatHandler_setMethodForStat: Stat method "<method>" for float-list stat "<statName>" is unsupported or not yet supported.*`

Stat method passed to `declareStat` is undefined.

`*f_EPTF_StatHandlerClient_registerResetFunction: cannot register null function reference.*`

The function reference specified is `_null_`.

`*f_EPTF_StatHandlerClient_registerResetFunction: re-registration of reset function <function>*`

The function reference is already registered.

`*f_EPTF_StatHandlerClient_registerResetFunction: cannot deregister null function reference.*`

The function reference specified is `_null_`.

`*as_EPTF_StatHandlerClient_autoDisconnect: restarting auto disconnect timer, pending messages: <n>*`

The automatic disconnect timer is restarted because there are <n> pending messages.

`*f_EPTF_StatHandlerUI_addLocalResetButton: cannot add element to gui: <xul>*`

The element with data structure `<xul>` could not be added to the GUI.

`*as_EPTF_StatHandlerUI_behavior: statistics not found: <statName>*`

The statistics `<statName>` referred by a bind-to-widget operation was not declared.

`*f_EPTF_StatHandler_DSProcessData: Unhandled element:*`

This warning message is reported when the DataSource functionality is used and the current data element is unknown for the feature.

`*f_EPTF_StatHandler_DSProcessData: Invalid iterator or externalData or parameter:*`

This warning message is reported when the DataSource functionality is used and the something with the request was wrong.

`*f_EPTF_StatHandler_getParams: Parameters are not correct:*`

This warning message is reported when the DataSource functionality is used and the parameters of the current data element are not correct.

`*f_EPTF_StatHandler_getParams: Too few parameter is given:*`

This warning message is reported when the DataSource functionality is used and the parameters of the current data element are less than required.

`*f_EPTF_StatHandler_getParams: Too many parameters are given:*`

This warning message is reported when the DataSource functionality is used and the parameters of the current data element are more than required.

= Examples

The "demo" directory of the deliverable contains the following examples:

* __EPTF_StatHandler_Demo.cfg__
* __EPTF_StatHandler_Demo.prj__
* __EPTF_StatHandler_Demo.ttcn__

= Terminology

*TitanSim Core (Load) Library(CLL):* +
It is that part of the TitanSim software that is totally project independent. (I.e., which is not protocol-, or application-dependent). The TitanSim CLL is to be supplied and supported by the TCC organization. Any TitanSim CLL development is to be funded centrally by Ericsson

*Statistics:* +
They are temporal statistics of EPTF Variables measured by the StatMeasure feature. The value of a statistics is calculated from a single EPTF Variable, as it changes throughout the time of execution.

*Aggregated Stat:* +
They are EPTF Variables calculated from other EPTF Variables (data sources). The word stat or statistics refers aggregated statistics throughout this document.

*Auxiliary Statistics:* +
It is an aggregated statistics that is not declared by the user of StatHandler directly, but as a result of declaring another stat. That statistics uses the calculated value of the auxiliary stat in its calculations.

*Auxiliary Variable:* +
An EPTF Variable used by an aggregated statistics to store some data.

*Provider Variable:* +
The Variable that affects the value of a given EPTF Variable.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

GUI:: Graphics User Interface

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 [1]

= References

[[_1]]
[1] ETSI ES 201 873-1 v3.2.1 (2007-02) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Guide for the TITAN TTCN-3 Test Executor

[[_3]]
[3] TitanSim CLL for TTCN-3 toolset with TITAN, Function Specification

[[_4]]
[4] TitanSim CLL for TTCN-3 toolset with TITAN, User Guide

[[_5]]
[5] EPTF CLL StatHandler, Function Description

[[_6]]
[6] EPTF CLL Variable, User Guide

[[_7]]
[7] TitanSim CLL for TTCN-3 toolset with TITAN +
http://ttcn.ericsson.se/products/libraries.shtml[Reference Guide]
