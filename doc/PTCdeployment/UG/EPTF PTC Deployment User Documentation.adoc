---
Author: Zsolt Szalai
Version: 3/198 17-CNL 113 512, Rev. A
Date: 2007-11-15

---
= EPTF PTC Deployment User Documentation
:author: Zsolt Szalai
:revnumber: 3/198 17-CNL 113 512, Rev. A
:revdate: 2007-11-15
:toc:

== About This Document

=== How to Read This Document

This is the User Documentation for the PTC deployment component, the `EPTF_PTCD_CT` of the Ericsson Performance Test Framework (TitanSim), Core Load Library (CLL).

=== Presumed Knowledge

To use this functionality, the knowledge of the TTCN-3 language <<_1, [1]>> is essential. Additionally, the user should be familiar with the TitanSim CLL.

== System Requirements

In order to use the PTC Deployment feature, the following system requirements must be satisfied:

* TITAN TTCN-3 Test Executor installed. For the installation guide see <<_2, [2]>>.

== Configuration

The executable test program behavior is determined via the run-time configuration file. This is a simple text file, which contains various sections. The usual suffix of configuration files is _.cfg_. For further information on the configuration file see <<_2, [2]>>.

The `tsp_PTCD_database` TSP can be used to give initial value to the PTCD database from the run-time configuration file.

= Description of the PTC Deploying Feature

In case you are simulating many users for your test, and have a non-homogenous computer park that is used for running many PTCs, you may want to specify how many PTCs you want to run on a selected computer.

With the PTCD component you can give a weight to every host, thus the hosts with bigger weights will be chosen more often for deploying a PTC. The weights must be positive integers.

To use the functionality in your component, you need to extend `EPTF_PTCD_CT` first. You also need to call `f_EPTF_PTCD_init_CT` when you are initializing your component.

== Host-Related Operations

=== Adding Hosts

With the function `f_EPTF_PTCD_AddHosts`, the database can be filled up with hosts and their weights. It requires a role, and a list of hosts with weights. If the role is already exists, the host will be appended to the existing hosts. Returns no value.

Example:

[source]
f_EPTF_PTCD_AddHosts("roleA",{{"hostA",35},{"192.168.1.2",11}});

=== Removing Hosts

The function `f_EPTF_PTCD_RemoveHosts` deletes the given hosts from the database. It requires a role, and a list of hosts. Returns no value.

Example:

[source]
f_EPTF_PTCD_RemoveHosts("roleB'",{"192.168.2.1'","hostC"});

=== Query Hosts

The function `f_EPTF_PTCD_GetHost` requires a role and a method indicating integer value what is optional and chooses a host from that role according to the way described in the Functional Description of EPTF PTCD <<_3, [3]>>. The default method is a random over against the deterministic way.

NOTE: When using the deterministic algorithm, you must query for a host at least sum of the weights to get all the hosts surely chosen, and than you can be sure that the distribution of the hosts are following the weights that was specified.

Returns the name of the host.

Example:
[source]
----
var charstring host := f_EPTF_PTCD_GetHost("roleA");

var charstring host := f_EPTF_PTCD_GetHost("roleA", 1);
----

If you would like to generate the host in a deterministic way.

= Abbreviations

CLL:: Core Load Library

EPTF:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TitanSim:: Ericsson Load Test Framework, formerly TITAN Load Test Framework

TTCN-3:: Testing and Test Control Notation version 3 <<_1, [1]>>.

PTC:: Parallel Test Component

PTCD:: PTC Deployment

= References

[[_1]]
[1] ETSI ES 201 873-1 v.2.2.1 (02/2003) +
The Testing and Test Control Notation version 3. Part 1: Core Language

[[_2]]
[2] User Documentation for the TITAN TTCN-3 Test Executor

[[_3]]
[3] EPTF PTC Deployment Functional Description
