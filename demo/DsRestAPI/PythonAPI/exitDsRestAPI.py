#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import json
import time
from DsRestAPI import *

print("###############################")
print("Initializing the DsRestAPI Python API")
print("-------------------------------")

api = DsRestAPI("http://localhost:9876/")


print("###############################")
print("Getting help for DataSource Sources element.")
print("-------------------------------")

print(api.getHelp("DataSource", "Sources"))


print("###############################")
print("Getting the DataSource Sources element.")
print("-------------------------------")

print(api.getData("DataSource", "Sources"))


print("###############################")
print("Pressing the exit button.")
print("-------------------------------")

request = """
[
    {
        "setData": {
            "source": "ExecCtrl",
            "element": "Exit",
            "content": "1",
            "tp": 1
        }
    }
]
"""

print(api.getList(json.loads(request)))
