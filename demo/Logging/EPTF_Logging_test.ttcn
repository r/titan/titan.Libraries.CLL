///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_Logging_test {

import from EPTF_CLL_Base_Functions all; // f_EPTF_Base_cleanup_CT
import from EPTF_CLL_Common_Definitions all; // c_EPTF_Common_debugSwitch
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Logging_Functions all;

type component LogTest_CT extends EPTF_Logging_CT {
  var integer v_LogTest_maskId := -1
  var integer v_LogTest_maskId_noClass := -1;
}

const EPTF_Logging_EventClassPrefixList c_LogTest_classList := {"TestError", "TestWarning", "TestDebug", "TestOperational"};
const integer c_LogTest_classIdx_TestError := 0;
const integer c_LogTest_classIdx_TestWarning := 1;
const integer c_LogTest_classIdx_TestDebug := 2;
const integer c_LogTest_classIdx_TestOperational := 3;
const integer c_LogTest_classIdx_Invalid := 123;

function f_EPTF_Logging_Test_Generic() runs on EPTF_Logging_CT {
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"3");

  log ("Logging disabled on component for all features");
  f_EPTF_Logging_disableAllLocal();
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"DIS_ALL:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"DIS_ALL:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"DIS_ALL:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"DIS_ALL:3");
  log ("Logging enabled on component for all features");
  f_EPTF_Logging_enableAllLocal();
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"ENA_ALL:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"ENA_ALL:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"ENA_ALL:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"ENA_ALL:3");
  
  log ("Logging disabled on component");
  f_EPTF_Logging_disableLocal(f_EPTF_Logging_myMaskId());
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"DIS:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"DIS:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"DIS:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"DIS:3");
  log ("Logging enabled on component");
  f_EPTF_Logging_enableLocal(f_EPTF_Logging_myMaskId());
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"ENA:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"ENA:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"ENA:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"ENA:3");
  log("Event class 2 is disabled");
  f_EPTF_Logging_disableLocalMask(f_EPTF_Logging_myMaskId(), 2);
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"DIS_CL:2");
  log("Event class 2 is enabled");
  f_EPTF_Logging_enableLocalMask(f_EPTF_Logging_myMaskId(), 2);
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"ENA_CL:2");

/*  log ("Logging disabled on all component");
  f_EPTF_disableAllComponentLog();
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"DIS_ALL:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"DIS_ALL:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"DIS_ALL:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"DIS_ALL:3");
  log ("Logging enabled on all component");
  f_EPTF_enableAllComponentLog();
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{0},"ENA_ALL:0");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{1},"ENA_ALL:1");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"ENA_ALL:2");
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{3},"ENA_ALL:3");
  log("Event class 2 is disabled globally");
  f_EPTF_disableComponentLogEvent(f_EPTF_Logging_myMaskId(), 2);
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"DIS_CL_ALL:2");
  log("Event class 2 is enabled globally");
  f_EPTF_enableComponentLogEvent(f_EPTF_Logging_myMaskId(), 2);
  f_EPTF_Logging_log(true,f_EPTF_Logging_myMaskId(),{2},"ENA_CL_ALL:2");*/
}

function f_EPTF_Logging_Test_GenericV2() runs on LogTest_CT {
  var integer i := 0;
  log ("Logging enabled for all log classes.");
  for(i := 0; i < sizeof(c_LogTest_classList); i := i + 1) {
    f_EPTF_Logging_enableLocalMask(v_LogTest_maskId, i);
  }
  f_EPTF_Logging_errorV2(log2str("{", c_LogTest_classIdx_TestError, "}"), v_LogTest_maskId, {c_LogTest_classIdx_TestError});
  f_EPTF_Logging_warningV2(log2str("{", c_LogTest_classIdx_TestWarning, "}"), v_LogTest_maskId, {c_LogTest_classIdx_TestWarning});
  if(c_EPTF_Common_debugSwitch) {
    log("Debug log active.");
    f_EPTF_Logging_debugV2(log2str("{", c_LogTest_classIdx_TestDebug, "}"), v_LogTest_maskId, {c_LogTest_classIdx_TestDebug});
  } else {
    log("Debug log inactive due to optimization.");
    f_EPTF_Logging_debugV2("This must NOT be logged", v_LogTest_maskId, {c_LogTest_classIdx_TestDebug});
  }
  f_EPTF_Logging_operationalLogV2(log2str("{", c_LogTest_classIdx_TestOperational, "}"), v_LogTest_maskId, {c_LogTest_classIdx_TestOperational});

  log ("Logging disabled on component LogTest, enabled on EPTF_Logging");
  f_EPTF_Logging_disableLocal(v_LogTest_maskId);
  f_EPTF_Logging_operationalLogV2("This must be logged (with EPTF_Logging component mask)", f_EPTF_Logging_myMaskId(), {EPTF_Logging_DEBUG});
  f_EPTF_Logging_errorV2("This must NOT be logged", v_LogTest_maskId, {c_LogTest_classIdx_TestError});
  f_EPTF_Logging_enableLocal(v_LogTest_maskId);

  log ("Logging disabled for classId ", c_LogTest_classList[c_LogTest_classIdx_TestWarning]);
  f_EPTF_Logging_disableLocalMask(v_LogTest_maskId, c_LogTest_classIdx_TestWarning);
  f_EPTF_Logging_operationalLogV2("This must be logged", v_LogTest_maskId, {c_LogTest_classIdx_TestOperational});
  f_EPTF_Logging_errorV2("This must NOT be logged", v_LogTest_maskId, {c_LogTest_classIdx_TestWarning});

  f_EPTF_Logging_errorV2("Invalid classId - this must NOT be logged", v_LogTest_maskId, {c_LogTest_classIdx_Invalid});

  f_EPTF_Logging_errorV2("This must NOT be logged", v_LogTest_maskId, {});
  f_EPTF_Logging_operationalLogV2("This must be logged with no log class", v_LogTest_maskId_noClass, {});
}

testcase tc_EPTF_log_test() runs on LogTest_CT {
  log("----------test generic-----------")
  f_EPTF_Logging_init_CT("LogTest");
  f_EPTF_Logging_Test_Generic();
  f_EPTF_Base_cleanup_CT();
  setverdict(pass);
}

testcase tc_EPTF_log_testV2() runs on LogTest_CT {
  log("----------test generic V2-----------")
  f_EPTF_Logging_operationalLogV2("This must be logged, logging is not yet initialized", 0, {});
  f_EPTF_Logging_init_CT("LogTest");
  v_LogTest_maskId := f_EPTF_Logging_registerComponentMasks(
    "LogTest",
    c_LogTest_classList);
  v_LogTest_maskId_noClass := f_EPTF_Logging_registerComponentMasks(
    "LogTest_noClass",
    {});
  f_EPTF_Logging_Test_GenericV2();
  f_EPTF_Base_cleanup_CT();
  setverdict(pass);
}

control
{
  execute(tc_EPTF_log_test());
  execute(tc_EPTF_log_testV2());
}

} // end of module
