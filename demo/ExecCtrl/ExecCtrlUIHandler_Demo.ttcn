///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: ExecCtrlUIHandler_Demo
// 
//  Purpose:
//    This module demonstrates the usage of the ExecCtrl feature
// 
//  Module Parameters:
//    tsp_numEntities - integer
//    tsp_testDuration - float
//    tsp_nrOfLGens - integer
//    tsp_LGname_prefix - charstring
// 
//  Module depends on:
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_LGenBase_Definitions>
//    <EPTF_CLL_LGenBase_Functions>
//    <EPTF_CLL_LGenBase_ConfigFunctions>
//    <EPTF_CLL_LGenBase_ConfigDefinitions>
//    <AppLib_A_Definitions>
//    <AppLib_A_Functions>
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <EPTF_CLL_ExecCtrl_Functions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_UIHandler_WidgetFunctions>
//    <EPTF_CLL_UIHandler_XTDPTemplateDefinitions>
//    <XTDP_PDU_Defs>
//    <EPTF_CLL_UIHandlerCLI_Definitions>
//    <EPTF_CLL_UIHandlerCLI_Functions>
//    <EPTF_CLL_UIHandlerClient_Functions>
//    <EPTF_CLL_RBTScheduler_Functions>
//    <EPTF_CLL_ExecCtrlUIHandler_Definitions>
//    <EPTF_CLL_ExecCtrlUIHandler_Functions>
//    <EPTF_CLL_LoggingUI_Definitions>  
//    <EPTF_CLL_LoggingUI_Functions>  
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_LoadRegulator_Definitions>
//    <EPTF_CLL_LoadRegulator_Functions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_LoadRegulatorUI_Definitions>
//    <EPTF_CLL_LoadRegulatorUI_Functions>
//
//  Current Owner:
//    ethjgi
// 
//  Last Review Date:
//    2009-02-03
// 
// 
///////////////////////////////////////////////////////////

module ExecCtrlUIHandler_Demo
{
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_Functions all;
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_ConfigDefinitions all;

import from AppLib_A_Definitions all;
import from AppLib_A_Functions all;
//import from AppLib_B_Definitions all;
//import from AppLib_B_Functions all;

import from EPTF_CLL_ExecCtrl_Definitions all;
import from EPTF_CLL_ExecCtrl_Functions all;

import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from EPTF_CLL_UIHandler_XTDPTemplateDefinitions all;
import from EPTF_CLL_UIHandler_XULFunctions all;

import from XTDP_PDU_Defs language "ASN.1:2002" all;

import from EPTF_CLL_UIHandlerCLI_Definitions all;
import from EPTF_CLL_UIHandlerCLI_Functions all;

import from EPTF_CLL_UIHandlerClient_Functions all;

import from EPTF_CLL_RBTScheduler_Functions all;

import from EPTF_CLL_ExecCtrlUIHandler_Definitions all;
import from EPTF_CLL_ExecCtrlUIHandler_Functions all;

import from EPTF_CLL_LoggingUI_Definitions all;  
import from EPTF_CLL_LoggingUI_Functions all;  
import from EPTF_CLL_Logging_Functions all;

import from EPTF_CLL_LoadRegulator_Definitions all;
import from EPTF_CLL_LoadRegulator_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;

import from EPTF_CLL_LoadRegulatorUI_Definitions all;
import from EPTF_CLL_LoadRegulatorUI_Functions all;

import from EPTF_CLL_StatHandlerClient_Definitions all;

modulepar integer tsp_numEntities := 10;
modulepar float tsp_testDuration := 30.0;
modulepar integer tsp_nrOfLGens := 2;
modulepar charstring tsp_LGname_prefix := "LG_";

type component ExecCtrl_Demo_CT 
extends EPTF_LGenBase_CT, EPTF_ExecCtrlClient_UIHandler_CT, LGen_A_CT //, LGen_B_CT 
{
  var integer v_myBIdx;
}

//////////////////////////////////
// Basic Call LGen
//////////////////////////////////

modulepar float tsp_T_busy := 1.0; // time until entity busy

type component LGenBasicCall_CT extends EPTF_LGenBase_CT, EPTF_ExecCtrlClient_UIHandler_CT
{
  var integer v_myBIdx;
}

//init the "application level virtual library"
const charstring c_myBasicCallAppLib_behaviorType := "LGen BasicCall";
const integer c_BasicCallAppLib_numberOfResources := 100;
function f_init_myBasicCallAppLib() runs on LGenBasicCall_CT {
  var integer i;
  
  v_myBIdx:=f_EPTF_LGenBase_declareBehaviorType(c_myBasicCallAppLib_behaviorType, c_BasicCallAppLib_numberOfResources, null, null, null);
  log(%definitionId,": myBIdx is ", v_myBIdx);
  
  // declare FSM (this is just a simple FSM):
  var EPTF_LGenBase_FsmTableDeclarator vl_EPTF_LGenBase_FsmTableDeclarator := {
    name := "FSM for outgoing call",//"BasicCall",
    fsmParams := {
      {timerList := {{"busyTimer",tsp_T_busy}}},
      {stateList := {"idle", "busy"}}
      ,{varList :=  {
          {name := "vf_chrono", initValue := {floatVal := 0.0}, scope := TC}
          ,{name := "vf_chrono_free", initValue := {floatVal := 0.0}, scope := TC}
          ,{name := "cf_one", initValue := {intVal := 1}, scope := TC}
          ,{name := "vf_running", initValue := {intVal := 0}, scope := TC}
          ,{name := "vf_starts", initValue := {intVal := 0}, scope := TC}
        }}
      ,{statMeasStatList :=  {
          {name := "mean_rtt", varName := "vf_chrono",statType := mean, scope := TC}
          ,{name := "mean_busy", varName := "vf_running",statType := max, scope := TC}
          //,{name := "chrono_free", varName := "",statType := chrono, scope := FSM}
        }}
      ,{statMeasWithParamsList :=  {
          {
            name := "maxChrono", 
            providerVarName := "vf_chrono",
            targetVarName := "",
            statMeasParams := {
              statType := max}, 
            scope := TC
          },
          {
            name := "chrono_busy", 
            providerVarName := "",
            targetVarName := "vf_chrono",
            statMeasParams := {
              statType := chrono}, 
            scope := FSM
          },
          {
            name := "meanFree", 
            providerVarName := "vf_chrono_free",
            targetVarName := "",
            statMeasParams := {
              statType := mean}, 
            scope := TC
          },
          {
            name := "chrono_free", 
            providerVarName := "",
            targetVarName := "vf_chrono_free",
            statMeasParams := {
              statType := chrono}, 
            scope := FSM
          },
          {
            name := "density_free", 
            providerVarName := "vf_chrono_free",
            targetVarName := "",
            statMeasParams := {
              params := {
                density := {
                  scale := {
                    min := 0.0,
                    max := 19.0,
                    n := 20,
                    scale := linear}
                }
              }
            },
            scope := TC
          },
          {
            name := "p95_free", 
            providerVarName := "vf_chrono_free",
            targetVarName := "",
            statMeasParams := {
              params := {
                percentile95 := {
                  scale := {
                    min := 0.0,
                    max := 29.0,
                    n := 30,
                    scale := linear}
                }
              }
            },
            scope := TC
          }
        }}
      ,{statHandlerStatList :=  {
          {
            name := "MaxBusyTime",
            providers := {
              {statMeas := "maxChrono"}
            },
            statMethod := c_EPTF_StatHandler_Method_Max,
            statResetValue := {floatVal := 0.0},
            scope := TC 
          },
          {
            name := "MeanFreeTime",
            providers := {
              {statMeas := "meanFree"}
            },
            statMethod := c_EPTF_StatHandler_Method_Mean,
            statResetValue := {floatVal := 0.0},
            scope := TC 
          },
          {
            name := "AvrgFreeTime",
            providers := {
              {varList := {"vf_chrono_free", "cf_one"}}
            },
            statMethod := c_EPTF_StatHandler_Method_GlobalAverage,
            statResetValue := {floatVal := 0.0},
            scope := TC 
          },
          {
            name := "Percentile95FreeTime",
            providers := {
              {statMeas := "p95_free"}
            },
            statMethod := c_EPTF_StatHandler_Method_Percentile95,
            statResetValue := {floatVal := 0.0},
            scope := TC 
          },
          {
            name := "densityFreeTime",
            providers := {
              {statMeas := "density_free"}
            },
            statMethod := c_EPTF_StatHandler_Method_Density,
            statResetValue := {integerlistVal := {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}},
            scope := TC 
          },
          {
            name := "NofBusy",
            providers := {
              {varList := {"vf_running"}}
            },
            statMethod := c_EPTF_StatHandler_Method_Sum,
            statResetValue := {intVal := 0},
            scope := TC 
          },
          {
            name := "NofStarts",
            providers := {
              {varList := {"vf_starts"}}
            },
            statMethod := c_EPTF_StatHandler_Method_Sum,
            statResetValue := {intVal := 0},
            scope := TC 
          }
        }
      }
    },
    table := { classicTable := {
      {eventToListen := {c_EPTF_LGenBase_behavior,c_EPTF_LGenBase_inputName_testMgmt_startTC,fsm},
        cellRow := {
          //state[0]==idle 
          {{
              {c_EPTF_LGenBase_stepName_timerStart,{timerName := "busyTimer"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmResetChrono, contextArgs :={statMeasName := "chrono_busy"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmStartChrono, contextArgs :={statMeasName := "chrono_busy"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmStopChrono, contextArgs :={statMeasName := "chrono_free"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmVarInc, contextArgs :={varParams := {"vf_running",{intVal := 1}}}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmVarInc, contextArgs :={varParams := {"vf_starts",{intVal := 1}}}}
            },omit,"busy"}
          //state[1]==busy
          ,{omit,omit,omit}
        }
      },{eventToListen := {c_EPTF_LGenBase_specialBName_timerTimeout,"busyTimer",fsm},
        cellRow := {
          //state=idle
          {omit,omit,omit}
          //state==busy
          ,{{
              {stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmStopChrono, contextArgs :={statMeasName := "chrono_busy"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmResetChrono, contextArgs :={statMeasName := "chrono_free"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmStartChrono, contextArgs :={statMeasName := "chrono_free"}}
              ,{stepOrFunctionName := c_EPTF_LGenBase_stepName_fsmVarDec, contextArgs :={varParams := {"vf_running",{intVal := 1}}}}
              ,{c_EPTF_LGenBase_stepName_trafficSuccess,omit}
            }, omit,"idle"}
        }
      }
    }}//table
  };
  
  vl_EPTF_LGenBase_FsmTableDeclarator.name := "FSM for outgoing call";  
  if(-1==f_EPTF_LGenBase_declareFSMTable(vl_EPTF_LGenBase_FsmTableDeclarator)){};
  
  vl_EPTF_LGenBase_FsmTableDeclarator.name := "FSM for incoming call";  
  if(-1==f_EPTF_LGenBase_declareFSMTable(vl_EPTF_LGenBase_FsmTableDeclarator)){};
}

// initialize the LGen
function f_LGen_BasicCall_init(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on LGenBasicCall_CT {
  
  f_EPTF_LGenBase_init(pl_selfName, 0, "BasicCall");
  
  //initialize my FSM: 
  f_init_myBasicCallAppLib() ;      //behavior type index==0 

  // my entity type is: Entity_A
  // number of entities is the same as c_myVirtualAppLib_numberOfResources
  var integer vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_A", {c_myBasicCallAppLib_behaviorType});
  //log("DEBUG: v_LGenBase_entityTypes = ", v_LGenBase_entityTypes);
    
  f_EPTF_ExecCtrlClient_UIHandler_init_CT(pl_selfName,pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler);
 
  //log("DEBUG: v_LGenBase_behaviorTypes = ", v_LGenBase_behaviorTypes);

  f_EPTF_LGenBase_log();

}

// This is the LGen behaviour:
function f_LGen_BasicCall_behavior(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on LGenBasicCall_CT
{
  log(%definitionId,": started...");

  f_LGen_BasicCall_init(pl_selfName, pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler); 

  log(%definitionId,": entering main alt-loop...")
  f_EPTF_Base_wait4Shutdown();

  f_EPTF_Base_cleanup_CT();
  log(%definitionId,": finished...");
  setverdict(pass);
}

// Creates LGens with entity types: Entity_A
function f_EPTF_myCreateLGenBasicCall
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  var LGenBasicCall_CT LG := LGenBasicCall_CT.create;
  LG.start(f_LGen_BasicCall_behavior(pl_componentName, self, f_EPTF_UIHandlerClient_defaultUIHandler()));
  return LG;
}

//////////////////////////////////
// End of Basic Call LGen
//////////////////////////////////

const charstring c_appl_stepNameInitTc0 := "Initialize behavior contexts for tc0"
const integer c_appl_stepIdxInitTc0 := 0;
function f_step_initTc0(in EPTF_LGenBase_TestStepArgs pl_ptr) 
runs on ExecCtrl_Demo_CT { 
  var integer bCtx;
  bCtx:= f_EPTF_LGenBase_getBehaviorCtxItem(pl_ptr.eIdx,c_A_bIdx,0)
  v_A_contexts[bCtx].fooMessage := "Entity " & f_EPTF_LGenBase_getEntityName(pl_ptr.eIdx) &": "& v_A_contexts[bCtx].fooMessage ;
  v_A_contexts[bCtx].byeMessage := "Entity " & f_EPTF_LGenBase_getEntityName(pl_ptr.eIdx) &": "& v_A_contexts[bCtx].byeMessage ;
  //  bCtx:= v_LGenBase_entities[pl_ptr.eIdx].bCtxList[c_B_bIdx][0]
  //  v_B_contexts[bCtx].firstMessage := "Entity " & v_LGenBase_entities[pl_ptr.eIdx].name
  //  & " sends its first message acting as \"B\" in testcase#0";
  // v_B_contexts[bCtx].secondMessage := "Entity " & v_LGenBase_entities[pl_ptr.eIdx].name
  //  & " sends its second message acting as \"B\" in testcase#0"
  //  f_LGenBase_testStepIsOver(pl_ptr)
}



//init the "application level virtual library"
const charstring c_myVirtualAppLib_behaviorType := "LGen demo application"; // ethzto
function f_init_myVirtualAppLib() runs on ExecCtrl_Demo_CT {
  var integer i;
  
  v_myBIdx:=f_EPTF_LGenBase_declareBehaviorType(c_myVirtualAppLib_behaviorType, 100, null, null, null);
  log(%definitionId,": myBIdx is ", v_myBIdx);
  i:=f_EPTF_LGenBase_declareStep(c_myVirtualAppLib_behaviorType,{c_appl_stepNameInitTc0,refers(f_step_initTc0)});
  //  i:=f_EPTF_LGenBase_declareStep(c_myVirtualAppLib_behaviorType,{c_appl_stepNameInitTc1,refers(f_step_initTc1)});
  
}

// note the applib init function call seq. must be consistent 
// with these c_xxx_bIdx declarations 
const integer c_appl_bIdx := 1;
const integer c_A_bIdx := 2;
const integer c_B_bIdx := 3;

function f_LGen_Demo_init(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on ExecCtrl_Demo_CT {
  
  f_EPTF_LGenBase_init(pl_selfName, 0, "DemoEntity#");
  
  //note the applib init function call seq. must be consistent with ...bIdx declarations 
  f_init_myVirtualAppLib() ;      //behavior type index==0 
  f_LGen_A_init(tsp_numEntities); //behavior type index==1
  //  f_LGen_B_init(tsp_numEntities); //behavior type index==2


  var integer vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_A", {c_A_behaviorType});
  vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_B", {c_myVirtualAppLib_behaviorType});
  vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_AB", {c_A_behaviorType, c_myVirtualAppLib_behaviorType});
  //log("DEBUG: v_LGenBase_entityTypes = ", v_LGenBase_entityTypes);
  
  connect(self:port_A,self:port_A);  
  
  f_EPTF_ExecCtrlClient_UIHandler_init_CT(pl_selfName,pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler);
  //f_EPTF_ExecCtrlClient_init_CT(pl_selfName, pl_ExecCtrlRef);   // Only after library init(s)
 
  //log("DEBUG: v_LGenBase_behaviorTypes = ", v_LGenBase_behaviorTypes);

  f_EPTF_LGenBase_log();

}

function f_LGen_Demo_B_init(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on ExecCtrl_Demo_CT {
  
  f_EPTF_LGenBase_init(pl_selfName, 0, "DemoEntity#");
  
  //note the applib init function call seq. must be consistent with ...bIdx declarations 
  f_init_myVirtualAppLib() ;      //behavior type index==0 
  f_LGen_A_init(tsp_numEntities); //behavior type index==1
  //  f_LGen_B_init(tsp_numEntities); //behavior type index==2


  var integer vl_eTypeIdx := f_EPTF_LGenBase_declareEntityType("Entity_B", {c_A_behaviorType});
  //log("DEBUG: v_LGenBase_entityTypes = ", v_LGenBase_entityTypes);
  
  connect(self:port_A,self:port_A);  
  
  f_EPTF_ExecCtrlClient_UIHandler_init_CT(pl_selfName,pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler);
 
  //log("DEBUG: v_LGenBase_behaviorTypes = ", v_LGenBase_behaviorTypes);

  f_EPTF_LGenBase_log();

}

function f_LGen_Demo_Empty_init(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on ExecCtrl_Demo_CT {
  
  f_EPTF_LGenBase_init(pl_selfName, 0, "DemoEntity#");
  
  //note the applib init function call seq. must be consistent with ...bIdx declarations 
  f_init_myVirtualAppLib() ;      //behavior type index==0 
  f_LGen_A_init(tsp_numEntities); //behavior type index==1
  //  f_LGen_B_init(tsp_numEntities); //behavior type index==2


  // intentionally no entity types!
  //log("DEBUG: v_LGenBase_entityTypes = ", v_LGenBase_entityTypes);
  
  connect(self:port_A,self:port_A);  
  
  f_EPTF_ExecCtrlClient_UIHandler_init_CT(pl_selfName,pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler);
 
  //log("DEBUG: v_LGenBase_behaviorTypes = ", v_LGenBase_behaviorTypes);

  f_EPTF_LGenBase_log();

}

function f_LGen_Demo_behavior(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler, in boolean pl_enableExit := false) runs on ExecCtrl_Demo_CT
{
  log(%definitionId,": started...");

  f_LGen_Demo_init(pl_selfName, pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler);
  
  timer t_exit := int2float(60+20*f_EPTF_Base_upcast(self));
  if (pl_enableExit) {
    t_exit.start;
  }

  log(%definitionId,": entering main EH...")
  alt {
//    [] as_EPTF_ExecCtrlClient_MgmtIf() {repeat} // ethzto: ExecCtrlClient
//    [] as_EPTF_SchedulerComp_ActionHandler() 
    [] as_LibA()
    [pl_enableExit] t_exit.timeout {
      action(f_EPTF_Base_selfName()&" exiting...");
    };
    //    []as_LibB() 
  }
  setverdict(pass);

   f_EPTF_Base_cleanup_CT();
  log(%definitionId,": finished...");
}

function f_LGen_Demo_B_behavior(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on ExecCtrl_Demo_CT
{
  log(%definitionId,": started...");

  f_LGen_Demo_B_init(pl_selfName, pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler); 

  log(%definitionId,": entering main EH...")
  alt {
//    [] as_EPTF_ExecCtrlClient_MgmtIf() {repeat} // ethzto: ExecCtrlClient
//    [] as_EPTF_SchedulerComp_ActionHandler() 
    [] as_LibA() 
    //    []as_LibB() 
  }
  setverdict(pass);

   f_EPTF_Base_cleanup_CT();
  log(%definitionId,": finished...");
}

function f_LGen_Demo_Empty_behavior(in charstring pl_selfName, in EPTF_ExecCtrl_CT pl_ExecCtrlRef, in EPTF_UIHandler_CT pl_UIHandlerClient_DefaultUIHandler) runs on ExecCtrl_Demo_CT
{
  log(%definitionId,": started...");

  f_LGen_Demo_Empty_init(pl_selfName, pl_ExecCtrlRef,pl_UIHandlerClient_DefaultUIHandler); 

  log(%definitionId,": entering main EH...")
  alt {
//    [] as_EPTF_ExecCtrlClient_MgmtIf() {repeat} // ethzto: ExecCtrlClient
//    [] as_EPTF_SchedulerComp_ActionHandler() 
    [] as_LibA() 
    //    []as_LibB() 
  }
  setverdict(pass);

   f_EPTF_Base_cleanup_CT();
  log(%definitionId,": finished...");
}

function f_ExecCtrl_Demo_initGui() runs on EPTF_UIHandler_CT{

  var XTDP_XML_Tag execCtrlMainWindow := {        
      xtdp_tabpanel := {      
        id := omit,
        maxheight := omit,
        orient := "vertical",
        widgets := {
          {
            hbox := {
              id := omit,
              flex := omit,
              disabled := omit,
              orient := "horizontal",
              widgets := {
                {
                  hbox := {
                    id := omit,
                    flex := omit,
                    disabled := omit,
                    orient := "vertical",
                    widgets := {
                      { 
                        tabbox := {
                          id := c_EPTF_GUI_Main_Tabbox_WidgetId,
                          flex := omit,
                          disabled := omit,
                          layout := omit,
                          tabs := {
                            {
                              id := "Execution_Control.tab2",
                              tablabel := "USER Execution Control" 
                            }
                          },
                          tabpanels := {
                          }
                        }
                      }/*,
                      {
                        hbox := {
                          id := omit,
                          flex := omit,
                          disabled := omit,
                          orient := "horizontal",
                          widgets := {
                            {
                              hbox := {
                                id := omit,
                                flex := omit,
                                disabled := omit,
                                orient := "vertical",
                                widgets := {
                                  {
                                    textlabel := {
                                      id := "timeelapsed",
                                      flex := omit,
                                      disabled := omit,
                                      style := omit,
                                      textvalue := "Time elapsed since Test was started: 0.0" 
                                    }
                                  },
                                  {
                                    textlabel := {
                                      id := "listeningon",
                                      flex := omit,
                                      disabled := omit,
                                      style := omit,
                                      textvalue := "Listening on ." 
                                    }
                                  }                      
                                }
                              }
                            },
                            {
                              button := {
                                checked := omit,
                                disabled := omit,
                                id := "EPTF_snapshot_button2",
                                flex := omit,
                                buttonlabel := "Snapshot",
                                buttontype := omit,
                                imageId := omit 
                              }                          
                            },
                            {
                              spacer := {
                                flex := 0.1,
                                id := omit
                              }
                            },
                            {
                              button := {
                                checked := omit,
                                disabled := omit,
                                id := "EPTF_exit_ttcn_button2",
                                flex := omit,
                                buttonlabel := "Exit TTCN",
                                buttontype := omit,
                                imageId := omit 
                              }                          
                            }                        
                          }
                        }
                      }*/
                    }
                  } 
                }              
              }
            }                      
          }
        }     
      }  
    };
      
  var boolean widgetExist := false;
  log("execCtrlMainWindow: ", execCtrlMainWindow);
  if(not f_EPTF_UIHandler_addElementToGui(
    execCtrlMainWindow, 
    "",
    widgetExist)) {
    log(%definitionId,": InitGUI failed. Exiting...");
    f_EPTF_Base_stopAll();
    //stop; // this is not needed
  }
}

type component EPTF_LoadRegulator_Demo_CT extends EPTF_LoadRegulator_CT, EPTF_LoadRegulatorUI_CT {
}

function f_getSutLoad_Demo() runs on EPTF_LoadRegulator_CT return float {
  return v_EPTF_LoadRegulator_cpsToReach;
}


// waits until ExecCtrl GUI is ready (StatusLED turns green):
function f_EPTF_LoadRegulator_waitForExecCtrlGUIDone(in EPTF_Var_CT pl_execCtrlCompRef) runs on EPTF_LoadRegulator_Demo_CT {
  timer t_event := 0.0;
  t_event.start;
  var charstring vl_guiDoneVarName := "EPTF_ExecCtrl.EventStatus.GUIDone";
  var boolean vl_varCreated := false;
  timer t_maxWait := 100.0;
  t_maxWait.start;
  var EPTF_Var_DirectContent vl_guiDoneValue;
  alt {
    [not vl_varCreated and f_EPTF_Var_isPresentRemote(pl_execCtrlCompRef,vl_guiDoneVarName)] t_event.timeout {
      t_maxWait.start;
      vl_varCreated:=true;
      t_event.start;
      repeat;
    }
    [vl_varCreated and f_EPTF_Var_getRemoteContent(pl_execCtrlCompRef,vl_guiDoneVarName,vl_guiDoneValue) and match(vl_guiDoneValue, {statusLEDVal := {led_green,*}})] t_event.timeout {
      // ExecCtrlGUI is ready, exit alt
    }
  }
}

function f_EPTF_LoadRegulator_behaviour_ExecCtrl(in EPTF_Var_CT pl_execCtrlCompRef, in integer pl_regulatorIdx) runs on EPTF_LoadRegulator_Demo_CT {
  if (pl_regulatorIdx>1) {
    f_EPTF_LoadRegulator_init_CT(tsp_EPTF_ExecCtrl_RegulatorNames[pl_regulatorIdx],refers(f_getSutLoad_Demo),refers(f_EPTF_LoadRegulator_calculateNextCps_limitMax),-);
  } else {
    f_EPTF_LoadRegulator_init_CT(tsp_EPTF_ExecCtrl_RegulatorNames[pl_regulatorIdx],refers(f_getSutLoad_Demo),-,-);
  }
  f_EPTF_LoadRegulator_setLoadToReach(50.0);
  f_EPTF_LoadRegulator_registerToExecCtrl(pl_execCtrlCompRef);

  // wait for ExecCtrl GUI to become ready:
  f_EPTF_LoadRegulator_waitForExecCtrlGUIDone(pl_execCtrlCompRef);

  // using EPTF_LoadRegulatorUI_CT is not mandatory!
  // if used: put it under the ExecCtrl/Regulator tabbox:
  var charstring vl_parentWidgetId := "Execution_Control.Regulator.tabbox";
  f_EPTF_LoadRegulatorUI_init_CT(tsp_EPTF_ExecCtrl_RegulatorNames[pl_regulatorIdx],
                                          refers(f_getSutLoad_Demo),
					  -,
					  vl_parentWidgetId,
					  mtc);
  timer t_wait;
  t_wait.start(tsp_testDuration  /*10.0*int2float(pl_regulatorIdx)*/);
  t_wait.timeout;
  f_EPTF_Base_cleanup_CT();
}


// Test config deployment manually
type component myMTC_CT extends EPTF_Base_CT, EPTF_UIHandler_CT, /*EPTF_ExecCtrl_UIHandler_CT,*/ EPTF_LoggingUI_CT,  EPTF_UIHandler_CLI_CT {} 


// Creates LGens with entity types: Entity_A, Entity_B, Entity_AB
function f_EPTF_myCreateLGen
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  var ExecCtrl_Demo_CT LG := ExecCtrl_Demo_CT.create;
  LG.start(f_LGen_Demo_behavior(pl_componentName, self, f_EPTF_UIHandlerClient_defaultUIHandler()));
  return LG;
}

function f_EPTF_myCreateExitingLGen
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  var ExecCtrl_Demo_CT LG := ExecCtrl_Demo_CT.create;
  LG.start(f_LGen_Demo_behavior(pl_componentName, self, f_EPTF_UIHandlerClient_defaultUIHandler(),true));
  return LG;
}

// Creates LGens with entity type: Entity_B
function f_EPTF_myCreateLGenB
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  var ExecCtrl_Demo_CT LG := ExecCtrl_Demo_CT.create;
  LG.start(f_LGen_Demo_B_behavior(pl_componentName, self,f_EPTF_UIHandlerClient_defaultUIHandler()));
  return LG;
}

// This function creates LGens with no entity type defined on them.
// That means this LGen cannot be used for anything. This kind of LGens
// can be created by mistake. The purpose of this is to show how these LGens
// will appear on the ExecCtrl GUI
function f_EPTF_myCreateLGenEmpty
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  var ExecCtrl_Demo_CT LG := ExecCtrl_Demo_CT.create;
  LG.start(f_LGen_Demo_Empty_behavior(pl_componentName, self, f_EPTF_UIHandlerClient_defaultUIHandler()));
  return LG;
}

// This is how the create function should behave if LGen Creation failed (return null)
function f_EPTF_myCreateLGenNull
(
  in charstring pl_hostname,
  in charstring pl_componentName
)
runs on EPTF_ExecCtrl_UIHandler_CT
return EPTF_ExecCtrlClient_CT {
  return null;
}

function f_EPTF_my_ExecCtrl_FSMStatsCreatedCallbackFn(in EPTF_ExecCtrl_CreateFSMStats pl_createFSMStats) runs on EPTF_ExecCtrl_UIHandler_CT {
  log(%definitionId&": FSMStats created: ", pl_createFSMStats);
}

function f_EPTF_my_ExecCtrl_UIHandler_behavior(
  in charstring pl_selfName, 
  in integer pl_nofLGens, 
  in EPTF_UIHandler_CT pl_uiHandler_compRef,
  in charstring pl_EPTF_GUI_Main_Tabbox_WidgetId,
  in EPTF_LoggingUI_CT pl_loggingUi_compRef := null) runs on EPTF_ExecCtrl_UIHandler_CT {

  var EPTF_ExecCtrl_LGenFunction_Entry_List vl_lgenCreatorFunctions := {
    {
      omit,
      refers(f_EPTF_myCreateLGen)
    },
    {
      omit,
      refers(f_EPTF_myCreateLGenBasicCall)
    },{
      omit,
      refers(f_EPTF_myCreateExitingLGen)
    },
    {
      "f_lgenCreatorB",
      refers(f_EPTF_myCreateLGenB)
    },
    {
      "f_lgenCreatorEmpty",
      refers(f_EPTF_myCreateLGenEmpty)
    },
    {
      omit,
      refers(f_EPTF_myCreateLGenNull)
    }
  }

  f_EPTF_ExecCtrl_loadConfig(pl_EPTF_ExecCtrl_LGenFunction_Entry_List := vl_lgenCreatorFunctions);
  f_EPTF_ExecCtrl_UIHandler_init_CT(pl_selfName, pl_nofLGens, 
    pl_uiHandler_compRef,pl_EPTF_GUI_Main_Tabbox_WidgetId,pl_loggingUi_compRef);
    
  f_EPTF_ExecCtrl_registerFSMStatsCreatedCallback(refers(f_EPTF_my_ExecCtrl_FSMStatsCreatedCallbackFn));

  f_EPTF_ExecCtrl_behavior(pl_selfName, pl_nofLGens);
}


testcase TC() runs on myMTC_CT
{
  log(%definitionId,": Creating UIHandler...");

  f_EPTF_UIHandler_init_CT("ExecCtrlDemo_UIHandler", true); // default altsteps      
  f_EPTF_UIHandler_clearGUI();
  if (not f_EPTF_UIHandler_addWindow()) {
    log(%definitionId,": Cannot add main window. Exiting...");
    f_EPTF_Base_stopAll();
    //stop; this is not necessary
  }
  f_EPTF_UIHandler_CLI_init_CT("ExecCtrlDemo_UIHandler");
  f_ExecCtrl_Demo_initGui();

  f_EPTF_LoggingUI_init_CT(
    "ExecCtrlDemo_UIHandler", self, c_EPTF_GUI_Main_Tabbox_WidgetId);

  log(%definitionId,": Creating ExecCtrl...");
  f_EPTF_Base_init_CT("ExecCtrl_Demo");

  var EPTF_ExecCtrl_UIHandler_CT ExtCtrl := EPTF_ExecCtrl_UIHandler_CT.create;
//  ExtCtrl.start(f_EPTF_ExecCtrl_UIHandler_behavior("ExecCtrl", tsp_nrOfLGens, self,c_EPTF_GUI_Main_Tabbox_WidgetId));
  ExtCtrl.start(f_EPTF_my_ExecCtrl_UIHandler_behavior("ExecCtrl", tsp_nrOfLGens, self,c_EPTF_GUI_Main_Tabbox_WidgetId));
//  f_EPTF_ExecCtrl_UIHandler_init("ExecCtrlDemo_UIHandler",tsp_nrOfLGens, self,c_EPTF_GUI_Main_Tabbox_WidgetId);
//  var EPTF_ExecCtrl_CT ExtCtrl := EPTF_ExecCtrl_CT.create;
//  ExtCtrl.start(f_EPTF_ExecCtrl_behavior("ExecCtrl", tsp_nrOfLGens));

  // These LGens will be in the "default" LGen pool
  log(%definitionId,": Creating LGens (ExecCtrlClients)...")
  var ExecCtrl_Demo_CT LG;
  for (var integer i:=0;i<tsp_nrOfLGens-1;i:=i+1) {
    LG := ExecCtrl_Demo_CT.create;
    LG.start(f_LGen_Demo_behavior(tsp_LGname_prefix & int2str(i), ExtCtrl, self)); //, i==1)); // second LGen will Exit
  }
  
  for (var integer i:=0;i<1;i:=i+1) {
    LG := ExecCtrl_Demo_CT.create;
    LG.start(f_LGen_Demo_B_behavior(tsp_LGname_prefix & int2str(i) &"_A", ExtCtrl,self));
  }
  
  // creating the load regulators:
  var EPTF_LoadRegulator_Demo_CT LoadReg;
  for(var integer i:=0; i<sizeof(tsp_EPTF_ExecCtrl_RegulatorNames); i:=i+1) { 
    LoadReg := EPTF_LoadRegulator_Demo_CT.create;
    LoadReg.start(f_EPTF_LoadRegulator_behaviour_ExecCtrl(ExtCtrl,i));
  }

  timer t;
  t.start(tsp_testDuration);

  log(%definitionId,": myMTC_CT: entering main EH...")
  alt
  {

  [] t.timeout
    {
      log(%definitionId,": myMTC_CT: main EH: timeout...")
    }
  } // alt

//  // remove logging tab:
//  f_EPTF_UIHandler_removeElementFromGui(c_EPTF_GUI_LoggingUI_Tabpanel_WidgetId);  

  f_EPTF_Base_cleanup_CT();

  log(%definitionId,": finished")
}

control {
  execute(TC());
}

}  // end of module
