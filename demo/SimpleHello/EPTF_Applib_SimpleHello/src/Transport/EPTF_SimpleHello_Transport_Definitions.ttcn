///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_SimpleHello_Transport_Definitions
// 
//  Purpose:
//    This module produces definitions for the SimpleHello Transport.
//
//  Module Parameters:
//    tsp_EPTF_SimpleHello_loggingEnable - *boolean* - enables logging of the transport of the AppLib
//    tsp_EPTF_SimpleHello_Transport_loggingComponentMask - *charstring* - component mask name for the transport of the AppLib
//
//  Module depends on:
//    <EPTF_SimpleHello_LGen_Definitions> 
//    <EPTF_SimpleHello_Logger_Definitions> 
//    <EPTF_CLL_Base_Definitions> 
//    <EPTF_CLL_Common_Definitions> 
//    <EPTF_CLL_UIHandler_Definitions> 
//    <EPTF_CLL_LoggingUI_Definitions> 
//    <EPTF_CLL_TransportCommPortUDP_Definitions> 
//    <EPTF_CLL_TransportRouting_Definitions> 
//    <EPTF_CLL_Logging_Definitions> 
// 
//  Current Owner:
//    Attila Fulop (EFLOATT), Bence Molnar(EBENMOL)
// 
//  Last Review Date:
//    2009-04-03
//
//  Detailed Comments:
//    This module contains definitions for the SimpleHello Transport.
//
///////////////////////////////////////////////////////////////
module EPTF_SimpleHello_Transport_Definitions {

//=========================================================================
// Import Part
//=========================================================================

import from EPTF_SimpleHello_LGen_Definitions all;
import from EPTF_SimpleHello_Logger_Definitions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_UIHandler_Definitions all;
import from EPTF_CLL_LoggingUI_Definitions all;
import from EPTF_CLL_TransportCommPortUDP_Definitions all;
import from EPTF_CLL_TransportRouting_Definitions all;
import from EPTF_CLL_Logging_Definitions all;

//=========================================================================
// Module parameters
//=========================================================================
modulepar {
  boolean tsp_EPTF_SimpleHello_loggingEnable := false;
  charstring tsp_EPTF_SimpleHello_Transport_loggingComponentMask := "EPTF_SimpleHello_Transport";
}

//=========================================================================
// Data Types
//=========================================================================

///////////////////////////////////////////////////////////
//  Group: EPTF_SimpleHello_RemoteTransport
// 
//  Purpose:
//    This group defines the types of the remote transport component
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_RemoteTransport
{

  ///////////////////////////////////////////////////////////
  // Type: EPTF_SimpleHello_RemoteTransport_PT
  //
  // Purpose:
  //   SimpleHello remote transport port type
  //
  //  Parameters:
  //    - *inout* - *octetstring* - message 
  //
  // Detailed Comments:
  //   The port receives a SimpleHello message as octetstring
  //
  ///////////////////////////////////////////////////////////
  type port EPTF_SimpleHello_RemoteTransport_PT message
  {
    inout octetstring;
  } with {extension "internal"}

  ///////////////////////////////////////////////////////////
  // Type: EPTF_SimpleHello_RemoteTransport_CT 
  //
  // Purpose:
  //   The type definition of the Remote Transport component 
  //   Trasmits message between Applib and Mapper
  //
  // Extensions:
  //   <EPTF_SimpleHello_Transport_Logging_CT>
  //
  // Variables:
  //   v_EPTF_SimpleHello_RemoteTransport_message - *octetstring* - The Recevied message from the SimpleHello Applib
  //   v_EPTF_SimpleHello_RemoteTransport_default - *default* - The default altstep reference
  //   vf_EPTF_SimpleHello_RemoteTransport_handleReceive - <f_EPTF_SimpleHello_messageProcess_FT> - Function reference to the registered handleReceive function  
  //   v_EPTF_SimpleHello_RemoteTransport_initialized - *boolean* - The component initialized variable
  //
  //  Timers:
  //    -
  //
  //  Ports:
  //   SimpleHello_PCO - <EPTF_SimpleHello_RemoteTransport_PT> - Communication port between the RemoteTransport and Mapper components
  //
  // Detailed Comments:
  //   -
  //
  ///////////////////////////////////////////////////////////
  type component EPTF_SimpleHello_RemoteTransport_CT extends
  EPTF_SimpleHello_Transport_Logging_CT
  {
    port EPTF_SimpleHello_RemoteTransport_PT    SimpleHello_PCO;
    var default                                 v_EPTF_SimpleHello_RemoteTransport_default;
    var octetstring                             v_EPTF_SimpleHello_RemoteTransport_message;
    var f_EPTF_SimpleHello_messageProcess_FT    vf_EPTF_SimpleHello_RemoteTransport_handleReceive := null;
    var boolean 			        v_EPTF_SimpleHello_RemoteTransport_initialized := false;
  }
} // group RemoteTransport

///////////////////////////////////////////////////////////
//  Group: EPTF_SimpleHello_LocalTransport
// 
//  Purpose:
//    This group defines the types of the local transport component
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_LocalTransport
{

  ///////////////////////////////////////////////////////////
  // Type: EPTF_SimpleHello_LocalTransport_CT 
  //
  // Purpose:
  //   The type definition of the Local Transport component 
  //   Trasmits message between Applib and Mapper
  //
  // Extensions:
  //   <EPTF_SimpleHello_Transport_Logging_CT>
  //   <EPTF_CommPort_UDP_CT>
  //   <EPTF_SimpleHello_LoggerClient_CT>
  //   <EPTF_LoggingUIClient_CT>
  //
  // Variables:
  //   vf_EPTF_SimpleHello_LocalTransport_receive - <f_EPTF_SimpleHello_messageProcess_FT> - The registered callback function to receive the messages
  //   v_EPTF_SimpleHello_LocalTransport_loggingEnabled - *integer* - The Variable of the GUI logging
  //   v_EPTF_SimpleHello_LocalTransport_guiEnabled - *boolean* - Gui Logging enabled
  //   v_EPTF_SimpleHello_LocalTransport_initialized  - *boolean* - The component initialized variable
  //
  //  Timers:
  //    -
  //
  //  Ports:
  //   -
  //
  // Detailed Comments:
  //   -
  //
  ///////////////////////////////////////////////////////////
  type component EPTF_SimpleHello_LocalTransport_CT  extends
    EPTF_SimpleHello_Transport_Logging_CT,
    EPTF_CommPort_UDP_CT,
    EPTF_SimpleHello_LoggerClient_CT ,
    EPTF_LoggingUIClient_CT
  {
    var f_EPTF_SimpleHello_messageProcess_FT  vf_EPTF_SimpleHello_LocalTransport_receive := null; //FIXME put to transport interfaces
    var integer 			      v_EPTF_SimpleHello_LocalTransport_loggingEnabled := -1;
    var integer				      v_EPTF_SimpleHello_LocalTransport_loggingMaskId := c_EPTF_Logging_invalidMaskId;    
    var boolean				      v_EPTF_SimpleHello_LocalTransport_guiEnabled := false;
    var boolean 			      v_EPTF_SimpleHello_LocalTransport_initialized := false;
  }
} // group LocalTransport

///////////////////////////////////////////////////////////
//  Group: EPTF_SimpleHello_Mapper
// 
//  Purpose:
//    This group defines the types of the predefined Mapper component
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_Mapper
{

  ///////////////////////////////////////////////////////////
  // Type: EPTF_SimpleHello_Mapper_CT
  //
  // Purpose:
  //   The type definition of the Mapper component 
  //
   // Extensions:
  //   <EPTF_SimpleHello_LocalTransport_CT>
  //   <EPTF_Routing_CT>
  //   <EPTF_UIHandler_CT>
  //   <EPTF_LoggingUI_CT>
  //
  // Variables:
  //   v_EPTF_SimpleHello_Mapper_default - *default* - The default altstep
  //   v_EPTF_SimpleHello_Mapper_message - *octetstring* - The received SimpleHello message
  //   v_EPTF_SimpleHello_Mapper_SendercomponentRef - <EPTF_Base_CT> - The component reference
  //   v_EPTF_SimpleHello_Mapper_ReceiverComponentRef - <EPTF_Base_CT> - The component reference
  //   v_EPTF_SimpleHello_senderID - *integer* - id of the sender
  //   v_EPTF_SimpleHello_Mapper_hashMapIdx - *integer* - HashMap to find the component id from the connection id
  //   v_EPTF_SimpleHello_Mapper_initialized - *boolean* - The component initialized variable
  //
  //  Timers:
  //    -
  //
  //  Ports:
  //   SimpleHello_PCO - <EPTF_SimpleHello_RemoteTransport_PT> - Communication port between the RemoteTransport and Mapper components
  //
  // Detailed Comments:
  //   -
  //
  ///////////////////////////////////////////////////////////
  type component EPTF_SimpleHello_Mapper_CT extends 
    EPTF_SimpleHello_LocalTransport_CT,
    EPTF_Routing_CT,
    EPTF_UIHandler_CT,
    EPTF_LoggingUI_CT
  {
    port EPTF_SimpleHello_RemoteTransport_PT  SimpleHello_PCO;
    var default                               v_EPTF_SimpleHello_Mapper_default;
    var octetstring                           v_EPTF_SimpleHello_Mapper_message; 
    var EPTF_Base_CT		              v_EPTF_SimpleHello_Mapper_SendercomponentRef;
    var EPTF_Base_CT			      v_EPTF_SimpleHello_Mapper_ReceiverComponentRef;
    var integer                               v_EPTF_SimpleHello_senderID;
    var integer                               v_EPTF_SimpleHello_Mapper_hashMapIdx := -1;
    var boolean				      v_EPTF_SimpleHello_Mapper_initialized := false;
  }
} // group Mapper

///////////////////////////////////////////////////////////
//  Group: EPTF_SimpleHello_Logging
// 
//  Purpose:
//    This group defines the component type for the logging of Local/Remote transport
//
///////////////////////////////////////////////////////////
group EPTF_SimpleHello_Logging
{

  ///////////////////////////////////////////////////////////////////////////////
  // Type: EPTF_SimpleHello_Transport_Logging_CT
  //
  // Purpose:
  //   Common logging component for Local/Remote transport
  //
  // Extends:
  //   EPTF_Logging_CT
  //
  // Elements:
  //   v_EPTF_SimpleHello_Transport_loggingMaskId - *integer* - logging mask ID 
  //
  ///////////////////////////////////////////////////////////////////////////////
  type component EPTF_SimpleHello_Transport_Logging_CT extends EPTF_Logging_CT 
  {
    var integer	v_EPTF_SimpleHello_Transport_loggingMaskId := c_EPTF_Logging_invalidMaskId;    
  }
  
} // group Logging

//=========================================================================
// Constants
//=========================================================================

///////////////////////////////////////////////////////////////////////////////
//  Constant: c_EPTF_SimpleHello_Mapper_routingHashMapName
// 
//  Purpose:
//    Name of the routing HashMap of the Mapper component
// 
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_SimpleHello_Mapper_routingHashMapName := "EPTF SimpleHello Routing HashMap";

//=========================================================================
// Constants for Logging classes
//=========================================================================

///////////////////////////////////////////////////////////////////////////////
//  Constant: c_EPTF_SimpleHello_Transport_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used on SimpleHello_LocalTransport_CT component
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Error", "Warning", "Debug" }
///////////////////////////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_SimpleHello_Transport_loggingEventClasses := { "Error", "Warning", "Debug" };

///////////////////////////////////////////////////////////////////////////////
//  Constant: c_EPTF_SimpleHello_Transport_loggingClassIdx_Error
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////////////////////////
const integer c_EPTF_SimpleHello_Transport_loggingClassIdx_Error := 0;

///////////////////////////////////////////////////////////////////////////////
//  Constant: c_EPTF_SimpleHello_Transport_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Warning
// 
//  Detailed Comments:
//    *1*
///////////////////////////////////////////////////////////////////////////////
const integer c_EPTF_SimpleHello_Transport_loggingClassIdx_Warning := 1;

///////////////////////////////////////////////////////////////////////////////
//  Constant: c_EPTF_SimpleHello_Transport_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Debug
// 
//  Detailed Comments:
//    *2*
///////////////////////////////////////////////////////////////////////////////
  const integer c_EPTF_SimpleHello_Transport_loggingClassIdx_Debug := 2;

} //end-of-module
