///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

module EPTF_Base_test {

import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Base_Functions all;

type component A_CT extends EPTF_Base_CT {
  var boolean v_A_initialized := false;
  var integer v_A_Var := 0;
}

type component B_CT extends EPTF_Base_CT {
  var boolean v_B_initialized := false;
  var integer v_B_Var := 0;
}

type component C_CT extends A_CT,B_CT {
  var boolean v_C_initialized := false;
  var integer v_C_Var := 0;
}

type component D_CT extends C_CT,B_CT,A_CT,EPTF_Base_CT {
  var boolean v_D_initialized := false;
  var integer v_D_Var := 0;
}


// mandatory functions for A_CT:

function f_A_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on A_CT {
  if (v_A_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_EPTF_Base_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_A_Var := 0;

  v_A_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_A_cleanup_CT));

  log("----A INIT DONE----");
}


function f_A_cleanup_CT() runs on A_CT {
  if (v_A_initialized == false) {
    return;
  }

  // reset your variables here if needed...

  log("A: v_A_Var: ", v_A_Var);
  v_A_initialized := false;
  log("----A CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_A_Behaviour(in charstring pl_selfName) runs on A_CT {
  f_A_init_CT(pl_selfName);
  f_EPTF_Base_cleanup_CT();
}

// mandatory functions for B_CT:

function f_B_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on B_CT {
  if (v_B_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_EPTF_Base_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_B_Var := 0;

  v_B_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_B_cleanup_CT));

  log("----B INIT DONE----");
}


function f_B_cleanup_CT() runs on B_CT {
  if (v_B_initialized == false) {
    return;
  }

  // reset your variables here if needed...

  log("B: v_B_Var: ", v_B_Var);
  v_B_initialized := false;
  log("----B CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_B_Behaviour(in charstring pl_selfName) runs on B_CT {
  f_B_init_CT(pl_selfName);
  timer t_wait := 1.0;
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_cleanup_CT();
}

// mandatory functions for C_CT:

function f_C_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on C_CT {
  if (v_C_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_A_init_CT(pl_selfName /*additional parameters*/); 
  f_B_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_A_Var := v_A_Var + 1;
  v_B_Var := v_B_Var + 1;
  v_C_Var := 0;

  v_C_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_C_cleanup_CT));

  log("----C INIT DONE----");
}


function f_C_cleanup_CT() runs on C_CT {
  if (v_C_initialized == false) {
    return;
  }

  // reset your variables here if needed...

  log("A: v_A_Var: ", v_A_Var);
  log("B: v_B_Var: ", v_B_Var);
  log("C: v_C_Var: ", v_C_Var);
  v_C_initialized := false;
  log("----C CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_C_Behaviour(in charstring pl_selfName) runs on C_CT {
  f_C_init_CT(pl_selfName);
  timer t_wait := 100.0;
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_cleanup_CT();
}

// mandatory functions for D_CT:

function f_D_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on D_CT {
  if (v_D_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_C_init_CT(pl_selfName /*additional parameters*/); 
  f_B_init_CT(pl_selfName /*additional parameters*/); 
  f_A_init_CT(pl_selfName /*additional parameters*/); 
  f_EPTF_Base_init_CT(pl_selfName); 

  // initialize your component variables here...
  v_A_Var := v_A_Var + 1;
  v_B_Var := v_B_Var + 1;
  v_C_Var := v_C_Var + 1;
  v_D_Var := 0;

  v_D_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_D_cleanup_CT));

  log("----D INIT DONE----");
}


function f_D_cleanup_CT() runs on D_CT {
  if (v_D_initialized == false) {
    return;
  }

  // reset your variables here if needed...

  log("A: v_A_Var: ", v_A_Var);
  log("B: v_B_Var: ", v_B_Var);
  log("C: v_C_Var: ", v_C_Var);
  log("D: v_D_Var: ", v_D_Var);
  v_D_initialized := false;
  log("----D CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_D_Behaviour(in charstring pl_selfName) runs on D_CT {
  f_D_init_CT(pl_selfName);
  f_EPTF_Base_cleanup_CT();
}

testcase tc_EPTF_Base_test() runs on D_CT {
  var A_CT v_A_CT := A_CT.create;
  var B_CT v_B_CT := B_CT.create;
  var C_CT v_C_CT := C_CT.create;
  
  v_A_CT.start(f_A_Behaviour("A"));
  v_B_CT.start(f_B_Behaviour("B"));
  v_C_CT.start(f_C_Behaviour("C"));
  
  f_D_Behaviour("D");
  setverdict(pass);
}

/////////////////////////////////////////////
// stop remote test
/////////////////////////////////////////////

// creates two comp-s and exits
function f_Comp_A_Behaviour(in charstring pl_selfName) runs on A_CT {
  f_A_init_CT(pl_selfName);
  var B_CT v_B_CT := B_CT.create;
  var C_CT v_C_CT := C_CT.create;
  v_B_CT.start(f_Comp_B_Behaviour("B",v_C_CT));
  v_C_CT.start(f_Comp_C_Behaviour("C"));
  f_EPTF_Base_cleanup_CT();
}

// Kills the C comp and exits
function f_Comp_B_Behaviour(in charstring pl_selfName, in C_CT pl_C_CT) runs on B_CT {
  f_B_init_CT(pl_selfName);
  // wait a bit before killing C
  timer t_wait := 1.0;
  t_wait.start; t_wait.timeout;
  
  // stop C
  f_EPTF_Base_stopRemote(pl_C_CT);
  
  // wait a bit before stopping mtc
  t_wait.start; t_wait.timeout;

  // stop mtc
  f_EPTF_Base_stopRemote(mtc);

  // wait a bit before exiting
  t_wait.start; t_wait.timeout;

  f_EPTF_Base_cleanup_CT();
}

// waits for a long time and exits
function f_Comp_C_Behaviour(in charstring pl_selfName) runs on C_CT {
  f_C_init_CT(pl_selfName);
  // wait a lot before exiting
  timer t_wait := 10.0;
  t_wait.start; t_wait.timeout;

  f_EPTF_Base_cleanup_CT();
}

// exits
function f_Comp_D_Behaviour(in charstring pl_selfName) runs on D_CT {
  f_D_init_CT(pl_selfName);
  timer t_wait := 5.0;
  // wait a little before exiting
  t_wait.start; t_wait.timeout;
  
  f_EPTF_Base_cleanup_CT();
}

testcase tc_EPTF_Base_test_stopRemote() runs on D_CT {
  var A_CT v_A_CT := A_CT.create;
  
  v_A_CT.start(f_Comp_A_Behaviour("A"));
  
  f_Comp_D_Behaviour("D");
  setverdict(pass);
}

/////////////////////////////////////////////
// stop all test
/////////////////////////////////////////////

// creates two comp-s and exits
function f_Comp_A2_Behaviour(in charstring pl_selfName) runs on A_CT {
  f_A_init_CT(pl_selfName);
  var B_CT v_B_CT := B_CT.create;
  var C_CT v_C_CT := C_CT.create;
  v_B_CT.start(f_Comp_B2_Behaviour("B",v_C_CT));
  v_C_CT.start(f_Comp_C2_Behaviour("C"));
  f_EPTF_Base_cleanup_CT();
}

// Kills the C comp and exits
function f_Comp_B2_Behaviour(in charstring pl_selfName, in C_CT pl_C_CT) runs on B_CT {
  f_B_init_CT(pl_selfName);
  timer t_wait := 1.0;
  // wait a bit before stopping mtc
  t_wait.start; t_wait.timeout;

  // stop all components including the current one
  f_EPTF_Base_stopAll();
}

// waits for a long time and exits
function f_Comp_C2_Behaviour(in charstring pl_selfName) runs on C_CT {
  f_C_init_CT(pl_selfName);
  // wait a lot before exiting
  timer t_wait := 10.0;
  t_wait.start; t_wait.timeout;

  f_EPTF_Base_cleanup_CT();
}

// exits
function f_Comp_D2_Behaviour(in charstring pl_selfName) runs on D_CT {
  f_D_init_CT(pl_selfName);
  timer t_wait := 10.0;
  // wait for kill
  t_wait.start; t_wait.timeout;
  
  f_EPTF_Base_cleanup_CT();
}

testcase tc_EPTF_Base_test_stopAll() runs on D_CT {
  action("THE VERDICT OF THIS TESTCASE SHOULD BE FAILED!");
  var A_CT v_A_CT := A_CT.create;
  
  v_A_CT.start(f_Comp_A2_Behaviour("A"));
  
  f_Comp_D_Behaviour("D");
  setverdict(pass);
  action("THE VERDICT OF THIS TESTCASE SHOULD BE FAILED!");
}

/////////////////////////////////////////////
// assert test
/////////////////////////////////////////////

// calls assert
function f_Comp_A3_Behaviour(in charstring pl_selfName) runs on A_CT {
  f_A_init_CT(pl_selfName);
  f_EPTF_Base_assert("",true);
  f_EPTF_Base_assert("msg1",true);
  action("THE VERDICT OF THIS TESTCASE SHOULD BE FAILED!");
  f_EPTF_Base_assert("msg2",false);
  log("This will not be printed");
  f_EPTF_Base_cleanup_CT();
}

testcase tc_EPTF_Base_test_assert() runs on A_CT {
  action("THE VERDICT OF THIS TESTCASE SHOULD BE FAILED!");
  f_A_init_CT("Main");
  var A_CT v_A_CT := A_CT.create;
  
  v_A_CT.start(f_Comp_A3_Behaviour("A"));
  
  all component.done;
  setverdict(pass);
  action("THE VERDICT OF THIS TESTCASE SHOULD BE FAILED!");
}

/////////////////////////////////////////////
// deadlock test
/////////////////////////////////////////////

type port Hello_PT message {
  inout integer;
} with {extension "internal"}

type component A_DeadLock_CT extends A_CT  {
  port Hello_PT hello_PCO
}

type component C_DeadLock_CT extends C_CT  {
  port Hello_PT hello_PCO
}

function f_A_deadlock_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on A_DeadLock_CT {
  if (v_A_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_EPTF_Base_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_A_Var := 0;

  v_A_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_A_deadlock_cleanup_CT));

  log("----A INIT DONE----");
}

function f_A_deadlock_cleanup_CT() runs on A_DeadLock_CT {
  if (v_A_initialized == false) {
    return;
  }

  v_A_initialized := false;
  hello_PCO.send(1); // this is only sent if this component is stopped
  log("----A CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_A_deadlock_Behaviour(in charstring pl_selfName) runs on A_DeadLock_CT {
  f_A_deadlock_init_CT(pl_selfName);
  timer t_wait := 100.0;
  t_wait.start; t_wait.timeout; // 'infinite while'
  f_EPTF_Base_cleanup_CT();
}

function f_C_deadlock_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on C_DeadLock_CT {
  if (v_C_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_EPTF_Base_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_A_Var := 0;

  v_C_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_C_deadlock_cleanup_CT));

  log("----C INIT DONE----");
}


function f_C_deadlock_cleanup_CT() runs on C_DeadLock_CT {
  if (v_C_initialized == false) {
    return;
  }

  v_C_initialized := false;
  hello_PCO.receive; // this will wait forever if someone does not kill the A component
  log("----C CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

testcase tc_EPTF_Base_test_deadlock() runs on C_DeadLock_CT {
  f_C_deadlock_init_CT("Main");
  
  var A_DeadLock_CT v_A_CT := A_DeadLock_CT.create;
  var B_CT v_B_CT := B_CT.create;
  connect(self:hello_PCO, v_A_CT:hello_PCO);
  v_A_CT.start(f_A_deadlock_Behaviour("A")); // this will not return;
  v_B_CT.start(f_Comp_B_Behaviour("B",mtc)); // this will stop mtc twice with stopRemote
  
  all component.done;
  f_EPTF_Base_cleanup_CT();
  setverdict(pass);
}

/////////////////////////////////////////////
// stopRemote from MTC test
/////////////////////////////////////////////

function f_A_StopRemoteFromMTC_init_CT(in charstring pl_selfName /* your additional input parameters */) runs on A_CT {
  if (v_A_initialized) {
    return;
  }

  // call all the init functions of the components that your component extends _explicitly_:
  f_EPTF_Base_init_CT(pl_selfName /*additional parameters*/); 

  // initialize your component variables here...
  v_A_Var := 0;

  v_A_initialized := true;

  // register your cleanup function:
  f_EPTF_Base_registerCleanup(refers(f_A_StopRemoteFromMTC_cleanup_CT));

  log("----A INIT DONE----");
}

function f_A_StopRemoteFromMTC_cleanup_CT() runs on A_CT {
  if (v_A_initialized == false) {
    return;
  }

  v_A_initialized := false;
  log("----A CLEANUP DONE----");
  // additional cleanup functions can be called here, but better if you do not call them. Register them instead.
}

function f_A_StopRemoteFromMTC_Behaviour(in charstring pl_selfName) runs on A_CT {
  f_A_StopRemoteFromMTC_init_CT(pl_selfName);
  timer t_wait := 10.0;
  t_wait.start; t_wait.timeout; // wait for a long time
  f_EPTF_Base_cleanup_CT();
}

testcase tc_EPTF_Base_test_stopRemoteFromMTC() runs on A_CT {
  f_A_StopRemoteFromMTC_init_CT("Main");
  
  var A_CT v_A1_CT := A_CT.create;
  var A_CT v_A2_CT := A_CT.create;
  var A_CT v_A3_CT := A_CT.create;
  v_A1_CT.start(f_A_StopRemoteFromMTC_Behaviour("A1"));
  v_A2_CT.start(f_A_StopRemoteFromMTC_Behaviour("A2"));
  v_A3_CT.start(f_A_StopRemoteFromMTC_Behaviour("A3"));
  
  timer t_wait := 2.0;
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_stopRemote(v_A1_CT);
  
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_cleanup_CT();
}


} // end of module
