///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: CS_B_Definitions
//
// Purpose:
//   Example user module containing used defined protocol-specific definitions for
//   protol "B" used in EPTF central scheduling //FIXME
//
// Module Parameters:
//    tsp_debug_B_CS_SchedulerFunctions - *booelan* - Enable logging of scheduler functions in A
//
//  Module depends on:
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_CS_Definitions>
//    <EPTF_CLL_CSUIHandler_Definitions>
//    <Common_Definitions>
//    <B_defs>
//    <EPTF_CLL_ExecCtrl_Definitions>
//    <PIPEasp_PortType>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

module CS_B_Definitions
{


import from EPTF_CLL_Common_Definitions all; // EPTF_IntegerList
import from EPTF_CLL_CS_Definitions all; // Base
import from EPTF_CLL_CSUIHandler_Definitions all; // Base

import from Common_Definitions all;
import from B_defs all;
import from EPTF_CLL_ExecCtrl_Definitions all;

import from PIPEasp_PortType all; // example port used in LGen_B

//=========================================================================
// Data Types
//=========================================================================


///////////////////////////////////////////////////////////
//  Type: CS_B_PT
// 
//  Purpose:
//     Port type for communication between Admin and LGen
//
//  Elements:
//     -
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type port CS_B_PT message {
  inout CS_B_LGenExecuteTrafficCaseList;
  inout CS_B_LGenExecutionResultList;
  inout CS_B_LGenStatus;
} with { extension "internal" }



/////////////////////////////////////////////////////////////////////////
//
//  Component type : CS_LGen_B_CT
//
//  Purpose:
//    Example component type collecting user specefic CS_LGen features for
//    protocol type 'A'
//
//  Elements:
//    -
//
//  Detailed Comments:
//    -
/////////////////////////////////////////////////////////////////////////
type component CS_LGen_B_CT extends EPTF_CS_LGenBase_CT
{
  // Append this component type definition with the necessary variables, timers, ports, etc.

  // port B_PT B_PCO;
  port PIPEasp_PT PIPEasp_PCO; // just as example

  port CS_B_PT CS_PCO;

  var CS_B_LGenExecuteTrafficCaseList   v_execBurst_msg := {}
  var CS_B_LGenExecutionResultList      v_execBurstResult_msg := {}
  var integer                           v_execBurstIdx := 0;
  var integer                           v_trafficCaseIdx := 0;
}


/////////////////////////////////////////////////////////////////////////
//
//  Component type : CS_ApplAdmin_B_CT
//
//  Purpose:
//    Example component type collecting user specefic CS_Admin features for
//    protocol type 'B'
//
//  Elements:
//    -
//
//  Detailed Comments:
//    -
/////////////////////////////////////////////////////////////////////////
type component CS_ApplAdmin_B_CT extends EPTF_CS_ApplAdminBase_UIHandler_CT
{

  // User-defined, traffic type dependent extension of traffic case data
  var CS_B_TrafficCaseDataExtensionList v_B_trafficCaseData := {};

  var CS_B_LGenExecuteTrafficCaseList   v_execBurst_msg := {}
  var CS_B_LGenExecutionResultList      v_execBurstResult_msg := {}
  var CS_B_LGenStatus                   v_lgenStatus_msg := {false};
  var integer                           v_execBurstIdx := 0;
  var integer                           v_trafficCaseIdx := 0;
  var EPTF_IntegerList                  v_currentRangeItems := {};

  var CS_ExecPhase                      v_execPhase := execCTRL_PREEXEC;

  var default v_msgHandler;
  port CS_B_PT CS_PCO;

  var EPTF_CS_LGenBase_CT v_lgenCompRef;
}

} // module

