///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: A_defs
//
// Purpose:
//   Example user module containing used defined protocol-specific definitions for
//   protol "A" used in EPTF central scheduling
//
// Module Parameters:
//   -
//
//  Module depends on:
//    <EPTF_CLL_CS_Definitions>
//    <Common_Definitions>
// 
// Current Owner:
//    Gabor Tatarka (egbotat)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////

module A_defs
{

//=========================================================================
// Imports
//=========================================================================

import from EPTF_CLL_CS_Definitions all;
import from Common_Definitions all;

//=========================================================================
// Data Types
//=========================================================================

const integer c_A_numTc := 4;

///////////////////////////////////////////////////////////
//  Type: A_tc0
// 
//  Purpose:
//     Record type for traffic case 0. Used for composing
//     traffic type dependent data which sent embedded in 
//     A_tc_union from Admin to LGen
// 
//  Elements:
//      stringField_0 - *charstring* - charstring data field
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record A_tc0
{
  charstring stringField_0
}

///////////////////////////////////////////////////////////
//  Type: A_tc1
// 
//  Purpose:
//     Record type for traffic case 1. Used for composing
//     traffic type dependent data which sent embedded in 
//     A_tc_union from Admin to LGen
// 
//  Elements:
//      stringField_1 - *charstring* - charstring data field
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record A_tc1
{
  charstring stringField_1
}

///////////////////////////////////////////////////////////
//  Type: A_tc2
// 
//  Purpose:
//     Record type for traffic case 2. Used for composing
//     traffic type dependent data which sent embedded in 
//     A_tc_union from Admin to LGen
// 
//  Elements:
//      stringField_2 - *charstring* - charstring data field
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record A_tc2
{
  charstring stringField_2
}

///////////////////////////////////////////////////////////
//  Type: A_tc3
// 
//  Purpose:
//     Record type for traffic case 3. Used for composing
//     traffic type dependent data which sent embedded in 
//     A_tc_union from Admin to LGen
// 
//  Elements:
//      stringField_3 - *charstring* - charstring data field
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record A_tc3
{
  charstring stringField_3
}

///////////////////////////////////////////////////////////
//  Type: A_tc_union
// 
//  Purpose:
//     Union type used for composing traffic type dependent
//     data which sent from Admin to LGen
// 
//  Elements:
//      tc0 - <A_tc0> data for traffic case 0
//      tc1 - <A_tc1> data for traffic case 1
//      tc2 - <A_tc2> data for traffic case 2
//      tc3 - <A_tc3> data for traffic case 3
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type union A_tc_union
{
  A_tc0 tc0,
  A_tc1 tc1,
  A_tc2 tc2,
  A_tc3 tc3
}

///////////////////////////////////////////////////////////
//  Type: Adata
// 
//  Purpose:
//     Record type for composing traffic type dependent data
//     which sent from Admin to LGen
// 
//  Elements:
//      execPhase - <EPTF_execPhase> - the current execution phase
//      tcData - <A_tc_union> - traffic case data
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record Adata
{
  CS_ExecPhase execPhase,
  A_tc_union tcData
}

///////////////////////////////////////////////////////////
//  Type: AdataReply
// 
//  Purpose:
//     Record type for composing traffic type dependent data
//     which sent from LGen to Admin
//
//  Elements:
//      intResult - *integer* - intResult data field
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record AdataReply
{
  A_tc_union tcData
}

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenExecuteTrafficCase
// 
//  Purpose:
//     Record type for triggering a traffic case
//
//  Elements:
//      trafficCaseSelector - *integer*
//      data - <Adata>
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record CS_A_LGenExecuteTrafficCase
{
  integer   trafficCaseSelector,
  Adata     data
}

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenExecuteTrafficCaseList
// 
//  Purpose:
//     Record type for triggering a list of traffic cases
//
//  Elements:
//      record of CS_A_LGenExecuteTrafficCase
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record of CS_A_LGenExecuteTrafficCase CS_A_LGenExecuteTrafficCaseList

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenExecutionResult
// 
//  Purpose:
//     Record type for result of a traffic case
//
//  Elements:
//      trafficCaseSelector - *integer*
//      execResult - *verdicttype*
//      data - AdataReply
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record CS_A_LGenExecutionResult
{
  integer     trafficCaseSelector,
  verdicttype execResult,
  AdataReply  data
}

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenExecutionResultList
// 
//  Purpose:
//     Record type for result of a list of traffic cases
//
//  Elements:
//      record of TcResult_A
// 
//  Detailed Comments:
//    
///////////////////////////////////////////////////////////
type record of CS_A_LGenExecutionResult CS_A_LGenExecutionResultList

///////////////////////////////////////////////////////////
//  Type: CS_A_LGenStatus
// 
//  Purpose:
//   Record type used by LGen to set its status/availability in the admin's
//   pool.
// 
//  Elements:
//   LGenEnabled - *boolean* - status of the LGen
// 
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
type record CS_A_LGenStatus
{
  boolean        LGenEnabled
}

///////////////////////////////////////////////////////////
//  Type: CS_A_TrafficCaseDataExtension
// 
//  Purpose:
//     User-defined, traffic type dependent extension of traffic case data.
// 
//  Elements:
//   -
//
//  Detailed Comments:
//   It is just an example to demonstrate how to extend the 
//   standard, library-defined traffic case data with protocol-
//   type specific data
///////////////////////////////////////////////////////////
type record CS_A_TrafficCaseDataExtension
{
  integer userId_A_k
}

///////////////////////////////////////////////////////////
// Type: CS_A_TrafficCaseDataExtensionList
//
// Purpose:
//   Record of CS_A_TrafficCaseDataExtension elements to provide a container
//   for arbitrary number of user-defined, traffic type dependent traffic case
//   data
//
// Elements:
//   record of <CS_A_TrafficCaseDataExtension>
//
//  Detailed Comments:
//   See also <EPTF_CS_trafficCaseDataList>
///////////////////////////////////////////////////////////
type record of CS_A_TrafficCaseDataExtension CS_A_TrafficCaseDataExtensionList;


} // module

