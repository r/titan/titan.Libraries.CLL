#!/usr/bin/env python
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import urllib.request, urllib.error, urllib.parse
import json
url = "http://localhost:3164/titansim.ajaxcall"
json_data = json.dumps({"requests":[ {"getData":{ "source":"ExecCtrl",  "element":"Start",  "ptcname":"",  "params":[]  }} ]})
request = urllib.request.Request(url, json_data, {'Content-Type': 'text'})
response = urllib.request.urlopen(request).read()

print(response)
