<!--Copyright (c) 2000-2019 Ericsson Telecom AB

All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v2.0
which accompanies this distribution, and is available at
http:www.eclipse.org/legal/epl-v10.html                           -->
<!DOCTYPE TITAN_GUI_project_file>
<Project TITAN_version="1.7.pl2" >
    <General>
        <Project_Name>EPTF_LGenBase_Demo</Project_Name>
        <Executable_Path>bin/EPTF_LGenBase_Demo</Executable_Path>
        <Working_Dir>bin</Working_Dir>
        <Build_Host>alpha</Build_Host>
        <Execution_Mode>Parallel</Execution_Mode>
        <ScriptFile_AfterMake>../HostAdmin/makefile_patch.sh</ScriptFile_AfterMake>
        <Log_Format>yes</Log_Format>
        <GNU_Make>yes</GNU_Make>
        <Update_Symlinks>yes</Update_Symlinks>
        <Create_Absolute_Symlinks>no</Create_Absolute_Symlinks>
        <Update_Makefile>yes</Update_Makefile>
        <Localhost_Execute>yes</Localhost_Execute>
        <Execute_Command>rsh %host &quot;cd %project_working_dir ; &quot;%executable&quot; %localhost %mctr_port&quot;</Execute_Command>
        <Execute_Hosts>alfa, beta, gamma</Execute_Hosts>
        <UnUsed_List></UnUsed_List>
    </General>
    <Configs>
        <Config>EPTF_LGenBaseDemo.cfg</Config>
    </Configs>
    <Test_Cases>
        <Test_Case>EPTF_LGenBaseDemo_Functions.tc_LGenBaseDemo_reg</Test_Case>
    </Test_Cases>
    <File_Group name="MainFileGroup" >
        <File_Groups>
            <File_Group name="Demo" >
                <File path="EPTF_LGenBaseDemo_CallerDefinitions.ttcn" />
                <File path="EPTF_LGenBaseDemo_CallerFunctions.ttcn" />
                <File path="EPTF_LGenBaseDemo_Definitions.ttcn" />
                <File path="EPTF_LGenBaseDemo_Functions.ttcn" />
                <File path="EPTF_LGenBaseDemo_ResponderDefinitions.ttcn" />
                <File path="EPTF_LGenBaseDemo_ResponderFunctions.ttcn" />
                <File path="EPTF_LGenBaseDemo_SiblingFsmDefinitions.ttcn" />
                <File path="EPTF_LGenBaseDemo_SiblingFsmFunctions.ttcn" />
                <File path="EPTF_LGenBaseDemo_TemplateFunctions.ttcn" />
                <File path="EPTF_LGenBaseDemo_TransportDefinitions.ttcn" />
                <File path="EPTF_LGenBaseDemo_TransportFunctions.ttcn" />
            </File_Group>
            <File_Group path="../../src/Common/EPTF_CLL_Common.grp" />
            <File_Group path="../../src/Base/EPTF_CLL_Base.grp" />
            <File_Group path="../../src/Variable/EPTF_CLL_Variable.grp" />
            <File_Group path="../../src/LGenBase/EPTF_CLL_LGenBase.grp" />
            <File_Group path="../../src/HashMap/EPTF_CLL_HashMap.grp" />
            <File_Group path="../../src/FreeBusyQueue/EPTF_CLL_FreeBusyQueue.grp" />
            <File_Group path="../../src/RedBlackTree/EPTF_CLL_RBtree.grp" />
            <File_Group path="../../src/Scheduler/EPTF_CLL_Scheduler.grp" />
            <File_Group path="../../src/Logging/EPTF_CLL_Logging.grp" />
            <File_Group path="../../src/RandomNArray/EPTF_CLL_RNA.grp" />
            <File_Group path="../../src/StatHandler/EPTF_CLL_StatHandler.grp" />
            <File_Group path="../../src/StatMeasure/EPTF_CLL_StatMeasure.grp" />
            <File_Group path="../../src/Semaphore/EPTF_CLL_Semaphore.grp" />
            <File_Group path="../../src/DataSource/EPTF_CLL_DataSource.grp" />
            <File_Group path="../../src/CommandLineInterface/EPTF_CLL_CLI.grp" />
            <File_Group name="TELNETAsp" >
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PortType.ttcn" />
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PT.cc" />
                <File path="../../../../TestPorts/TELNETasp_CNL113320/src/TELNETasp_PT.hh" />
            </File_Group>
            <File_Group name="PIPEasp" >
                <File path="../../../../TestPorts/PIPEasp_CNL113334/src/PIPEasp_PortType.ttcn" />
                <File path="../../../../TestPorts/PIPEasp_CNL113334/src/PIPEasp_Types.ttcn" />
                <File path="../../../../TestPorts/PIPEasp_CNL113334/src/PIPEasp_PT.cc" />
                <File path="../../../../TestPorts/PIPEasp_CNL113334/src/PIPEasp_PT.hh" />
            </File_Group>
            <File_Group name="Useful" >
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCFileIO_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCDateTime.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths_Functions.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCMaths_GenericTypes.ttcn" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding.cc" />
                <File path="../../../TCCUsefulFunctions_CNL113472/src/TCCEncoding_Functions.ttcn" />
		<File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion_Functions.ttcn" />
		<File path="../../../TCCUsefulFunctions_CNL113472/src/TCCConversion.cc" />
            </File_Group>
            <File_Group name="XTDP_PM" >            
              <File path="../../../../ProtocolModules/COMMON/src/UsefulTtcn3Types.ttcn" />
              <File path="../../../../ProtocolModules/COMMON/src/XSD.ttcn" />
              <File path="../../../../ProtocolModules/XTDP_CNL113663/src/generated_files/ttcn_ericsson_se_protocolModules_xtdp_xtdl.ttcn" />
              <File path="../../../../ProtocolModules/XTDP_CNL113663/src/generated_files/ttcn_ericsson_se_protocolModules_xtdp_xtdp.ttcn" />
            </File_Group>
        </File_Groups>
    </File_Group>
</Project>
