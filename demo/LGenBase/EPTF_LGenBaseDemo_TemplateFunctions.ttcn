///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_LGenBaseDemo_TemplateFunctions
//
//  Purpose:
//    This module contains functions to
//    demonstrate the use of the template handling functions of the LGenBase
//
//  Module Parameters:
//    -
//
//  Module depends on:
//
//  Current Owner:
//    ELSZSKU
//
//  Last Review Date:
//    20 - -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////
module EPTF_LGenBaseDemo_TemplateFunctions
{
import from EPTF_CLL_LGenBase_ConfigFunctions all;
import from EPTF_CLL_LGenBase_Definitions all;
import from EPTF_CLL_LGenBase_TemplateFunctions all;
import from EPTF_LGenBaseDemo_CallerDefinitions all
import from EPTF_LGenBaseDemo_TransportDefinitions all

///////////////////////////////////////////////////////////
// Encodes the Register message
// The function uses the f_EPTF_LGenBase_getTemplateContentFast
// function. This is a fast method, but the arguments must
// be in their declaration order.
// Demonstrates also how to retrieve the template set
// associated to the traffic case in the scenario declaration.
///////////////////////////////////////////////////////////
public function f_LGenBaseDemo_Caller_encodeRegister(
  in EPTF_LGenBase_TestStepArgs pl_args,
  in LGenBaseDemo_Transport_MessageData pl_msgData)
runs on LGenBaseDemo_Caller_CT
return charstring{
  var integer vl_templateSetIdx := f_EPTF_LGenBase_templateSetIdxOfStep(pl_args)
  var charstring vl_ret := ""
  var charstring vl_callId := int2str(pl_msgData.entityIdx)
  vl_callId := substr("000000",0,6-lengthof(vl_callId)) & vl_callId 
  var charstring vl_toId := int2str(pl_msgData.entityIdx+1)
  vl_toId := substr("000000",0,6-lengthof(vl_toId)) & vl_toId
  //Check if there is external template
  var integer vl_templateIdx := 
  f_EPTF_LGenBase_getExtTemplIdxFromSet(
    vl_templateSetIdx, 
    v_LGenBaseDemo_Caller_msgRegisterTemplTypeIdx)
  if(-1 < vl_templateIdx){
    //Create from template
    //This case the arguments MUST be in order of the arguments declared
    //in the f_EPTF_LGenBase_declareTemplateType
    f_EPTF_LGenBase_getTemplateContentFast(
      vl_templateSetIdx, 
      vl_templateIdx,
      {
        "+36(1)725100",
        vl_callId,
        "+36(1)725100",
        vl_toId,
        int2str(pl_msgData.entityIdx),
        int2str(pl_msgData.fsmCtxIdx)
      }, 
      vl_ret)
  }else{
    vl_ret := 
      "LGenBase Demo RegisterMsg created internally\n\r"&
      "From: +36(1)725100"&vl_callId&"\n\r"&
      "To: +36(1)725100"&vl_toId&"\n\r"&
      "UserData: "&int2str(pl_msgData.entityIdx)&":"&int2str(pl_msgData.fsmCtxIdx)&";\n\r"
  }
  return vl_ret
}

///////////////////////////////////////////////////////////
// Encodes the ReRegister message
// The function uses the f_EPTF_LGenBase_getTemplateContent
// function. This is a slower method, but the arguments
// does not have to be in their declaration order.
///////////////////////////////////////////////////////////
public function f_LGenBaseDemo_Caller_encodeReRegister(
  in LGenBaseDemo_Transport_MessageData pl_msgData)
runs on LGenBaseDemo_Caller_CT
return charstring{
  var charstring vl_ret := ""
  var charstring vl_callId := int2str(pl_msgData.entityIdx)
  vl_callId := substr("000000",0,6-lengthof(vl_callId)) & vl_callId 
  var charstring vl_toId := int2str(pl_msgData.entityIdx+1)
  vl_toId := substr("000000",0,6-lengthof(vl_toId)) & vl_toId
  //Check if there is external template
  if(-1 < v_LGenBaseDemo_Caller_msgRegisterTemplTypeIdx){
    //Create from template
    //This case the arguments MUST be in order of the arguments declared
    //in the f_EPTF_LGenBase_declareTemplateType
    f_EPTF_LGenBase_getTemplateContent(
      v_LGenBaseDemo_Caller_msgRegisterTemplTypeIdx, 
      {
        {"eIdx",int2str(pl_msgData.entityIdx)},
        {"phonePrefix","+36(1)725100"},
        {"fromNr",vl_callId},
        {"fIdx",int2str(pl_msgData.fsmCtxIdx)}
      }, 
      vl_ret)
  }else{
    vl_ret := 
      "LGenBase Demo ReRegisterMsg created internally\n\r"&
      "From: +36(1)725100"&vl_callId&"\n\r"&
      "To: \n\r"&
      "UserData: "&int2str(pl_msgData.entityIdx)&":"&int2str(pl_msgData.fsmCtxIdx)&";\n\r"
  }
  return vl_ret
}

///////////////////////////////////////////////////////////
// Encodes the DoMsg message
// The function uses the f_EPTF_LGenBase_getTemplateContentFast
// function. This is a fast method, but the arguments must
// be in their declaration order.
// Demonstrates also how to retrieve the template set
// associated to the traffic case in the scenario declaration.
///////////////////////////////////////////////////////////
public function f_LGenBaseDemo_Caller_encodeDoMsg(
  in EPTF_LGenBase_TestStepArgs pl_args,
  in LGenBaseDemo_Transport_MessageData pl_msgData)
runs on LGenBaseDemo_Caller_CT
return charstring{
  var charstring vl_ret := ""
  var charstring vl_callId := int2str(pl_msgData.entityIdx)
  vl_callId := substr("000000",0,6-lengthof(vl_callId)) & vl_callId 
  var charstring vl_toId := int2str(pl_msgData.entityIdx+1)
  vl_toId := substr("000000",0,6-lengthof(vl_toId)) & vl_toId
  //Check if there is external template
  var integer vl_templateSetIdx := f_EPTF_LGenBase_templateSetIdxOfStep(pl_args)
  var integer vl_templateIdx := 
  f_EPTF_LGenBase_getExtTemplIdxFromSet(
    vl_templateSetIdx, 
    v_LGenBaseDemo_Caller_msgDoMsgTemplTypeIdx)
  if(-1 < vl_templateIdx){
    //Create from template
    //This case the arguments MUST be in order of the arguments declared
    //in the f_EPTF_LGenBase_declareTemplateType
    f_EPTF_LGenBase_getTemplateContent(
      vl_templateIdx,
      {
        {"phonePrefix","+36(1)725100"},
        {"fromNr",vl_callId},
        {"eIdx",int2str(pl_msgData.entityIdx)},
        {"fIdx",int2str(pl_msgData.fsmCtxIdx)},
        {"phonePrefixTo","+36(1)725100"},
        {"toNr",vl_toId}
      }, 
      vl_ret)
  }else{
    vl_ret := 
      "LGenBase Demo DoMsg message created internally\n\r"&
      "From: +36(1)725100"&vl_callId&"\n\r"&
      "To: +36(1)725100"&vl_toId&"\n\r"&
      "UserData: "&int2str(pl_msgData.entityIdx)&":"&int2str(pl_msgData.fsmCtxIdx)&";\n\r"
  }
  return vl_ret
}

}  // end of module
