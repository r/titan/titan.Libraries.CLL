///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
The purpose of this demo is to give a basic view of the use of the LGenBase
feature of the TitanSim Core Load Library.

The scenario is the following:
- There are two components: the Caller and the Responder.
- The Caller simulates a user, which one sends "Register" messages to the 
  Responder component.
- The Responder sends back an answer with randomly set true or false result
  (successful or failed registration).
- The Caller sends Register messages until the first successful registration,
  or 3 failed registrations.
- When a user got back a true result, starts to send "ReRegister" messages
  until all the users finished either because of a successful registration, or
  3 failed registrations.
- Then the users registered successfully start to send "DoMsg" messages.
- They send the "DoMsg" messages for 4 seconds (it can be changed via module
  parameter).
  
The Responder manages a simple database about the registrations, and sets the
verdict to fail either if a user sends Register message after a successful
registration, or it sends ReRegister or DoMsg messages without successful
registration.

Each message exchange follows this scenario:
- The caller sends a message.
- The Responder sends back an answer.
- The Caller sends back an acknowledgement message.



The project tries to demonstrate how to
- describe FSMs
- use FSM variables and statistics
- dispatch and catch FSM events either in FSMs, or with listener functions
- use FSM events to pass data described in the FSM or at their dispatching
- use FSM timers
- write custom FSM test steps
- build up scenarios joining traffic cases related to each other
- write and use custom functions used in the management of traffic cases
- use external templates to describe the content of a message
- store temporary data in a storage related to an entity
- store temporary data in FSM variables
- reach statistics built in to the traffic cases
and shows some convenience functions.

/////////////////////////////////////////////////////////////
In order to use the included .project file you must declare a
path variable with name "TCC_Common". The path variable must
point to the folder