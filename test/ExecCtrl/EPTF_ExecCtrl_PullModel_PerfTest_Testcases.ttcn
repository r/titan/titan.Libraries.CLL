///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_ExecCtrl_PullModel_PerfTest_Testcases
// 
//  Purpose:
//    This module contains the testcases to test GUI build up time.
// 
//  Module depends on:
//   <EPTF_ExecCtrl_Test_Definitions>
//   <EPTF_ExecCtrl_Test_Function>
//   <EPTF_CLL_Base_Functions>           
//   <EPTF_CLL_Common_Definitions>
//   <EPTF_CLL_CLI_Definitions>
//   <EPTF_CLL_ExecCtrlTimeProfile_Definitions>
//   <EPTF_CLL_ExecCtrl_Functions>
//   <EPTF_CLL_ExecCtrl_CLIFunctions>
//   <EPTF_CLL_ExecCtrl_Definitions>
//   <EPTF_CLL_UIHandler_WidgetFunctions>
//   <EPTF_CLL_ExecCtrlUIHandler_Definitions>
//   <EPTF_CLL_HashMapInt2Int_Functions>
//   <EPTF_CLL_HashMapOct2Int_Functions>
//   <EPTF_CLL_HashMapStr2Int_Functions>
// 
//  Current Owner:
//    Jozsef Gyurusi (ETHJGI)
// 
//  Last Review Date:
//    2013-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////

module EPTF_ExecCtrl_PullModel_PerfTest_Testcases {

import from EPTF_ExecCtrl_PullModel_PerfTest_Definitions all;
import from EPTF_ExecCtrl_PullModel_PerfTest_Functions all;
import from EPTF_CLL_Base_Functions all;


// this test is the same as the testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_Manual, but with lot of entity groups
testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_Manual() runs on ExecCtrl_PullModelPerfTest_CT
{

  f_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_mainTestCase(
    pl_nofExternalStatHandlers := 4,
    pl_usePullModeForStats := false
  );

  f_EPTF_Base_wait4Shutdown();
  f_EPTF_Base_stop(pass);
}

testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_Orig() runs on ExecCtrl_PullModelPerfTest_CT
{

  f_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_mainTestCase(
    pl_nofExternalStatHandlers := 0,
    pl_usePullModeForStats := false
  );

  f_EPTF_Base_stop(pass);
}

testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_OrigWithPullModel() runs on ExecCtrl_PullModelPerfTest_CT
{

  f_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_mainTestCase(
    pl_nofExternalStatHandlers := 0,
    pl_usePullModeForStats := true
  );

  f_EPTF_Base_stop(pass);
}

testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_StatHandlerSeparationOnly() runs on ExecCtrl_PullModelPerfTest_CT
{

  f_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_mainTestCase(
    pl_nofExternalStatHandlers := 4,
    pl_usePullModeForStats := false
  );

  f_EPTF_Base_stop(pass);
}

testcase tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_StatHandlerSeparationWithPullModel() runs on ExecCtrl_PullModelPerfTest_CT
{

  f_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_mainTestCase(
    pl_nofExternalStatHandlers := 4,
    pl_usePullModeForStats := true
  );

  f_EPTF_Base_stop(pass);
}

control {
  execute(tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_Orig());
  execute(tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_OrigWithPullModel());
  execute(tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_StatHandlerSeparationOnly());
  execute(tc_ExecCtrl_DS_UIFromXUL_With_ExternalStatHandler_LotOfEGrps_StatHandlerSeparationWithPullModel());
}

} //end of module EPTF_ExecCtrl_PullModel_PerfTest_Testcases
