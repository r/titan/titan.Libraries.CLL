///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <TTCN3.hh>
#include <netinet/in.h> // htons
//#include <arpa/inet.h> // inet_ntoa
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>

#include "EPTF_ExecCtrl_Test_Functions.hh"

namespace EPTF__ExecCtrl__Test__Functions
{

  FLOAT f__EPTF__runTop()
  {
    int i,p, g;
    float j;
    p=getpid();
    char buffer[35];
//    sprintf(buffer,"./runTop.sh %d 2>&-",p);
    sprintf(buffer,"./runTop.sh %d",p);
    i=system( buffer );
    FILE *fr;
    fr = fopen("topresult.txt", "rt");  
    char line[80];
    fgets(line, 80, fr);

//    printf("line=%s\n",line);
    sscanf(line, "%d", &g);
//    printf("g=%d\n",g);

    fclose(fr);
    remove("topresult.txt");
    return float(g)/10;      
//    printf("i=%d\n",i);
//    j=float(i)/10;    printf("j=%f\n",j);

  }

}
