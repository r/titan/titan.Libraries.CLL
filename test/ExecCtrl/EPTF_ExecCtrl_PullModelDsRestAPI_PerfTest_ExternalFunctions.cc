///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <TTCN3.hh>
#include <netinet/in.h> // htons
//#include <arpa/inet.h> // inet_ntoa
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>

#include "EPTF_ExecCtrl_PullModelDsRestAPI_PerfTest_Functions.hh"

  CHARSTRING EPTF__ExecCtrl__PullModelDsRestAPI__PerfTest__Functions::ef__EPTF__DsRestAPI__Test__getJSON(
  const CHARSTRING& pl__fileContent
  ){

  CHARSTRING retVal="";
  int len= pl__fileContent.lengthof();
  if(len == 0) return retVal;

  const char* str=(const char*)pl__fileContent;
  CHARSTRING result("");
  int pos=0;
  int pos1=0; // pos of open bracket
  int numOfBracket=0;

  while(str[pos] != '{' && pos < len) pos++; // anything until open bracket "{"
  //pos1=pos;
  if (pos == len) {
    return result;
  }
  result += str[pos];
  numOfBracket++;
  pos++;
  while(numOfBracket != 0 && pos < len) { // go until the final end bracket "}"
    if(str[pos] == '{'){
      numOfBracket++;
    }
    if(str[pos] == '}'){
      numOfBracket--;
    }
    if(str[pos] != '\n') {
      result += str[pos];
    }
    pos++;
  }

  //return CHARSTRING (len-pos,str+pos1);
  return result;
}
