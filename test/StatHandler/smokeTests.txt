///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_StatHandler_Test_Testcases.tc_StatHandler_testAutoDisconnect
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_GUI
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_testMean
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_declareStat
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_mean
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_mean_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_globalAverage_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_min_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_intMin_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_max_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_intMax_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_sum_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_intSum_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_standardDev_moreClients
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_density_moreClients_realtime
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_density_moreClients_sampled
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_density_moreClients_sampledAtSync
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_density_moreClients_timeLine
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_percentile95_HK48363
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_min
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_mean
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_standardDev
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_globalAverage
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_density
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_percentile95
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerAggregatedStat_percentileP  
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_max
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_mean
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_standardDev
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_density
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_percentile95
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_registerMeasuredStat_percentileP 
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_useComponentName 
  
EPTF_StatHandler_Test_Testcases.tc_StatHandler_Test_dataSourceTest
EPTF_StatHandler_Test_Testcases.tc_StatHandler_DS_Neg_WrongResetComponent
EPTF_StatHandler_Test_Testcases.tc_StatHandler_DS_Neg_WrongResetAll
