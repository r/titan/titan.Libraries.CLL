#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

#endres=`top bd00.50n2|grep Cpu| tail -n1 | sed 's/.*Cpu(s): //g;s/%us,.*//g'`   // global cpu usage
#endres=$( echo "scale=0; $endres*10" | bc -q)
#endres=`echo $valtozo | sed 's/\.0*//g'`

#echo $1
#endres=`top bd00.50n1 | grep $1 ` // | awk '{print $9}'`
#endres=`top -b -n 1 -p $1 | sed -ne '8p' | awk '{print $9}'`


if [ "`uname -n`" == "ehubuux110" ]
then
endres=`ps -o 'pcpu' -p $1 | tail -1`
echo $endres > topresult.txt
exit
fi

endres=`ps -o '%cpu' $1 | tail -n1`

echo $endres
echo $endres > topresult.txt
exit

