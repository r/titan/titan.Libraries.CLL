///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Semaphore_Test_Functions
// 
//  Purpose:
//    This module contains the test functions of generic EPTF Semaphore.
// 
//  Module depends on:
//   <EPTF_CLL_Semaphore_Functions> 
//   <EPTF_Semaphore_Test_Definitions> 
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_Semaphore_Test_Functions {
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Semaphore_Functions all;
import from EPTF_Semaphore_Test_Definitions all;

//=========================================================================
// Altsteps
//=========================================================================
altstep as_EPTF_Semaphore_Test_lock() runs on EPTF_Semaphore_Test_CT {
  [] t_lock.timeout {
    f_EPTF_Semaphore_unlock(v_semaphoreIdx);
    t_update.start;
    repeat;
  }
  [] t_update.timeout {
    t_update.start;
    repeat;
  }
}

altstep as_EPTF_Semaphore_Test_multiLock() runs on EPTF_Semaphore_Test_CT {
  [] t_lock.timeout {
    f_EPTF_Semaphore_decreaseLock(v_semaphoreIdx);
    f_EPTF_Semaphore_decreaseLock(v_semaphoreIdx);
    f_EPTF_Semaphore_decreaseLock(v_semaphoreIdx);
    t_update.start;
    repeat;
  }
  [] t_update.timeout {
    t_update.start;
    repeat;
  }
}


altstep as_Semaphore_test_default() runs on EPTF_Semaphore_Test_CT {
  [] t_lock.timeout {
    // wait for unlock, that will not happen:
    if (not f_EPTF_Semaphore_waitForUnlock(v_semaphoreIdx2,1.0)) {
      setverdict(fail);
    }
    // unlock the other semaphore:
    f_EPTF_Semaphore_unlock(v_semaphoreIdx);
    log("Semaphore ",v_semaphoreIdx," unlocked.");
    repeat;
  }
}

}
