///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_Common_IndexArray_Test_Testcases
//
// Purpose:
//   This module contains testcases for testing IndexArray
//
// Module Parameters:
//
//  Module depends on:
// 
// Current Owner:
//    Bence Molnar (ebenmol)
//
// Last Review Date:
//    20089-09-07
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_Common_IndexArray_Test_Testcases {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Common_IndexArrayDefinitions all;
import from EPTF_CLL_Common_IndexArrayFunctions all;
import from EPTF_Common_IndexArray_Test_Definitions all;
import from EPTF_Common_IndexArray_Test_Functions all;

//=========================================================================
// Testcases
//=========================================================================
///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_getOrCreateFreeSlot
//
// Purpose: testing function f_EPTF_Common_IndexArray_getOrCreateFreeSlot
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_getOrCreateFreeSlot() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_freeSlot;
  var integer vl_idx;

  // indexArray is empty, returned free slot value should be 0
  vl_freeSlot := f_EPTF_Common_IndexArray_getOrCreateFreeSlot(vl_indexArray);
  log("vl_indexArray: ",vl_indexArray,", vl_freeSlot: ",vl_freeSlot);
  f_EPTF_Common_Test_setVerdictFromBool(vl_freeSlot == 0);

  // setting new values to 0th index
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // indexArray contains 1 element, returned free slot value should be 1
  vl_freeSlot := f_EPTF_Common_IndexArray_getOrCreateFreeSlot(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_freeSlot: ",vl_freeSlot);
  f_EPTF_Common_Test_setVerdictFromBool(vl_freeSlot == 1);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_getElement
//
// Purpose: testing function f_EPTF_Common_IndexArray_getElement
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_getElement() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_element;
  var integer vl_idx;

  // getting the 1st element, which is not exists
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // getting the 1st element
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10);

  // getting the -1st element, which is not exists
  vl_idx := -1;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);

  // getting the 2nd element, which is not exists
  vl_idx := 1;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_setElement
//
// Purpose: testing function f_EPTF_Common_IndexArray_setElement
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_setElement() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_element;
  var integer vl_idx;

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // getting the 1st element
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10);

  // setting new values to index -2 (non existent element)
  vl_idx := -2;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // the function throws an error message, check in the log!!!!!!
  
  // setting new values to index 1 (2nd element)
  vl_idx := 1;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,-2);
  // getting the 2nd element, it should be 1
  vl_idx := 1;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 1);

  // setting new values to index 9 (10th element)
  vl_idx := 9;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // getting the 10th element, it should be 1
  vl_idx := 9;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10);
  // getting the 9th element, it should be -1
  vl_idx := 8;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_setNewElement
//
// Purpose: testing function f_EPTF_Common_IndexArray_setNewElement
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_setNewElement() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_element;
  var integer vl_idx;
  var integer vl_newElement;

  // setting new values to index 0 (1st element)
  vl_newElement := f_EPTF_Common_IndexArray_setNewElement(vl_indexArray,10);
  // getting the 1st element
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element,", vl_newElement: ",vl_newElement);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10); //the content is the same
  f_EPTF_Common_Test_setVerdictFromBool(vl_newElement == vl_idx); // the index is the same

  // setting new values to index 1 (2nd element)
  vl_idx := 1;
  vl_newElement := f_EPTF_Common_IndexArray_setNewElement(vl_indexArray, -2);
  // getting the 2nd element, it should be 1
  vl_idx := 1;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 1); //the content is the same
  f_EPTF_Common_Test_setVerdictFromBool(vl_newElement == vl_idx); // the index is the same

  // setting new values to index 9 (10th element)
  vl_idx := 9;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // 2nd element is -1, it should change to 0
  vl_newElement := f_EPTF_Common_IndexArray_setNewElement(vl_indexArray, 0);
  // getting the 10th element, it should be 1
  vl_idx := 2;
  vl_element := f_EPTF_Common_IndexArray_getElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 0); //the content is the same
  f_EPTF_Common_Test_setVerdictFromBool(vl_newElement == vl_idx); // the index is the same
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_freeElement
//
// Purpose: testing function f_EPTF_Common_IndexArray_freeElement
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_freeElement() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_firstBusy;
  var integer vl_element;
  var integer vl_idx;

  // indexArray is empty
  // free the -1th index
  vl_idx := -1;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);
  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);
  // free the 1st index
  vl_idx := 1;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == -1);

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == -1);

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // setting new values to index 1 (2nd element)
  vl_idx := 1;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_element: ",vl_element);
  f_EPTF_Common_Test_setVerdictFromBool(vl_element == 10);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == 1);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_arrayIsEmpty
//
// Purpose: testing function f_EPTF_Common_IndexArray_arrayIsEmpty
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_arrayIsEmpty() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var boolean vl_isEmpty;
  var integer vl_element;
  var integer vl_idx;

  // indexArray is empty
  vl_isEmpty := f_EPTF_Common_IndexArray_arrayIsEmpty(vl_indexArray);
  log("vl_indexArray: ",vl_indexArray,", vl_isEmpty: ",vl_isEmpty);
  f_EPTF_Common_Test_setVerdictFromBool(vl_isEmpty);

  // setting new values to 0th index
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // indexArray is not empty
  vl_isEmpty := f_EPTF_Common_IndexArray_arrayIsEmpty(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_isEmpty: ",vl_isEmpty);
  f_EPTF_Common_Test_setVerdictFromBool(not vl_isEmpty);

  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  // indexArray is empty
  vl_isEmpty := f_EPTF_Common_IndexArray_arrayIsEmpty(vl_indexArray);
  log("vl_indexArray: ",vl_indexArray,", vl_isEmpty: ",vl_isEmpty);
  f_EPTF_Common_Test_setVerdictFromBool(vl_isEmpty);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_getFirstBusyIdx
//
// Purpose: testing function f_EPTF_Common_IndexArray_getFirstBusyIdx
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_getFirstBusyIdx() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_firstBusy;
  var integer vl_element;
  var integer vl_idx;

  // indexArray is empty
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log(", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == -1);

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == 0);

  // setting new values to index 1 (2nd element)
  vl_idx := 1;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,-2);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == 0);

  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == 1);

  // indexArray is empty
  vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  // setting new values to index 9 (10th element)
  vl_idx := 9;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == 9);

  // free the 9th index
  vl_idx := 9;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  // getting first busy index
  vl_firstBusy := f_EPTF_Common_IndexArray_getFirstBusyIdx(vl_indexArray);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_firstBusy: ",vl_firstBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_firstBusy == -1);
}

///////////////////////////////////////////////////////////
// Name: tc_common_indexArray_test_getNextBusyIdx
//
// Purpose: testing function f_EPTF_Common_IndexArray_getNextBusyIdx
///////////////////////////////////////////////////////////
testcase tc_common_indexArray_test_getNextBusyIdx() runs on EPTF_Common_Test_CT
{
  var EPTF_Common_IndexArray vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  var integer vl_nextBusy;
  var integer vl_element;
  var integer vl_idx;

  // indexArray is empty
  // getting next busy index
  vl_idx := 0;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log(", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);

  // setting new values to index 0 (1st element)
  vl_idx := 0;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray,vl_idx,10);
  // getting next busy index
  vl_idx := -1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);
  // getting next busy index
  vl_idx := 0;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);
  // getting next busy index
  vl_idx := 1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);

  // setting new values to index 1 (2nd element)
  vl_idx := 1;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,-2);
  // getting next busy index
  vl_idx := 1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);

  // free the 0th index
  vl_idx := 0;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  // getting next busy index
  vl_idx := 0;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == 1);
  // getting next busy index
  vl_idx := 1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);

  // indexArray is empty
  vl_indexArray := c_EPTF_emptyCommon_IndexArray;
  // setting new values to index 9 (10th element)
  vl_idx := 9;
  f_EPTF_Common_IndexArray_setElement(vl_indexArray, vl_idx,10);
  // getting next busy index
  vl_idx := 1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == 9);

  // free the 9th index
  vl_idx := 9;
  vl_element := f_EPTF_Common_IndexArray_freeElement(vl_indexArray, vl_idx);
  // getting next busy index
  vl_idx := 1;
  vl_nextBusy := f_EPTF_Common_IndexArray_getNextBusyIdx(vl_indexArray, vl_idx);
  log("vl_idx: ",vl_idx,", vl_indexArray: ",vl_indexArray,", vl_nextBusy: ",vl_nextBusy);
  f_EPTF_Common_Test_setVerdictFromBool(vl_nextBusy == -1);
}

//=========================================================================
// Control
//=========================================================================
control {
  execute(tc_common_indexArray_test_getOrCreateFreeSlot());
  execute(tc_common_indexArray_test_getElement());
  execute(tc_common_indexArray_test_setElement());
  execute(tc_common_indexArray_test_setNewElement());
  execute(tc_common_indexArray_test_freeElement());
  execute(tc_common_indexArray_test_arrayIsEmpty());
  execute(tc_common_indexArray_test_getFirstBusyIdx());
  execute(tc_common_indexArray_test_getNextBusyIdx());
}

}
