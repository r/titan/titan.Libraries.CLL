#!/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

sed -e '
/# Set these variables.../ a\
COMMONCPP_DIR= \
MLSIM_DIR = \

s/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include -I$(OSS_DIR)\/include -I$(SS7_HOME)\/include -DTCAP_CAA20118_R8 -DTCAP_CAA201694_R10 -DCP_R12A30 -DT_ADDRESS -DEINSS7_THREADSAFE -D__EXTENSIONS__ -D_REENTRANT -D_POSIX_PTHREAD_SEMANTICS -DEINSS7_ADDR_IN_RESP/g
s/COMPILER_FLAGS = -L/COMPILER_FLAGS = -L -w/g

/ARCHIVE_DIR = ./ {
a\

a\
# Directory for EINSS7
a\
SS7_HOME = /vobs/ttcn/TCC_Common/Other/EINSS7/19089-lzy2143405_1_R1C/EABss7019
a\

a\
# Directory for OSS
a\
OSS_DIR = /mnt/projects/TTCN/Tools/oss/ossasn1/solaris-2.4/latest
}

s/-lcrypto $($(PLATFORM)_LIBS)/-lcrypto $($(PLATFORM)_LIBS) \\/g

s/SOLARIS8_LIBS = -lsocket -lnsl/SOLARIS8_LIBS = -lsocket -lnsl -lxnet -lrt -ldl -lresolv -lkstat -lpthread /g

/-lcrypto $($(PLATFORM)_LIBS) \\/ {
a\
	$(OSS_DIR)/lib/libasn1code.a -L$(SS7_HOME)/lib -leinss7_n_r -lpthread -lposix4
}
' <$1 >$2
