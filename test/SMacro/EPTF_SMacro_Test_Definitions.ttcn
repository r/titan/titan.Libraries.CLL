///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_SMacro_Test_Definitions
//
// Purpose:
//   This module contains data types for testing EPTF SMacro.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_CLL_SMacro_Definitions>
//    <EPTF_CLL_Base_Definitions>
//
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_SMacro_Test_Definitions {
//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_SMacro_Definitions all;
import from EPTF_CLL_Base_Definitions all;

//=========================================================================
//Component Types
//=========================================================================
type component SMacro_Test_CT extends EPTF_SMacro_CT, EPTF_Base_CT {
  var charstring  v_macro_name1;
  var charstring  v_macro_name2;
  var charstring  v_macro_name3;
  var charstring  v_macro_name4;
  var charstring  v_macro_name5;
  var charstring  v_macro_name6;

  var charstring v_macro_value1;
  var charstring v_macro_value2;
  var charstring v_macro_value3;
  var charstring v_macro_value4;
  var charstring v_macro_value5;
  var charstring v_macro_value6;
}

type component SMacro_Test_InvalidMacroNameWithLoggingInitialized_CT extends SMacro_Test_CT {
  var boolean v_warningLogged := false;
}

} //module
