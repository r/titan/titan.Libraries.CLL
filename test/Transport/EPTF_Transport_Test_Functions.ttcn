///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Transport_Test_Functions
// 
//  Purpose:
//    This module contains the test functions of generic EPTF Transport.
// 
//  Module depends on:
//   <EPTF_Transport_Test_Definitions>
//   <EPTF_CLL_TransportIPL4_Definitions> 
//   <IPL4asp_Types>
//
// 
//  Current Owner:
//    Balazs Barcsik (EBALBAR)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////

module EPTF_Transport_Test_Functions
{
//=========================================================================
// Import part
//=========================================================================
import from EPTF_Transport_Test_Definitions all;
import from EPTF_CLL_TransportIPL4_Definitions all;
import from IPL4asp_Types all;
import from EPTF_CLL_Transport_CommonDefinitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from LANL2asp_Types all;
import from EPTF_CLL_TransportIPL2_Definitions all;
import from EPTF_CLL_TransportIPL2_Functions all;

//=========================================================================
// Functions
//=========================================================================
function f_DemoOutgoingMessageProcess() runs on RoutingDemo_CT
{
  log("Outgoing Message Process function called!");
}

function f_DemoIncomingMessageProcess() runs on RoutingDemo_CT
{
  log("Incoming Message Process function called!");
}

function f_DemoIPL4Receive() runs on IPL4Demo_CT 
{
  log("IPL4 Message received!");
  log("The received message is: ", v_EPTF_CommPort_IPL4_incomingMessage);
  log("The connid is: ",v_EPTF_CommPort_IPL4_incomingMessage.asp_RecvFrom.connId);
}
function f_EPTF_TransportIPL4_getIncomingMessage() runs on EPTF_TransportIPL4_CT return octetstring {
  return v_EPTF_CommPort_IPL4_incomingMessage.asp_RecvFrom.msg;
}
function f_DemoIPL4ReceiveEvent() runs on IPL4Demo_CT 
{
  log("IPL4 Event Message received!");
}

group Routing_Functions
{
  function f_EPTF_Routing_Test_processIncomingMessage() runs on EPTF_Transport_Test_CT 
  {
    if(v_Test_inCalled) {
      log("wrong initiation");
      setverdict(fail);
    } else {
      v_Test_inCalled := true;
    }
  }
  function f_EPTF_Routing_Test_processOutgoingMessage() runs on EPTF_Transport_Test_CT 
  {
    if(v_Test_outCalled) {
      log("wrong initiation");
      setverdict(fail);
    } else {
      v_Test_outCalled := true;
    }
  }
}
group MessageBufferManager_Functions
{
  
}
group TransportIPL4 {
  function f_EPTF_TransportIPL4_Test_MsgHandler1() runs on EPTF_Transport_Test_CT {
    log("Received Message");
    
    v_msgCount := v_msgCount + 1;
  }
  function f_EPTF_TransportIPL4_Test_EventHandler1() runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;
  }
  function f_EPTF_TransportIPL4_Test_MsgHandler2() runs on EPTF_Transport_Test_CT {
    log("Received Message");
    v_msgCount := v_msgCount + 1;
  }
  function f_EPTF_TransportIPL4_Test_EventHandler2() runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;
  }
  function f_EPTF_TransportIPL4_Test_getMsgLen(in octetstring stream,  inout ro_integer args) return integer {
    if  (args ==  {2,3}) {
      setverdict(pass);
    } else {
      log("Error: wrong given arguments: ", args, ", expected: {2,3}");
      setverdict(fail);
    }
    return 4;
  }  
  function f_EPTF_TransportIPL4_Test_send() runs on EPTF_Transport_Test_CT  {
    
    if  (v_setMsg ==  f_EPTF_TransportIPL4_getIncomingMessage()) {
      setverdict(pass);
    } else {
      log("Error: wrong received message: ", f_EPTF_TransportIPL4_getIncomingMessage(), ", expected: ", v_setMsg);
      setverdict(fail);
    }
  }
  
group DTE_Handling {
  function f_EPTF_CommPort_IPL4_Test_dteInEventHandler_send() runs on EPTF_Transport_Test_CT  {
    
    v_msgCount := v_msgCount + 1;
    if  (v_setMsg !=  f_EPTF_TransportIPL4_getIncomingMessage()) {
      setverdict(fail, "Error: wrong received message: ", f_EPTF_TransportIPL4_getIncomingMessage(), ", expected: ", v_setMsg);
    }
  }
  function f_EPTF_TransportIPL4_Test_dteInEventHandler() runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_CommPort_IPL4_Test_dteInEventHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_CommPort_IPL4_Event: Dynamic test case error occured during executing event handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }
  function f_EPTF_CommPort_IPL4_Test_dteInEventHandler_withLGenType_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_TransportIPL4_defaultEvent: Dynamic test case error occured during executing old event handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }
  function f_EPTF_CommPort_IPL4_Test_dteInMsgHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_CommPort_IPL4_Receive: Dynamic test case error occured during executing message handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }
  function f_EPTF_CommPort_IPL4_Test_dteInMsgHandler_withLGenType_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_TransportIPL4_defaultReceive: Dynamic test case error occured during executing old message handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }
  function f_EPTF_CommPort_IPL4_Test_dteInMsgHandler_send() runs on EPTF_Transport_Test_CT  {
    
    v_msgCount := v_msgCount + 1;
    if  (v_setMsg !=  f_EPTF_TransportIPL4_getIncomingMessage()) {
      setverdict(fail, "Error: wrong received message: ", f_EPTF_TransportIPL4_getIncomingMessage(), ", expected: ", v_setMsg);
    }
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }

  function f_EPTF_CommPort_IPL4_Test_dteInListenHandler() runs on EPTF_Transport_Test_CT  {
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_CommPort_IPL4_Test_dteInListenHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*f_EPTF_CommPort_IPL4_send: Dynamic test case error occured during executing listen handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }

  function f_EPTF_CommPort_IPL4_Test_dteInConnectHandler() runs on EPTF_Transport_Test_CT  {
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_CommPort_IPL4_Test_dteInConnectHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*f_EPTF_CommPort_IPL4_send: Dynamic test case error occured during executing connect handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }

  function f_EPTF_CommPort_IPL4_Test_dteInCloseHandler() runs on EPTF_Transport_Test_CT  {
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_CommPort_IPL4_Test_dteInCloseHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*f_EPTF_CommPort_IPL4_send: Dynamic test case error occured during executing close handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }

  function f_EPTF_Transport_Test_EventHandler_dteInEventHandler(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event)
  runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_TransportIPL4_Test_dteInEventHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_TransportIPL4_defaultEvent: Dynamic test case error occured during executing event handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }

  function f_EPTF_Transport_Test_MsgHandler_dteInMsgHandler(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in HostName pl_remHost,
    in PortNumber pl_remPort,
    in HostName pl_locHost,
    in PortNumber pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    v_msgCount := v_msgCount + 1;
    // generate DTE:
    var float vl_zero:=0.0;    
    vl_zero := vl_zero/vl_zero;
  }
  function f_EPTF_TransportIPL4_Test_dteInMsgHandler_checkWarning_DTE_preamble_FN(in charstring pl_message) runs on EPTF_Transport_Test_dte_CT {
    if (match(pl_message,pattern "*as_EPTF_TransportIPL4_defaultReceive: Dynamic test case error occured during executing message handler*")) {
      setverdict(pass,"Expected warning received");
      v_expectedWarningWasLogged := true;
    }
  }
}// group DTE_Handling
  
group RetransmissionAfterClose {

  function f_EPTF_Transport_Test_MsgHandler_RetransmissionAfterClose(  
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    action("Received Message on connection ", pl_connId);
    
    v_msgCount := v_msgCount + 1;
  }

}//group RetransmissionAfterClose
  
  function tc_EPTF_IPL4_SCTP_test2_msgHandler(
  in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
    ) runs on EPTF_Transport_Test_CT  {
    action("MSG_HANDLER");
    if  (v_EPTF_CommPort_IPL4_aspRecvFrom ==  v_expected_ASP_recvFrom) {
      setverdict(pass);
    } else {
      log("Error: wrong received message: ", v_EPTF_CommPort_IPL4_aspRecvFrom, ", expected: ", v_expected_ASP_recvFrom);
      setverdict(fail);
    }
  }  
  
  function f_EPTF_Transport_Test_MsgHandler1(  
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    log("Received Message");
    
    v_msgCount := v_msgCount + 1;
  }
  function f_EPTF_Transport_Test_EventHandler1(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event)
  runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;
    log("Received event, connId: ", pl_connId);
  }
  
group TemporarlyUnavailableCheck {
  function f_EPTF_Transport_Test_EventHandler_TemporarlyUnavailableCheck(in EPTF_Transport_TransportType pl_transportType, in ConnectionId pl_connId, in ASP_Event pl_event)
    runs on EPTF_Transport_Test_TemporarlyUnavailableCheck_CT {
    v_eventCounter := v_eventCounter + 1;
    log("Received event, connId: ", pl_connId," event: ", pl_event);
    if (ischosen(pl_event.result) and pl_event.result.errorCode == ERROR_AVAILABLE) {
      if (pl_connId == -1) {
        setverdict(fail, "Conn_id is invalid in ERROR_AVAILABLE handler: pl_connId = -1");
        f_EPTF_Base_stopAll(none);
      }
      v_errorAvailableTriggered := true;
    }
  }
  function f_EPTF_Transport_Test_MsgHandler_TemporarlyUnavailableCheck(in EPTF_Transport_TransportType pl_transportType, in integer pl_connId, in charstring pl_remHost, in integer pl_remPort,
    in charstring pl_locHost, in integer pl_locPort, in ProtoTuple pl_proto, in integer pl_userData, in octetstring pl_msg) runs on EPTF_Transport_Test_TemporarlyUnavailableCheck_CT {
    log("Received Message, connId: ", pl_connId, " msg: ", oct2int(pl_msg));
    v_msgCounter := v_msgCounter + 1;
  }
} // ~group TemporarlyUnavailableCheck
  
  function f_EPTF_Transport_Test_MsgHandler2(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    log("Received Message, connId: ", pl_connId, " msg: ", oct2int(pl_msg));
    v_msgCount := v_msgCount + 1;
  }
  function f_EPTF_Transport_Test_EventHandler2(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event) runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;    
  }
  // artf384340
  function f_EPTF_Transport_Test_MsgHandlerDelayed(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    log("Received Message, connId: ", pl_connId, " msg: ", oct2int(pl_msg));
    v_msgCount := v_msgCount + 1;
    var float vl_actRelTimeInSecs := f_EPTF_Base_getRelTimeInSecs();    
    v_countTimeDistanceRelTimeInSecsInReceive := v_countTimeDistanceRelTimeInSecsInReceive+(vl_actRelTimeInSecs-v_lastSendCallRelTimeInSecs);
  }
  function f_EPTF_Transport_Test_EventHandlerDelayed(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event) runs on EPTF_Transport_Test_CT {
    v_eventCount := v_eventCount + 1;    
  }

  function f_EPTF_Transport_Test_EventHandlerSaveLastCreatedConnId(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event) runs on EPTF_Transport_Test_CT {
    v_lastCreatedConnId := pl_connId;    
  }
  
  function f_EPTF_Transport_Test_getMsgLen(
    in EPTF_Transport_TransportType pl_transportType,
    in octetstring stream,  
    inout EPTF_IntegerList pl_args) 
  return integer {
    if  (pl_args ==  {2,3}) {
      setverdict(pass);
    } else {
      log("Error: wrong given arguments: ", pl_args, ", expected: {2,3}");
      setverdict(fail);
    }
    return 4;
  }
  
  function f_EPTF_Transport_Test2_getMsgLen(
    in EPTF_Transport_TransportType pl_transportType,
    in octetstring stream,  
    inout EPTF_IntegerList pl_args) 
  return integer {
    if  (pl_args ==  {2,3}) {
      setverdict(pass);
    } else {
      log("Error: wrong given arguments: ", pl_args, ", expected: {2,3}");
      setverdict(fail);
    }
    return 2;
  }


  
  function f_EPTF_Transport_Test_send(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT  {
    
    if  (v_setMsg ==  f_EPTF_TransportIPL4_getIncomingMessage()) {
      setverdict(pass);
    } else {
      log("Error: wrong received message: ", f_EPTF_TransportIPL4_getIncomingMessage(), ", expected: ", v_setMsg);
      setverdict(fail);
    }
  }
  
  function f_EPTF_Transport_Test_MsgHandler3(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    log("Received Message, connId: ", pl_connId, " msg: ", oct2int(pl_msg));
    v_msgCount := v_msgCount + 1;
    if  (pl_connId ==  oct2int(pl_msg)) {
      setverdict(pass);
    } else {
      log("Error: wrong connectionId while receiving message: ", pl_connId, ", expected: ", oct2int(pl_msg));
      setverdict(fail);
    }
    
  }
  
  function f_EPTF_Transport_Test_MsgHandler4(
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_CT {
    log("Received Message, connId: ", pl_connId, " msg: ", oct2int(pl_msg));
    v_msgCount := v_msgCount + 1;
    if  (pl_connId >=  oct2int(pl_msg)) {
      setverdict(pass);
    } else {
      log("Error: wrong connectionId while receiving message: ", pl_connId, ", expected: ", oct2int(pl_msg));
      setverdict(fail);
    }
    
  }
  
  
  function f_EPTF_Logging_Count_Test( in charstring pl_message
  ) runs on EPTF_Transport_Test_CT {
    if  (pl_message == "EPTF_Logging:Warning: Warning: Buffer is full, clearing buffer..."){
      setverdict(pass);
    }
  
  }
  
  function f_EPTF_Transport_Test_initCT2(in octetstring pl_expMsg)
  runs on EPTF_Transport_Test_CT2
  {
    f_EPTF_Base_init_CT("VLAN test CT");
    vd_EPTF_Transport_Test_defaultReceive := activate(as_EPTF_Transport_Test_defaultReceive(pl_expMsg));
    map(self:IPL2_PCO, system:IPL2_PCO);
    //TODO: complite the test: connect the IPL2 testport side with the transport side,
    //the transport side should send a message that contains VLAN tag(s),
    //at the testport side we have to check the received message
  }
  
  function f_EPTF_Transport_Test_handleIncomingPacket(octetstring   type_field,
  octetstring   payload, in octetstring pl_expMsg)
  runs on EPTF_Transport_Test_CT2
  {
    var verdicttype vl_verdict := none;
    action("Payload: "&log2str(payload));
    /*if(pl_expMsg == type_field&payload){
      vl_verdict := pass;    
    }
    else
    {
      vl_verdict := fail;
    }*/
    f_EPTF_Base_stopAll(vl_verdict);
  }
  
  private altstep as_EPTF_Transport_Test_defaultReceive (in octetstring pl_expMsg)
  runs on EPTF_Transport_Test_CT2
  {
    var ASP_LANL2 vl_message;
    /*var ASP_v2_LANL2 vl_message_v2;
    var ASP_LANL2_Error vl_error;
    var ASP_v2_LANL2_Error vl_error_v2;*/

    [] IPL2_PCO.receive (ASP_LANL2 : ?) -> value vl_message
    {
      f_EPTF_Transport_Test_handleIncomingPacket(vl_message.type_field, vl_message.payload, pl_expMsg);
      repeat;
    }

    /*[] IPL2_PCO.receive (ASP_v2_LANL2 : ?) -> value vl_message_v2
    {
      f_EPTF_TransportIPL2_handleIncomingPacket(vl_message_v2.interface_id, vl_message_v2.type_field, vl_message_v2.payload);
      repeat;
    }

    [] IPL2_PCO.receive (ASP_LANL2_Error : ?) -> value vl_error
    {
      if(c_EPTF_Common_debugSwitch and f_EPTF_TransportIPL2_debugEnabled()) {
        f_EPTF_TransportIPL2_debug(%definitionId&": Got error ASP from IPL2_PCO: " & log2str(vl_error));
      }
      repeat;
    }

    [] IPL2_PCO.receive (ASP_v2_LANL2_Error : ?) -> value vl_error_v2
    {
      if(c_EPTF_Common_debugSwitch and f_EPTF_TransportIPL2_debugEnabled()) {
        f_EPTF_TransportIPL2_debug(%definitionId&": Got error ASP from IPL2_PCO: " & log2str(vl_error_v2));
      }
      repeat;
    }

    [] IPL2_PCO.receive { repeat; }*/
  }
  
  function f_EPTF_IPL2_VLAN_senderBehavior()
  runs on EPTF_TransportIPL2_CT
  {
    var Result vl_result;
    var EPTF_TransportIPL2_VLAN_TCIs vl_vlanTCIs := {{PCP := 3, CFI := 0, VID := 4000}};
	f_EPTF_Transport_init(IPL2, "VLANsenderCT");
	f_EPTF_Transport_connect(IPL2, {udp:={}}, tsp_LocalHostIP, tsp_LocalHostPort, tsp_RemoteHostIP, tsp_RemoteHostPort, "Ltype", vl_result, false, vl_vlanTCIs);
	f_EPTF_Transport_send(IPL2, vl_result.connId, str2oct("1986"), vl_result);
  }  
}


group ReconnectDisabled {
  function f_EPTF_Transport_Test_reconnectDisabled_MsgHandler1(  
    in EPTF_Transport_TransportType pl_transportType,
    in integer pl_connId,
    in charstring pl_remHost,
    in integer pl_remPort,
    in charstring pl_locHost,
    in integer pl_locPort,
    in ProtoTuple pl_proto,
    in integer pl_userData,
    in octetstring pl_msg
  ) runs on EPTF_Transport_Test_ReconnectDisabled_CT {
    log("Received Message");
    
    v_msgCount := v_msgCount + 1;
  }
  function f_EPTF_Transport_Test_reconnectDisabled_EventHandler1(
    in EPTF_Transport_TransportType pl_transportType,
    in ConnectionId pl_connId,
    in ASP_Event pl_event)
  runs on EPTF_Transport_Test_ReconnectDisabled_CT {
    v_eventCount := v_eventCount + 1;
    log("Received event: ",pl_event,", connId: " ,pl_connId);
    
    if (ischosen(pl_event.connOpened)) {
      // server received an incoming client connection
      v_serverConnId := pl_event.connOpened.connId;
      v_nofClientCon_opens := v_nofClientCon_opens + 1;
      log("Incoming client connection opened on server");
      if (v_nofClientCon_opens>1 and (v_clientCommLostDetected or v_clientSCTPShutdownDetected)) {
        setverdict(fail,"Conn_open detected on server after COMM_LOST but client reconnection was not enabled")
      }
    } else if (ischosen(pl_event.sctpEvent)) {
      if (ischosen(pl_event.sctpEvent.sctpAssocChange)) {
        if (pl_event.sctpEvent.sctpAssocChange.sac_state == SCTP_COMM_UP) {
          if (v_serverConnId>=0 and pl_event.sctpEvent.sctpAssocChange.clientId != v_serverConnId) {
            // client connection is up:
            v_clientConnId := pl_event.sctpEvent.sctpAssocChange.clientId;
            log("Client connection is UP");
            v_nofClientCommUps := v_nofClientCommUps + 1;
          } else if (v_serverConnId>=0 and pl_event.sctpEvent.sctpAssocChange.clientId == v_serverConnId) {
            // server connection is up
            log("Server connection is UP");
          }
          
          if (v_nofClientCommUps>1 and (v_clientCommLostDetected or v_clientSCTPShutdownDetected)) {
            setverdict(fail,"COMM_UP detected after COMM_LOST but reconnection was not enabled")
          }
        } else if(pl_event.sctpEvent.sctpAssocChange.sac_state == SCTP_COMM_LOST) {
          // connection lost
          if (pl_event.sctpEvent.sctpAssocChange.clientId == v_serverConnId) {
            log("Server connection to the client is lost");
          } else if (pl_event.sctpEvent.sctpAssocChange.clientId == v_clientConnId) {
            log("Client connection to the server is lost");
          }
          
          // the com_lost should come with the client ID
          if (pl_event.sctpEvent.sctpAssocChange.clientId == v_clientConnId) {
            v_clientCommLostDetected := true;
          }
        }
      } else if (ischosen(pl_event.sctpEvent.sctpShutDownEvent)) {
        if (pl_event.sctpEvent.sctpShutDownEvent.clientId == v_serverConnId) {
          log("Server connection to the client is shut down");
        } else if (pl_event.sctpEvent.sctpShutDownEvent.clientId == v_clientConnId) {
          log("Client connection to the server is shut down");
          v_clientSCTPShutdownDetected := true;
        }
      }
    } else if (ischosen(pl_event.connClosed)) {
      
      if (pl_event.connClosed.connId == v_serverConnId) {
        log("Connection closed on server");
      } else if (pl_event.connClosed.connId == v_clientConnId) {
        log("Connection closed on client");
        v_clientConnClosedDetected := true;
      }
    }
  }
} //group ReconnectDisabled

}
