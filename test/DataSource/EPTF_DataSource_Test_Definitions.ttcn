///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_DataSource_Test_Definitions
//
//  Purpose:
//    This module provides definitions for testing DataSource
//
//  Module depends on:
//    -
//
//  Current Owner:
//    TitanSim Group
//
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    Provide definitions to test
//
//
///////////////////////////////////////////////////////////////
module EPTF_DataSource_Test_Definitions
{

import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_Logging_Definitions all;

const charstring c_EPTF_CLL_DataSource_Test_var_prefix := "GUIDataSourceVar_";

//TODO: ez itt nagyon nem stimmt. XML kene legyen, vagy ezt kell encodolni, hogy jo legyen.
// charstring, ELSZSKU-tol kerni mintat, azt kiegesziteni az adott esetnek megfeleloen

/*/
const charstring c_EPTF_CLL_DataSource_Test_XUL_nothingspecial := "
<window xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
        <tabbox id='Execution_Control.tabbox' layout='LISTCARD'>
            <tabs>
                <iteratordata element='LGenList' id='LGen' source='ExecCtrl'/>
                <tab id='Execution_Control.Clients.%LGen%.tab' label='%LGen%'/>
                <!-- <tab id='Execution_Control.EG.tab' label='Entity groups'/> -->
                <!-- <tab id='Execution_Control.LGens.tab' label='Clients'/> -->
            </tabs>
            <tabpanels>
                <iteratordata element='LGenList' id='LGen' source='ExecCtrl'/>
                <tabpanel orientation='vertical'>
                    <hbox id='Execution_Control.EG.data' orientation='vertical'>
                        <iterator element='EntityGroups' id='EGList' source='ExecCtrl'>
                            <label disabled='false' flex='0.000000' value='%EGList% count:'/>
                            <textbox flex='1.000000' id='EPTF_ExecCtrl.EG.%EGList%.count' multiline='false' readonly='true' value=''>
                                <externaldata element='EntityCount' source='ExecCtrl'>
                                    <params>
                                        <dataparam>%EGList%</dataparam>
                                    </params>
                                </externaldata>
                            </textbox>
                        </iterator>
                    </hbox>
                </tabpanel>
            </tabpanels>
        </tabbox>
    </hbox>
</window>
";
/*/
const charstring c_EPTF_CLL_DataSource_Test_XUL_nothingspecial :=
"<window xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>"&
"<!-- <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'> -->"&
"    <hbox id='EPTF_Main_hbox' orientation='vertical' flex='1'>"&
"        <tabbox id='Execution_Control.tabbox' layout='LISTCARD' flex='1'>"&
"            <tabs>"&
"                <tab id='Execution_Control.EG.tab' label='Entity groups'/>"&
"                <tab id='Execution_Control.LGens.tab' label='Clients'/>"&
"            </tabs>"&
"            <tabpanels>"&
"                <tabpanel orientation='vertical'>"&
"                    <hbox id='Execution_Control.EG.LIST.data' orientation='vertical' flex='1'>"&
"                        <iterator element='EntityGroups' id='EGList' source='ExecCtrl'>"&
"                            <label disabled='false' flex='0.000000' value='%EGList% count:'/>"&
"                            <textbox flex='1.000000' id='EPTF_ExecCtrl.EG.%EGList%.count' multiline='false' readonly='true' value=''>"&
"                                <externaldata element='EntityCount' source='ExecCtrl'>"&
"                                    <params>"&
"                                        <dataparam name='EntityGroup' value='%EGList%'/>"&
"                                    </params>"&
"                                </externaldata>"&
"                            </textbox>"&
"                        </iterator>"&
"                    </hbox>"&
"                </tabpanel>"&
"                <tabpanel orientation='vertical'>"&
"                    <tabbox id='Execution_Control.Clients.tabbox' layout='LISTCARD' flex='1'>"&
"                        <tabs>"&
"                            <iteratordata element='LGenList' id='LGen' source='ExecCtrl'/>"&
"                            <tab id='Execution_Control.Clients.%LGen%.tab' label='%LGen%'/>"&
"                        </tabs>"&
"                        <tabpanels>"&
"                            <iteratordata element='LGenList' id='LGen' source='ExecCtrl'/>"&
"                            <tabpanel orientation='vertical'>"&
"                                <tabbox id='Execution_Control.Clients.EGroups.%LGen%.tabbox' layout='LISTCARD' flex='1'>"&
"                                    <tabs>"&
"                                        <iteratordata element='EntityGroups' id='EGrp' ptcname='%LGen%' source='LGenBase'/>"&
"                                        <tab id='Execution_Control.Clients.%LGen%.%EGrp%.tab' label='%EGrp%'/>"&
"                                    </tabs>"&
"                                    <tabpanels>"&
"                                        <iteratordata element='EntityGroups' id='EGrp' ptcname='%LGen%' source='LGenBase'/>"&
"                                        <tabpanel orientation='vertical'>"&
"                                            <tabbox id='Execution_Control.Clients.%LGen%.%EGrp%.Scenarios.tabbox' layout='LISTCARD' flex='1'>"&
"                                                <tabs>"&
"                                                    <iteratordata element='Scenarios' id='Scenario' ptcname='%LGen%' source='LGenBase'>"&
"                                                        <params>"&
"                                                            <dataparam name='EntityGroup' value='%EGrp%'/>"&
"                                                        </params>"&
"                                                    </iteratordata>"&
"                                                    <tab id='Execution_Control.Clients.%LGen%.%EGrp%.%Scenario%.tab' label='%LGen%.%EGrp%.%Scenario%'/>"&
"                                                </tabs>"&
"                                                <tabpanels>"&
"                                                    <iteratordata element='Scenarios' id='Scenario' ptcname='%LGen%' source='LGenBase'>"&
"                                                        <params>"&
"                                                            <dataparam name='EntityGroup' value='%EGrp%'/>"&
"                                                        </params>"&
"                                                    </iteratordata>"&
"                                                    <tabpanel orientation='vertical'>"&
"                                                    </tabpanel>"&
"                                                </tabpanels>"&
"                                            </tabbox>"&
"                                        </tabpanel>"&
"                                    </tabpanels>"&
"                                </tabbox>"&
"                            </tabpanel>"&
"                        </tabpanels>"&
"                    </tabbox>"&
"                </tabpanel>"&
"            </tabpanels>"&
"        </tabbox>"&
"    </hbox>"&
"<!--</Widgets>-->"&
"</window>"
;
/*/
/*
tabpanelben legyen benne

                                                        <tree id='Execution_Control.Clients.%LGen%.%Scenario%.TCtree' rows='3.000000'>
                                                            <treecols>
                                                                <treecol editable='false' flex='1.000000' label='TC name' widgetType='string'/>
                                                                <treecol editable='false' flex='1.000000' label='Enabled' widgetType='checkBox'/>
                                                                <treecol editable='false' flex='1.000000' label='State' widgetType='string'/>
                                                            </treecols>
                                                            <treechildren>
                                                                <treeitem>
                                                                    <iteratordata element='TrafficCases' id='TC' ptcname='%LGen%' source='LGenBase'>
                                                                        <params>
                                                                            <dataparam>%EGrp%</dataparam>
                                                                            <dataparam>%Scenario%</dataparam>
                                                                        </params>
                                                                    </iteratordata>
                                                                    <treerow>
                                                                        <treecell id='%LGen%.%EG%.%Scenario%.%TC%.TCName' label='' tooltiptext=''>
                                                                            <externaldata element='TCName' ptcname='%LGen%' source='LGenBase'>
                                                                                <params>
                                                                                    <dataparam>%EG%</dataparam>
                                                                                    <dataparam>%Scenario%</dataparam>
                                                                                    <dataparam>%TC%</dataparam>
                                                                                </params>
                                                                            </externaldata>
                                                                        </treecell>
                                                                        <treecell id='%LGen%.%EG%.%Scenario%.%TC%.Enabled' label='false' tooltiptext=''>
                                                                            <externaldata element='TCEnabled' ptcname='%LGen%' source='LGenBase'>
                                                                                <params>
                                                                                    <dataparam>%EG%</dataparam>
                                                                                    <dataparam>%Scenario%</dataparam>
                                                                                    <dataparam>%TC%</dataparam>
                                                                                </params>
                                                                            </externaldata>
                                                                        </treecell>
                                                                        <treecell id='%LGen%.%EG%.%Scenario%.%TC%.StateName' label='Idle' tooltiptext=''>
                                                                            <externaldata element='TCStateName' ptcname='%LGen%' source='LGenBase'>
                                                                                <params>
                                                                                    <dataparam>%EG%</dataparam>
                                                                                    <dataparam>%Scenario%</dataparam>
                                                                                    <dataparam>%TC%</dataparam>
                                                                                </params>
                                                                            </externaldata>
                                                                        </treecell>
                                                                    </treerow>
                                                                </treeitem>
                                                            </treechildren>
                                                        </tree>


const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider1 := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider1_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider2 := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider2_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider3 := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider3_result := {xtdp_hbox := {}};

const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_notRegisteredProvider := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_notRegisteredProvider_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_notValidIterator := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_notValidIterator_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_twoIterators := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_twoIterators_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_notValidDataType := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_notValidDataType_result := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_twoDataTypes := {xtdp_hbox := {}};
const XTDP_XML_Tag c_EPTF_CLL_DataSource_Test_XUL_registeredProvider_twoDataTypes_result := {xtdp_hbox := {}};
*/
type component EPTF_CLL_DataSource_Client_CT extends EPTF_Base_CT, EPTF_Var_CT, EPTF_DataSourceClient_CT {
  var charstring v_Status :="";
}

type component EPTF_CLL_DataSource_Source_CT extends EPTF_DataSource_CT,EPTF_DataSourceClient_CT,EPTF_Var_CT {
  timer t_altwait := 1.0;
}

type component EPTF_CLL_DataSource_Test_Client_CT extends EPTF_CLL_DataSource_Client_CT {
  timer t_periodicUpdate := 1.2345;
}

//this component is used in NonBlocking testcases on DataSource Server
type component EPTF_CLL_DataSource_Test_NonBlocking_CT extends EPTF_CLL_DataSource_Source_CT, EPTF_CLL_Test_NonBlocking_CT {}

//this component is used in NonBlocking testcases on DataSource Client
type component EPTF_CLL_DataSourceClient_Test_NonBlocking_CT extends EPTF_CLL_DataSource_Test_Client_CT, EPTF_CLL_Test_NonBlocking_CT {}

type record of EPTF_Var_DirectContent EPTF_Var_DirectContentList;

//this component is used in NonBlocking testcases
type component EPTF_CLL_Test_NonBlocking_CT extends EPTF_Base_CT, EPTF_Logging_CT, EPTF_Var_CT {
  var EPTF_BooleanList              v_Test_NonBlocking_listCalled           := {};  //the element of this list is initialized in the testcase and the handler can check its state
  var EPTF_BooleanList              v_Test_NonBlocking_handlerCalled        := {};  //the element of this list is initialized in the testcase and the handler can check its state  
  var EPTF_Var_DirectContentList    v_Test_NonBlocking_expectedValueList    := {};  //the elements are the expected value of the test checked in the handler
  var EPTF_Var_DirectContentList    v_Test_NonBlocking_valueToSetList       := {};  //the elements are the values that are set to the variables during the test
  var EPTF_CharstringList           v_Test_NonBlocking_testDescriptionList  := {};  //the elements are the descriptions of the tests
  var EPTF_Var_CT                   v_Test_NonBlocking_clientRef;                   //reference to client DataSource used for test purposes
  var EPTF_CLL_DataSource_Test_NonBlocking_requestList v_Test_NonBlocking_ReqList := {};    //request params  
}

type union EPTF_CLL_DataSource_Test_NonBlocking_Handler {
  fcb_EPTF_DataSource_getDataHandler        dataHandler,
  fcb_EPTF_DataSource_getDataValueHandler   dataValueHandler
};

type record EPTF_CLL_DataSource_Test_NonBlocking_request {
    charstring source,
    charstring ptcName,
    charstring element,
    charstring method,
    EPTF_DataSource_Params params,
    EPTF_IntegerList indexList
};

type record of EPTF_CLL_DataSource_Test_NonBlocking_request EPTF_CLL_DataSource_Test_NonBlocking_requestList;

// indexes of userData array in handler functions
const integer c_Test_NonBlocking_UserData_Index_Positive := 0;
const integer c_Test_NonBlocking_UserData_Index_NonBlockingEffect := 1;
const integer c_Test_NonBlocking_UserData_Index_TestIndex := 2;

// these are used in nonBlocking testcases to indicate if the test behaves as blocking function
const integer c_Test_NonBlocking_Without_Effect := 0;
const integer c_Test_NonBlocking_With_NonBlocking_Effect := 1;

// these are used to indicate that the test is positive or negative.
const integer c_Test_NonBlocking_Negative := 0;
const integer c_Test_NonBlocking_Positive := 1;

//these constants indicates if the ret value should be ok or not
const EPTF_Var_DirectContent c_Test_NonBlocking_UserData_ExpectedValue_NOT_SET := {unknownVal := {omit}};
const integer c_Test_NonBlocking_UserData_ExpectedValue_builtIn := 3;
const integer c_Test_NonBlocking_UserData_ExpectedValue_2 := 4;
const integer c_Test_NonBlocking_UserData_ExpectedValue_UnexpectedError := 5;

//these constants indicate the handler function type
const integer c_Test_NonBlocking_Callable_Function_getCondition := 0;
const integer c_Test_NonBlocking_Callable_Function_getConditionClient := 1;
const integer c_Test_NonBlocking_Callable_Function_checkData := 2;
const integer c_Test_NonBlocking_Callable_Function_getDataValue := 3;
const integer c_Test_NonBlocking_Callable_Function_setDataValue := 4;

const EPTF_Var_DirectContent c_Test_NonBlocking_NOTHING_TO_SET := {unknownVal := {omit}};

//it is used to generate error on handler, when this parameter is in the list of params the handler will return errorcode
const charstring c_TestNonBlocking_BuiltIn_Error_paramName := "errorParam";

template charstring t_EPTF_CLL_DataSource_Test_CheckGUIVariable_Manual_XUL(charstring pl_mainWindowWidgetId,charstring pl_PTCName := "") :=
"
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
<window height='386.000000' id='"&pl_mainWindowWidgetId&"' orientation='vertical' title='TTCN constructed window' width='820.000000'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables in PTC "&pl_PTCName&"' />
          <textbox flex='1.000000' id='Var.AllVars.value' multiline='false' readonly='true' value=''>
              <externaldata element='VarValue' source='VarProvider' ptcname='"&pl_PTCName&"'>
                  <params>
                      <dataparam name='VarValue' value='allVar'></dataparam>
                  </params>
              </externaldata>
          </textbox>
          <iterator element='VarList' id='VarList' source='VarProvider' ptcname='"&pl_PTCName&"'>
              <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
              <textbox flex='1.000000' id='Var.%VarList%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider' ptcname='"&pl_PTCName&"'>
                      <params>
                          <dataparam name='VarValue' value='%VarList%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
          </iterator>
      </hbox>
    </hbox>
</window>
</Widgets>
";

// this is a good XUL. It contains two iterators with  different ID:
const charstring c_EPTF_CLL_DataSource_Test_CheckTwoIteratorsWithDifferentID_XUL := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
<window xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables' />
          <textbox flex='1.000000' id='Var.AllVars.value' multiline='false' readonly='true' value=''>
              <externaldata element='VarValue' source='VarProvider'>
                  <params>
                      <dataparam name='VarValue' value='allVar'></dataparam>
                  </params>
              </externaldata>
          </textbox>
          <iterator element='VarList' id='VarList' source='VarProvider'>
              <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
              <textbox flex='1.000000' id='Var.%VarList%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider'>
                      <params>
                          <dataparam name='VarValue' value='%VarList%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
          </iterator>
          <label disabled='false' flex='0.000000' value='Dummy %VarList%'/>
          <iterator element='VarList' id='VarList2' source='VarProvider'>
              <label disabled='false' flex='0.000000' value='Value of %VarList2% :'/>
              <textbox flex='1.000000' id='Var2.%VarList2%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider'>
                      <params>
                          <dataparam name='VarValue' value='%VarList2%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
          </iterator>
      </hbox>
    </hbox>
</window>
</Widgets>
";

// this is an erroneus XUL for negative testing. It contains two iterators with the same ID but not included in each other:
const charstring c_EPTF_CLL_DataSource_Test_CheckTwoIteratorsWithSameID_XUL := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
<window xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables' />
          <textbox flex='1.000000' id='Var.AllVars.value' multiline='false' readonly='true' value=''>
              <externaldata element='VarValue' source='VarProvider'>
                  <params>
                      <dataparam name='VarValue' value='allVar'></dataparam>
                  </params>
              </externaldata>
          </textbox>
          <iterator element='VarList' id='VarList' source='VarProvider'>
              <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
              <textbox flex='1.000000' id='Var.%VarList%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider'>
                      <params>
                          <dataparam name='VarValue' value='%VarList%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
          </iterator>
          <iterator element='VarList' id='VarList' source='VarProvider'>
              <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
              <textbox flex='1.000000' id='Var2.%VarList%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider'>
                      <params>
                          <dataparam name='VarValue' value='%VarList%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
          </iterator>
      </hbox>
    </hbox>
</window>
</Widgets>
";

// this is an erroneus XUL for negative testing. It contains two iterators with the same ID but not included in each other:
const charstring c_EPTF_CLL_DataSource_Test_CheckTwoIteratorsWithSameID_Inside_XUL := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
<window xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl' height='386.000000' id='EPTF_Main_Window' orientation='vertical' title='TTCN constructed window' width='820.000000'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables' />
          <textbox flex='1.000000' id='Var.AllVars.value' multiline='false' readonly='true' value=''>
              <externaldata element='VarValue' source='VarProvider'>
                  <params>
                      <dataparam name='VarValue' value='allVar'></dataparam>
                  </params>
              </externaldata>
          </textbox>
          <iterator element='VarList' id='VarList' source='VarProvider'>
              <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
              <textbox flex='1.000000' id='Var.%VarList%.value' multiline='false' readonly='true' value=''>
                  <externaldata element='VarValue' source='VarProvider'>
                      <params>
                          <dataparam name='VarValue' value='%VarList%'></dataparam>
                      </params>
                  </externaldata>
              </textbox>
              <iterator element='VarList' id='VarList' source='VarProvider'>
                  <label disabled='false' flex='0.000000' value='Value of %VarList% :'/>
                  <textbox flex='1.000000' id='Var2.%VarList%.value' multiline='false' readonly='true' value=''>
                      <externaldata element='VarValue' source='VarProvider'>
                          <params>
                              <dataparam name='VarValue' value='%VarList%'></dataparam>
                          </params>
                      </externaldata>
                  </textbox>
              </iterator>
          </iterator>
      </hbox>
    </hbox>
</window>
</Widgets>
";


const charstring c_EPTF_CLL_DataSource_Test_CheckDistributionChart_Manual_XUL := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables' />

          <distributionchart
            disabled='false' flex='1.000000'
            id='EPTF_ExecCtrl.Statistics.DistributionChart'
            title='Distribution Chart Wave'>

			<intervallimits id='ILimitsId' >
			  <value>0.0</value>
			  <value>1.0</value>
                          <value>2.0</value>
                          <value>3.0</value>
                          <value>4.0</value>
                          <value>5.0</value>
                          <value>6.0</value>
                          <value>7.0</value>
                          <value>8.0</value>
                          <value>9.0</value>
                          <value>10.0</value>
                          <value>11.0</value>
                          <value>12.0</value>
                          <value>13.0</value>
                          <value>14.0</value>
                          <value>15.0</value>
                          <value>16.0</value>
                          <value>17.0</value>
                          <value>18.0</value>
                          <value>19.0</value>
                          <value>20.0</value>
                          <value>21.0</value>
                          <value>22.0</value>
                          <value>23.0</value>
			</intervallimits>
			<valuelist color='red'
				id='EPTF_ExecCtrl.Statistics.DistributionChart.Value' legend='densityFreeTime'>
			  <externaldata element='Wave' source='CheckDistributionChart_Provider' />
			</valuelist>
          </distributionchart>
          <textbox flex='1.000000' id='DistChartData' multiline='false' readonly='true' value=''>
            <externaldata
               element='Wave' source='CheckDistributionChart_Provider'/>
          </textbox>

      </hbox>
    </hbox>
</Widgets>
";


const charstring c_EPTF_CLL_DataSource_Test_CheckChart_Manual_XUL := "
<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
    <hbox id='EPTF_Main_hbox' orientation='vertical'>
      <hbox id='Params' orientation='vertical'>
          <label disabled='false' flex='0.000000' value='Available variables' />

          <chart
            disabled='false' flex='1.000000'
            id='EPTF_ExecCtrl.Statistics.Chart'
            title='Chart Traces'>
             <trace color='blue' id='EPTF_ExecCtrl.Statistics.EG.DefaultEGrp.SC.DefaultSc.TC.DefaultTC1.CPSChart.targetCPS' maxPoints='2048' name='Target CPS' physicalUnitX='sec' physicalUnitY='call/sec'>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace1'></dataparam>
                      </params>
               </externaldata>
             </trace>
             <trace color='red' id='EPTF_ExecCtrl.Statistics.EG.DefaultEGrp.SC.DefaultSc.TC.DefaultTC1.CPSChart.currentCPS' maxPoints='2048' name='Current CPS' physicalUnitX='sec' physicalUnitY='call/sec'>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace2'></dataparam>
                      </params>
               </externaldata>
             </trace>
          </chart>
          <chart
            disabled='false' flex='1.000000'
            id='EPTF_ExecCtrl.Statistics.Chart2'
            title='Chart Traces'>
             <trace color='blue' id='EPTF_ExecCtrl.Statistics.EG.DefaultEGrp.SC.DefaultSc.TC.DefaultTC1.CPSChart.targetCPS2' maxPoints='2048' name='Target CPS' physicalUnitX='sec' physicalUnitY='call/sec'>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace1'></dataparam>
                      </params>
               </externaldata>
             </trace>
             <trace color='red' id='EPTF_ExecCtrl.Statistics.EG.DefaultEGrp.SC.DefaultSc.TC.DefaultTC1.CPSChart.currentCPS2' maxPoints='2048' name='Current CPS' physicalUnitX='sec' physicalUnitY='call/sec'>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace2'></dataparam>
                      </params>
               </externaldata>
             </trace>
          </chart>
          <textbox flex='1.000000' id='DistChartData.trace1' multiline='false' readonly='true' value=''>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace1'></dataparam>
                      </params>
               </externaldata>
          </textbox>
          <textbox flex='1.000000' id='DistChartData.trace2' multiline='false' readonly='true' value=''>
               <externaldata
                  element='ChartData' source='CheckChart_Provider'>
                      <params>
                          <dataparam name='TraceName' value='trace2'></dataparam>
                      </params>
               </externaldata>
          </textbox>

      </hbox>
    </hbox>
</Widgets>
";


}
