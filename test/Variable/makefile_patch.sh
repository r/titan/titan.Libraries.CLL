#!/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////



sed -e '
s/CPPFLAGS = .*/CPPFLAGS = -DEPTF_DEBUG -D$(PLATFORM) -I$(TTCN3_DIR)\/include -I$(OPENSSL_DIR)\/include -DNO_IPV6 -DLKSCTP_1_0_9 -DUSE_SCTP/g
s/CPPFLAGS_TTCN3 =/CPPFLAGS_TTCN3 = -DEPTF_DEBUG/g
s/COMPILER_FLAGS =/COMPILER_FLAGS = -w /g
s/SOLARIS8_LIBS = -lsocket -lnsl -lxml2/SOLARIS8_LIBS = -lsocket -lnsl -lxml2 -lresolv -lpcap/g

s/OPENSSL_DIR = $(TTCN3_DIR)/OPENSSL_DIR = \/mnt\/projects\/TTCN\/Tools\/openssl-0.9.8e/g

s/LINUX_LIBS =/LINUX_LIBS = -lpcap -lutil/g
s/SOLARIS_LIBS =/SOLARIS_LIBS = -lnsl -lxml2 -lresolv -lpcap/g
' <$1 >$2



#sed '
#s/SOLARIS_LIBS = -lsocket -lnsl -lxml2/SOLARIS_LIBS = -lsocket -lnsl -lxml2 -lresolv/g
#s/SOLARIS8_LIBS = -lsocket -lnsl -lxml2/SOLARIS8_LIBS = -lsocket -lnsl -lxml2 -lresolv/g
#' <$1 >$2
