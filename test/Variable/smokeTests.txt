///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
EPTF_Variable_Test_Testcases.tc_EPTF_Var_CreateAndModifyTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_AdjustTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_UnsubscribeTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_GuardfunctionTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_CalculationfunctionTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_PostprocfunctionTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_BufferedTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexAdjustTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexDoubleBufferedTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexParalelAdjustTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexParalelSubscribeTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexParalelBufferedTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_createVarFromCfg
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_loadVarFromCfg
EPTF_Variable_Test_Testcases.tc_EPTF_Var_getRemote
EPTF_Variable_Test_Testcases.tc_EPTF_Var_adjustRemote_blocking
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_chained
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_sampledAtSync_chained
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_realtime_chained
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_timelined_chained
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeTimeout
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_resubscribeTimeout

EPTF_Variable_Test_Testcases.tc_EPTF_Var_SubscribeTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_LongBufferedTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexSubscribeTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexBufferedTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_ComplexParalelUnsubscribeTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_SimpleHashMapTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_adjustRemote_blocking
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_directContent_EmptyOctetString
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_directContent_unknownVal
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkPeriodLength  
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange        
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_timelined    
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_sampledAtSync     
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeWithCustomPeriod_checkContentChange_realtime        
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subscribeRefTimeout
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subsCanAdjust_flagTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subsCanAdjust_adjustMainTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subsCanAdjust_adjustSubsTest
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_subsCanAdjust_callBack
EPTF_Variable_Test_Testcases.tc_EPTF_Var_Test_maxWaitTimeForByeAck
