///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
In order to use the project file, you have to define some variables
in the Eclipse workplace.
Select Window - Preferences - General - Workspace - Linked resources.
Than create the following new variables:
- WorkingDir that points to the path of the Working directory ,
  probably C:\Bin.
You have to set a valid working directory and makefile generationproperties also.
