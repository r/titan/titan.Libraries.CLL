///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "EPTF_DsRestAPI_Test_Functions.hh"
#include "EPTF_CLL_Common_Definitions.hh"
#include <dirent.h> 
#include <stdio.h> 

  EPTF__CLL__Common__Definitions::EPTF__CharstringList EPTF__DsRestAPI__Test__Functions::ef__EPTF__DsRestAPI__Test__getDirContent(
  const CHARSTRING& pl__pathOfDir
  ){
    DIR *dir;
    struct dirent *entry;
    EPTF__CLL__Common__Definitions::EPTF__CharstringList retVal;
    int i =0;
    
    dir = opendir (pl__pathOfDir);
    if (dir != NULL){
      while ((entry = readdir (dir)) != NULL){
        retVal[i++] = (entry->d_name);
      }
      closedir (dir);
    }
    else{
      perror ("Couldn't open the directory");
    }
    return retVal;
  }

  CHARSTRING EPTF__DsRestAPI__Test__Functions::ef__EPTF__DsRestAPI__Test__getJSON(
  const CHARSTRING& pl__fileContent
  ){

  CHARSTRING retVal="";
  int len= pl__fileContent.lengthof();
  if(len == 0) return retVal;

  const char* str=(const char*)pl__fileContent;
  CHARSTRING result("");
  int pos=0;
  int pos1=0; // pos of open bracket
  int numOfBracket=0;

  while(str[pos] != '{' && pos < len) pos++; // anything until open bracket "{"
  //pos1=pos;
  if (pos == len) {
    return result;
  }
  result += str[pos];
  numOfBracket++;
  pos++;
  while(numOfBracket != 0 && pos < len) { // go until the final end bracket "}"
    if(str[pos] == '{'){
      numOfBracket++;
    }
    if(str[pos] == '}'){
      numOfBracket--;
    }
    if(str[pos] != '\n') {
      result += str[pos];
    }
    pos++;
  }

  //return CHARSTRING (len-pos,str+pos1);
  return result;
}
