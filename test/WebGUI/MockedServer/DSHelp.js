///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
DSHelp = {
    "sources": [
        {
            "source": "DataSource",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "Sources",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "Source"
                        },
                        "description": "This iterator enlists the data sources that have been registered into the DataSource"
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "PTCs",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "PTC"
                                },
                                "description": "This data element returns the list of PTCs for a given data source",
                                "parameters": [
                                    {
                                        "name": "Source",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "Source"
                                            }
                                        },
                                        "description": "the data source. Should be one item in the list returned by 'Sources'",
                                        "exampleValue": "DataSource"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "dataElementPresent",
                                        "valueType": "boolType",
                                        "description": "This condition returns true if the specified dataElement is present",
                                        "parameters": [
                                            {
                                                "name": "Source",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Source"
                                                    }
                                                },
                                                "description": "the data source",
                                                "exampleValue": "DataSource"
                                            },
                                            {
                                                "name": "PTCName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "PTC"
                                                    }
                                                },
                                                "description": "the PTC name (optional)",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "Element",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the Element. This parameter is followed by an optional list of the parameterName-Value pairs",
                                                "exampleValue": "PTCs"
                                            },
                                            {
                                                "name": "ParamName",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the ParamName",
                                                "exampleValue": "Source"
                                            },
                                            {
                                                "name": "ParamValue",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the ParamValue",
                                                "exampleValue": "DataSource"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "sizeOf",
                                        "valueType": "intType",
                                        "description": "This dataelement returns the size of the iterator list or 1 for single elements",
                                        "parameters": [
                                            {
                                                "name": "Source",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Source"
                                                    }
                                                },
                                                "description": "the data source",
                                                "exampleValue": "DataSource"
                                            },
                                            {
                                                "name": "PTCName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "PTC"
                                                    }
                                                },
                                                "description": "the PTC name (optional)",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "Element",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the Element. This parameter is followed by an optional list of the parameterName-Value pairs",
                                                "exampleValue": "PTCs"
                                            },
                                            {
                                                "name": "ParamName",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the ParamName",
                                                "exampleValue": "Source"
                                            },
                                            {
                                                "name": "ParamValue",
                                                "typeDescriptor": {
                                                    "valueType": "charstringType"
                                                },
                                                "description": "the ParamValue",
                                                "exampleValue": "DataSource"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "!=",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument differs from the second",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "some string"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "some other string"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "<",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument is less than the second. The value of the parameters should be a float number.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "1.0"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "2.0e-2"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "<=",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument is less or equal than the second. The value of the parameters should be a float number.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "1.0"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "2.0e-2"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "==",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument is the same as of the second",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "some string"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "some other string"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": ">",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument is more than the second. The value of the parameters should be a float number.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "1.0"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "2.0e-2"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": ">=",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument is more or equal than the second. The value of the parameters should be a float number.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "1.0"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the second param to compare",
                                "exampleValue": "2.0e-2"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "and",
                        "valueType": "boolType",
                        "description": "This condition returns the logical 'and' of the arguments",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "boolType"
                                },
                                "description": "the first param. Type: booltype",
                                "exampleValue": "true"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "boolType"
                                },
                                "description": "the second param. Type: booltype",
                                "exampleValue": "false"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: \"JSON\", \"TEXT\". Default: \"JSON\"",
                                "exampleValue": "TEXT"
                            },
                            {
                                "name": "Source",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The source to select the help for. If not specified (or empty string \"\" is given) it means 'all available sources'",
                                "exampleValue": "DataSource"
                            },
                            {
                                "name": "Element",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The element to select the help for. If not specified (or empty string \"\" is given) it means 'all available elements'",
                                "exampleValue": ""
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "match",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument matches the second. The ttcn match function is called for the given arguments in order. The second parameter used as a pattern string for matching.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "some string"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the second param to compare. This parameter is used as a pattern string for matching",
                                "exampleValue": "*str*"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "not",
                        "valueType": "boolType",
                        "description": "This condition returns the logical 'not' of the argument",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "boolType"
                                },
                                "description": "the first param. Type: booltype",
                                "exampleValue": "false"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "not match",
                        "valueType": "boolType",
                        "description": "This condition tests if the value of the first argument does not match the second. The ttcn match function is called for the given arguments in order. The second parameter used as a pattern string for matching.",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the first param to compare",
                                "exampleValue": "some string"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the second param to compare. This parameter is used as a pattern string for matching",
                                "exampleValue": "*str*"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "or",
                        "valueType": "boolType",
                        "description": "This condition returns the logical 'or' of the arguments",
                        "parameters": [
                            {
                                "name": "Par1",
                                "typeDescriptor": {
                                    "valueType": "boolType"
                                },
                                "description": "the first param. Type: booltype",
                                "exampleValue": "true"
                            },
                            {
                                "name": "Par2",
                                "typeDescriptor": {
                                    "valueType": "boolType"
                                },
                                "description": "the second param. Type: booltype",
                                "exampleValue": "false"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "StatManager",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "LEDlimit",
                        "valueType": "statusLEDType",
                        "description": "This dataElement creates/returns a limits statistics for a given source and reference variable.",
                        "parameters": [
                            {
                                "name": "blackLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes black (optional)",
                                "exampleValue": "10.0"
                            },
                            {
                                "name": "blueLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes blue (optional)",
                                "exampleValue": "20.0"
                            },
                            {
                                "name": "redLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes red (optional)",
                                "exampleValue": "30.0"
                            },
                            {
                                "name": "yellowLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes yellow (optional)",
                                "exampleValue": "40.0"
                            },
                            {
                                "name": "greenLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes green (optional)",
                                "exampleValue": "50.0"
                            },
                            {
                                "name": "defaultColor",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "Specifies the default colour of the LED (colour below the lowest limit). Its value should be one of: \"blue\",\"black\",\"yellow\",\"green\" or \"red\". (optional, default: \"black\")",
                                "exampleValue": "green"
                            },
                            {
                                "name": "enableValueInLEDText",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "If set to \"yes\": enables value indication in the label of the statusLED. When enabled, the value of the data variable will appear in the LED text. Possible values: \"yes\", \"no\" (optional, default: \"no\")",
                                "exampleValue": "yes"
                            },
                            {
                                "name": "VarId",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "the name of the EPTF Variable that contains the source data for the limits statistics (its value should be float or integer type)",
                                "exampleValue": "varNameThatContainsFloatOrIntValue"
                            },
                            {
                                "name": "refVarId",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The name of the reference variable (optional). If specified the limits are considered as relative difference in percentile from the value of the reference variable. If not specified, the limits will be taken as absolute values. The reference variable should contain a float or integer type value.",
                                "exampleValue": "referenceVarNameThatContainsFloatOrIntValue"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "LEDlimitValue",
                        "valueType": "statusLEDType",
                        "description": "This dataElement creates/returns a limits statistics for a given source and reference value.",
                        "parameters": [
                            {
                                "name": "blackLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes black (optional)",
                                "exampleValue": "10.0"
                            },
                            {
                                "name": "blueLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes blue (optional)",
                                "exampleValue": "20.0"
                            },
                            {
                                "name": "redLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes red (optional)",
                                "exampleValue": "30.0"
                            },
                            {
                                "name": "yellowLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes yellow (optional)",
                                "exampleValue": "40.0"
                            },
                            {
                                "name": "greenLimit",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "Limiting value above which the LED colour becomes green (optional)",
                                "exampleValue": "50.0"
                            },
                            {
                                "name": "defaultColor",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "Specifies the default colour of the LED (colour below the lowest limit). Its value should be one of: \"blue\",\"black\",\"yellow\",\"green\" or \"red\". (optional, default: \"black\")",
                                "exampleValue": "green"
                            },
                            {
                                "name": "enableValueInLEDText",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "If set to \"yes\": enables value indication in the label of the statusLED. When enabled, the value of the data variable will appear in the LED text. Possible values: \"yes\", \"no\" (optional, default: \"no\")",
                                "exampleValue": "yes"
                            },
                            {
                                "name": "value",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "the value of the source data for the limits statistics (its value should be float or integer type)",
                                "exampleValue": "20.0"
                            },
                            {
                                "name": "refValue",
                                "typeDescriptor": {
                                    "valueType": "floatType"
                                },
                                "description": "The name of the reference value (optional). If specified the limits are considered as relative difference in percentile from the value of the reference value. If not specified, the limits will be taken as absolute values. The reference value should contain a float or integer type value.",
                                "exampleValue": "100.0"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "HostAdmin",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "CPUs",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "CPU"
                        },
                        "description": " This iterator lists the CPUs on a given host."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "CPULoad",
                                "valueType": "floatType",
                                "description": "This data element returns the CPULoad of a CPU",
                                "parameters": [
                                    {
                                        "name": "CPU_ID",
                                        "typeDescriptor": {
                                            "reference": {
                                                "isIndexInListOf": true,
                                                "typeName": "CPU"
                                            }
                                        },
                                        "description": "Cpu id. Valid values: 0..HostNumCPUs",
                                        "exampleValue": "HostNumCPUs"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "Processes",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "Process"
                        },
                        "description": "This iterator lists the processes on a given host."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "ProcCPULoad",
                                "valueType": "floatType",
                                "description": "This data element returns the CPU load of the process",
                                "parameters": [
                                    {
                                        "name": "ProcID",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "Process"
                                            }
                                        },
                                        "description": "process id",
                                        "exampleValue": "1"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ProcMEMUsage",
                                "valueType": "intType",
                                "description": "This data element returns the memory usage of the given process",
                                "parameters": [
                                    {
                                        "name": "ProcID",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "Process"
                                            }
                                        },
                                        "description": "process id",
                                        "exampleValue": "1"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ProcName",
                                "valueType": "charstringType",
                                "description": "This data element returns the name of the given process (=selfName)",
                                "parameters": [
                                    {
                                        "name": "ProcID",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "Process"
                                            }
                                        },
                                        "description": "process id",
                                        "exampleValue": "1"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "AvgCPULoad",
                        "valueType": "floatType",
                        "description": "This data element returns the average CPU load of the host, type float"
                    }
                },
                {
                    "dataElement": {
                        "name": "HostCPULoad",
                        "valueType": "floatType",
                        "description": "This data element returns the CPU load of the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "HostFreeMemory",
                        "valueType": "intType",
                        "description": "This data element returns the free memory on the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "HostName",
                        "valueType": "charstringType",
                        "description": "This data element returns the name of the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "HostNumCPUs",
                        "valueType": "intType",
                        "description": "This data element returns the number of CPUs on the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "HostPhysicalMemory",
                        "valueType": "intType",
                        "description": "This data element returns the physical memory available on the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "IPv4Addr",
                        "valueType": "charstringType",
                        "description": "This data element returns the IPv4 address of the host, type charsting"
                    }
                },
                {
                    "dataElement": {
                        "name": "IPv6Addr",
                        "valueType": "charstringType",
                        "description": "This data element returns the IPv6 address of the host, type charsting"
                    }
                },
                {
                    "dataElement": {
                        "name": "MaxCPULoad",
                        "valueType": "floatType",
                        "description": "This data element returns the maximal CPU load of the host, type float"
                    }
                },
                {
                    "dataElement": {
                        "name": "MinCPULoad",
                        "valueType": "floatType",
                        "description": "This data element returns the minimal CPU load of the host, type float"
                    }
                },
                {
                    "dataElement": {
                        "name": "ResetMinMaxAvgCPULoad",
                        "valueType": "intType",
                        "description": " This data element defines a button to reset the min/max/average CPU load of the host"
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "StatHandler",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "ResetAll",
                        "valueType": "intType",
                        "description": "This constant defines the data element for the ResetAll button"
                    }
                },
                {
                    "dataElement": {
                        "name": "ResetComponent",
                        "valueType": "intType",
                        "description": "This constant defines the data element for the ResetComponent button"
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "ExecCtrl",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "Clients",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "LGen"
                        },
                        "description": "This iterator enlists all the ExecCtrl clients (usually load generators)."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "EntityGroups",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "EntityGroup"
                                },
                                "description": "This iterator enlists the entity groups deployed onto the specified client PTC.",
                                "parameters": [
                                    {
                                        "name": "LGen",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "LGen"
                                            }
                                        },
                                        "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                        "exampleValue": "LGen01"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "EGrpOffsetForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the offset of the first entity of the entity group which is allocated on the given LGen.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "EGrpSizeForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the size of that part of the entity group which is allocated on the given LGen.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "EntityTypesForLGen",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "EntityType"
                                },
                                "description": "This iterator enlists the supported entity types of the load generator clients.",
                                "parameters": [
                                    {
                                        "name": "LGen",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "LGen"
                                            }
                                        },
                                        "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                        "exampleValue": "LGen01"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "NofCurrentAvailEntitiesForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the number of current available entities for an LGen for a given entity type.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityType",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityType"
                                                    }
                                                },
                                                "description": "Name of the entity type. Should be one item in the list returned by 'EntityTypesForLGen'",
                                                "exampleValue": "ExecCtrlDefaultETypeName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "NofMaxAvailEntitiesForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the number of max available entities for an LGen for a given entity type.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityType",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityType"
                                                    }
                                                },
                                                "description": "Name of the entity type. Should be one item in the list returned by 'EntityTypesForLGen'",
                                                "exampleValue": "ExecCtrlDefaultETypeName"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "EGrpsOnClients",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "EGrpOnClient"
                        },
                        "description": "This iterator enlists all the EntityGroups on all Clients."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "ClientOfEGrpOnClient",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "LGen"
                                },
                                "description": "This dataElement returns the client name for the EGrpOnClient item.",
                                "parameters": [
                                    {
                                        "name": "EGrpOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EGrpOnClient"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl EntityGroup and client load generator PTC. Should be one item in the list returned by 'EGrpsOnClients'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EntityGroupOfEGrpOnClient",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "EntityGroup"
                                },
                                "description": "This dataElement returns the entity group for the EGrpOnClient item.",
                                "parameters": [
                                    {
                                        "name": "EGrpOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EGrpOnClient"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl EntityGroup and client load generator PTC. Should be one item in the list returned by 'EGrpsOnClients'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "EntityGroups",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "EntityGroup"
                        },
                        "description": "This iterator enlists the entity groups of the whole configuration."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "Clients",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "LGen"
                                },
                                "description": "This iterator enlists the ExecCtrl clients (usually load generators) that the entity group is distributed on.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "EGrpOffsetForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the offset of the first entity of the entity group which is allocated on the given LGen.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "EGrpSizeForLGen",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the size of that part of the entity group which is allocated on the given LGen.",
                                        "parameters": [
                                            {
                                                "name": "LGen",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "LGen"
                                                    }
                                                },
                                                "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                "exampleValue": "LGen01"
                                            },
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "Scenarios",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "Scenario"
                                },
                                "description": "This iterator enlists the scenarios of an entity group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "Clients",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "LGen"
                                        },
                                        "description": "This iterator enlists all the ExecCtrl clients (usually load generators)."
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "ScStatusForLGen",
                                                "valueType": "statusLEDType",
                                                "description": "This dataElement returns the status of the scenario on the given LGen.",
                                                "parameters": [
                                                    {
                                                        "name": "LGen",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "LGen"
                                                            }
                                                        },
                                                        "description": "Name of the ExecCtrl client load generator PTC. Should be one item in the list returned by 'Clients'",
                                                        "exampleValue": "LGen01"
                                                    },
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                                {
                                    "dataElement": {
                                        "name": "Phases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "Phase"
                                        },
                                        "description": "This iterator enlists the phases of a scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "ScGrpScEnablePhase",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the enable phase checkbox for the given phase for the given scenario in a scenario group.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "Phase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Phase"
                                                            }
                                                        },
                                                        "description": "Name of the phase. Should be one item in the list returned by 'Phases'",
                                                        "exampleValue": "loadgen"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                                {
                                    "dataElement": {
                                        "name": "TrafficCases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "TrafficCase"
                                        },
                                        "description": "This iterator enlists the traffic cases of a scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "FsmStats",
                                                "valueType": "charstringlistType",
                                                "typeDescriptor": {
                                                    "isListOf": true,
                                                    "typeName": "FSMStatistic"
                                                },
                                                "description": "This iterator enlists the TC scope statistics of the FSMs of the given traffic cases.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            },
                                            "children": [
                                                {
                                                    "dataElement": {
                                                        "name": "FSMStat",
                                                        "valueType": "floatType",
                                                        "description": "This dataElement returns the value of the given FSM statistics of a TC.",
                                                        "parameters": [
                                                            {
                                                                "name": "EntityGroup",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "EntityGroup"
                                                                    }
                                                                },
                                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                                "exampleValue": "EGrp01"
                                                            },
                                                            {
                                                                "name": "Scenario",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "Scenario"
                                                                    }
                                                                },
                                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                                "exampleValue": "Scenario01"
                                                            },
                                                            {
                                                                "name": "TrafficCase",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "TrafficCase"
                                                                    }
                                                                },
                                                                "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                                "exampleValue": "TrafficCase01"
                                                            },
                                                            {
                                                                "name": "Statistic",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "FSMStatistic"
                                                                    }
                                                                },
                                                                "description": "Name of the FSM statistics. Should be one item in the list returned by 'FsmStats'",
                                                                "exampleValue": "NofStarts"
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStats",
                                                "valueType": "charstringlistType",
                                                "typeDescriptor": {
                                                    "isListOf": true,
                                                    "typeName": "Statistic"
                                                },
                                                "description": "This iterator enlists the TC scope statistics of the given traffic cases.",
                                                "parameters": []
                                            },
                                            "children": [
                                                {
                                                    "dataElement": {
                                                        "name": "TcStat",
                                                        "valueType": "floatType",
                                                        "description": "This dataElement returns the value of the given TC statistics.",
                                                        "parameters": [
                                                            {
                                                                "name": "EntityGroup",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "EntityGroup"
                                                                    }
                                                                },
                                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                                "exampleValue": "EGrp01"
                                                            },
                                                            {
                                                                "name": "Scenario",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "Scenario"
                                                                    }
                                                                },
                                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                                "exampleValue": "Scenario01"
                                                            },
                                                            {
                                                                "name": "TrafficCase",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "TrafficCase"
                                                                    }
                                                                },
                                                                "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                                "exampleValue": "TrafficCase01"
                                                            },
                                                            {
                                                                "name": "Statistic",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "Statistic"
                                                                    }
                                                                },
                                                                "description": "Name of the TC statistics. Should be one item in the list returned by 'TcStats'",
                                                                "exampleValue": "CurrentCPS"
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    "dataElement": {
                                                        "name": "TcStatDelta",
                                                        "valueType": "floatType",
                                                        "description": "This dataElement returns the value of the given TC statistics delta element.",
                                                        "parameters": [
                                                            {
                                                                "name": "EntityGroup",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "EntityGroup"
                                                                    }
                                                                },
                                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                                "exampleValue": "EGrp01"
                                                            },
                                                            {
                                                                "name": "Scenario",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "Scenario"
                                                                    }
                                                                },
                                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                                "exampleValue": "Scenario01"
                                                            },
                                                            {
                                                                "name": "TrafficCase",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "TrafficCase"
                                                                    }
                                                                },
                                                                "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                                "exampleValue": "TrafficCase01"
                                                            },
                                                            {
                                                                "name": "Statistic",
                                                                "typeDescriptor": {
                                                                    "reference": {
                                                                        "typeName": "Statistic"
                                                                    }
                                                                },
                                                                "description": "Name of the TC statistics. Should be one item in the list returned by 'TcStats'",
                                                                "exampleValue": "CurrentCPS"
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcExecTime",
                                                "valueType": "floatType",
                                                "description": "This dataElement returns the execution time of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcGroupFinishStatus",
                                                "valueType": "statusLEDType",
                                                "description": "This dataElement returns the traffic case group finish status LED.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcGrpFinishStatus",
                                                "valueType": "statusLEDType",
                                                "description": "This dataElement returns the traffic case group-finish status LED.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    },
                                                    {
                                                        "name": "Condition",
                                                        "typeDescriptor": {
                                                            "valueType": "charstringType"
                                                        },
                                                        "description": "Specifies a group finish condition of a traffic case. Valid values: \"FinTraffic\", \"RangeLoops\", \"Starts\", \"Success\", \"Fail\", \"Timeout\", \"Error\", \"ExecTime\"",
                                                        "exampleValue": "Starts"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcRegulator",
                                                "valueType": "charstringType",
                                                "description": "This dataElement returns the regulator used for the traffic case CPS.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcSingleShotEnableLog",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the enable log checkbox for the single shot for the given traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcSingleShotEntityIdx",
                                                "valueType": "intType",
                                                "description": "This dataElement returns the index of the entity for which the single shot traffic should be started for the given traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcSingleShotStart",
                                                "valueType": "intType",
                                                "description": "This dataElement returns the start single shot button for the given traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStart",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the start checkbox of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStartButton",
                                                "valueType": "intType",
                                                "description": "This dataElement returns the start button of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStartDelay",
                                                "valueType": "floatType",
                                                "description": "This dataElement returns the start delay of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStatus",
                                                "valueType": "statusLEDType",
                                                "description": "This dataElement returns the traffic case status LED.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStop",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the stop checkbox of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStopButton",
                                                "valueType": "intType",
                                                "description": "This dataElement returns the stop button of the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcTargetCPSOrWeight",
                                                "valueType": "floatType",
                                                "description": "This dataElement returns the target CPS for traffic case in non-weighted scenario and the traffic case weight for traffic case in weighted-scenario.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcTimeProfile",
                                                "valueType": "charstringType",
                                                "description": "This dataElement returns the time profile used for the traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case of an scenario. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                                {
                                    "dataElement": {
                                        "name": "CurrentCPS",
                                        "valueType": "floatType",
                                        "description": "This dataElement returns the value of the given Current CPS statistics for the scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "IsInScGroup",
                                        "valueType": "boolType",
                                        "description": "This condition returns true if the given scenario is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "IsWeightedSc",
                                        "valueType": "boolType",
                                        "description": "This condition returns true if the given scenario is weighted.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScCPS",
                                        "valueType": "floatType",
                                        "description": "This dataElement returns the scenario CPS of the weighted scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGroupOfSc",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the scenario group of the given scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpCurrentPhase",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the current phase of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpMode",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the scenario group mode of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpReset",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the scenario group reset checkbox of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpScStatus",
                                        "valueType": "statusLEDType",
                                        "description": "This dataElement returns the scenario group-status LED.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpStart",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the scenario group start checkbox of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpStatus",
                                        "valueType": "statusLEDType",
                                        "description": "This dataElement returns the scenario group status LED of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpTerminate",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the scenario group terminate checkbox of the scenario that is in a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScInstanceName",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the instance name of the scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScIsWeighted",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns \"Decl\" if the scenario is non-weighted, \"Weighted\" if the scenario is weighted.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScPhaseListName",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the name of the phase-list if the scenario belongs to a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScRegulatorName",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the name of regulator which regulates the target CPS  of the weighted scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScReset",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the scenario reset button.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScScGrpName",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the name of the scenario group to which the scenario belongs to.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStart",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the start checkbox of the scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStartButton",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the scenario start button.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStatus",
                                        "valueType": "statusLEDType",
                                        "description": "This dataElement returns the status of the scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStop",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the stop checkbox of the scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStopButton",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the scenario stop button.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScTargetCPS",
                                        "valueType": "floatType",
                                        "description": "This dataElement returns the target CPS of the weighted scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "TcRegulator",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the regulator used for the weighted scenario CPS.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario of an entity group. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "EGrpLGenPool",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the name of the LGen pool on which the entity group is distributed on.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EGrpOffset",
                                "valueType": "intType",
                                "description": "This dataElement returns the offset of the first entity in the entity group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EGrpSize",
                                "valueType": "intType",
                                "description": "This dataElement returns the size of an Entity Group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EGrpType",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the Entity Type of an Entity Group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "NofActiveEntities",
                                "valueType": "intType",
                                "description": "This dataElement returns number active entities of the given entity group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "NofDeployedLGens",
                                "valueType": "charstringType",
                                "description": "This dataElement returns number of LGens the given entity group is distributed on.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "EntityTypesForLGenOnClients",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "EntityTypeForLGenOnClient"
                        },
                        "description": "This iterator enlists all the EntityTypes on all Clients."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "ClientOfEntityTypesForLGenOnClients",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "LGen"
                                },
                                "description": "This dataElement returns the client name for the EntityTypesForLGenOnClients item.",
                                "parameters": [
                                    {
                                        "name": "EntityTypeForLGenOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityTypeForLGenOnClient"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl entity type and client load generator PTC. Should be one item in the list returned by 'EntityTypesForLGenOnClients'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EntityTypeOfEntityTypesForLGenOnClients",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "EntityType"
                                },
                                "description": "This dataElement returns the entity type for the EntityTypesForLGenOnClients item.",
                                "parameters": [
                                    {
                                        "name": "EntityTypeForLGenOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityTypeForLGenOnClient"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl entity type and client load generator PTC. Should be one item in the list returned by 'EntityTypesForLGenOnClients'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "GlobalStats",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "GlobalStatistic"
                        },
                        "description": "This iterator enlists the global scope statistic."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "GlobalStat",
                                "valueType": "floatType",
                                "description": "This dataElement returns the value of the given global statistics.",
                                "parameters": [
                                    {
                                        "name": "Statistic",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "GlobalStatistic"
                                            }
                                        },
                                        "description": "Name of the global statistics. Should be one item in the list returned by 'GlobalStats'",
                                        "exampleValue": "Success"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "PhaseLists",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "PhaseList"
                        },
                        "description": "This iterator enlists the phaselists."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "Phases",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "Phase"
                                },
                                "description": "This iterator enlists the phases of phaselist.",
                                "parameters": [
                                    {
                                        "name": "PhaseList",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "PhaseList"
                                            }
                                        },
                                        "description": "Name of the phaselist. Should be one item in the list returned by 'PhaseLists'",
                                        "exampleValue": "BasicPhases"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "PhaseListEnablePhase",
                                        "valueType": "boolType",
                                        "description": "This dataElement returns the enable phase checkbox for the given Phase in given PhaseList.",
                                        "parameters": [
                                            {
                                                "name": "PhaseList",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "PhaseList"
                                                    }
                                                },
                                                "description": "Name of the phaselist. Should be one item in the list returned by 'PhaseLists'",
                                                "exampleValue": "BasicPhases"
                                            },
                                            {
                                                "name": "Phase",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Phase"
                                                    }
                                                },
                                                "description": "Name of the phase. Should be one item in the list returned by 'Phases'",
                                                "exampleValue": "loadgen"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "ScenarioGroups",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "ScenarioGroup"
                                },
                                "description": "This iterator enlists the scenario groups of a configuration for a given phase list.",
                                "parameters": [
                                    {
                                        "name": "PhaseList",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "PhaseList"
                                            }
                                        },
                                        "description": "Name of the phaselist. Should be one item in the list returned by 'PhaseLists'",
                                        "exampleValue": "BasicPhases"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "Phases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "Phase"
                                        },
                                        "description": "This iterator enlists the phases of a scenario group.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioGroup"
                                                    }
                                                },
                                                "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                                "exampleValue": "ScGrp"
                                            },
                                            {
                                                "name": "PhaseList",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "PhaseList"
                                                    }
                                                },
                                                "description": "Name of the phaselist. Should be one item in the list returned by 'PhaseLists'",
                                                "exampleValue": "BasicPhases"
                                            }
                                        ]
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "ScGrpEnablePhase",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the enable phase checkbox for the given scenario group.",
                                                "parameters": [
                                                    {
                                                        "name": "ScenarioGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "ScenarioGroup"
                                                            }
                                                        },
                                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                                        "exampleValue": "ScGrp"
                                                    },
                                                    {
                                                        "name": "Phase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Phase"
                                                            }
                                                        },
                                                        "description": "Name of the phase. Should be one item in the list returned by 'Phases'",
                                                        "exampleValue": "loadgen"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "ScenariosOfScenarioGroupsWithPhaseList",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "ScenarioOfScenarioGroup"
                                },
                                "description": "This iterator enlists all Scenarios of the ScenarioGroups with the given PhaseList.",
                                "parameters": [
                                    {
                                        "name": "PhaseList",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "PhaseList"
                                            }
                                        },
                                        "description": "Name of the phaselist. Should be one item in the list returned by 'PhaseLists'",
                                        "exampleValue": "BasicPhases"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "ScenarioGroupOfScenariosOfScenarioGroupsWithPhaseList",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "ScenarioGroup"
                                        },
                                        "description": "This dataElement returns the ScenarioGroup for the ScenariosOfScenarioGroupsWithPhaseList item.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioOfScenarioGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioOfScenarioGroup"
                                                    }
                                                },
                                                "description": "A pair of ExecCtrl ScenarioInstance and ScenarioGroup. Should be one item in the list returned by 'ScenariosOfScenarioGroupsWithPhaseList'",
                                                "exampleValue": "0.0"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScenarioOfScenariosOfScenarioGroupsWithPhaseList",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "ScenarioInstance"
                                        },
                                        "description": "This dataElement returns the ScenarioInstance for the ScenariosOfScenarioGroupsWithPhaseList item.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioOfScenarioGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioOfScenarioGroup"
                                                    }
                                                },
                                                "description": "A pair of ExecCtrl ScenarioInstance and ScenarioGroup. Should be one item in the list returned by 'ScenariosOfScenarioGroupsWithPhaseList'",
                                                "exampleValue": "0.0"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "Phases",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "Phase"
                        },
                        "description": "This iterator enlists the phases."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "TrafficCasesOfScenarios",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "TrafficCaseOfScenario"
                                },
                                "description": "This iterator enlists all the TrafficCases of all Scenarios."
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "ScenarioOfTrafficCasesOfScenarios",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "ScenarioInstance"
                                        },
                                        "description": "This dataElement returns the ScenarioInstance for the TrafficCasesOfScenarios item.",
                                        "parameters": [
                                            {
                                                "name": "TrafficCaseOfScenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "TrafficCaseOfScenario"
                                                    }
                                                },
                                                "description": "A pair of ExecCtrl ScenarioInstance and TrafficCase. Should be one item in the list returned by 'TrafficCasesOfScenarios'",
                                                "exampleValue": "0.0"
                                            }
                                        ]
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "ScGrpScEnablePhase",
                                                "valueType": "boolType",
                                                "description": "This dataElement returns the enable phase checkbox for the given phase for the given scenario instance in a scenario group.",
                                                "parameters": [
                                                    {
                                                        "name": "ScenarioInstance",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "ScenarioInstance"
                                                            }
                                                        },
                                                        "description": "Name of the scenario instance.",
                                                        "exampleValue": "EGrpName.ScName"
                                                    },
                                                    {
                                                        "name": "Phase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Phase"
                                                            }
                                                        },
                                                        "description": "Name of the phase. Should be one item in the list returned by 'Phases'",
                                                        "exampleValue": "loadgen"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "RegulatedItems",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "RegulatedItemId"
                        },
                        "description": "This iterator enlists the regulated items in the configuration."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "RegulatedItemEnabled",
                                "valueType": "boolType",
                                "description": "This dataElement returns the checkbox to enable the regulation of the regulated item.",
                                "parameters": [
                                    {
                                        "name": "RegulatedItemId",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatedItemId"
                                            }
                                        },
                                        "description": "Id of the regulated item. Should be one item in the list returned by 'RegulatedItems'",
                                        "exampleValue": "CpsToReach"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatedItemName",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the regulated item name of the regulated item.",
                                "parameters": [
                                    {
                                        "name": "RegulatedItemId",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatedItemId"
                                            }
                                        },
                                        "description": "Id of the regulated item. Should be one item in the list returned by 'RegulatedItems'",
                                        "exampleValue": "CpsToReach"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatedItemType",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the regulated item type (\"Scenario CPS\" or \"TrafficCase CPS\")for the regulated item.",
                                "parameters": [
                                    {
                                        "name": "RegulatedItemId",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatedItemId"
                                            }
                                        },
                                        "description": "Id of the regulated item. Should be one item in the list returned by 'RegulatedItems'",
                                        "exampleValue": "CpsToReach"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatedItemWeight",
                                "valueType": "floatType",
                                "description": "This dataElement returns the weight of the regulated item.",
                                "parameters": [
                                    {
                                        "name": "RegulatedItemId",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatedItemId"
                                            }
                                        },
                                        "description": "Id of the regulated item. Should be one item in the list returned by 'RegulatedItems'",
                                        "exampleValue": "CpsToReach"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatorName",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the regulator name that regulates the regulated item.",
                                "parameters": [
                                    {
                                        "name": "RegulatedItemId",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatedItemId"
                                            }
                                        },
                                        "description": "Id of the regulated item. Should be one item in the list returned by 'RegulatedItems'",
                                        "exampleValue": "CpsToReach"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "Regulators",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "RegulatorName"
                        },
                        "description": "This iterator enlists the regulators appearing in the configuration."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "RegulatorCurrentLoad",
                                "valueType": "floatType",
                                "description": "This dataElement returns current load of the regulator.",
                                "parameters": [
                                    {
                                        "name": "RegulatorName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatorName"
                                            }
                                        },
                                        "description": "Name of the regulator. Should be one item in the list returned by 'Regulators'",
                                        "exampleValue": "LoadRegulator"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatorStatus",
                                "valueType": "statusLEDType",
                                "description": "This dataElement returns status LED of the regulator.",
                                "parameters": [
                                    {
                                        "name": "RegulatorName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatorName"
                                            }
                                        },
                                        "description": "Name of the regulator. Should be one item in the list returned by 'Regulators'",
                                        "exampleValue": "LoadRegulator"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatorTargetLoad",
                                "valueType": "floatType",
                                "description": "This dataElement returns target load of the regulator. The regulator adapts the total value so that the curret load reach this value.",
                                "parameters": [
                                    {
                                        "name": "RegulatorName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatorName"
                                            }
                                        },
                                        "description": "Name of the regulator. Should be one item in the list returned by 'Regulators'",
                                        "exampleValue": "LoadRegulator"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "RegulatorTotalValue",
                                "valueType": "floatType",
                                "description": "This dataElement returns total value of the regulator. This total value is devided among the regulated items regulated by the regulator according to their weights.",
                                "parameters": [
                                    {
                                        "name": "RegulatorName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "RegulatorName"
                                            }
                                        },
                                        "description": "Name of the regulator. Should be one item in the list returned by 'Regulators'",
                                        "exampleValue": "LoadRegulator"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "ScenarioGroups",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "ScenarioGroup"
                        },
                        "description": "This iterator enlists the scenario groups of a configuration."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "Scenarios",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "ScenarioInstance"
                                },
                                "description": "This iterator enlists the scenarios of a scenario group. Returns the scenario instance name",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "EntityGroupOfScenarioInstance",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "EntityGroup"
                                        },
                                        "description": "This dataElement returns the entity group name for the scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "NofDeployedLGens",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns number of LGens the given scenario instance is distributed on.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScGrpScStatus",
                                        "valueType": "statusLEDType",
                                        "description": "This dataElement returns the scenario group-status LED inside the scenario group.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioGroup"
                                                    }
                                                },
                                                "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                                "exampleValue": "ScGrp"
                                            },
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScenarioTypeNameOfScenarioInstance",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "Scenario"
                                        },
                                        "description": "This dataElement returns the scenario type name for the scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "TrafficCases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "TrafficCase"
                                        },
                                        "description": "This iterator enlists the traffic cases of a scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "ScGrpCurrentPhase",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the current phase of the scenario group.",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScGrpMode",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the scenario group mode.",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScGrpReset",
                                "valueType": "boolType",
                                "description": "This dataElement returns the scenario group reset checkbox.",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScGrpStart",
                                "valueType": "boolType",
                                "description": "This dataElement returns the scenario group start checkbox.",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScGrpStatus",
                                "valueType": "statusLEDType",
                                "description": "This dataElement returns the scenario group status LED.",
                                "parameters": [
                                    {
                                        "name": "ScenarioGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioGroup"
                                            }
                                        },
                                        "description": "Name of the scenario group. Should be one item in the list returned by 'ScenarioGroups'",
                                        "exampleValue": "ScGrp"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "ScenariosOnClients",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "ScenarioOnClient"
                        },
                        "description": "This iterator enlists all the Scenarios on all EntityGroups of all Clients."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "ClientOfScenariosOnClients",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "LGen"
                                },
                                "description": "This dataElement returns the client name for the ScenariosOnClients item.",
                                "parameters": [
                                    {
                                        "name": "ScenarioOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioOnClient"
                                            }
                                        },
                                        "description": "A triple of ExecCtrl Scenario, EntityGroup and client load generator PTC. Should be one item in the list returned by 'ScenariosOnClients'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "EntityGroupOfScenariosOnClients",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "EntityGroup"
                                },
                                "description": "This dataElement returns the EntityGroup for the ScenariosOnClients item.",
                                "parameters": [
                                    {
                                        "name": "ScenarioOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioOnClient"
                                            }
                                        },
                                        "description": "A triple of ExecCtrl Scenario, EntityGroup and client load generator PTC. Should be one item in the list returned by 'ScenariosOnClients'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScenarioOfScenariosOnClients",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "Scenario"
                                },
                                "description": "This dataElement returns the Scenario for the ScenariosOnClients item.",
                                "parameters": [
                                    {
                                        "name": "ScenarioOnClient",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ScenarioOnClient"
                                            }
                                        },
                                        "description": "A triple of ExecCtrl Scenario, EntityGroup and client load generator PTC. Should be one item in the list returned by 'ScenariosOnClients'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "TrafficCasesOfScenarios",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "TrafficCaseOfScenario"
                        },
                        "description": "This iterator enlists all the TrafficCases of all Scenarios."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "ScenarioOfTrafficCasesOfScenarios",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "ScenarioInstance"
                                },
                                "description": "This dataElement returns the ScenarioInstance for the TrafficCasesOfScenarios item.",
                                "parameters": [
                                    {
                                        "name": "TrafficCaseOfScenario",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "TrafficCaseOfScenario"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl ScenarioInstance and TrafficCase. Should be one item in the list returned by 'TrafficCasesOfScenarios'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "EntityGroupOfScenarioInstance",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "EntityGroup"
                                        },
                                        "description": "This dataElement returns the entity group name for the scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "NofDeployedLGens",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns number of LGens the given scenario instance is distributed on.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScenarioTypeNameOfScenarioInstance",
                                        "valueType": "charstringType",
                                        "typeDescriptor": {
                                            "typeName": "Scenario"
                                        },
                                        "description": "This dataElement returns the scenario type name for the scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "TrafficCases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "TrafficCase"
                                        },
                                        "description": "This iterator enlists the traffic cases of a scenario instance.",
                                        "parameters": [
                                            {
                                                "name": "ScenarioInstance",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ScenarioInstance"
                                                    }
                                                },
                                                "description": "Name of the scenario instance.",
                                                "exampleValue": "EGrpName.ScName"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "TrafficCaseOfTrafficCasesOfScenarios",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "TrafficCase"
                                },
                                "description": "This dataElement returns the TrafficCase for the TrafficCasesOfScenarios item.",
                                "parameters": [
                                    {
                                        "name": "TrafficCaseOfScenario",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "TrafficCaseOfScenario"
                                            }
                                        },
                                        "description": "A pair of ExecCtrl ScenarioInstance and TrafficCase. Should be one item in the list returned by 'TrafficCasesOfScenarios'",
                                        "exampleValue": "0.0"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "ConfigFile",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the ConfigFile of the application."
                    }
                },
                {
                    "dataElement": {
                        "name": "EndOfConfig",
                        "valueType": "statusLEDType",
                        "description": "This dataElement returns the EndOfConfig variable. Green LED indicates when the configuration is finished."
                    }
                },
                {
                    "dataElement": {
                        "name": "EndOfTest",
                        "valueType": "statusLEDType",
                        "description": "This dataElement returns the EndOfTest variable. Green LED indicates when the Test is done, and the test report is generated."
                    }
                },
                {
                    "dataElement": {
                        "name": "Executable",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the Executable of the application."
                    }
                },
                {
                    "dataElement": {
                        "name": "Exit",
                        "valueType": "intType",
                        "description": "This dataElement returns the ExitButton variable. When its value is changed, the button is pressed."
                    }
                },
                {
                    "dataElement": {
                        "name": "GUIDone",
                        "valueType": "statusLEDType",
                        "description": "This dataElement returns the GUIDone variable. Green LED indicates when the GUI is ready."
                    }
                },
                {
                    "dataElement": {
                        "name": "GlobalProgressbar",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the Global ProgressBar variable. Shows the global progress as a percentage value."
                    }
                },
                {
                    "dataElement": {
                        "name": "Progressbar",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the ProgressBar variable. Shows the progress as a percentage value."
                    }
                },
                {
                    "dataElement": {
                        "name": "ProgressbarText",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the ProgressBar text variable. It contains a text of what is in progress."
                    }
                },
                {
                    "dataElement": {
                        "name": "ReadyToRun",
                        "valueType": "statusLEDType",
                        "description": "This dataElement returns the ReadyToRun variable. Green LED indicates when the traffic cases are ready to run."
                    }
                },
                {
                    "dataElement": {
                        "name": "Restart",
                        "valueType": "intType",
                        "description": "This dataElement restarts the application.",
                        "parameters": [
                            {
                                "name": "Executable",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "Name of the Executable.",
                                "exampleValue": "Executable"
                            },
                            {
                                "name": "ConfigFile",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "Name of the Configuration File",
                                "exampleValue": "ConfigFile.cfg"
                            },
                            {
                                "name": "TestCases",
                                "typeDescriptor": {
                                    "valueType": "charstringlistType"
                                },
                                "description": "Name of the TestCases separated by space",
                                "exampleValue": "Main_module.tc_1 Main_module.tc_2"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "Snapshot",
                        "valueType": "intType",
                        "description": "This dataElement returns the Snapshot test variable. The main SnapshotButton can be pressed by changing the value of this variable."
                    }
                },
                {
                    "dataElement": {
                        "name": "Start",
                        "valueType": "intType",
                        "description": "This dataElement returns the Start test variable. The main StartButton can be pressed by changing the value of this variable."
                    }
                },
                {
                    "dataElement": {
                        "name": "StartCommand",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the Start Command of the application."
                    }
                },
                {
                    "dataElement": {
                        "name": "Stop",
                        "valueType": "intType",
                        "description": "This dataElement returns the Stop test variable. The main StopButton can be pressed by changing the value of this variable."
                    }
                },
                {
                    "dataElement": {
                        "name": "TcStats",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "Statistic"
                        },
                        "description": "This iterator enlists the TC scope statistics of the given traffic cases.",
                        "parameters": []
                    }
                },
                {
                    "dataElement": {
                        "name": "Terminate",
                        "valueType": "intType",
                        "description": "This dataElement returns the Terminate test variable. The main TerminateButton can be pressed by changing the value of this variable."
                    }
                },
                {
                    "dataElement": {
                        "name": "TestCases",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the TestCases of the application."
                    }
                },
                {
                    "dataElement": {
                        "name": "TimeElapsed",
                        "valueType": "charstringType",
                        "description": "This dataElement returns the TimeElapsed variable. Its value shows the elapsed time since the main StartButton was pressed."
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "LGenBase",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "EntityGroups",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "EntityGroup"
                        },
                        "description": "This iterator returns the list of entity groups."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "Scenarios",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "Scenario"
                                },
                                "description": "This iterator returns the list of scenarios in the specified entity group.",
                                "parameters": [
                                    {
                                        "name": "EntityGroup",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "EntityGroup"
                                            }
                                        },
                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                        "exampleValue": "EGrp01"
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "TrafficCases",
                                        "valueType": "charstringlistType",
                                        "typeDescriptor": {
                                            "isListOf": true,
                                            "typeName": "TrafficCase"
                                        },
                                        "description": "This iterator returns the list of traffic cases in the specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    },
                                    "children": [
                                        {
                                            "dataElement": {
                                                "name": "CpsToReach",
                                                "valueType": "floatType",
                                                "description": "This data element can be used to set and get the target CPS value of the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "IsTcPresent",
                                                "valueType": "boolType",
                                                "description": "This condition returns true if specified traffic case exists, and returns false otherwise.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "LastCps",
                                                "valueType": "floatType",
                                                "description": "This data element returns the last (current) CPS of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "MaxBusyEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the highest number of busy entities during the specified traffic case execution.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "MaxRunningEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the highest number of running entities during the specified traffic case execution.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "MinAvailableEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the smallest number of available entities during the specified traffic case execution.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfAllEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of all entities.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfAvailableEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of available entities for the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfErrors",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of errors reported by the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfFails",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of failed traffic cases.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfNotFinishedEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of finished traffic cases.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfRunningEntities",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of running entities for the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfStarts",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of starts.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfSuccesses",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of succeeded traffic cases.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "NrOfTimeouts",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of timeouts reported by the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "RangeLoops",
                                                "valueType": "intType",
                                                "description": "This data element returns the range loops of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "ReceivedAnswers",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of received messages by the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "SentMessages",
                                                "valueType": "intType",
                                                "description": "This data element returns the number of sent messages by the specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TCName",
                                                "valueType": "charstringType",
                                                "description": "This data element returns the name specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcAbortButton",
                                                "valueType": "boolType",
                                                "description": "This data element returns the abort button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcEnabled",
                                                "valueType": "boolType",
                                                "description": "This data element returns true if traffic case is enabled, and false otherwise.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcEnabledAtStartup",
                                                "valueType": "boolType",
                                                "description": "This data element returns true if traffic case is enabled at start up, and returns false otherwise.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcPauseButton",
                                                "valueType": "boolType",
                                                "description": "This data element returns the pause button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcRestoreButton",
                                                "valueType": "boolType",
                                                "description": "This data element returns the abort button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcSingleShotBtn",
                                                "valueType": "boolType",
                                                "description": "This data element returns the single shot button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStartButton",
                                                "valueType": "boolType",
                                                "description": "This data element returns the start button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcState",
                                                "valueType": "intType",
                                                "description": "This data element returns the state of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStateName",
                                                "valueType": "charstringType",
                                                "description": "This data element returns the state name of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcStopButton",
                                                "valueType": "boolType",
                                                "description": "This data element returns the stop button (checkbox) of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TcUserData",
                                                "valueType": "charstringType",
                                                "description": "This data element returns the user data linked to specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "TrafficType",
                                                "valueType": "charstringType",
                                                "description": "This data element returns the traffic type of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "UniqueName",
                                                "valueType": "charstringType",
                                                "description": "This data element returns the unique name of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            "dataElement": {
                                                "name": "Weight",
                                                "valueType": "floatType",
                                                "description": "This data element returns the weight of specified traffic case.",
                                                "parameters": [
                                                    {
                                                        "name": "EntityGroup",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "EntityGroup"
                                                            }
                                                        },
                                                        "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                        "exampleValue": "EGrp01"
                                                    },
                                                    {
                                                        "name": "Scenario",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "Scenario"
                                                            }
                                                        },
                                                        "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                        "exampleValue": "Scenario01"
                                                    },
                                                    {
                                                        "name": "TrafficCase",
                                                        "typeDescriptor": {
                                                            "reference": {
                                                                "typeName": "TrafficCase"
                                                            }
                                                        },
                                                        "description": "Name of the traffic case. Should be one item in the list returned by 'TrafficCases'",
                                                        "exampleValue": "TrafficCase01"
                                                    }
                                                ]
                                            }
                                        }
                                    ]
                                },
                                {
                                    "dataElement": {
                                        "name": "IsScPresent",
                                        "valueType": "boolType",
                                        "description": "This condition returns true if specified scenario exists, and returns false otherwise.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "IsWeightedSc",
                                        "valueType": "boolType",
                                        "description": "This condition returns true if specified scenario is weighted, and returns false otherwise.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScAbortButton",
                                        "valueType": "intType",
                                        "description": "This data element returns the abort push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScCpsToReach",
                                        "valueType": "floatType",
                                        "description": "This data element can be used to set and get the target CPS for the specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScEnabled",
                                        "valueType": "boolType",
                                        "description": "This data element returns the enabled state of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScLockCPS",
                                        "valueType": "boolType",
                                        "description": "This data element returns true if the specified scenario's CPS is locked, and returns false otherwise.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScName",
                                        "valueType": "charstringType",
                                        "description": "This data element returns the name of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScPauseButton",
                                        "valueType": "intType",
                                        "description": "This data element returns the pause push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScRestoreButton",
                                        "valueType": "intType",
                                        "description": "This data element returns the restore push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScSingleShotBtn",
                                        "valueType": "intType",
                                        "description": "This data element returns the single shot push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStartButton",
                                        "valueType": "intType",
                                        "description": "This data element returns the start push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScState",
                                        "valueType": "intType",
                                        "description": "This data element returns the state of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStateName",
                                        "valueType": "charstringType",
                                        "description": "This data element returns the state name of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "ScStopButton",
                                        "valueType": "intType",
                                        "description": "This data element returns the stop push button's value of specified scenario.",
                                        "parameters": [
                                            {
                                                "name": "EntityGroup",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "EntityGroup"
                                                    }
                                                },
                                                "description": "Name of the entity group the traffic case belongs to. Should be one item in the list returned by 'EntityGroups'",
                                                "exampleValue": "EGrp01"
                                            },
                                            {
                                                "name": "Scenario",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "Scenario"
                                                    }
                                                },
                                                "description": "Name of the scenario the traffic case belongs to. Should be one item in the list returned by 'Scenarios'",
                                                "exampleValue": "Scenario01"
                                            }
                                        ]
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "TrafficCasesOfScenarios",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "TrafficCaseOfScenario"
                        },
                        "description": "This iterator returns the list of traffic cases of all scenarios."
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "EntityGroupOfTrafficCasesOfScenarios",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "EntityGroup"
                                },
                                "description": "This dataElement returns the EntityGroup for the TrafficCasesOfScenarios item.",
                                "parameters": [
                                    {
                                        "name": "TrafficCaseOfScenarios",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "TrafficCaseOfScenario"
                                            }
                                        },
                                        "description": "A triple of LGenBase entity group, scenario and traffic case. Should be one item in the list returned by 'TrafficCases'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ScenarioOfTrafficCasesOfScenarios",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "Scenario"
                                },
                                "description": "This dataElement returns the Scenario for the TrafficCasesOfScenarios item.",
                                "parameters": [
                                    {
                                        "name": "TrafficCaseOfScenarios",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "TrafficCaseOfScenario"
                                            }
                                        },
                                        "description": "A triple of LGenBase entity group, scenario and traffic case. Should be one item in the list returned by 'TrafficCases'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "TrafficCaseOfTrafficCasesOfScenarios",
                                "valueType": "charstringType",
                                "typeDescriptor": {
                                    "typeName": "TrafficCase"
                                },
                                "description": "This dataElement returns the TrafficCase for the TrafficCasesOfScenarios item.",
                                "parameters": [
                                    {
                                        "name": "TrafficCaseOfScenarios",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "TrafficCaseOfScenario"
                                            }
                                        },
                                        "description": "A triple of LGenBase entity group, scenario and traffic case. Should be one item in the list returned by 'TrafficCases'",
                                        "exampleValue": "0.0.0"
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "HasActiveTc",
                        "valueType": "boolType",
                        "description": "This condition returns true if the current PTC has at least one  active traffic case, and returns false otherwise."
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                }
            ]
        },
        {
            "source": "HostAdminServer",
            "dataElements": [
                {
                    "dataElement": {
                        "name": "Hosts",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "HostName"
                        },
                        "description": "This iterator enlists the hosts tracked by the HostAdminServer"
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "CPUs",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "CPU"
                                },
                                "description": "This iterator enlists the CPUs for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host. Should be one item in the list given by the iterator \"Hosts\"",
                                        "exampleValue": ""
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "PerCpuLoad",
                                        "valueType": "floatType",
                                        "description": "This dataElement returns the PerCpuLoad variable for a cpu on a host",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "CpuIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "CPU"
                                                    }
                                                },
                                                "description": "Specifies the CPU index (as returned by the CPUs iterator)",
                                                "exampleValue": "0"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "ProcessesOnHost",
                                "valueType": "charstringlistType",
                                "typeDescriptor": {
                                    "isListOf": true,
                                    "typeName": "ProcessOnHost"
                                },
                                "description": "This iterator enlists the processes on a given host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host. Should be one item in the list given by the iterator \"Hosts\"",
                                        "exampleValue": ""
                                    }
                                ]
                            },
                            "children": [
                                {
                                    "dataElement": {
                                        "name": "CpuLoad",
                                        "valueType": "floatType",
                                        "description": "This dataElement returns the CpuLoad variable for a process or host",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "ProcessIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ProcessOnHost"
                                                    }
                                                },
                                                "description": "Specifies the process index (as returned by the processes iterator",
                                                "exampleValue": ""
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "MemUsage",
                                        "valueType": "intType",
                                        "description": "This dataElement returns the MemUsage variable for a process",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "ProcessIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ProcessOnHost"
                                                    }
                                                },
                                                "description": "Specifies the process index (as returned by the processes iterator",
                                                "exampleValue": ""
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "MemUsageHRF",
                                        "valueType": "charstringType",
                                        "description": "his dataElement returns the MemUsage variable for a process in Human Readable Fromat",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "ProcessIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ProcessOnHost"
                                                    }
                                                },
                                                "description": "Specifies the process index (as returned by the processes iterator",
                                                "exampleValue": ""
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "PID",
                                        "valueType": "intType",
                                        "description": " This dataElement returns the PID variable for a process",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "ProcessIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ProcessOnHost"
                                                    }
                                                },
                                                "description": "Specifies the process index (as returned by the processes iterator)",
                                                "exampleValue": "0"
                                            }
                                        ]
                                    }
                                },
                                {
                                    "dataElement": {
                                        "name": "SelfName",
                                        "valueType": "charstringType",
                                        "description": "This dataElement returns the SelfName variable for a process",
                                        "parameters": [
                                            {
                                                "name": "HostName",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "HostName"
                                                    }
                                                },
                                                "description": "the name of the host",
                                                "exampleValue": ""
                                            },
                                            {
                                                "name": "ProcessIdx",
                                                "typeDescriptor": {
                                                    "reference": {
                                                        "typeName": "ProcessOnHost"
                                                    }
                                                },
                                                "description": "Specifies the process index (as returned by the processes iterator)",
                                                "exampleValue": "0"
                                            }
                                        ]
                                    }
                                }
                            ]
                        },
                        {
                            "dataElement": {
                                "name": "CpuAvg",
                                "valueType": "floatType",
                                "description": "This dataElement returns the CpuAvg variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "CpuLoad",
                                "valueType": "floatType",
                                "description": "This dataElement returns the CpuLoad variable for a process or host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "CpuMax",
                                "valueType": "floatType",
                                "description": "This dataElement returns the CpuMax variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "CpuMin",
                                "valueType": "floatType",
                                "description": "This dataElement returns the CpuMin variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "FreeMem",
                                "valueType": "intType",
                                "description": "This dataElement returns the FreeMem [kB] variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "FreeMemHRF",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the FreeMem variable for a host in Human Readable Fromat (e.g. '1.5 MB', '3.8 GB')",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "IPv4Addr",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the IPv4 Address variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "IPv6Addr",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the IPv6 Address variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "PhysicalMem",
                                "valueType": "intType",
                                "description": "This dataElement returns the PhysicalMem [kB] variable for a host",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "PhysicalMemHRF",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the PhysicalMem variable for a host in Human Readable Fromat (e.g. '1.5 MB', '3.8 GB')",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "ResetMinMaxAvgCPULoad",
                                "valueType": "intType",
                                "description": "This dataElement returns the ResetMinMaxAvgCPULoad variable for a host which can be used to reset the Min/Max/Avg/CPULoad",
                                "parameters": [
                                    {
                                        "name": "HostName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "HostName"
                                            }
                                        },
                                        "description": "the name of the host",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "ProcessNames",
                        "valueType": "charstringlistType",
                        "typeDescriptor": {
                            "isListOf": true,
                            "typeName": "ProcessName"
                        },
                        "description": "This iterator enlists the process names (globally, regardless of hostname)"
                    },
                    "children": [
                        {
                            "dataElement": {
                                "name": "CpuLoad",
                                "valueType": "floatType",
                                "description": "This dataElement returns the CpuLoad variable for a process or host",
                                "parameters": [
                                    {
                                        "name": "SelfName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ProcessName"
                                            }
                                        },
                                        "description": "Specifies the selfName of a process",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "HostName",
                                "valueType": "charstringType",
                                "description": "This dataElement returns the host name variable for a process",
                                "parameters": [
                                    {
                                        "name": "SelfName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ProcessName"
                                            }
                                        },
                                        "description": "Specifies the selfName of a process",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "MemUsage",
                                "valueType": "intType",
                                "description": "This dataElement returns the MemUsage variable for a process",
                                "parameters": [
                                    {
                                        "name": "SelfName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ProcessName"
                                            }
                                        },
                                        "description": "Specifies the selfName of a process",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        },
                        {
                            "dataElement": {
                                "name": "MemUsageHRF",
                                "valueType": "charstringType",
                                "description": "his dataElement returns the MemUsage variable for a process in Human Readable Fromat",
                                "parameters": [
                                    {
                                        "name": "SelfName",
                                        "typeDescriptor": {
                                            "reference": {
                                                "typeName": "ProcessName"
                                            }
                                        },
                                        "description": "Specifies the selfName of a process",
                                        "exampleValue": ""
                                    }
                                ]
                            }
                        }
                    ]
                },
                {
                    "dataElement": {
                        "name": "NofHosts",
                        "valueType": "intType",
                        "description": "This dataElement returns the number of hosts"
                    }
                },
                {
                    "dataElement": {
                        "name": "ProcessSortOption",
                        "valueType": "charstringType",
                        "description": "DataElement to set the process sort option. Value should be one of: { \"EPTF SelfName\", \"CPU Load\", \"Memory Usage\", \"Unsorted\" } which is the list returned by the iterator \"ProcessSortOptionList\""
                    }
                },
                {
                    "dataElement": {
                        "name": "ProcessSortOptionList",
                        "valueType": "charstringlistType",
                        "description": "This iterator enlists the sort options for process sorting that can be used in the data element \"ProcessSortOption\""
                    }
                },
                {
                    "dataElement": {
                        "name": "ResetMinMaxAvgCPULoad",
                        "valueType": "intType",
                        "description": "This dataElement returns the ResetMinMaxAvgCPULoad variable for all hosts"
                    }
                },
                {
                    "dataElement": {
                        "name": "help",
                        "valueType": "charstringType",
                        "description": "This data element returns the help information about all dataElements supported",
                        "parameters": [
                            {
                                "name": "Format",
                                "typeDescriptor": {
                                    "valueType": "charstringType"
                                },
                                "description": "The format of the output. Valid values: JSON, TEXT. Default: JSON",
                                "exampleValue": "TEXT"
                            }
                        ]
                    }
                },
                {
                    "dataElement": {
                        "name": "isMultipleHosts",
                        "valueType": "boolType",
                        "description": "This condition returns true if there are more than one hosts"
                    }
                }
            ]
        }
    ]
}