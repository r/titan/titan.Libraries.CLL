///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
function DataGenerator() {

    var LISTLENGTH = 3;

    var TypeEnum = {
        intType : 1,
        floatType : 2,
        boolType : 3,
        charstringType : 4,
        octetstringType : 5,
        hexstringType : 6,
        bitstringType : 7,
        integerlistType : 8,
        floatlistType : 9,
        charstringlistType : 10,
        statusLEDType : 11
    }

    function genPredef(content, tp) {
        var obj = {
            node : {
                val : content,
                tp : tp
            }
        };
        return obj;
    }
    
    function genString(source, element) {
        var obj = {
            node : {
                val : source + " " + element,
                tp : TypeEnum.charstringType
            }
        }
        return obj;
    }

    function genStringList(source, element) {
        var obj = {list : []};
        for (var i = 0; i < LISTLENGTH; ++i) {
            var stringnode = genString(source, element);
            stringnode.node.val += " " + i;
            obj.list.push(stringnode);
        }
        return obj;
    }

    function genInt(source, element) {
        var obj = {
            node : {
                val : Math.floor((Math.random() * 5) + 1),
                tp : TypeEnum.intType
            }
        }
        return obj;
    }

    function genIntList(source, element) {
        var obj = {list : []};
        for (var i = 0; i < LISTLENGTH; ++i) {
            obj.list.push(genInt(source, element));
        }
        return obj;
    }

    function genFloat(source, element) {
        var obj = {
            node : {
                val : Math.random(),
                tp : TypeEnum.floatType
            }
        }
        return obj;
    }

    function genFloatList(source, element) {
        var number = Math.floor((Math.random() * 5) + 1);
        var obj = {list : []};
        for (var i = 0; i < LISTLENGTH; ++i) {
            obj.list.push(genFloat(source, element));
        }
        return obj;
    }

    function genBool(source, element) {
        var obj = {
            node : {
                val : Math.floor((Math.random() * 2)) == 0,
                tp : TypeEnum.boolType
            }
        }
        return obj;
    }
    
    function genStatusLEDType(source, element) {
        var LedList = [
            "[led:blue]Idle",
            "[led:blue]Stopped",
            "[led:black]Off",
            "[led:green]Running",
            "[led:red]Stopping"
        ];
        var number = Math.floor((Math.random() * LedList.length));
        var obj = {
            node : {
                val : LedList[number],
                tp : TypeEnum.statusLEDType
            }
        }
        return obj;
    }

    this.generateContent = function(source, element, type) {
        var obj;
        
        switch (type) {
            case "intType":
                obj = genInt(source, element);
                break;
            case "floatType":
                obj = genFloat(source, element);
                break;
            case "boolType":
                obj = genBool(source, element);
                break;
            case "charstringType":
                obj = genString(source, element);
                break;
            case "charstringlistType":
                obj = genStringList(source, element);
                break;
            case "floatlistType":
                obj = genFloatList(source, element);
                break;
            case "integerlistType":
                obj = genIntList(source, element);
                break;
            case "statusLEDType":
                obj = genStatusLEDType(source, element);
                break;
            default:
                mlog("Unsupported type, using charstringType: ", "source: " + source, "element: " + element, "type: " + type, "");
                obj = genString(source, element, TypeEnum.charstringType);
                break;
        }
        
        return obj;
    }
}