///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
var WebApplications = WebApplications || [];

WebApplications.push({"application": new MockTest_Application()});

var responses = {};
var requests = {};
var descriptions = {};

function MockTest_Application() {
    "use strict";
    var v_appBase = new WebAppBase();

    var jsfiles = [
        "WebApplications/MockTest/DsRestAPITestConfig.js",
        "WebApplications/MockTest/TestConfigModelFiller.js",
        "WebApplications/MockTest/AjvSelfTest.js",
        
        "Utils/DsRestAPI/DsRestAPI.js",
        "Utils/DsRestAPI/MockedServer/DSHelp.js",
        "Utils/DsRestAPI/MockedServer/DataGenerator.js",
        "Utils/DsRestAPI/MockedServer/Model.js",
        "Utils/DsRestAPI/MockedServer/DsRestAPIComm_Mock.js",

        // The test requests
        "WebApplications/MockTest/Testcases/all.request",
        "WebApplications/MockTest/Testcases/test1.request",
        "WebApplications/MockTest/Testcases/test2.request",
        "WebApplications/MockTest/Testcases/test3.request",
        "WebApplications/MockTest/Testcases/test4.request",
        "WebApplications/MockTest/Testcases/test5.request",
        "WebApplications/MockTest/Testcases/test6.request",
        "WebApplications/MockTest/Testcases/test7.request",
        "WebApplications/MockTest/Testcases/test8.request",
        "WebApplications/MockTest/Testcases/test9.request",
        "WebApplications/MockTest/Testcases/test10.request",
        "WebApplications/MockTest/Testcases/test11.request",
        "WebApplications/MockTest/Testcases/test12.request",
        "WebApplications/MockTest/Testcases/test13.request",
        "WebApplications/MockTest/Testcases/test14.request",
        "WebApplications/MockTest/Testcases/test15.request",
        "WebApplications/MockTest/Testcases/test16.request",
        "WebApplications/MockTest/Testcases/test17.request",
        "WebApplications/MockTest/Testcases/test18.request",
        "WebApplications/MockTest/Testcases/test19.request",

        // The responses
        "WebApplications/MockTest/Testcases/all.response",
        "WebApplications/MockTest/Testcases/test1.response",
        "WebApplications/MockTest/Testcases/test2.response",
        "WebApplications/MockTest/Testcases/test3.response",
        "WebApplications/MockTest/Testcases/test4.response",
        "WebApplications/MockTest/Testcases/test5.response",
        "WebApplications/MockTest/Testcases/test6.response",
        "WebApplications/MockTest/Testcases/test7.response",
        "WebApplications/MockTest/Testcases/test8.response",
        "WebApplications/MockTest/Testcases/test9.response",
        "WebApplications/MockTest/Testcases/test10.response",
        "WebApplications/MockTest/Testcases/test11.response",
        "WebApplications/MockTest/Testcases/test12.response",
        "WebApplications/MockTest/Testcases/test13.response",
        "WebApplications/MockTest/Testcases/test14.response",
        "WebApplications/MockTest/Testcases/test15.response",
        "WebApplications/MockTest/Testcases/test16.response",
        "WebApplications/MockTest/Testcases/test17.response",
        "WebApplications/MockTest/Testcases/test18.response",
        "WebApplications/MockTest/Testcases/test19.response"
    ];

    this.info = function () {
        return {
            defaultIcon : "WebApplications/RequestConsole/main_icon.png",
            defaultName : "Mock Test"
        };
    };

    this.load = function (p_webAppModel) {
        v_appBase.load(jsfiles, [], start, p_webAppModel.getFileHandler());
    };

    this.unload = function(webappUnloaded) {
        webappUnloaded(true);
    };

    function start(p_callback) {
        var model = new Model(new DataGenerator());
        var testConfigModelFiller = new TestConfigModelFiller(model);
        testConfigModelFiller.fillModel(DsRestAPITestConfig);
        var mock = new CDsRestAPIComm(true, model);
        runTests(mock);
        p_callback();
    }

    function Comparator(p_key, p_response) {
        var response = p_response;
        this.successful = false;
        var v_this = this;

        this.compare = function (answer) {
            var expected = JSON.stringify(response);
            var actualObj = answer;
            var actual = JSON.stringify(actualObj);
            var isErrorExpected = response.contentList &&
                response.contentList.length > 0 && response.contentList[0] && (
                    (response.contentList[0].list &&
                        typeof response.contentList[0].list[0].node.val === 'string' &&
                        response.contentList[0].list[0].node.val.toLowerCase().indexOf("error") > -1) ||
                    (response.contentList[0].node &&
                        typeof response.contentList[0].node.val === 'string' &&
                        response.contentList[0].node.val.toLowerCase().indexOf("error") > -1));
            var isErrorReceived = actualObj.contentList &&
                actualObj.contentList.length > 0 && actualObj.contentList[0] && (
                    (actualObj.contentList[0].list &&
                        typeof actualObj.contentList[0].list[0].node.val === 'string' &&
                        actualObj.contentList[0].list[0].node.val.match(/(error|cannot|invalid)/i)) ||
                    (actualObj.contentList[0].node &&
                        typeof actualObj.contentList[0].node.val === 'string' &&
                        actualObj.contentList[0].node.val.match(/(error|cannot|invalid)/i)));

            v_this.successful = (isErrorExpected && isErrorReceived) || (actual == expected);

            if (!v_this.successful)
                console.log("-----------------\ntestcase: " + p_key + "\nexpected: " + expected + "\nreceived: " + actual + "\n-----------------");
        };
    }

    function runTests(p_mock) {
        var mock = p_mock;
        var successful = true;
        var selftest = new AjvSelfTest();

        if (!selftest.validate(mock.getSchema())) {
            console.error("Schema validator selftest failed!");
        } else {
            console.log("Schema validator selftest passed.");
        }
        var requestmap = [];
        var keymap = [];
        for (var key in requests)
            if (requests.hasOwnProperty(key)) {
                requestmap.push(requests[key]);
                keymap.push(key);
            }

        var errors = requestmap.reduce(function(prev, current, index, context) {
            console.log("Commencing test #" + keymap[index]);
            var comparator = new Comparator(index, responses[keymap[index]]);
            var err = "";
            var isHexstring = false;
            if (current.requests && current.requests[0])
                isHexstring = (current.requests[0].listOfGetData ? false : (current.requests[0].getData ? current.requests[0].getData.element === "help" : current.requests[0].setData.element === "help"));
            mock.ajaxCall(current, function ajaxCallCallback(answer) {
                answer = {"contentList": answer};
                if (isHexstring) {
                    // response to an "element: help" request --> node.val is a hexstring
                    var answerObj = answer;
                    for(var i = 0; i < answerObj.contentList.length; i++)
                        if (!answerObj.contentList[i].node.val.startsWith("Error")) // Errors are not in hexstring.
                            answerObj.contentList[i].node.val = hex2a(answerObj.contentList[i].node.val);
                    answer = answerObj;
                }
                comparator.compare(answer);
            });
            if (!comparator.successful) {
                err = "Wrong response for " + keymap[index];
                if (descriptions[keymap[index]])
                    err += " -- Description of the test step: " + descriptions[keymap[index]];
            }
            return prev.concat(err);
        }, []);

        var errormsg = errors.reduce(function(prev, current, index, context) {return current.length?prev + current + "\n":prev;}, "");

        successful = errors.reduce(function(prev, current, index, context) {
            return (prev && !current.length);
        }, true);

        if (successful) {
            alert("Test result: pass");
        } else {
            alert("Test result: fail\n" + errormsg);
        }
    }
    
    function hex2a(hex)
    {
        var str = '';
        for (var i = 0; i < hex.length; i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }
}
