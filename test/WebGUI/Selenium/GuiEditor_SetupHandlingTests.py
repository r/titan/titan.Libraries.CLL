#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

from BaseTestCase import *
from Framework_CommonFunctions import *
from GuiEditor_CommonFunctions import *

class GuiEditor_SetupHandlingTests(BaseTestCase):

    def test_newSetup(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        newSetup(self.driver)
        setupName = getSetupName(self.driver)
        
        self.driver.implicitly_wait(0.5)
        
        self.assertEqual(setupName, "... unsaved setup ...", "Displayed setup name is incorrect, it is " + setupName + " and it should be: ... unsaved setup ...")
        self.assertFalse(treeExists(self.driver, "GuiEditor_RequestTree"), "The request tree exists, but it should not in a new setup")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "There is a viewmodel editor, but there should be none in a new setup")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "There is a view editor, but there should be none in a new setup")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ImportEditor"), "There is an imports editor, but there should be none in a new setup")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_HtmlEditor"), "The html editor does not exists but it should")
        
        saveSetup(self.driver)
        self.assertTrue(dialogExists(self.driver, "SaveSetupAs"), "Save as dialog should have appeared after clicking the save button")

    def test_loadFromConfigAtStart(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        setupName = getSetupName(self.driver)
        
        self.driver.implicitly_wait(0.5)
        
        self.assertEqual(setupName, "Test1", "Displayed setup name is incorrect, it is " + setupName + " and it should be: Test1")
        self.assertTrue(treeExists(self.driver, "GuiEditor_HelpTree"), "There is no help tree, but it must always exist")
        self.assertTrue(treeExists(self.driver, "GuiEditor_RequestTree"), "There is no request tree, but there should be")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "There is no viewmodel editor, but there should be")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "There is no view editor, but there should be")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ImportEditor"), "There is an import editor, but there should be none")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_HtmlEditor"), "The html editor does not exists but it should")

    def test_loadFromDialog(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        loadSetup(self.driver, "Test2")
        setupName = getSetupName(self.driver)
        
        self.driver.implicitly_wait(0.5)
        
        self.assertEqual(setupName, "Test2", "Displayed setup name is incorrect, it is " + setupName + " and it should be: Test2")
        self.assertTrue(treeExists(self.driver, "GuiEditor_HelpTree"), "There is no help tree, but it must always exist")
        self.assertTrue(treeExists(self.driver, "GuiEditor_RequestTree"), "There is no request tree, but there should be")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "There is no viewmodel editor, but there should be")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "There is a view editor, but there should not be")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ImportEditor"), "There is an import editor, but there should be none")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_HtmlEditor"), "The html editor does not exists but it should")

    def test_saveSetup(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        setupName = getSetupName(self.driver)
        viewmodelEditor = self.driver.find_element_by_id("GuiEditor_ViewmodelEditor_0")
        
        time.sleep(0.5)
        rightClickLabel(self.driver, "GuiEditor_ViewmodelEditor_0")
        time.sleep(0.5)
        if not clickInContextMenu(self.driver, "delete"):
            self.fail("No option for delete in context menu")
        time.sleep(0.5)
        
        saveSetup(self.driver)
        newSetup(self.driver)
        loadSetup(self.driver, setupName)
        
        try:
            viewmodelEditor = self.driver.find_element_by_id("GuiEditor_ViewmodelEditor_0")
            self.fail("Deleted ViewModel Editor is still present in the saved setup")
        except NoSuchElementException as e:
            pass

    def test_saveSetupAs(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        origSetupName = getSetupName(self.driver)
        
        newSetup(self.driver)
        newSetupName = 'tmp_setup'
        saveSetupAs(self.driver, newSetupName)
        setupName = getSetupName(self.driver)
        if not setupName == newSetupName: self.fail("Displayed setup name is incorrect, it is " + setupName + " and it should be: " + newSetupName)
        
        loadSetup(self.driver, origSetupName)
        loadSetup(self.driver, newSetupName)
        
        self.driver.implicitly_wait(0.5)
        
        self.assertFalse(treeExists(self.driver, "GuiEditor_RequestTree"), "The request tree exists, but it should not")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewmodelEditor"), "There is a viewmodel editor, but there should be none")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ViewEditor"), "There is a view editor, but there should be none")
        self.assertFalse(editorTypeExists(self.driver, "GuiEditor_ImportEditor"), "There is an import editor, but there should be none")
        self.assertTrue(editorTypeExists(self.driver, "GuiEditor_HtmlEditor"), "The html editor does not exists but it should")

    def test_saveSetupAsOverwrite(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        self.assertTrue(saveSetupAs(self.driver, "Test2", True, False), "SaveAs overwrite checking is not ok")

if __name__ == "__main__":
    unittest.main(catchbreak=True)