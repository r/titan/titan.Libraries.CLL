#!/bin/bash
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

echo "Checking if Xvfb is running..."
XVFB_PID=`pgrep Xvfb`

if [ "$?" == "0" ]; then
    
    echo "Xvfb is running, finding display port..."
    DISPLAY=`ps --format command --no-headers -ww --pid $XVFB_PID | grep -o ':[0-9]*'`
    echo "Found display port: "$DISPLAY
    export DISPLAY=$DISPLAY.0
    
    chmod u+x runTests.py
    ./runTests.py
    
    exit 0
    
else
    
    echo -n "Xvfb is not running, starting it on display :999 with pid: "
    Xvfb :999 -fp /usr/share/fonts/misc &
    XVFB_PID=$!
    echo $XVFB_PID

    killXvfb_trap() {
        kill $XVFB_PID
        echo "Killed Xvfb ($XVFB_PID)"
    }

    trap killXvfb_trap INT
    trap killXvfb_trap TERM

    export DISPLAY=:999.0

    chmod u+x runTests.py
    ./runTests.py

    killXvfb_trap
    
    exit 0
fi
