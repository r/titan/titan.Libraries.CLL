#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait

class BaseTestCase(unittest.TestCase):

    # yes... a \ has to be escaped even in a raw string, and a string can not end with \, so we have to create a string that does not end with a \ and get the \ character from its index...
    HTML_FILE = ("file:///" + os.path.abspath("../Test.html").replace(r"\\ "[0], r"/"))

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.maximize_window()
        self.driver.implicitly_wait(3)
        # these are not built in members, but no one said we could not add them
        self.driver.wait = WebDriverWait(self.driver, 3)
        self.driver.applicationStarted = False

    def tearDown(self):
        # Get the list produced by 'window.onerror' in Utilities.js
        javaScript = "return window.jsErrors.length"
        jsErrorsLength = int(self.driver.execute_script(javaScript))
        x = 0
        while (x < jsErrorsLength):
            if (x == 0): 
                print('\n*** Test id :', self.id())
            javaScript = "return window.jsErrors[" + str(x) + "] " 
            print(self.driver.execute_script(javaScript) + '\n')
            x += 1

        self.driver.quit()

if __name__ == "__main__":
    unittest.main(catchbreak=True)