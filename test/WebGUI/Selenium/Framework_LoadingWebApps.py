#!/usr/bin/env python
# -*- coding: utf-8 -*-
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

import time

from BaseTestCase import *
from Framework_CommonFunctions import *

from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException

class Framework_LoadingWebApps(BaseTestCase):

    def test_titleContainsWebGUI(self):
        self.driver.get(self.HTML_FILE)
        self.assertIn("TitanSim", self.driver.title)

    def test_mockedServer(self):
        self.driver.get(self.HTML_FILE)
        browserCompatibilityCheck(self.driver)
        
        time.sleep(1)
        button = self.driver.wait.until(EC.element_to_be_clickable((By.ID,'applications_header_appBtn_4')))
        button.click()
        
        alert = Alert(self.driver)
        alert_text = alert.text
        alert.accept()
        
        self.assertEqual("Test result: pass", alert_text, "The mocked server test did not pass")

    # ---------- testing starting applications ----------

    def checkApplicationStart(self, element_id):
        try:
            element = self.driver.find_element_by_id(element_id)
        except NoSuchElementException as e:
            self.fail("element by id " + element_id + " not found after starting application")

    def test_titanSimGuiStart(self):
        self.driver.get(self.HTML_FILE)
        startCustomizableApp(self.driver)
        self.checkApplicationStart("customAppMainview")

    def test_guiEditorStart(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        self.checkApplicationStart("GuiEditor_MainView")

    def test_requestTesterStart(self):
        self.driver.get(self.HTML_FILE)
        startRequestTester(self.driver)
        self.checkApplicationStart("RequestConsole_MainView")

    # ---------- testing switching applications ----------

    def checkApplicationSwitch(self, element_id1, element_id2):
        try:
            self.driver.find_element_by_id(element_id1)
            self.fail("element by id " + element_id + " found, but it should not exist after switching applications")
        except NoSuchElementException as e:
            pass

    def test_switchTitanSimGuiEditor(self):
        self.driver.get(self.HTML_FILE)
        startCustomizableApp(self.driver)
        startGuiEditor(self.driver)
        self.checkApplicationSwitch("customAppMainview", "GuiEditor_MainView")

    def test_switchGuiEditorRequestTester(self):
        self.driver.get(self.HTML_FILE)
        startGuiEditor(self.driver)
        startRequestTester(self.driver)
        self.checkApplicationSwitch("GuiEditor_MainView", "RequestConsole_MainView")

    def test_switchRequestTesterTitanSimGui(self):
        self.driver.get(self.HTML_FILE)
        startRequestTester(self.driver)
        startGuiEditor(self.driver)
        self.checkApplicationSwitch("RequestConsole_MainView", "customAppMainview")

if __name__ == "__main__":
    unittest.main(catchbreak=True)