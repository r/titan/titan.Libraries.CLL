///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_WebGUI_Test_Testcases
//
// Purpose:
//   This module contains testcases for testing EPTF WebGUI.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_WebGUI_Test_Definitions>
//    <EPTF_WebGUI_Test_Functions>
//
// Current Owner:
//    Daniel Gobor (ednigbo)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    To write new tests, edit the Selenium tests in the Selenium directory.
//    To create a new group of tests, create a new .py file that contains a class which extends baseTestCase
//    To write the testcases, simply create a new function in this class whose name begins with test_
//    Finally, import the created module in runTests.py
//    This TTCN testcase will automatically execute the new tests
///////////////////////////////////////////////////////////
module EPTF_WebGUI_Test_Testcases {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_WebGUI_Test_Definitions all;
import from EPTF_WebGUI_Test_Functions all;
import from EPTF_CLL_Base_Functions all;
import from TCCConversion_Functions all;

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_WebGUI_Test
//
// Purpose:
//   to test the WebGUI using selenium tests
//
// Requirement:
//   -
//
// Action:
//   - calls f_EPTF_WebGUI_init_CT() function which calls base init
//   - calls createOfflineWebGUI.sh which creates the test environment
//   - calls runTests.py which executes the Selenium tests
//   - if the test output contains error of fail, the test will fail
//   - calls f_EPTF_Base_stop which does the cleanup
//
// Expected Result:
//   - pass
///////////////////////////////////////////////////////////
testcase tc_EPTF_WebGUI_Test() runs on WebGUI_Test_CT {
  f_EPTF_WebGUI_Test_init_CT("tc_EPTF_WebGUI_Test");
  
  var charstring vl_createOfflineWebGUICommand := "./createOfflineWebGUI.sh";
  var charstring vl_stdout := "";
  var charstring vl_stderr := "";
  
  if(f_EPTF_Base_executeShell(vl_createOfflineWebGUICommand, vl_stdout, vl_stderr) != 0) {
    setverdict(inconc, "Cannot create offline WebGUI, ERROR:<br/>" & vl_stderr);
    f_EPTF_Base_stop(none);
  }
  
  var charstring vl_runSeleniumTests := "cd WebGUI/htdocs/Selenium; ./runTestsWithXvfb.sh";
  vl_stdout := "";
  vl_stderr := "";
  
  var integer vl_errorCode := f_EPTF_Base_executeShell(vl_runSeleniumTests, vl_stdout, vl_stderr);
  
  var charstring vl_fail := "*[fF]ail*"
  var charstring vl_error := "*[eE]rror*";
  
  if (vl_errorCode != 0 or match(vl_stdout, pattern vl_fail) or match(vl_stdout, pattern vl_error) or match(vl_stderr, pattern vl_fail) or match(vl_stderr, pattern vl_error)) {
    var charstring vl_escaped_stdout := f_replaceEveryOccurenceOfSubstring(vl_stdout, "<", "&lt;");
    vl_escaped_stdout := f_replaceEveryOccurenceOfSubstring(vl_escaped_stdout, ">", "&gt;");
    vl_escaped_stdout := f_replaceEveryOccurenceOfSubstring(vl_escaped_stdout, "\n", "<br/>");
    var charstring vl_escaped_stderr := f_replaceEveryOccurenceOfSubstring(vl_stderr, "<", "&lt;");
    vl_escaped_stderr := f_replaceEveryOccurenceOfSubstring(vl_escaped_stderr, ">", "&gt;");
    vl_escaped_stderr := f_replaceEveryOccurenceOfSubstring(vl_escaped_stderr, "\n", "<br/>");
    setverdict(fail, "Selenium tests failed: <br/>STD OUT:<br/>" & vl_escaped_stdout & "<br/>STD ERROR:<br/>" & vl_escaped_stderr);
  } else {
    setverdict(pass);
  }
  
  var charstring vl_deleteOfflineWebGUI := "./deleteOfflineWebGUI.sh";
  vl_stdout := "";
  vl_stderr := "";
  
  if(f_EPTF_Base_executeShell(vl_deleteOfflineWebGUI, vl_stdout, vl_stderr) != 0) {
    action("Cannot delete offline WebGUI, ERROR: " & vl_stderr);
  }

  f_EPTF_Base_stop(none);
}

//=========================================================================
// Control
//=========================================================================
control {
  execute(tc_EPTF_WebGUI_Test());
}

}  // module
