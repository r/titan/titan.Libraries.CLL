///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLI_Test_Functions
// 
//  Purpose:
//    This module contains the implementation of generic EPTF_CLL_CLI functions.
// 
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Commmon_Functions>
// 
//  Current Owner:
//    ethbaat
// 
//  Last Review Date:
//    -
//
//  Detailed Comments:
//    This module contains the functions for testing EPTF_CLL_CLI
//    It can be useful for testing components extension of EPTF_CLL_CLI_CT and/or EPTF_CLL_CLI_client_CT
//
///////////////////////////////////////////////////////////////

module EPTF_CLI_Test_Functions {

import from EPTF_CLI_Test_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_CLI_Definitions all;
import from EPTF_CLL_CLI_Functions all;

group CLI_Terminal {
  //============================================
  // f_EPTF_CLI_Test_terminal_init_CT
  //============================================
  function f_EPTF_CLI_Test_terminal_init_CT(in charstring pl_selfName, in charstring pl_prompt:="") runs on EPTF_CLI_Test_terminal {
    if(v_EPTF_CLI_Test_terminal_initiated) { return};
    f_EPTF_Base_init_CT(pl_selfName);
    v_EPTF_CLI_Test_terminal_rcvdMsg:="";
    v_EPTF_CLI_Test_terminal_rcvdInt:=-1;
    v_EPTF_CLI_Test_terminal_prompt:= pl_prompt;
    log(">>>map is coming");
    map(self:EPTF_CLI_Test_terminal_PCO,system:EPTF_CLI_Test_terminal_PCO);
    //vd_alt:= activate(as_EPTF_CLI_Test_CLI_terminal_msgHandler());
    f_EPTF_Base_registerCleanup(refers(f_EPTF_CLI_Test_terminal_cleanup_CT));
    log("registered");
    v_EPTF_CLI_Test_terminal_initiated:=true;
  }
  //============================================
  // f_EPTF_CLI_Test_terminal_cleanup_CT
  //============================================
  function f_EPTF_CLI_Test_terminal_cleanup_CT() runs on EPTF_CLI_Test_terminal {
    if(not v_EPTF_CLI_Test_terminal_initiated) {return};
    if(vd_alt!=null) {
      deactivate(vd_alt);
      vd_alt:=null;
    }
    unmap(self:EPTF_CLI_Test_terminal_PCO,system:EPTF_CLI_Test_terminal_PCO);
    v_EPTF_CLI_Test_terminal_initiated:=false;
  }
  //============================================
  // f_EPTF_CLI_Test_CLI_terminal_behavior
  //============================================
  function f_EPTF_CLI_Test_CLI_terminal_behavior(in charstring pl_selfName,in template CmdAndAnswerList pl_cmdAndAnswerList, in charstring pl_prompt:="", in float pl_timeout:=3.0) 
  runs on EPTF_CLI_Test_terminal {

    f_EPTF_CLI_Test_terminal_init_CT(pl_selfName, pl_prompt);
    log(">>>Terminal:Init done");

    var boolean vl_stopped:=false, vl_exit:=false;
    timer T; T.start(pl_timeout);
    for(var integer i:=0;i<sizeof(pl_cmdAndAnswerList);i:=i+1) {
      T.stop; 
      T.start(pl_timeout);
      EPTF_CLI_Test_terminal_PCO.send(valueof(pl_cmdAndAnswerList[i].command));
      select(valueof(pl_cmdAndAnswerList[i].command)) {
        case("exit") { vl_exit:=true; break;}
        case("quit") { vl_exit:=true;break;}
        case("stop") { vl_stopped:=true;break;}
        case("stopAll"){ vl_stopped:=true;break;}
      }
      alt {
        [] EPTF_CLI_Test_terminal_PCO.receive(charstring:?) ->value v_EPTF_CLI_Test_terminal_rcvdMsg {
            if( v_EPTF_CLI_Test_terminal_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Test_terminal_rcvdMsg == pl_prompt) {
              log("Prompt received")
              repeat;
            } else if( match(v_EPTF_CLI_Test_terminal_rcvdMsg,pl_cmdAndAnswerList[i].expectedAnswer) ) {
              setverdict(pass);
              alt {
                [] EPTF_CLI_Test_terminal_PCO.receive(charstring:?) ->value v_EPTF_CLI_Test_terminal_rcvdMsg {
                    if( v_EPTF_CLI_Test_terminal_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Test_terminal_rcvdMsg == pl_prompt) {
                      log("Prompt received")
                    }
                  }
                [] EPTF_CLI_Test_terminal_PCO.receive(integer:?)-> value v_EPTF_CLI_Test_terminal_rcvdInt { }
                [] T.timeout { setverdict(fail, "timeout occured"); }
              }
              //>>>>EXIT
            } else if( v_EPTF_CLI_Test_terminal_rcvdMsg=="\n") {
              log("\n received and accepted"); repeat;
            } else {
              log(">>>> Unexpected answer received: ",v_EPTF_CLI_Test_terminal_rcvdMsg);
              log(">>>> Expected                  : ",pl_cmdAndAnswerList[i].expectedAnswer);
              //log(">>>> Sizes: rcvd: ", lengthof(v_EPTF_CLI_Test_terminal_rcvdMsg), " expected: ", lengthof(pl_cmdAndAnswerList[i].expectedAnswer));
              setverdict(fail);
            }
          }
        [] EPTF_CLI_Test_terminal_PCO.receive(integer:?)-> value v_EPTF_CLI_Test_terminal_rcvdInt {
            log(">>>>>Integer received",v_EPTF_CLI_Test_terminal_rcvdInt); 
            repeat };
        [] T.timeout {
            if(match("",pl_cmdAndAnswerList[i].expectedAnswer))
            //substr(pl_cmdAndAnswerList[i].command,0,1)=="#" or substr(pl_cmdAndAnswerList[i].command,0,2)=="//")
            {
              setverdict(pass);
            }else {
              setverdict(fail, "timeout occured");
            }
          }
      }//alt

    }//for

    if( vl_exit) {
      alt{
        [] EPTF_CLI_Test_terminal_PCO.receive(integer:?)-> value v_EPTF_CLI_Test_terminal_rcvdInt {
            log(">>>>>Integer received at exit: ",v_EPTF_CLI_Test_terminal_rcvdInt); 
            f_EPTF_Base_stop(pass);
           };
        [] T.timeout { 
            setverdict(fail, "timeout occured");
          }
      }
    } else if( vl_stopped ) {
      if(T.running) { T.stop;};
      f_EPTF_Base_wait4Shutdown();
    } else {
      if(T.running) {T.stop;};
      log(">>>Terminal successfully exits");
      f_EPTF_Base_stop(pass);
    }
  }//f_
}//group CLI_Terminal

group CLI_display {
  //============================================
  // f_EPTF_CLI_Test_display_init_CT
  //============================================
  function f_EPTF_CLI_Test_display_init_CT(in charstring pl_selfName, in charstring pl_prompt:="") runs on EPTF_CLI_Test_display {
    if(v_EPTF_CLI_Test_display_initiated) { return};
    f_EPTF_Base_init_CT(pl_selfName);
    v_EPTF_CLI_Test_display_rcvdMsg:="";
    v_EPTF_CLI_Test_display_rcvdInt:=-1;
    v_EPTF_CLI_Test_display_prompt:= pl_prompt;
    log(">>>map is coming");
    map(self:EPTF_CLI_Test_display_PCO,system:EPTF_CLI_Test_display_PCO);
    //vd_alt:= activate(as_EPTF_CLI_Test_CLI_terminal_msgHandler());
    f_EPTF_Base_registerCleanup(refers(f_EPTF_CLI_Test_display_cleanup_CT));
    log("registered");
    v_EPTF_CLI_Test_display_initiated:=true;
  }
  //============================================
  // f_EPTF_CLI_Test_display_cleanup_CT
  //============================================
  function f_EPTF_CLI_Test_display_cleanup_CT() runs on EPTF_CLI_Test_display {
    if(not v_EPTF_CLI_Test_display_initiated) {return};
    if(vd_alt!=null) {
      deactivate(vd_alt);
      vd_alt:=null;
    }
    unmap(self:EPTF_CLI_Test_display_PCO,system:EPTF_CLI_Test_display_PCO);
    v_EPTF_CLI_Test_display_initiated:=false;
  }
  //============================================
  // f_EPTF_CLI_Test_CLI_display_behavior
  //============================================
  function f_EPTF_CLI_Test_CLI_display_behavior(in charstring pl_selfName,/*in CmdAndAnswerList pl_cmdAndAnswerList:={},*/in charstring pl_prompt:="") 
  runs on EPTF_CLI_Test_display {
    f_EPTF_CLI_Test_display_init_CT(pl_selfName, pl_prompt);
    log(">>>display:Init done");
    timer T; T.start(15.0)
    template charstring tl_elapsedTimePattern:= pattern "Elapsed time: " & "[\d]#(1,)\.[\d]#(1,6)"; //1.012268"
    template charstring tl_elapsedTimeAndPromptPattern := pattern "Elapsed time: " & "[\d]#(1,)\.[\d]#(1,6)" & "\n" & pl_prompt;

    alt {
      [] EPTF_CLI_Test_display_PCO.receive(charstring:?) ->value v_EPTF_CLI_Test_display_rcvdMsg {
          if( v_EPTF_CLI_Test_display_rcvdMsg == "\n"&pl_prompt or v_EPTF_CLI_Test_display_rcvdMsg == pl_prompt) {
            log("Prompt received");
            repeat;
          } else if( match(v_EPTF_CLI_Test_display_rcvdMsg,tl_elapsedTimePattern) or
            match(v_EPTF_CLI_Test_display_rcvdMsg,tl_elapsedTimeAndPromptPattern)) {
            setverdict(pass);
            repeat;
          } else if( v_EPTF_CLI_Test_display_rcvdMsg=="\n") {
            log("\n received and accepted"); 
            repeat;
          } else {
            log(">>>> Unexpected answer received: ",v_EPTF_CLI_Test_display_rcvdMsg);
          }
        }
      [] EPTF_CLI_Test_display_PCO.receive(integer:?)-> value v_EPTF_CLI_Test_display_rcvdInt {
          log(v_EPTF_CLI_Test_display_rcvdInt); 
          repeat };
      [] T.timeout { 
          setverdict(fail, "timeout occured");
        }
    }//alt

    //}//for
    log(">>>display successfully exits");
    f_EPTF_Base_stop(pass);

  }//f_

}//group CLI_display

group Server_Client {
  //============================================
  // f_EPTF_CLI_Test_CLI_server_behavior
  //============================================
  function f_EPTF_CLI_Test_CLI_server_behavior(in charstring pl_selfName) runs on EPTF_CLI_CT {
    f_EPTF_CLI_init_CT(pl_selfName);
    f_EPTF_Base_wait4Shutdown();
  };
  
  //============================================
  // f_EPTF_CLI_Test_CLI_client_behavior
  //============================================
  function f_EPTF_CLI_Test_CLI_client_behavior(
    in charstring pl_selfName, 
    in EPTF_CLI_CT pl_CLI_compRef := null
    ) runs on EPTF_CLI_Client_CT {
    f_EPTF_CLI_Client_init_CT( pl_selfName,pl_CLI_compRef);
    f_EPTF_CLI_Client_registerCommand(
      "echo",
      "echoes its arguments",
      refers(f_EPTF_CLI_Client_echoHandler));
    f_EPTF_Base_wait4Shutdown();
  }
  
  //============================================
  // f_EPTF_CLI_Test_CLI_client2_behavior
  //============================================
  function f_EPTF_CLI_Test_CLI_client2_behavior(
    in charstring pl_selfName, 
    in EPTF_CLI_CT pl_CLI_compRef := null
    ) runs on EPTF_CLI_Client_CT {
    f_EPTF_CLI_Client_init_CT( pl_selfName,pl_CLI_compRef);
    f_EPTF_CLI_Client_registerCommand(
      "ECHO2",
      "ECHO2 help",
      refers(f_EPTF_CLI_Client_echoHandler));
    f_EPTF_Base_wait4Shutdown();
  }
  
  //============================================
  // f_EPTF_CLI_Client_echoHandler
  //============================================
  function f_EPTF_CLI_Client_echoHandler(in charstring pl_commandArgs, inout charstring pl_result) runs on EPTF_CLI_Client_CT return integer {
    if (pl_commandArgs=="help") {
      pl_result := "echo - prints out its parameters\n"&
      "This is the detailed help message of the echo command\n"&
      "If called with ""ERROR!"" argument it exits with code 1\n"&
      "If called with -t <duration> as the first argument\n"&
      "the result is printed after that time has passed (in seconds).";
      return 0; // OK
    }
    if (pl_commandArgs=="ERROR!") {
      pl_result := "Syntax error!"
      return 1; // set nonzero exit code
    }
    if (regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,0)=="-t") {
      // -t option is given as the first argument
      // extract the duration value:
      var float vl_duration:=str2float(regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,1));
      // delay response:
      timer t_wait := vl_duration;
      t_wait.start; t_wait.timeout;
      // set the result to the remaining string:
      pl_result := regexp(pl_commandArgs,c_EPTF_CLI_anyWord&c_EPTF_CLI_anyWord&c_EPTF_CLI_everything,2);
      return 0; //OK
    }
    pl_result := pl_commandArgs;
    return 0; // OK
  }
  
}//Server_Client

}//module
