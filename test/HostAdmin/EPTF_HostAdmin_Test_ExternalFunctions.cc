//////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////


#include <errno.h>
#include <pthread.h>
#include "EPTF_HostAdmin_Test_Functions.hh"

static void * thread_start(void *arg) {
           /*struct thread_info *tinfo = arg;
           char *uargv, *p;

           printf("Thread %d: top of stack near %p; argv_string=%s\n",
                   tinfo->thread_num, &p, tinfo->argv_string);

           uargv = strdup(tinfo->argv_string);
           if (uargv == NULL)
               handle_error("strdup");

           for (p = uargv; *p != '\0'; p++)
               *p = toupper(*p);

           return uargv;*/
  while(true) {
    1*1;
    //sleep(100);
  }
  return 0;
}

void EPTF__HostAdmin__Test__Functions::f__EPTF__HostAdmin__Test__Process__CPULoad__startThreads(
  const INTEGER& pl__nofThread,
  const FLOAT& pl__desiredLoad
) {
  pthread_attr_t attr;  
  int s = pthread_attr_init(&attr);
  pthread_t thread_id;

  for (int i = 0; i < pl__nofThread; i++) {
    s = pthread_create(&thread_id, &attr,
                                  &thread_start, NULL);
  }
}

// end of file
