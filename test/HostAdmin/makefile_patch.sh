#!/bin/sh
#///////////////////////////////////////////////////////////////////////////////
#// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
#//                                                                           //
#// All rights reserved. This program and the accompanying materials          //
#// are made available under the terms of the Eclipse Public License v2.0     //
#// which accompanies this distribution, and is available at                  //
#// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
#///////////////////////////////////////////////////////////////////////////////

sed -e '
s/CPPFLAGS = .*/CPPFLAGS = -D$(PLATFORM) -I$(TTCN3_DIR)\/include -DNO_IPV6/g
s/SOLARIS_LIBS = -lsocket -lnsl/SOLARIS_LIBS = -lsocket -lnsl -lkstat -lpthread -lrt/g
s/SOLARIS8_LIBS = -lsocket -lnsl/SOLARIS8_LIBS = -lsocket -lnsl -lkstat -lpthread -lrt -lresolv/g
s/LINUX_LIBS =/LINUX_LIBS = -lpthread -lutil/g
s/WIN32_LIBS =/WIN32_LIBS = -lpthread -lutil/g
' <$1 >$2
