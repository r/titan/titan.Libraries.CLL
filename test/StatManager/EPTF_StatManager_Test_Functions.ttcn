///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
module EPTF_StatManager_Test_Functions {

import from EPTF_CLL_StatManager_Functions all;
import from EPTF_StatManager_Test_Definitions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_UIHandler_WidgetFunctions all;
import from ttcn_ericsson_se_protocolModules_xtdp_xtdl all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_Logging_Functions all;

public function f_EPTF_StatManager_Test_initCT(in charstring pl_selfName)runs on EPTF_StatManager_Test_CT{
  f_EPTF_StatManager_init_CT(pl_selfName);
  f_EPTF_UIHandler_init_CT(pl_selfName);
  f_EPTF_Base_registerCleanup(refers(f_EPTF_StatManager_Test_cleanup_CT));
  v_ready:=false;  
}

public function f_EPTF_StatManager_Test_cleanup_CT()runs on EPTF_StatManager_Test_CT {
  v_ready := false;
}

public function f_EPTF_StatManager_Test_DSProcessData(
  out charstring pl_dataVarName,
  in charstring pl_source,
  in charstring pl_ptcName,
  in charstring pl_element,
  in EPTF_DataSource_Params pl_params)
runs on EPTF_StatManager_Test_CT return integer
{
  select(pl_element){
  	case(c_StatManager_Test_dataElementSourceVar){
  	  pl_dataVarName := c_StatManager_Test_dataElementSourceVar;
  	  return 0;
  	}
  	case(c_StatManager_Test_dataElementRefVar){
  	  pl_dataVarName := c_StatManager_Test_dataElementRefVar;
  	  return 0;	  
  	}  
  }
  return -1;
}

public function f_EPTF_StatManager_Test_checkReady(
  in charstring pl_source,
  in charstring pl_ptcName) runs on EPTF_StatManager_Test_CT{
    if(pl_source=="StatManagerTest"){
    	v_ready:=true;    
    }
}

public function f_EPTF_StatManager_Test_checkLED(in charstring pl_expected)runs on EPTF_StatManager_Test_CT {
  var charstring vl_snapshot := f_EPTF_UIHandler_snapshot();
  var Widgets vl_received;
  var charstring vl_led;
  dec_Widgets(char2oct(vl_snapshot),vl_received);
  vl_led := log2str(vl_received.window.embeddedwidgets.embeddedwidget_list[0].hbox.embeddedwidgets.embeddedwidget_list[0].tree.treechildren.treeitemgroups.treeitemgroup_list[0].treeitem.treerowgroups.treerowgroup_list[0].treerow.treecellgroups.treecellgroup_list[0].treecell.label_);
  if(not match(vl_led, pattern pl_expected)){setverdict(fail, log2str(match(vl_led, pattern pl_expected)))}; 
}
  
function f_EPTF_StatManager_Test_init (in VarVal pl_sourceVarVal := {floatVal := 0.0}, in VarVal pl_refVarVal := {floatVal := -1.0}, in charstring pl_params, out integer pl_sourceIdx, out integer pl_refIdx, in charstring pl_message := "") runs on EPTF_StatManager_Test_CT {
  f_EPTF_StatManager_Test_initCT("StatManagerTest");
  f_EPTF_StatManager_Test_ExpectedWarning_setExpectedWarning(pl_message);
  var integer vl_sourceIdx := -1, vl_refIdx := -1;

  if(ischosen(pl_sourceVarVal.intVal))
  {
    f_EPTF_Var_newInt(c_StatManager_Test_dataElementSourceVar, pl_sourceVarVal.intVal, vl_sourceIdx);
  }else{
    f_EPTF_Var_newFloat(c_StatManager_Test_dataElementSourceVar, pl_sourceVarVal.floatVal, vl_sourceIdx);
  }
  
  f_EPTF_DataSource_registerReadyCallback(refers(f_EPTF_StatManager_Test_checkReady));
  f_EPTF_DataSourceClient_registerData(c_StatManager_Test_DataSource_sourceId, "StatManagerTest", refers(f_EPTF_StatManager_Test_DSProcessData), self);
  f_EPTF_DataSourceClient_sendReady(c_StatManager_Test_DataSource_sourceId, "StatManagerTest",  self);
  
  const charstring c_gui := "
  <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  	<window height='300.000000' id='Window' orientation='vertical' title='tc_EPTF_StatManager_Test_base' width='800.000000'>
  		<hbox flex='1.000000' orientation='horizontal'>
          <tree flex='1.0' hidecolumnpicker='true' id='myTable'>
            <treecols>
              <treecol label='Status' widgetType='statusLEDWithText'/>
              <treecol label='Value' widgetType='string'/>
            </treecols>
          	<treechildren>
          		<treeitem>
          			<treerow>
     					<externalvalue id='srcVar' element='sourceVar' source='StatManagerTest'>
            				<treecell>
            					<externaldata element='LEDlimit' source='StatManager' ptcname='StatManagerTest'>
            						<params>
            							<dataparam name='VarId' value='%srcVar::ref%'/>
            							<dataparam name='redLimit' value='25.0'/>
            							<dataparam name='yellowLimit' value='50.0'/>
            							<dataparam name='greenLimit' value='75.0'/>
            							<dataparam name='defaultColor' value='black'/>
            							<dataparam name='enableValueInLEDText' value='yes'/>
            						</params>
            					</externaldata>
            				</treecell>
            				<treecell label='value0'/>
            			</externalvalue>
          			</treerow>
          		</treeitem>
          	</treechildren>
          </tree>
        </hbox>
  	</window>
  </Widgets>";
  
  var charstring vl_guiPre:= "
  <Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  	<window height='300.000000' id='Window' orientation='vertical' title='tc_EPTF_StatManager_Test_base' width='800.000000'>
  		<hbox flex='1.000000' orientation='horizontal'>
          <tree flex='1.0' hidecolumnpicker='true' id='myTable'>
            <treecols>
              <treecol label='Status' widgetType='statusLEDWithText'/>
              <treecol label='Value' widgetType='string'/>
            </treecols>
          	<treechildren>
          		<treeitem>
          			<treerow>
     					<externalvalue id='srcVar' element='sourceVar' source='StatManagerTest'>";
  var charstring vl_guiPost := "</externalvalue>
          			</treerow>
          		</treeitem>
          	</treechildren>
          </tree>
        </hbox>
  	</window>
  </Widgets>";
  
  if (ischosen(pl_refVarVal.floatVal)){
    if(pl_refVarVal.floatVal != -1.0){
      vl_guiPre := vl_guiPre & "
      <externalvalue id='refVar' element='refVar' source='StatManagerTest'>";
      vl_guiPost := "</externalvalue>
      " & vl_guiPost;
      
      f_EPTF_Var_newFloat(c_StatManager_Test_dataElementRefVar, pl_refVarVal.floatVal, vl_refIdx);
    }
  }else{
    vl_guiPre := vl_guiPre & "
      <externalvalue id='refVar' element='refVar' source='StatManagerTest'>";
      vl_guiPost := "</externalvalue>
      " & vl_guiPost;
      
      f_EPTF_Var_newInt(c_StatManager_Test_dataElementRefVar, pl_refVarVal.intVal, vl_refIdx);
  }
  
  var charstring vl_gui := vl_guiPre & "<treecell>
  										<externaldata element='LEDlimit' source='StatManager' ptcname='StatManagerTest'>" &
            					 pl_params & 
            					 "</externaldata>
            					  </treecell>
            					  <treecell label='value0'/>"
            					 & vl_guiPost;
  
  f_EPTF_UIHandler_clearGUI();
  timer t_wait := 0.0;
  t_wait.start;
  alt{
    [v_ready]t_wait.timeout{}
  }
  f_EPTF_UIHandler_createGUI(vl_gui);

  t_wait.start(2.0);
  t_wait.timeout;
  
  pl_sourceIdx := vl_sourceIdx;
  pl_refIdx := vl_refIdx;  
}

group CheckExcpectedWarning {

  private function f_EPTF_StatManager_Test_ExpectedWarning_init() runs on EPTF_StatManager_Test_CT {
    if (v_StatManager_Test_expectedWarningInitialized) {
      return;
    }
    
    v_StatManager_Test_expectedWarningInitialized:=true;
    v_StatManager_Test_expectedWarningArrived := false;
    v_StatManager_Test_expectedWarningMsg:="";
    f_EPTF_Base_registerCleanup(refers(f_EPTF_StatManager_Test_ExpectedWarning_cleanup));  
    f_EPTF_Logging_registerPreambleFn(refers(f_EPTF_StatManager_Test_ExpectedWarning_checkExpectedWarning_preamble_FT));    
  }

  private function f_EPTF_StatManager_Test_ExpectedWarning_cleanup() runs on EPTF_StatManager_Test_CT {
    if (not v_StatManager_Test_expectedWarningInitialized) {
      return;
    }
    v_StatManager_Test_expectedWarningInitialized:=false;
    if (v_StatManager_Test_expectedWarningMsg != "" and not v_StatManager_Test_expectedWarningArrived) {
      setverdict(fail,"Expected warning message ",v_StatManager_Test_expectedWarningMsg," was not logged!");
    } else {
      setverdict(pass,"Expected warning message was logged");
    }
  }

  public function f_EPTF_StatManager_Test_ExpectedWarning_setExpectedWarning(in charstring pl_expectedWarningMsg) runs on EPTF_StatManager_Test_CT {
    if (pl_expectedWarningMsg=="") {
      return;
    }
    f_EPTF_StatManager_Test_ExpectedWarning_init();
    v_StatManager_Test_expectedWarningMsg := pl_expectedWarningMsg;
  }

  private function f_EPTF_StatManager_Test_ExpectedWarning_checkExpectedWarning_preamble_FT(in charstring pl_message) runs on EPTF_StatManager_Test_CT {
    if (not match(pl_message,pattern "*Warning*")) {
      return;
    }
    
    if (match(pl_message,pattern v_StatManager_Test_expectedWarningMsg)) {
      v_StatManager_Test_expectedWarningArrived := true;
    }
  }

} // group CheckExcpectedWarning

}//module
