///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_Base_Test_Testcases
//
// Purpose:
//   This module contains testcases for testing EPTF Base.
//
// Module Parameters:
//    -
//  Module depends on:
//    <EPTF_Base_Test_Definitions>
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_Base_Test_Functions>
//
// Current Owner:
//    Balazs Barcsik (ebalbar)
//
// Last Review Date:
//    -
//
//  Detailed Comments:
//    -
///////////////////////////////////////////////////////////
module EPTF_Base_Test_Testcases {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_Base_Test_Definitions all;
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_Base_Test_Functions all;
import from TCCEnv_Functions all;
import from EPTF_CLL_Common_Definitions all;
//=========================================================================
// Testcases
//=========================================================================
testcase tc_EPTF_Base_test() runs on D_CT {
  var A_CT v_A_CT := A_CT.create;
  var B_CT v_B_CT := B_CT.create;
  var C_CT v_C_CT := C_CT.create;
  
  v_A_CT.start(f_A_Behaviour("A"));
  v_B_CT.start(f_B_Behaviour("B"));
  v_C_CT.start(f_C_Behaviour("C"));
  
  f_D_Behaviour("D");
  setverdict(pass);
}


testcase tc_EPTF_Base_test_stopRemote() runs on D_CT {
  var A_CT v_A_CT := A_CT.create;
  
  v_A_CT.start(f_Comp_A_Behaviour("A"));
  
  setverdict(pass);
  f_Comp_D_Behaviour("D");
}


testcase tc_EPTF_Base_test_stopAll() runs on D_CT {
  var A_CT v_A_CT := A_CT.create;
  
  v_A_CT.start(f_Comp_A2_Behaviour("A"));
  
  f_Comp_D_Behaviour("D");
  setverdict(pass);
}



testcase tc_EPTF_Base_test_assert() runs on A_CT {
  //This testcase shall be run if EPTF_DEBUG defined
  if(c_EPTF_Common_debugSwitch==true) {
    action("NEGATIVE TEST!!! THE VERDICT OF THIS TESTCASE SHOULD BE PASS!");
    f_A_init_CT("Main");
    var A_CT v_A_CT := A_CT.create;

    v_A_CT.start(f_Comp_A3_Behaviour("A"));

    all component.done;
    setverdict(pass);
    action("NEGATIVE TEST!!! THE VERDICT OF THIS TESTCASE SHOULD BE PASS!");
  } 
  else {
    setverdict(inconc);
  }

}


testcase tc_EPTF_Base_test_deadlock() runs on C_DeadLock_CT {
  f_C_deadlock_init_CT("Main");
  
  var A_DeadLock_CT v_A_CT := A_DeadLock_CT.create;
  var B_CT v_B_CT := B_CT.create;
  connect(self:hello_PCO, v_A_CT:hello_PCO);
  v_A_CT.start(f_A_deadlock_Behaviour("A")); // this will not return;
  v_B_CT.start(f_Comp_B_Behaviour("B",mtc)); // this will stop mtc twice with stopRemote
  
  setverdict(pass);
  all component.done;
  f_EPTF_Base_cleanup_CT();
}


testcase tc_EPTF_Base_test_stopRemoteFromMTC() runs on A_CT {
  f_A_StopRemoteFromMTC_init_CT("Main");
  
  var A_CT v_A1_CT := A_CT.create;
  var A_CT v_A2_CT := A_CT.create;
  var A_CT v_A3_CT := A_CT.create;
  v_A1_CT.start(f_A_StopRemoteFromMTC_Behaviour("A1"));
  v_A2_CT.start(f_A_StopRemoteFromMTC_Behaviour("A2"));
  v_A3_CT.start(f_A_StopRemoteFromMTC_Behaviour("A3"));
  
  timer t_wait := 2.0;
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_stopRemote(v_A1_CT);
  
  t_wait.start; t_wait.timeout;
  f_EPTF_Base_stop(pass);
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_init_TC
// 
// Purpose:
//   to test f_EPTF_Base_init_CT() function
// 
// Requirement:
//   -
//
// Action:
//   - calls f_EPTF_Base_init_CT() function
//   - checks the selfname 
//   - create and start a test component
//   - calls f_EPTF_Base_cleanup_CT() function
//   - repeat the function calls again
//
// Expected Result:
//   - selfname is correct
//   - the init and cleanup functions of test component called in the expected order
//   - for the second time everything run properly
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_init_TC() runs on Test_CT {
  var charstring vl_selfname := "EPTF_Base_Test";

  f_EPTF_Base_init_CT(vl_selfname);
  if (f_EPTF_Base_selfName() == vl_selfname) {
    setverdict(pass);
  } else {
    log("Error: wrong selfname: ", f_EPTF_Base_selfName(), ", expected: ", vl_selfname);
    setverdict(fail);
  }
  var Test_CT vc_test := Test_CT.create("Test1");
  vc_test.start(f_A_behaviour());
  f_EPTF_Base_cleanup_CT();
  vl_selfname := "EPTF_Base_Test2";
  f_EPTF_Base_init_CT(vl_selfname);
  if (f_EPTF_Base_selfName() == vl_selfname) {
    setverdict(pass);
  } else {
    log("Error: wrong selfname: ", f_EPTF_Base_selfName(), ", expected: ", vl_selfname);
    setverdict(fail);
  }
  vc_test := Test_CT.create("Test2");
  vc_test.start(f_A_behaviour());
  f_EPTF_Base_cleanup_CT();
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_RegisterCleanup
// 
// Purpose:
//   to test f_EPTF_Base_registerCleanup() function
// 
// Requirement:
//   -
//
// Action:
//   - calls f_EPTF_Base_init_CT() function
//   - calls f_EPTF_Base_registerCleanup() function with a cleanup method
//   - calls f_EPTF_Base_registerCleanup() function again with another cleanup method
//   - calls f_EPTF_Base_cleanup_CT() function
//   - checks the cleanup methods calling order
//
// Expected Result:
//   - cleanup method registered second time called first
//   - both of the cleanup method called
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_RegisterCleanup() runs on Test_CT {

  f_EPTF_Base_init_CT("RegisterCleanup Test");
  f_EPTF_Base_registerCleanup(refers(f_cleanup1));
  f_EPTF_Base_registerCleanup(refers(f_cleanup2));
  f_EPTF_Base_cleanup_CT();
  if (v_cleanupOrder == 0) {
    setverdict(pass);
  } else {
    log("Error: one or more cleanup function not called");
    setverdict(fail);
  }

}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_cleanup_CT
// 
// Purpose:
//   to test f_EPTF_Base_cleanup_CT() function
// 
// Requirement:
//   -
//
// Action:
//   - calls f_EPTF_Base_init_CT() function
//   - create and start a test component
//   - calls f_EPTF_Base_cleanup_CT() function
//   - checks that f_EPTF_Base_cleanup_CT() stoped the running test component
//
// Expected Result:
//   - f_EPTF_Base_cleanup_CT() function stopes the running coponents
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_cleanup_CT()  runs on Test_CT {
  timer t;
  f_EPTF_Base_init_CT("Cleanup_CT Test");
  var Test_CT vc_test := Test_CT.create;
  vc_test.start(f_A_behaviour());
  f_EPTF_Base_cleanup_CT();
  t.start(3.0);
  alt {
    [] all component.done {
    setverdict(pass);  
      }
    [] t.timeout {
      log("Error: ptc not stopped remotely by mtc");
      setverdict(fail);  
    }
  }

}

function f_EPTF_Base_Test_getPid_behaviour(in integer pl_childId) runs on Test_CT {
  f_EPTF_Base_init_CT("CompInfoTest: "&int2str(pl_childId));
  action("Pid of component ", f_EPTF_Base_selfName(), ": ", f_EPTF_Base_getPid());
  var integer vl_pidOfMtc := f_EPTF_Base_getPidByName("CompInfoTest: 0");
  action("Pid of MTC (CompInfoTest): ", vl_pidOfMtc);
  
  // check mtc pid (get it again):
  if (vl_pidOfMtc!=f_EPTF_Base_getPidByName("CompInfoTest: 0")) {
    setverdict(fail, "Pid of component ""CompInfoTest"" does not match with the value requested from mtc before");
  }
  
  // check my pid:
  if (f_EPTF_Base_getPid()!=f_EPTF_Base_getPidByName(f_EPTF_Base_selfName())) {
    setverdict(fail, "Pid of component "&f_EPTF_Base_selfName()& " does not match with the value requested from mtc");
  }
  // check my pid again:
  if (f_EPTF_Base_getPid()!=f_EPTF_Base_getPidByName(f_EPTF_Base_selfName())) {
    setverdict(fail, "Pid of component "&f_EPTF_Base_selfName()& " does not match with the value requested from mtc");
  }
  
  timer t_wait;
  t_wait.start(0.5);
  t_wait.timeout;//if pl_childId==1  wait for the 3rd component to be created
  
  var EPTF_Base_ComponentInfoList vl_compInfoList := f_EPTF_Base_getComponentInfoAll();
  action("List of all components: ",vl_compInfoList);
  
  if (sizeof(vl_compInfoList)!=3) {
    setverdict(fail, "The returned list of all components is missing items. There should be 3. The list of components: "&log2str(vl_compInfoList));
  }
  
  f_EPTF_Base_wait4Shutdown();
}


type component GetPidParallelTest_CT extends Test_CT {
  timer t_getPid1 := 0.0;
  timer t_getPid2 := 0.0;
  timer t_trigger := 0.1;
}

altstep as_EPTF_Base_Test_getPidParallel_handler(timer t_event) runs on GetPidParallelTest_CT {
  [] t_trigger.timeout {
    t_trigger.start;
    repeat;
  }
  [] t_event.timeout {
    action("*********Start******");
    action("Getting pid for ",t_event,": ",f_EPTF_Base_getPidByName("CompInfoTest: 0"));
    action("*********Done ******");
    repeat;
  }
}

function f_EPTF_Base_Test_getPidParallel_behaviour(in integer pl_childId) runs on GetPidParallelTest_CT {
  f_EPTF_Base_init_CT("CompInfoTest: "&int2str(pl_childId));
  activate(as_EPTF_Base_Test_getPidParallel_handler(t_getPid1));
  activate(as_EPTF_Base_Test_getPidParallel_handler(t_getPid2));
  t_getPid1.start;
  t_getPid2.start;
  t_trigger.start;
  
  
  // wait because otherwise the handlers above will not send requests
  timer t_wait := 2.0;
  t_wait.start; t_wait.timeout;
  var EPTF_Base_ComponentInfoList vl_compInfoList := f_EPTF_Base_getComponentInfoAll();
  action("List of all components: ",vl_compInfoList);
  if (sizeof(vl_compInfoList)!=3) {
    setverdict(fail, "The returned list of all components is missing items. There should be 3. The list of components: "&log2str(vl_compInfoList));
  }
  
  if (vl_compInfoList[0].compRef!=mtc) {
      setverdict(fail,"Component ref for "&vl_compInfoList[0].selfName&" is not correct: "&log2str(vl_compInfoList[0].compRef)&" Expected value: mtc");
  }
  
  for(var integer i:=1; i<sizeof(vl_compInfoList); i:=i+1) {
    if (f_EPTF_Base_upcast(vl_compInfoList[i].compRef)!=f_EPTF_Base_upcast(self)+i-2) {
      setverdict(fail,"Component ref for "&vl_compInfoList[i].selfName&" is not correct: "&log2str(vl_compInfoList[i].compRef)&" Expected value: "&int2str(f_EPTF_Base_upcast(self)+i-2));
    }
  }
  f_EPTF_Base_wait4Shutdown();
}


///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_getComponentInfo
// 
// Purpose:
//   to test component info functions
// 
// Requirement:
//   -
//
// Expected Result:
//   -
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_getComponentInfo() runs on Test_CT {
  f_EPTF_Base_init_CT("CompInfoTest: 0");
  
  var Test_CT vl_child := Test_CT.create;
  vl_child.start(f_EPTF_Base_Test_getPid_behaviour(1));
  
  timer t_wait;
  t_wait.start(0.2); t_wait.timeout; // wait to start, order will be always the same

  var GetPidParallelTest_CT vl_child2 := GetPidParallelTest_CT.create;
  vl_child2.start(f_EPTF_Base_Test_getPidParallel_behaviour(2));
  
  action("Pid of component ", f_EPTF_Base_selfName(), ": ", f_EPTF_Base_getPid());

  // wait for hello msgs:
  t_wait.start(1.0); t_wait.timeout;
  action("Pid of PTC (CompInfoTest: 1): ", f_EPTF_Base_getPidByName("CompInfoTest: 1"));
  action("Pid of PTC (CompInfoTest: 2): ", f_EPTF_Base_getPidByName("CompInfoTest: 2"));

  // check my pid:
  if (f_EPTF_Base_getPid()!=f_EPTF_Base_getPidByName(f_EPTF_Base_selfName())) {
    setverdict(fail, "Pid of component "&f_EPTF_Base_selfName()& " does not match with the value requested from mtc");
  }

  // check nonexistent PTC pid:
  if (-1!=f_EPTF_Base_getPidByName("NonExistentComponent")) {
    setverdict(fail, "The function f_EPTF_Base_getPidByName should return -1 for nonexistent component");
  }

  // check my component info:
  var EPTF_Base_ComponentInfo vl_compInfo;
  var template EPTF_Base_ComponentInfo vl_compInfoExpected := {
    selfName := f_EPTF_Base_selfName(),
    compRef := self,
    hostName := f_EPTF_Base_getHostName(),
    pid := f_EPTF_Base_getPid()
  };
  if(0!=f_EPTF_Base_getComponentInfoByName(f_EPTF_Base_selfName(), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&f_EPTF_Base_selfName()&")");
  };
  // selfName
  if (not match(vl_compInfo,vl_compInfoExpected)) {
    setverdict(fail, "CompInfo of component "&valueof(vl_compInfoExpected.selfName)&" requested from MTC: "&log2str(vl_compInfo)&
      " does not match with the expected value: "&log2str(vl_compInfoExpected));
  }
  
  // check remote component info:
  //child:
  vl_compInfoExpected := {
    selfName := "CompInfoTest: 1",
    compRef := vl_child,
    hostName := f_EPTF_Base_getHostName(),
    pid := ?
  };
  if(0!=f_EPTF_Base_getComponentInfoByName(valueof(vl_compInfoExpected.selfName), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&valueof(vl_compInfoExpected.selfName)&")");
  };
  action("vl_compInfo: ", vl_compInfo);
  action("vl_compInfoExpected: ", vl_compInfoExpected);
  if (not match(vl_compInfo,vl_compInfoExpected)) {
    setverdict(fail, "CompInfo of component "&valueof(vl_compInfoExpected.selfName)&" requested from MTC: "&log2str(vl_compInfo)&
      " does not match with the expected value: "&log2str(vl_compInfoExpected));
  }
  //child2:
  vl_compInfoExpected := {
    selfName := "CompInfoTest: 2",
    compRef := vl_child2,
    hostName := f_EPTF_Base_getHostName(),
    pid := ?
  };
  if(0!=f_EPTF_Base_getComponentInfoByName(valueof(vl_compInfoExpected.selfName), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&valueof(vl_compInfoExpected.selfName)&")");
  };
  action("vl_compInfo: ", vl_compInfo);
  action("vl_compInfoExpected: ", vl_compInfoExpected);
  if (not match(vl_compInfo,vl_compInfoExpected)) {
    setverdict(fail, "CompInfo of component "&valueof(vl_compInfoExpected.selfName)&" requested from MTC: "&log2str(vl_compInfo)&
      " does not match with the expected value: "&log2str(vl_compInfoExpected));
  }
  // nonexistent PTC:
  vl_compInfoExpected := {
    selfName := "NonExistentComponent",
    compRef := null,
    hostName := "",
    pid := ?
  };
  if(0==f_EPTF_Base_getComponentInfoByName(valueof(vl_compInfoExpected.selfName), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&valueof(vl_compInfoExpected.selfName)&")");
  };
  
  
  // check list of all components:
  var EPTF_Base_ComponentInfoList vl_compInfoList := f_EPTF_Base_getComponentInfoAll();
  var template EPTF_Base_ComponentInfoList vl_compInfoListExpected := {
    {
      selfName := f_EPTF_Base_selfName(),
      compRef := self,
      hostName := f_EPTF_Base_getHostName(),
      pid := f_EPTF_Base_getPid()
    },
    {
      selfName := "CompInfoTest: 1",
      compRef := vl_child,
      hostName := f_EPTF_Base_getHostName(),
      pid := ?
    },
    {
      selfName := "CompInfoTest: 2",
      compRef := vl_child2,
      hostName := f_EPTF_Base_getHostName(),
      pid := ?
    }
  };
  action("vl_compInfoList: ", vl_compInfoList);
  action("vl_compInfoListExpected: ", vl_compInfoListExpected);
  if (not match(vl_compInfoList,vl_compInfoListExpected)) {
    setverdict(fail, "CompInfoList requested from MTC: "&log2str(vl_compInfoList)&
      " does not match with the expected value: "&log2str(vl_compInfoListExpected));
  }
  

  t_wait.start(20.0); t_wait.timeout;
  f_EPTF_Base_stop(pass);
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_getComponentInfo_TwoPTCsWithSameName
// 
// Purpose:
//   to test component info functions if more than one PTC exists with the same selfName
// 
// Requirement:
//   -
//
// Expected Result:
//   -
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_getComponentInfo_TwoPTCsWithSameName() runs on Test_CT {
  f_EPTF_Base_init_CT("CompInfoTest: 0");
  
  var Test_CT vl_child := Test_CT.create;
  vl_child.start(f_EPTF_Base_Test_getPid_behaviour(1));

  timer t_wait;
  t_wait.start(0.2); t_wait.timeout; // wait to start, order will be always the same
  
  var GetPidParallelTest_CT vl_child2 := GetPidParallelTest_CT.create;
  vl_child2.start(f_EPTF_Base_Test_getPid_behaviour(1)); // start component with same name
  
  action("Pid of component ", f_EPTF_Base_selfName(), ": ", f_EPTF_Base_getPid());

  // wait for hello msgs:
  t_wait.start(1.0); t_wait.timeout;
  action("Pid of PTC (CompInfoTest: 1): ", f_EPTF_Base_getPidByName("CompInfoTest: 1"));

  // check my pid:
  if (f_EPTF_Base_getPid()!=f_EPTF_Base_getPidByName(f_EPTF_Base_selfName())) {
    setverdict(fail, "Pid of component "&f_EPTF_Base_selfName()& " does not match with the value requested from mtc");
  }

  // check my component info:
  var EPTF_Base_ComponentInfo vl_compInfo;
  var template EPTF_Base_ComponentInfo vl_compInfoExpected := {
    selfName := f_EPTF_Base_selfName(),
    compRef := self,
    hostName := f_EPTF_Base_getHostName(),
    pid := f_EPTF_Base_getPid()
  };
  if(0!=f_EPTF_Base_getComponentInfoByName(f_EPTF_Base_selfName(), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&f_EPTF_Base_selfName()&")");
  };
  // selfName
  if (not match(vl_compInfo,vl_compInfoExpected)) {
    setverdict(fail, "CompInfo of component "&valueof(vl_compInfoExpected.selfName)&" requested from MTC: "&log2str(vl_compInfo)&
      " does not match with the expected value: "&log2str(vl_compInfoExpected));
  }
  
  // check remote component info:
  //child:
  vl_compInfoExpected := {
    selfName := "CompInfoTest: 1",
    compRef := vl_child,
    hostName := f_EPTF_Base_getHostName(),
    pid := ?
  };
  if(0!=f_EPTF_Base_getComponentInfoByName(valueof(vl_compInfoExpected.selfName), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&valueof(vl_compInfoExpected.selfName)&")");
  };
  action("vl_compInfo: ", vl_compInfo);
  action("vl_compInfoExpected: ", vl_compInfoExpected);
  if (not match(vl_compInfo,vl_compInfoExpected)) {
    setverdict(fail, "CompInfo of component "&valueof(vl_compInfoExpected.selfName)&" requested from MTC: "&log2str(vl_compInfo)&
      " does not match with the expected value: "&log2str(vl_compInfoExpected));
  }

  // check list of all components:
  var EPTF_Base_ComponentInfoList vl_compInfoList := f_EPTF_Base_getComponentInfoAll();
  var template EPTF_Base_ComponentInfoList vl_compInfoListExpected := {
    {
      selfName := f_EPTF_Base_selfName(),
      compRef := self,
      hostName := f_EPTF_Base_getHostName(),
      pid := f_EPTF_Base_getPid()
    },
    {
      selfName := "CompInfoTest: 1",
      compRef := vl_child,
      hostName := f_EPTF_Base_getHostName(),
      pid := ?
    },
    {
      selfName := "CompInfoTest: 1",
      compRef := vl_child2,
      hostName := f_EPTF_Base_getHostName(),
      pid := ?
    }
  };
  action("vl_compInfoList: ", vl_compInfoList);
  action("vl_compInfoListExpected: ", vl_compInfoListExpected);
  if (not match(vl_compInfoList,vl_compInfoListExpected)) {
    setverdict(fail, "CompInfoList requested from MTC: "&log2str(vl_compInfoList)&
      " does not match with the expected value: "&log2str(vl_compInfoListExpected));
  }
  t_wait.start(20.0); t_wait.timeout;
  f_EPTF_Base_stop(pass);
}


///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_getComponentInfo_returnValueUsage
// 
// Purpose:
//   Check if the component reference returned by f_EPTF_Base_getComponentInfoByName
//   can be assigned to EPTF_Base_CT type variable
// 
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_getComponentInfo_returnValueUsage() runs on Test_CT {
  f_EPTF_Base_init_CT("ComponentInfo_returnValueUsage");

  var EPTF_Base_ComponentInfo vl_compInfo;
  if(0!=f_EPTF_Base_getComponentInfoByName(f_EPTF_Base_selfName(), vl_compInfo)) {
    setverdict(fail,"Invalid return code for getComponentInfoByName("&f_EPTF_Base_selfName()&")");
  };
  var EPTF_Base_CT vl_compRef := vl_compInfo.compRef; // this should compile
  
  if (vl_compRef!=self) {
    setverdict(fail,"Wrong result returned by f_EPTF_Base_getComponentInfoByName: ", vl_compRef, ". Should be: ", self);
  } else {  
    setverdict(pass);
  }
  f_EPTF_Base_stop(none);
}


// dummy check-condition funtion which returns true after 1 sec is passed after the component was started
function f_EPTF_Base_Test_waitForCondition_checkConditionFn() runs on Test_CT return boolean {
  if (f_EPTF_Base_getRelTimeInSecs()>1.0) {
    return true;
  } else {  
    return false;
  }
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_waitForCondition_maxWaitTime
// 
// Purpose:
//   Check if the <f_EPTF_Base_waitForCondition> exits after desired time
//   with good return value if the maximal wait time expires;
//   No checkCondition func ref is specified.
// 
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_waitForCondition_maxWaitTime() runs on Test_CT {
  f_EPTF_Base_init_CT("waitForCondition_maxWaitTime");
  
  const float c_maxTime := 3.14;
  timer t_check :=c_maxTime*2.0;
  t_check.start;
  if(false != f_EPTF_Base_waitForCondition(null, c_maxTime)) {
    setverdict(fail,"Max wait time expired for f_EPTF_Base_waitForCondition, but it returned true");
  }
  var float vl_endTime := t_check.read;
  if ((vl_endTime-c_maxTime)*(vl_endTime-c_maxTime)/(c_maxTime*c_maxTime)>0.1) {
    setverdict(fail,"f_EPTF_Base_waitForCondition exit time is ",vl_endTime, "but it should be ",c_maxTime);
  }
  f_EPTF_Base_stop(pass);

}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_waitForCondition_maxWaitTime_withFn
// 
// Purpose:
//   Check if the <f_EPTF_Base_waitForCondition> exits after desired time
//   with good return value if the maximal wait time expires;
//   The checkCondition fn was specified
// 
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_waitForCondition_maxWaitTime_withFn() runs on Test_CT {
  f_EPTF_Base_init_CT("waitForCondition_maxWaitTime_withFn");
  
  const float c_maxTime := 0.314;
  timer t_check :=c_maxTime*2.0;
  t_check.start;
  if(false != f_EPTF_Base_waitForCondition(refers(f_EPTF_Base_Test_waitForCondition_checkConditionFn), c_maxTime)) {
    setverdict(fail,"Max wait time expired for f_EPTF_Base_waitForCondition, but it returned true");
  }
  var float vl_endTime := t_check.read;
  if ((vl_endTime-c_maxTime)*(vl_endTime-c_maxTime)/(c_maxTime*c_maxTime)>0.1) {
    setverdict(fail,"f_EPTF_Base_waitForCondition exit time is ",vl_endTime, "but it should be ",c_maxTime);
  }
  f_EPTF_Base_stop(pass);

}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_waitForCondition_withFn
// 
// Purpose:
//   Check if the <f_EPTF_Base_waitForCondition> exits after desired time
//   with good return value if the condition is fulfilled;
//   No max wait time is given
// 
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_waitForCondition_withFn() runs on Test_CT {
  f_EPTF_Base_init_CT("waitForCondition_withFn");
  
  const float c_maxTime := 1.0;
  timer t_check :=c_maxTime*2.0;
  t_check.start;
  if(true != f_EPTF_Base_waitForCondition(refers(f_EPTF_Base_Test_waitForCondition_checkConditionFn))) {
    setverdict(fail,"f_EPTF_Base_waitForCondition should return true");
  }
  var float vl_endTime := t_check.read;
  if ((vl_endTime-c_maxTime)*(vl_endTime-c_maxTime)/(c_maxTime*c_maxTime)>0.1) {
    setverdict(fail,"f_EPTF_Base_waitForCondition exit time is ",vl_endTime, "but it should be ",c_maxTime);
  }
  f_EPTF_Base_stop(pass);

}

type component Test_waitForCondition_CT extends Test_CT {
  timer t_event := 3.0;
}

altstep as_EPTF_Base_Test_waitForCondition_terminate() runs on Test_waitForCondition_CT {
  [] t_event.timeout;
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_waitForCondition_infinity
// 
// Purpose:
//   Check if the <f_EPTF_Base_waitForCondition> exits
//   with good return value if the alt is terminated by
//   an activated default altstep;
//   No max wait time is given, no func ref is given => would waiting forever
// 
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_waitForCondition_infinity() runs on Test_waitForCondition_CT {
  f_EPTF_Base_init_CT("waitForCondition_infinity");
  
  t_event.start;
  
  // this alt exits from the waiting alt:
  activate(as_EPTF_Base_Test_waitForCondition_terminate());
  
  if(false != f_EPTF_Base_waitForCondition()) {
    setverdict(fail,"f_EPTF_Base_waitForCondition should return false");
  }
  f_EPTF_Base_stop(pass);

}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_getStartCommand
//
// Purpose:
//   Check if the <f_EPTF_Base_getStartCommand> exits
//   with good return value
//
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_getStartCommand() runs on Test_CT {
  f_EPTF_Base_init_CT("getStartCommand");

  var charstring vl_stdOut := "";
  var charstring vl_stdErr := "";
  var charstring vl_command := "which compiler";
  var charstring vl_ttcn3Dir := "";

  //var charstring vl_ttcn3Dir := regexp(vl_stdOut,"(*)(compiler)(*)",0);
  vl_ttcn3Dir := f_GetEnv("TTCN3_DIR");
  if(lengthof(vl_ttcn3Dir) < 1){
    setverdict(inconc,"The value of TTCN3_DIR environment variable: ",vl_ttcn3Dir," is wrong.");
    f_EPTF_Base_stop(none);
  }

  var charstring vl_expectedExecutable := "EPTF_Base_Test";
  var charstring vl_expectedConfigFile := "EPTF_Base_Test.cfg";
  // var EPTF_CharstringList vl_expectedTestCase := {}; //when control part is executed empty is OK, if TC manually started: {%moduleId&"."&%testcaseId}

  // when it stared with default config, no config file string at the end of the start command
  var charstring vl_pattern := "("&vl_ttcn3Dir&"[/]#(1,)bin/ttcn3_start "&vl_expectedExecutable&"*"&"["&vl_expectedConfigFile&"]#(0,1)"&"*)";
  var charstring vl_executable := "";
  var charstring vl_configFile := "";
  var EPTF_CharstringList vl_testCases := {};
  var charstring vl_startCommand := f_EPTF_Base_getStartCommand(vl_executable, vl_configFile, vl_testCases);
  var charstring vl_expectedResult := regexp(vl_startCommand,vl_pattern,0);

  if(vl_expectedResult != vl_startCommand) {
    setverdict(fail,"Return value of: f_EPTF_Base_getStartCommand is wrong: ", vl_startCommand,". It should be: ", vl_expectedResult);
    f_EPTF_Base_stop(none);
  }
  if(vl_expectedExecutable != vl_executable) {
    setverdict(fail,"Returned executable of: f_EPTF_Base_getStartCommand is wrong: ", vl_executable,". It should be: ", vl_expectedExecutable);
    f_EPTF_Base_stop(none);
  }
  if(not match(vl_configFile, pattern "*"&vl_expectedConfigFile)) {
    setverdict(fail,"Returned config file of: f_EPTF_Base_getStartCommand: ", vl_configFile," does not match with pattern: ", "*"&vl_expectedConfigFile);
    f_EPTF_Base_stop(none);
  }
  // not checked see the detail above --> vl_expectedTestCase
  /*
  if(vl_expectedTestCase != vl_testCases) {
    setverdict(fail,"Returned test case of: f_EPTF_Base_getStartCommand is wrong: ", vl_testCases,". It should be: ", vl_expectedTestCase);
    f_EPTF_Base_stop(none);
  }
  */
  f_EPTF_Base_stop(pass);
}

///////////////////////////////////////////////////////////
// Testcase: tc_EPTF_Base_Test_executeShell
//
// Purpose:
//   Check if the <f_EPTF_Base_executeShell> exits
//   with good return values and out parameters
//
// Requirement:
//   -
//
// Expected Result:
//   pass verdict
///////////////////////////////////////////////////////////
testcase tc_EPTF_Base_Test_executeShell() runs on Test_CT {
  f_EPTF_Base_init_CT("executeShell");

  var charstring vl_stdOut := "";
  var charstring vl_stdErr := "";
  var charstring vl_command := "";
  var integer vl_errorCode := 0;
  var charstring vl_str := "abcd";

  // false command with stderr redirected to /dev/null: empty stdout, empty on stderr, error code not zero
  vl_command := "bash -c \""&vl_str&" 2>/dev/null\"";
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) != 0){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode == 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should not be zero.");
    f_EPTF_Base_stop(none)
  }

  // echo command with stdout redirected to /dev/null: empty on stdout, empty on stderr, error code zero
  vl_command := "bash -c \"echo "&vl_str&" 1>/dev/null\"";
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) != 0){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }

  // false command: empty stdout, error message on stderr, error code not zero
  vl_command := vl_str;
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) != 0){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) == 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should not be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode == 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should not be zero.");
    f_EPTF_Base_stop(none)
  }

  // echo command: vl_str on stdout, empty on stderr, error code zero
  vl_command := "echo "&vl_str;
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(vl_stdOut != vl_str&"\n"){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should be:",vl_str&"\n.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }

  // false commands with stderr redirected to stdout: vl_str on stdout and stderr, error code not zero
  vl_command := "bash -c \""&vl_str&";"&vl_str&" 2>&1\"";
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) == 0){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should not be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) == 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should not be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode == 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should not be zero.");
    f_EPTF_Base_stop(none)
  }

  // echo commands with stdout redirected to stderr: vl_str on stdout and stderr, error code 0
  vl_command := "bash -c \"echo "&vl_str&"; echo "&vl_str&" 1>&2\"";
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(vl_stdOut != vl_str&"\n"){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should be:",vl_str&"\n.");
    f_EPTF_Base_stop(none)
  }
  if(vl_stdErr != vl_str&"\n"){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be:",vl_str&"\n.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 0){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }

  // exit command: empty stdout, empty stderr, error code vl_exitCode
  var integer vl_exitCode := 9;
  vl_command := "exit "&int2str(vl_exitCode);
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) != 0){
    setverdict(fail,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(fail,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 9){
    setverdict(fail,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be: "&int2str(vl_exitCode)&".");
    f_EPTF_Base_stop(none)
  }

  // read a file which is bigger than the buffer (65534): file content in stdout, empty stderr, error code zero
  vl_command := "wc -c <"&%moduleId&".cc"; // it refers to EPTF_Base_Test_Testcases.cc which size is bigger than the buffer
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) == 0){
    setverdict(inconc,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should not be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(inconc,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 0){
    setverdict(inconc,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }
  var integer vl_numOfBytes := str2int(substr(vl_stdOut, 0, lengthof(vl_stdOut)-1)); // remove "\n" from the end of string
  vl_command := "cat "&%moduleId&".cc";
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(lengthof(vl_stdOut) == 0){
    setverdict(inconc,"Returned stdOut is wrong for command ",vl_command,":",vl_stdOut,". It should not be empty.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdErr) != 0){
    setverdict(inconc,"Returned stdErr is wrong for command ",vl_command,":",vl_stdErr,". It should be empty.");
    f_EPTF_Base_stop(none)
  }
  if(vl_errorCode != 0){
    setverdict(inconc,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }
  if(lengthof(vl_stdOut) != vl_numOfBytes){ // checks whether everything is read
    setverdict(fail,"Returned stdout's length is wrong for command ",vl_command,":",lengthof(vl_stdOut),". It should be: "&int2str(vl_numOfBytes)&".");
    f_EPTF_Base_stop(none)
  }

  // during the execution of f_EPTF_Base_executeShell the default altsteps enabled
  vl_command := "sleep 5";
  var default dl_timeout := activate(as_EPTF_Base_Test_timeout());
  t_wait1.start(3.0);
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr);
  if(vl_errorCode != 0){
    setverdict(inconc,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }
  if(v_counter1 != 1){
    setverdict(fail,"Default alt step did not work during the execution of "&%definitionId&".");
    f_EPTF_Base_stop(none);
  }
  deactivate(dl_timeout);

  // during the execution of f_EPTF_Base_executeShell the default altsteps disabled
  v_counter1 := 0;
  vl_command := "sleep 5";
  dl_timeout := activate(as_EPTF_Base_Test_timeout());
  t_wait1.start(3.0);
  vl_errorCode := f_EPTF_Base_executeShell(vl_command, vl_stdOut, vl_stdErr, false);
  if(vl_errorCode != 0){
    setverdict(inconc,"Returned error code is wrong for command ",vl_command,":",vl_errorCode,". It should be zero.");
    f_EPTF_Base_stop(none)
  }
  if(v_counter1 != 0){
    setverdict(fail,"Default alt step worked during the execution of "&%definitionId&".");
    f_EPTF_Base_stop(none);
  }
  deactivate(dl_timeout);

  f_EPTF_Base_stop(pass);
}

//=========================================================================
// Control
//=========================================================================
control {
  select (f_GetEnv("TTCN_TEST_TYPE")) {
    case ("SMOKE") {
	  execute(tc_EPTF_Base_test());
	  execute(tc_EPTF_Base_test_stopRemote());
	  execute(tc_EPTF_Base_test_stopAll());
	  execute(tc_EPTF_Base_test_assert());
	  execute(tc_EPTF_Base_test_deadlock());
	  execute(tc_EPTF_Base_test_stopRemoteFromMTC());
	  execute(tc_EPTF_Base_Test_init_TC());
	  execute(tc_EPTF_Base_Test_RegisterCleanup());
	  execute(tc_EPTF_Base_Test_cleanup_CT());
          execute(tc_EPTF_Base_Test_getComponentInfo());
          execute(tc_EPTF_Base_Test_getComponentInfo_TwoPTCsWithSameName());
          execute(tc_EPTF_Base_Test_getComponentInfo_returnValueUsage());
          execute(tc_EPTF_Base_Test_waitForCondition_maxWaitTime());
          execute(tc_EPTF_Base_Test_waitForCondition_maxWaitTime_withFn());
          execute(tc_EPTF_Base_Test_waitForCondition_withFn());
          execute(tc_EPTF_Base_Test_waitForCondition_infinity());
	}
	case else {
	  execute(tc_EPTF_Base_test());
	  execute(tc_EPTF_Base_test_stopRemote());
	  execute(tc_EPTF_Base_test_stopAll());
	  execute(tc_EPTF_Base_test_assert());
	  execute(tc_EPTF_Base_test_deadlock());
	  execute(tc_EPTF_Base_test_stopRemoteFromMTC());
	  execute(tc_EPTF_Base_Test_init_TC());
	  execute(tc_EPTF_Base_Test_RegisterCleanup());
	  execute(tc_EPTF_Base_Test_cleanup_CT());
          execute(tc_EPTF_Base_Test_getComponentInfo());
          execute(tc_EPTF_Base_Test_getComponentInfo_TwoPTCsWithSameName());
          execute(tc_EPTF_Base_Test_getComponentInfo_returnValueUsage());
          execute(tc_EPTF_Base_Test_waitForCondition_maxWaitTime());
          execute(tc_EPTF_Base_Test_waitForCondition_maxWaitTime_withFn());
          execute(tc_EPTF_Base_Test_waitForCondition_withFn());
          execute(tc_EPTF_Base_Test_waitForCondition_infinity());
          execute(tc_EPTF_Base_Test_getStartCommand());
          execute(tc_EPTF_Base_Test_executeShell());
    }
  }
}
}
