///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatHandler_ExternalFunctions.cc
// 
//  Purpose:
//    Implementation of the external functions of EPTF StatHandler
// 
///////////////////////////////////////////////////////////////

#include "EPTF_CLL_StatHandler_Functions.hh"

///////////////////////////////////////////////////////////
//  Function: f__EPTF__StatHandler__downcast
// 
//  Purpose:
//    Implementation of the external function <f_EPTF_StatHandler_downcast>.
///////////////////////////////////////////////////////////
EPTF__CLL__StatHandler__Definitions::EPTF__StatHandler__CT EPTF__CLL__StatHandler__Functions::f__EPTF__StatHandler__downcast(const EPTF__CLL__Base__Definitions::EPTF__Base__CT& pl__baseCompRef) {
  return EPTF__CLL__StatHandler__Definitions::EPTF__StatHandler__CT((component)pl__baseCompRef);
}

