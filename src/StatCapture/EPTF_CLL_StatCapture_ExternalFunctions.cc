///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include "EPTF_CLL_StatCapture_Functions.hh"

namespace EPTF__CLL__StatCapture__Functions
{


/* Function: f__EPTF__Stat__Capture__get__string__size

   Returns the length of the string in bytes.

   Returns:
   The length of the string in bytes.  
INTEGER f__EPTF__Stat__Capture__get__string__size (const CHARSTRING& pl_logstring)
{
  return INTEGER (strlen(pl_logstring));
}
*/

/* Function: f__EPTF__StatCapture__get__time

   Returns the actual time and date as a string

   Returns:
   The actual time and date as a string.  */
CHARSTRING f__EPTF__StatCapture__get__time (const CHARSTRING& formatstring = "%Y-%m-%d_%H.%M.%S", const BOOLEAN& milisec = false) //filename format
{
  struct timeval tv;
  gettimeofday(&tv,NULL);
  TTCN_Buffer buffer;
  unsigned char *ptr2;
  size_t buff_size=256;
  size_t data_size;
  buffer.get_end(ptr2,buff_size);
  //float msec = tv.tv_usec/1000000.0; 
  char *ptr=(char *)ptr2;
  
  //char msecstr[10] = "";
  //sprintf(msecstr, "%f.6", msec); 
  
  time_t tim=time(NULL);
  tm *now=localtime(&tim);
  // setlocale (LC_ALL, "");
  data_size=strftime(ptr, buff_size, formatstring, now);

  if (milisec) {
    data_size+=sprintf(ptr+data_size, ".%d", (int) tv.tv_usec);
  }
  buffer.increase_length(data_size);
  CHARSTRING ret_val;
  buffer.get_string(ret_val);
  return ret_val;
   
   
}

}
