///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
// Clear XML namespace elements from source
// -----------------------------------------
// For validation because if its contain namespace
// elements the validation produce a "Element 'Widgets':
// No matching global declaration available for
// the validation root." error.

//=========================================================================
// DEFINES: setup different versions
//=========================================================================
// Version 3.48: change check the nested existence of iterator id {7.950767 / 1.185142}
#define VERSION_3 3.48

// Clear namespace elements from input
#define CLEAR_XML_NAMESPACE_ELEMENTS 1

// All DEBUG Actions enable or disabled
//#define KI_DEBUG_ACTIONS 1

// Use time measure
//#define USE_TIME_MEASURE 1

// Print memory usage
//#define PRINT_USED_MEMORY 1

// With XML check
// #define WITH_XML_CHECK 1


#include "Universal_charstring.hh"
#include <sys/types.h>
#include <stdio.h>
#include <sstream>
#include "rapidxml.hh"
#include <algorithm>
///////////////////////////////////////////////////////////////////////////////
// Override rapidxml: print [rapidxml_print.hpp:copy_and_expand_chars]
//                          for no conversion of <>'"& characters
///////////////////////////////////////////////////////////////////////////////
namespace rapidxml
{
    ///////////////////////////////////////////////////////////////////////
    // Internal
    //! \cond internal
    namespace internal
    {
        ///////////////////////////////////////////////////////////////////////////
        // Internal character operations
        // Copy characters from given range to given output iterator and expand
        // characters into references (&lt; &gt; &apos; &quot; &amp;)
        template<class OutIt>
        inline OutIt copy_and_expand_chars(const char *begin, const char *end, char noexpand, OutIt out)
        {
            while (begin != end)
                *out++ = *begin++;
            return out;
        }
    }
}
///////////////////////////////////////////////////////////////////////////////
#include "rapidxml_print.hh"
#include "EPTF_CLL_UIHandler_GUIFunctions.hh"
#include "EPTF_CLL_DataSource_Definitions.hh"
#include "EPTF_CLL_DataSource_Functions.hh"
#include "EPTF_CLL_UIHandler_Definitions.hh"
#include "EPTF_CLL_UIHandler_WidgetFunctions.hh"
#include "EPTF_CLL_Variable_Definitions.hh"
#include "ttcn_ericsson_se_protocolModules_xtdp_xtdl.hh"

#include <libxml/parser.h>
#include <libxml/xmlschemas.h>
#include <string.h>

// ACTION macros ->
#ifdef KI_DEBUG_ACTIONS
#define ACTION_GUIFUNCTIONS(txt, args...) TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, txt, ## args);
#else
#define ACTION_GUIFUNCTIONS(txt, args...) no_action_in_guifunctions(txt);
#endif
#define ACTION_GUIFUNCTIONS_PERMANENT(txt, args...) TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, txt, ## args); // no_action_in_guifunctions(txt);
//#define ACTION_GUIFUNCTIONS_PERMANENT(txt, args...) no_action_in_guifunctions(txt);
#define NO_ACTION_GUIFUNCTIONS(txt, args...) no_action_in_guifunctions(txt);

#ifdef USE_TIME_MEASURE
#define MB_OF_MEASURES  12
#define START_MEASURE(pl_idx, pl_str) start_timer(&v_timer_measure_start[pl_idx], &v_tv_measure[pl_idx]); strcpy(&v_time_measure_str[pl_idx][0],pl_str);
#define STOP_MEASURE(pl_idx) stop_timer(&v_timer_measure_start[pl_idx], &v_tv_measure[pl_idx], &v_time_measure_diff_summ[pl_idx]);
#else
#define START_MEASURE(pl_idx, pl_str) ;
#define STOP_MEASURE(pl_idx) ;
#endif

#define MAX_RESERVED_ITVALUES 2048
#define MAX_RESERVED_REPLACE 1024
#define MAX_RESERVED_ESCAPE 2048
#define MAX_RESERVED_PARSE 1024

using namespace rapidxml;
using namespace internal;

using namespace std;
using namespace EPTF__CLL__DataSource__Functions;
using namespace EPTF__CLL__UIHandler__WidgetFunctions;
using namespace EPTF__CLL__Variable__Definitions;

namespace EPTF__CLL__UIHandler__WidgetFunctions {

//=========================================================================
// External Variables [TTCN-3]
//=========================================================================
// EPTF_CLL_UIHandler_Definitions:
// -------------------------------
// v_UIHandler_GUIFunctions_nofRunning_processExternalData_handlers
// - number of running processes counter used waiting for all process done
// -------------------------------
// v_EPTF_UIHandler_WidgetFunctions_XTDP_XSD_filepath
// - validator XSD path if it its not  empty: "" this is the validator XSD path.
//   if empty, the validate process is not necessary.

//=========================================================================
// External Functions [TTCN-3]
//=========================================================================
// EPTF_CLL_UIHandler_WidgetFunctions:
// -----------------------------------
// f_EPTF_UIHandler_GUIFunctions_waitForProcessExternalData_handlers
// - wait for all non blocking data request process done
// -----------------------------------
// f_EPTF_DataSource_getData_nonBlocking
// - non blocking get data function

//=========================================================================
// Variables
//=========================================================================

  static bool v_EPTF_CLL_UIHandler_GUIFunctions_initialized = false;
  // ------------------------------------------------------------------------------------
  // CONSTANTS                                                 [lifetime: END OF PROGRAM]
  static const char c_xml_head[] = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
  static const char c_widgets_start[] = "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>";
  static const char c_widgets_end[] = "</Widgets>";
  static const char c_xml_root_name[] = "Widgets";
  static const char c_xml_element[] = "element";
  static bool v_case_sensitive = true;
  static const char c_str_iterator[] = "iterator";
  static const char c_str_metaiterator[] = "metaiterator";
  static const char c_str_externalvalue[] = "externalvalue";
  static const char c_str_condition[] = "condition";
  static const char c_str_insertif[] = "insertif";
  static const char c_str_element[] = "element";
  static const char c_str_id[] = "id";
  static const char c_str_ptcname[] = "ptcname";
  static const char c_str_source[] = "source";
  static const char c_str_params[] = "params";
  static const char c_str_dataparam[] = "dataparam";
  static const char c_str_name[] = "name";
  static const char c_str_value[] = "value";
  static const char c_str_negate[] = "negate";
  static const char c_str_true[] = "true";
  static const char c_str_metadata[] = "metadata";
  static const char c_str_offset_id[] = "offset_id";
  static const char c_str_orig_node_ptr[] = "orig_node_ptr";
  static const char c_str_orig_node_name[] = "orig_node_name";
  static const char c_str_orig_itvalues[] = "orig_itvalues";
  static const char c_xml_insert_name[] = "insert_node";
  static const char c_xml_delete_name[] = "delete_node";
  static const char c_str_TODO[] = "TODO";
  static const char c_str_empty[] = "";
  static const char c_str_replace_sign[] = "%";
  static const char c_str_replace_idx[] = "::idx";
  static const char c_str_replace_count[] = "::count";
  static const char c_str_replace_ref[] = "::ref";

  static const char c_str_xml_version[] = "version";
  static const char c_str_xml_version_value[] = "1.0";
  static const char c_str_xml_encoding[] = "encoding";
  static const char c_str_xml_encoding_value[] = "UTF-8";
  static const char c_str_xml_standalone[] = "standalone";
  static const char c_str_xml_standalone_value[] = "yes";

  static std::size_t v_size_root_name = 0;
  static std::size_t v_size_iterator = 0;
  static std::size_t v_size_metaiterator = 0;
  static std::size_t v_size_externalvalue = 0;
  static std::size_t v_size_condition = 0;
  static std::size_t v_size_insertif = 0;
  static std::size_t v_size_element = 0;
  static std::size_t v_size_id = 0;
  static std::size_t v_size_ptcname = 0;
  static std::size_t v_size_source = 0;
  static std::size_t v_size_params = 0;
  static std::size_t v_size_dataparam = 0;
  static std::size_t v_size_name = 0;
  static std::size_t v_size_value = 0;
  static std::size_t v_size_true = 0;
  static std::size_t v_size_orig_node_ptr = 0;
  static std::size_t v_size_orig_node_name = 0;
  static std::size_t v_size_orig_itvalues = 0;
  static std::size_t v_size_xml_insert_name = 0;
  static std::size_t v_size_xml_delete_name = 0;
  static std::size_t v_size_offset_id = 0;


  // Temporary attributes
  static const char * v_temp_attributes[] = { c_str_orig_node_ptr,
                                       c_str_orig_node_name,
                                       c_str_orig_itvalues,
                                       c_str_offset_id};

  // Temporary attributes length
  static std::size_t * v_temp_attributes_size[] = { &v_size_orig_node_ptr,
                                                    &v_size_orig_node_name,
                                                    &v_size_orig_itvalues,
                                                    &v_size_offset_id};

  // REAL Variables
  // ------------------------------------------------------------------------------------
  // Iterator ID index                                              [lifetime: reset_XML]
  static int v_iterator_idx = 0;

  // MAIN DATABASE
  // ============================================
  // Document [rapidxml]
  static xml_document<> v_doc;
  // Playlist document [rapidxml]
  static xml_document<> v_playlist_doc;

  // For XML parts
  static vector<char *> v_xmlStrs;
#ifdef PRINT_USED_MEMORY
  static long v_lastXmlStrs_length = 0;
  static long v_xmlStrs_length = 0;
#endif
  // For variable data
  static vector<variable_data> v_variableData;
  // variable data map
  static map<long, int> v_variableDataMap;
  // variable data map OFFSET ID
  static long v_variableDataMapOffsetID;

  // widget id map for BASE nodes TO doc OR playlist_doc
  static map<string, xml_node<> *> v_widgetIDMap;
  // clone widget id map for nodes in clone_doc
  static map<string, node_parent> v_cloneWidgetIDMap;
  // playlist widget id map for nodes TO playlist_doc
  static map<string, playlist_data> v_playlistWidgetIDMap;
  // Playlist item idx
  int v_playlistIdx = 0;

  // ------------------------------------------------------------------------------------
  // root and clone root node                                // [lifetime: for test only]
  /* TIME DEBUG DATA */
#ifdef USE_TIME_MEASURE
  static clock_t v_timer_full_start;
  static struct timeval v_tv_full;
  static double v_time_full_diff_summ = 0.0;
  static clock_t v_timer_process_start;
  static struct timeval v_tv_process;
  static double v_time_process_diff_summ = 0.0;
  static clock_t v_timer_measure_start[MB_OF_MEASURES];
  static struct timeval v_tv_measure[MB_OF_MEASURES];
  static double v_time_measure_diff_summ[MB_OF_MEASURES];
  static char v_time_measure_str[MB_OF_MEASURES][256];
#endif




  //=========================================================================
  // Functions
  //=========================================================================

  ///////////////////////////////////////////////////////////
  //  Function: init_XML
  //
  //  Purpose:
  //    Initializes the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Initialize the parser variables.
  //
  ///////////////////////////////////////////////////////////
  void init_XML(void)
  {
    ACTION_GUIFUNCTIONS("init_XML - v_EPTF_CLL_UIHandler_GUIFunctions_initialized[%d]", v_EPTF_CLL_UIHandler_GUIFunctions_initialized);
    if(v_EPTF_CLL_UIHandler_GUIFunctions_initialized == false) {
      // Init size variables for quick comparison
      v_size_root_name = measure(c_xml_root_name);
      v_size_iterator = measure(c_str_iterator);
      v_size_metaiterator = measure(c_str_metaiterator);
      v_size_externalvalue = measure(c_str_externalvalue);
      v_size_condition = measure(c_str_condition);
      v_size_insertif = measure(c_str_insertif);
      v_size_element = measure(c_str_element);
      v_size_id = measure(c_str_id);
      v_size_ptcname = measure(c_str_ptcname);
      v_size_source = measure(c_str_source);
      v_size_params = measure(c_str_params);
      v_size_dataparam = measure(c_str_dataparam);
      v_size_name = measure(c_str_name);
      v_size_value = measure(c_str_value);
      v_size_true = measure(c_str_true);
      v_size_orig_node_ptr = measure(c_str_orig_node_ptr);
      v_size_orig_node_name = measure(c_str_orig_node_name);
      v_size_orig_itvalues = measure(c_str_orig_itvalues);
      v_size_xml_insert_name = measure(c_xml_insert_name);
      v_size_xml_delete_name = measure(c_xml_delete_name);
      v_size_offset_id = measure(c_str_offset_id);

      // [lifetime: reset_XML]
      // Reset number of running processes external variable
      v_iterator_idx = 0;            // Iterator ID index
      // RapidXML documents
      v_doc.clear();
      v_playlist_doc.clear();
      if(init_xml_doc(&v_playlist_doc) == false) {
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Playlist XML doc cannot be loaded!");
      }
      v_xmlStrs.clear();       // For xml strings
#ifdef PRINT_USED_MEMORY
      v_xmlStrs_length = 0;
#endif
      v_variableData.clear();  // For variable data
      v_variableDataMap.clear(); // variable data map
      v_variableDataMapOffsetID = 0l;  // variable data map OFFSET ID
      v_widgetIDMap.clear();     // widget id map
      v_playlistWidgetIDMap.clear();  // playlist widget id map
      v_playlistIdx = 0;  // Playlist item idx

      // [lifetime: expand_XML() or refreshed_XML_forNode()]
      v_cloneWidgetIDMap.clear(); // clone widget id map
      v_EPTF_CLL_UIHandler_GUIFunctions_initialized = true;
    }
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: cleanup_XML
  //
  //  Purpose:
  //    cleanup the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    cleanup the XML parser variables.
  //
  ///////////////////////////////////////////////////////////
  void cleanup_XML(void) {
    ACTION_GUIFUNCTIONS("cleanup_XML - v_EPTF_CLL_UIHandler_GUIFunctions_initialized[%d]", v_EPTF_CLL_UIHandler_GUIFunctions_initialized);
    if(v_EPTF_CLL_UIHandler_GUIFunctions_initialized == true){
      // Reset all data
      reset_XML();
      v_playlist_doc.clear();
      v_EPTF_CLL_UIHandler_GUIFunctions_initialized = false;
    }
    return;
  }


  ///////////////////////////////////////////////////////////
  //  Function: reset_XML
  //
  //  Purpose:
  //    reset the full XML document
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    reset the full XML document.
  //
  ///////////////////////////////////////////////////////////
  void reset_XML(void) {
    ACTION_GUIFUNCTIONS("reset_XML[v_widgetIDMap.size() | v_variableDataMap.size() | v_cloneWidgetIDMap.size() = %d | %d | %d]", (int)v_widgetIDMap.size(), (int)v_variableDataMap.size(), (int)v_cloneWidgetIDMap.size());
    // [lifetime: reset_XML]
    v_iterator_idx = 0;            // Iterator ID index
    // For xml strings
    for(unsigned int vl_idx=0; vl_idx< v_xmlStrs.size(); vl_idx++){
      ACTION_GUIFUNCTIONS("reset_XML - v_xmlStrs[%d]->%s",vl_idx, v_xmlStrs[vl_idx]);
      free(v_xmlStrs[vl_idx]);
      v_xmlStrs[vl_idx] = 0;
    }
    v_xmlStrs.clear();
#ifdef PRINT_USED_MEMORY
    v_xmlStrs_length = 0;
#endif
    // Clear variable data and variable data map
    clear_variable_data();
    v_widgetIDMap.clear();     // widget id map
    v_playlistWidgetIDMap.clear();  // playlist widget id map
    v_playlistIdx = 0;  // Playlist item idx

#ifdef KI_DEBUG_ACTIONS
    // For v_cloneWidgetIDMap
    for(std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.begin(); vl_node_map != v_cloneWidgetIDMap.end(); vl_node_map++) {
      node_parent vl_node_parent = vl_node_map->second;
      ACTION_GUIFUNCTIONS("reset_XML - v_cloneWidgetIDMap[%s]->%ld : %ld : %d", vl_node_map->first.c_str(), vl_node_parent.node_offset_id, vl_node_parent.parent_offset_node_id, (int)vl_node_parent.iterator_like );
    }
#endif
    v_cloneWidgetIDMap.clear(); // clone widget id map

    // Clear DOCUMENTS
    v_doc.clear();
    v_playlist_doc.clear();
    if(init_xml_doc(&v_playlist_doc) == false) {
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Playlist XML doc cannot be loaded!");
    }
    return;
  }


  ///////////////////////////////////////////////////////////
  //  Function: reset_clone_XML
  //
  //  Purpose:
  //    reset the clone XML document
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    reset the clone XML document.
  //
  ///////////////////////////////////////////////////////////
  void reset_clone_XML(void){
    ACTION_GUIFUNCTIONS("reset_clone_XML[v_widgetIDMap.size() | v_variableDataMap.size() | v_cloneWidgetIDMap.size() = %d | %d | %d]", (int)v_widgetIDMap.size(), (int)v_variableDataMap.size(), (int)v_cloneWidgetIDMap.size());
    // [lifetime: reset_XML]
    v_iterator_idx = 0;            // Iterator ID index

    // Clear variable data and variable data map
    clear_variable_data();
#ifdef KI_DEBUG_ACTIONS
    // For v_cloneWidgetIDMap
    for(std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.begin(); vl_node_map != v_cloneWidgetIDMap.end(); vl_node_map++) {
      node_parent vl_node_parent = vl_node_map->second;
      ACTION_GUIFUNCTIONS("reset_clone_XML - v_cloneWidgetIDMap[%s]->%ld : %ld : %d", vl_node_map->first.c_str(), vl_node_parent.node_offset_id, vl_node_parent.parent_offset_node_id, (int)vl_node_parent.iterator_like );
    }
#endif
    v_cloneWidgetIDMap.clear(); // clone widget id map
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: expand_XML
  //
  //  Purpose:
  //    Expand XML conditional, iterator, external value elements
  //
  //  Parameters:
  //   pl_xml - *in* *charstring* - the input xul XML
  //   pl_parentWidgetID - *in* *charstring* - the parent widget ID
  //   pl_expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Return Value:
  //    Returns the result of validation and process.
  //    pl_expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Errors:
  //    - The XML invalid[<error code>] : {<xml string>}
  //    - NO root element in the XML : {<xml string>}
  //    - is_valid_XML: The XML doc cannot be loaded or is not well-formed: {<xml string>}!
  //    - is_valid_XML: The root element in the xml is '<act root>' instead of 'Widgets'!
  //    - is_valid_XML: The the schema cannot be loaded or is not well-formed[<schema filename>]!
  //    - is_valid_XML: The schema patch [<xsd:element name="Widgets" />] invalid!
  //    - is_valid_XML: Unable to create a parser context for the schema[<schema filename>]!
  //    - is_valid_XML: The schema itself is not valid[<schema filename>]!
  //    - is_valid_XML: unable to create a validation context for the schema[<schema filename>]!
  //    - is_valid_XML: Invalid XML {<xml string>} with schema [<schema filename>] - message: <error_message>!
  //    - get_data_parameters: unknown attribute [<attribute name>=<attribute value>]!
  //    - get_data_parameters: invalid call!");
  //
  //  Detailed Comments:
  //    Load XML string into rapidXML document. Validate it if
  //    v_EPTF_UIHandler_WidgetFunctions_XTDP_XSD_filepath XSD path are set.
  //    Append physical PTRs into entities "orig_node_ptr" Clone the XML document
  //    than expand all iterator, condition and externalvalue elements using
  //    f_EPTF_DataSource_getData_nonBlocking external function.
  //
  ///////////////////////////////////////////////////////////
  bool expand_XML(const CHARSTRING& pl_xml, const CHARSTRING& pl_parentWidgetID, CHARSTRING& pl_expandedXML)
  {
#ifdef USE_TIME_MEASURE
    reset_timer();
    start_timer(&v_timer_full_start, &v_tv_full);
#endif
    bool vl_ret = true;
    int vl_validity = VALID_XML;
    // root and clone root node                                    // [lifetime: temporary]
    xml_node<> * vl_root_node = v_doc.first_node();
    xml_document<> vl_clone_doc; // Clone document [rapidxml]
    vl_clone_doc.clear();
    xml_node<> * vl_clone_root_node = vl_clone_doc.first_node();
    process_data vl_process_data;
    vl_process_data.ITValuesMapsVector.clear();
    vl_process_data.checkWidgetIDMapLastRun.clear();
    vl_process_data.cloneDocPTR = &vl_clone_doc;
    vl_process_data.waitforIdx = EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers.size_of();
    EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[vl_process_data.waitforIdx] = 0;
    ACTION_GUIFUNCTIONS("expand_XML[PRE:init_XML] {v_size_iterator:%d} {v_variableData.size():%d}vl_clone_doc:%ld", (int)v_size_iterator, (int)v_variableData.size(),(unsigned long)vl_process_data.cloneDocPTR);
    init_XML();
    pl_expandedXML = CHARSTRING("");

    // copy because XML parser are modify it
    char * vl_cstr_in = (char *)((const char *)pl_xml);
    // Search start
    while(*vl_cstr_in!='\0' && *vl_cstr_in!='<'){ vl_cstr_in++; };
    char * vl_cstr = strdup_internal(vl_cstr_in);
    char * vl_parentID = strdup_internal((const char *)pl_parentWidgetID);
    bool vl_no_parent = false;  // Have parent flag
#ifdef PRINT_USED_MEMORY
    v_lastXmlStrs_length = strlen(vl_cstr);
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"expand_XML[START:v_xmlStrs] - [%s]: + {%s}",vl_parentID,vl_cstr);
#endif
    ACTION_GUIFUNCTIONS("expand_XML[START:v_xmlStrs] - (%ld)*[%s]: + {%s}",(unsigned long)&vl_process_data,vl_parentID,vl_cstr);
#ifdef CLEAR_XML_NAMESPACE_ELEMENTS
    clear_namespace_elements(vl_cstr);  // Clear unhandled namespace elements
#endif
    // If have parent id
    if(*vl_parentID != '\0'){
      // and have window in XML
      if(strstr(vl_cstr,"<window")) {
        // The parent are the main document -> no parent ID
        *vl_parentID = '\0';
      }
    }

    // If no parent -> Reset and set act_doc
    if(*vl_parentID == '\0') {
      reset_XML();
      vl_no_parent = true;
    }
#ifdef KI_DEBUG_ACTIONS
    std::string vl_xml_out_doc_as_string_in;
    rapidxml::print(std::back_inserter(vl_xml_out_doc_as_string_in), v_playlist_doc);
    ACTION_GUIFUNCTIONS("expand_XML[v_playlist_doc:IN]{%s}",(char *)((const char *)vl_xml_out_doc_as_string_in.c_str()));
#endif

    // If no parent
    if(vl_no_parent == true) {
      // Parse full document
      // -----------------------------------------------------------------------------------------------------
      // Validate the XML
      const char * vl_XSD_filepath = (const char *)EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__EPTF__UIHandler__WidgetFunctions__XTDP__XSD__filepath;
      ACTION_GUIFUNCTIONS("expand_XML[INPUT] (%s} -> {%s}", vl_XSD_filepath, vl_cstr);
      // If XSD file path are set
      if(vl_XSD_filepath != 0 &&  *vl_XSD_filepath != '\0'){
        vl_validity = is_valid_XML(vl_cstr, vl_XSD_filepath);
      }
      if(vl_validity != VALID_XML) {
        vl_ret = false;
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The XML invalid[%d] : {%s}",  vl_validity, (const char *)pl_xml);
      } else {
        // Load XML into rapidxml doc
        ACTION_GUIFUNCTIONS("expand_XML[Load XML into rapidxml doc]");
        try {
          v_doc.parse<parse_no_entity_translation>(vl_cstr);
          ACTION_GUIFUNCTIONS("expand_XML[Load XML into rapidxml doc] try(%ld)",(long)v_doc.first_node());
          vl_root_node = v_doc.first_node();
        } catch (rapidxml::parse_error &e) { // int vl_err
          ACTION_GUIFUNCTIONS("expand_XML[Load XML into rapidxml doc] catch(%s)",e.what());
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The XML doc cannot be loaded or is not well-formed: {%s}!", e.what());
          vl_root_node = NULL;
          vl_validity = ERROR_XML_LOAD;
        }
        if(vl_root_node == NULL){
          vl_ret = false;
          if(vl_validity == VALID_XML) {
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: NO root element in the XML : {%s}",  (const char *)pl_xml);
          }
        } else {
          // Map the nodes
          append_node_ptrs(&v_doc, &vl_process_data, vl_root_node);
          action_print_node("expand_XML - v_doc {%s}",vl_root_node);
          // Clone the document ->
          // copy whole tree into second tree, which will contain metaiterators and the children expanded according the values of the iterator
          ACTION_GUIFUNCTIONS("expand_XML[CLONE:START]");
          vl_clone_root_node = vl_clone_doc.clone_node(vl_root_node);
          vl_clone_doc.append_node(vl_clone_root_node);

          vl_clone_root_node = vl_clone_doc.first_node();
          ACTION_GUIFUNCTIONS("expand_XML[CLONE:END]->expand_XML_elements:START");
          // Expand XML elements
          expand_XML_elements( vl_clone_root_node , &vl_process_data, 0l);
          ACTION_GUIFUNCTIONS("expand_XML{f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers:CALL}");
          // Wait for all data processed
          f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers(vl_process_data.waitforIdx);

          // remove debug and ptr attributes
          vl_clone_root_node = vl_clone_doc.first_node();
          action_print_node("expand_XML - v_playlist_doc.out_process_for_metaiterators{start} {%s}",vl_clone_root_node);
          out_process_for_metaiterators(vl_process_data.cloneDocPTR , vl_clone_root_node, (xml_node<>*) 0);
          vl_clone_root_node = vl_clone_doc.first_node();
          action_print_node("expand_XML - v_playlist_doc.out_process_for_metaiterators{END} {%s}",vl_clone_root_node);
          ACTION_GUIFUNCTIONS("expand_XML{out_process_for_metaiterators:END}");
          std::string vl_xml_out_as_string;
          rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_clone_doc);
          pl_expandedXML = CHARSTRING((char *) vl_xml_out_as_string.c_str());
        }
      }
    } else {
      // Insert XML
      // -----------------------------------------------------------------------------------------------------
      variable_data vl_var_data_local;
      variable_data * vl_var_data_ptr = 0;
      variable_data * vl_var_parent_data_ptr = 0;
      xml_node<> * vl_orig_node_ptr = 0;
      xml_node<> * vl_node_dest = 0;      // Destination node
      std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.find(vl_parentID);
      ACTION_GUIFUNCTIONS("expand_XML - Insert XML - v_cloneWidgetIDMap[FIND:{%s}]", vl_parentID);
      print_variableData("expand_XML[Insert XML]");
      if(vl_node_map != v_cloneWidgetIDMap.end()) {
        node_parent vl_node_parent = vl_node_map->second;
        if(init_xml_doc(&vl_clone_doc) == false) {
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Clone XML doc cannot be loaded!");
        }
        vl_node_dest = vl_clone_doc.first_node();
        vl_orig_node_ptr = vl_node_parent.orig_node_ptr;
        // Find node ID
        std::map<long, int>::iterator vl_data_map = v_variableDataMap.find(vl_node_parent.node_offset_id);
        // iterator item
        if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
          vl_var_data_ptr = &v_variableData[vl_data_map->second];
        }
        if(vl_node_parent.parent_offset_node_id != 0) {
          vl_data_map = v_variableDataMap.find(vl_node_parent.parent_offset_node_id);
          vl_orig_node_ptr = vl_node_parent.orig_node_ptr;
          // normal data item -> parent iterator
          if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
            vl_var_parent_data_ptr = &v_variableData[vl_data_map->second];
          }
        }
        // Normal item and no parent iterator
        if(vl_var_parent_data_ptr == 0 && vl_var_data_ptr == 0) {
          ACTION_GUIFUNCTIONS("expand_XML - Normal item and no parent iterator");
          vl_orig_node_ptr = vl_node_parent.orig_node_ptr;
          vl_var_data_local.node_offset_id = vl_node_parent.node_offset_id;  // Node OFFSET ID
          vl_var_data_local.orig_node_ptr = 0;                               // Original Node PTR
          vl_var_data_local.orig_node_name = (char *)c_str_empty;            // Original Node Name
          vl_var_data_local.widget_id = (char *)vl_parentID;                 // Node Widget ID
          vl_var_data_local.local_var_idx = -1;                              // Local Variable Index
          vl_var_data_local.remote_var_name = (char *)c_str_empty;           // Remote Variable Name
          vl_var_data_local.it_values_list.clear();                          // IT Values List
          vl_var_data_local.deleted = false;
          vl_var_parent_data_ptr = &vl_var_data_local;
        }
      }
      action_print_node("expand_XML - vl_node_parent.orig_node_ptr {%s}",vl_orig_node_ptr);
      if(vl_var_parent_data_ptr == 0 && vl_var_data_ptr == 0){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "WARNING: Widget ID not found : {%s}:%ld:%ld!",  (const char *)vl_parentID, (long)vl_var_parent_data_ptr, (long)vl_var_data_ptr);
      } else {
        if(vl_var_data_ptr != 0) {
        }
        if(vl_var_parent_data_ptr != 0) {
        }
        if(vl_var_parent_data_ptr == 0) {
          vl_var_parent_data_ptr = vl_var_data_ptr;
        }
        // No error: Insert
        ACTION_GUIFUNCTIONS("expand_XML - parse document{%s}", vl_cstr);
        // 1. parse document [rapidxml]
        xml_document<> vl_print_doc;
        xml_node<> * v_print_root_node = 0;
        vl_print_doc.clear();

        try {
          vl_print_doc.parse<parse_no_entity_translation>(vl_cstr);
          v_print_root_node = vl_print_doc.first_node();
          // new append
          if (compare_name(v_print_root_node->name(),measure(v_print_root_node->name()), c_xml_root_name, v_size_root_name)) {
            v_print_root_node = v_print_root_node->first_node();
          }
        } catch (int vl_err) {
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The XML doc cannot be loaded or is not well-formed: {%d}!", vl_err);
          v_print_root_node = NULL;
        }
        if(v_print_root_node == NULL){
          vl_ret = false;
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: NO root element in the XML : {%s}",  (const char *)pl_xml);
        } else {
          vector< xml_node<> * > vl_nodelist;
          vl_nodelist.clear();
          // 2. If its single node add to base v_doc, if depend from previous iterator into playlist v_playlist_doc
          xml_node<>* vl_node_source = 0;
          char * vl_orig_node_id =  vl_var_parent_data_ptr->widget_id;
          if(vl_orig_node_ptr!=0) {
            vl_orig_node_id = get_attribute(vl_orig_node_ptr, (char *)c_str_id);
          }
          // Base item: Change original items
          // ------------------------------------------------------
          if(strchr(vl_orig_node_id, '%')==0) {
            // remove original data
            vl_node_source = vl_orig_node_ptr;
            if(vl_node_source == 0){
               TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Orig node PTR not found[base] {%s}!", vl_parentID);
            } else {
              xml_node<> * vl_parent_node = vl_orig_node_ptr->parent();
              xml_node<> * vl_prev_node = 0;
              // Copy child nodes and attributes
              // 2013.11.07. - new append
              {
                vl_parent_node = vl_node_source;
                // Just insert into
                for (xml_node<> * vl_child = v_print_root_node; vl_child; vl_child = vl_child->next_sibling()){
                  vl_node_source = v_doc.clone_node(vl_child);
                  vl_nodelist.push_back(vl_node_source);
                  vl_parent_node->insert_node(vl_prev_node, vl_node_source);
                  // 3. Map the nodes: append_node_ptrs
                  append_node_ptrs(&v_doc, &vl_process_data, vl_node_source);
                  action_print_node("expand_XML - v_doc[vl_node_source] {%s}",vl_node_source);
                }
              }
            }
          } else {
            // ITS dynamical -> playlist
            // Its depend from previous iterator: Insert into playlist document. Under the process when ID are available ->
            // -------------------------------------------------------------------------------------------------------------
            // Insert into playlist
            // check if its in playlist -> than change, or delete
            vl_root_node = v_playlist_doc.first_node();
            // If no error
            if(vl_root_node != 0) {
              xml_node<> * vl_node = 0;
              // check if exist
              std::map<std::string, playlist_data>::iterator vl_playlist_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
              if(vl_playlist_map != v_playlistWidgetIDMap.end()) {
                playlist_data vl_playlist =  vl_playlist_map->second;
                vl_node = vl_playlist.playlist_node_ptr;
                vl_node->remove_all_nodes();
                // Clone name and value
                vl_node->name(c_xml_insert_name, v_size_xml_insert_name);
              } else {
                // Append the insert node item e
                vl_node = v_playlist_doc.allocate_node(node_element, c_xml_insert_name);
                char * vl_ptrbuff =  v_playlist_doc.allocate_string(vl_parentID);
                vl_node->append_attribute(v_playlist_doc.allocate_attribute(c_str_id, vl_ptrbuff));
                vl_root_node->append_node(vl_node);
                // playlist widget id map
                playlist_data vl_playlist;
                vl_playlist.playlist_node_ptr = vl_node;
                vl_playlist.idx = v_playlistIdx++;
                // If have erease it
                std::map<string, playlist_data>::iterator vl_node_check_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
                if(vl_node_check_map != v_playlistWidgetIDMap.end()) {
                  v_playlistWidgetIDMap.erase(std::string(vl_parentID));
                }
                v_playlistWidgetIDMap.insert(std::make_pair(std::string(vl_parentID), vl_playlist));
                ACTION_GUIFUNCTIONS("expand_XML - v_playlistWidgetIDMap.insert{%s}->%ld", vl_parentID, (long)vl_node);
              }

              // Copy child nodes and attributes
              vl_node_source = v_playlist_doc.clone_node(v_print_root_node);
              vl_node->append_node(vl_node_source);
              vl_nodelist.push_back(vl_node_source);
              // 3. Map the nodes: append_node_ptrs
              append_node_ptrs(&v_playlist_doc, &vl_process_data, vl_node_source);
              action_print_node("expand_XML - v_playlist_doc[vl_node_source] {%s}",vl_node_source);
              // If have more
              for (xml_node<> * vl_child = v_print_root_node->next_sibling(); vl_child; vl_child = vl_child->next_sibling()){
                vl_node_source = v_playlist_doc.clone_node(vl_child);
                vl_node->append_node(vl_node_source);
                // 3. Map the nodes: append_node_ptrs
                append_node_ptrs(&v_playlist_doc, &vl_process_data, vl_node_source);
                vl_nodelist.push_back(vl_node_source);
              }
              vl_root_node = v_playlist_doc.first_node();
              action_print_node("expand_XML - v_playlist_doc.append_node_ptrs {%s}",vl_root_node);
            }

          }
          // If no error
          if(vl_root_node  !=0 ) {
            // Like refreshed_XML_forNode
            std::ostringstream vl_outxml;
            vl_outxml << c_widgets_start;
            for (vector<xml_node<> *>::iterator vl_nextnode = vl_nodelist.begin(); vl_nextnode != vl_nodelist.end(); vl_nextnode++) {
              action_print_node("expand_XML_INSERT[copy->]{%s}", (xml_node<> * )*vl_nextnode);
              // 2013.11.07. - new append
              vl_node_source = vl_clone_doc.clone_node((xml_node<> * )*vl_nextnode);
              vl_node_dest->insert_node(0, vl_node_source);
              vl_clone_root_node = vl_node_source;
              // 5. expand XML elements
              expand_XML_elements( vl_clone_root_node , &vl_process_data, 0l);

              // 6. Wait for all data processed
              ACTION_GUIFUNCTIONS("expand_XML{f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers:CALL_1}");
              f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers(vl_process_data.waitforIdx);

              // 7. remove debug and ptr attributes
              out_process_for_metaiterators(vl_process_data.cloneDocPTR, vl_clone_root_node, (xml_node<>*) 0);

              std::string vl_xml_out_doc_as_string;
              rapidxml::print(std::back_inserter(vl_xml_out_doc_as_string), v_doc);
              // 8. Print OUTPUT document [rapidxml]
              vl_print_doc.clear();
              xml_node<> * vl_print_root_node;
              vl_print_root_node = vl_print_doc.clone_node(vl_clone_root_node);
              vl_print_doc.append_node(vl_print_root_node);
              std::string vl_xml_proc_as_string;
              rapidxml::print(std::back_inserter(vl_xml_proc_as_string), vl_print_doc);

              vl_outxml << vl_xml_proc_as_string;
#ifdef KI_DEBUG_ACTIONS
              ACTION_GUIFUNCTIONS("expand_XML_INSERT[CONCAT][%ld]:{%s}", (long)((xml_node<> * )*vl_nextnode),(char *)((const char *)vl_xml_proc_as_string.c_str()));
#endif
              vl_print_doc.clear();
            } /* EndFor */

            vl_outxml << c_widgets_end;

            vl_root_node = v_playlist_doc.first_node();
            // If loaded PRINT OUT
            if(vl_root_node != 0) {
#ifdef KI_DEBUG_ACTIONS
              std::string vl_xml_out_playlist_as_string;
              rapidxml::print(std::back_inserter(vl_xml_out_playlist_as_string), v_playlist_doc);
              ACTION_GUIFUNCTIONS("expand_XML_INSERT[PLAYLIST]{%s}",(char *)((const char *)vl_xml_out_playlist_as_string.c_str()));
#endif
            }

            pl_expandedXML = CHARSTRING((char *) vl_outxml.str().c_str());
            ACTION_GUIFUNCTIONS("expand_XML_INSERT[OUT]{%s}",(char *)((const char *)pl_expandedXML));

          } else {
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Orig node PTR not found[dyn] {%s}!", vl_parentID);
          }
          vl_nodelist.clear();
        }
      }
    }
    // Save if all processed
    if(vl_ret == true) {
      v_xmlStrs.push_back(vl_cstr);
#ifdef PRINT_USED_MEMORY
      v_xmlStrs_length += v_lastXmlStrs_length;
#endif
    } else {
      // cleanup else
      free(vl_cstr);
    }
    free(vl_parentID);
    // Cleanup [expand_XML]
    vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
    // For internal maps
    for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
      vl_process_data.ITValuesMapsVector[vl_idx].clear();
    }
    vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps
    if(vl_process_data.waitforIdx==0){
      f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers__reset();
    }
    print_variableData("expand_XML[OUT]");
#ifdef KI_DEBUG_ACTIONS
    std::string vl_xml_out_as_string;
    rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_clone_doc);
    ACTION_GUIFUNCTIONS("expand_XML[vl_clone_doc]{%s}",(char *)((const char *)vl_xml_out_as_string.c_str()));
    // Full OUT:
    std::string vl_xml_doc_out_as_string;
    rapidxml::print(std::back_inserter(vl_xml_doc_out_as_string), v_doc);
    ACTION_GUIFUNCTIONS("expand_XML[v_doc]{%s}",(char *)((const char *)vl_xml_doc_out_as_string.c_str()));
#endif

    ACTION_GUIFUNCTIONS("expand_XML[OUT] - (%ld)*{%s}",(unsigned long)&vl_process_data,(char *)((const char *)pl_expandedXML));
    vl_clone_doc.clear();
#ifdef USE_TIME_MEASURE
    stop_timer(&v_timer_full_start, &v_tv_full, &v_time_full_diff_summ);
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"expand_XML[END] - {full=%f} | {process=%f} - [%f] percent",(v_time_full_diff_summ/10000),(v_time_process_diff_summ/10000), (double)(((double)v_time_process_diff_summ*100)/v_time_full_diff_summ));
    for(int vl_idx=0;vl_idx<MB_OF_MEASURES;vl_idx++) {
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"expand_XML[END] - {full=%f} | {process=%f}| {measure=%f}[%d] - [%s]: - measure [%f] percent",(v_time_full_diff_summ/10000),(v_time_process_diff_summ/10000),(v_time_measure_diff_summ[vl_idx]/10000), vl_idx, &v_time_measure_str[vl_idx][0], (double)(((double)v_time_measure_diff_summ[vl_idx]*100)/v_time_full_diff_summ));
    }
#endif
#ifdef PRINT_USED_MEMORY
    // Document [rapidxml]
    long vl_doc_len = v_doc.doc_memory_size();
    // Clone document [rapidxml]
    long vl_clone_doc_len = vl_clone_doc.doc_memory_size();
    long vl_playlist_doc_len = v_playlist_doc.doc_memory_size();
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"expand_XML[END:MEMORY] - {v_doc=%ld} + {vl_clone_doc=%ld} + {v_playlist_doc=%ld} = %ld - {v_xmlStrs=%ld}",vl_doc_len,vl_clone_doc_len,vl_playlist_doc_len,(vl_doc_len+vl_clone_doc_len+vl_playlist_doc_len), v_xmlStrs_length);
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"expand_XML[END:GLOBAL] - {v_variableData=%ld | v_variableDataMap=%ld |v_widgetIDMap=%ld |v_cloneWidgetIDMap=%ld |v_playlistWidgetIDMap=%ld}", (long)v_variableData.size(),(long)v_variableDataMap.size(),(long)v_widgetIDMap.size(),(long)v_cloneWidgetIDMap.size(),(long)v_playlistWidgetIDMap.size());
#endif
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: remove_XML
  //
  //  Purpose:
  //    Remove XML elements from active document
  //
  //  Parameters:
  //   pl_removeWidgetID - *in* *charstring* - the removed widget ID
  //   pl_expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Return Value:
  //    Returns the result of validation and process.
  //    pl_expandedXML - *out* *charstring* - the expanded XML string after remove
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Remove XML elements from active document.
  //
  ///////////////////////////////////////////////////////////
  bool remove_XML(const CHARSTRING& pl_removeWidgetID)
  {
	  bool vl_ret = true;
	  const char * vl_parentID = (const char *)pl_removeWidgetID;
      // root root node                                    // [lifetime: temporary]
	  xml_node<> * vl_root_node = v_doc.first_node();
      ACTION_GUIFUNCTIONS("remove_XML[START]->%s", vl_parentID);

	  // If no parent -> Reset and set act_doc
	  if(*vl_parentID != '\0') {
      variable_data vl_var_data_local;
      variable_data * vl_var_data_ptr = 0;
      variable_data * vl_var_parent_data_ptr = 0;
      xml_node<> * vl_orig_node_ptr = 0;
      std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.find(vl_parentID);
      // In the clone map ->
      if(vl_node_map != v_cloneWidgetIDMap.end()) {
        node_parent vl_node_parent = vl_node_map->second;
        //std::map<xml_node<> *, int>::iterator vl_data_map = v_variableDataMap.find(vl_node_parent.node_offset_id);
        std::map<long, int>::iterator vl_data_map = v_variableDataMap.find(vl_node_parent.node_offset_id);
        // iterator item
        if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
          vl_var_data_ptr = &v_variableData[vl_data_map->second];
        }
        if(vl_node_parent.parent_offset_node_id != 0) {
          // Find node ID
          vl_data_map = v_variableDataMap.find(vl_node_parent.parent_offset_node_id);
          vl_orig_node_ptr = vl_node_parent.orig_node_ptr;
          // normal data item -> parent iterator
          if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
            vl_var_parent_data_ptr = &v_variableData[vl_data_map->second];
          }
        }
        std::map<std::string, xml_node<> *>::iterator vl_widget_map = v_widgetIDMap.find(std::string(vl_parentID));
        if(vl_widget_map != v_widgetIDMap.end()) {
          xml_node<> * vl_node = vl_widget_map->second;
          // If in base document ->
          if(vl_node->document() == &v_doc) {
            ACTION_GUIFUNCTIONS("remove_XML - vl_node->document() == &v_doc(BASE DELETE) - [%s]", vl_parentID);
            // Remove childs from widgetIDMap; cloneWidgetIDMap
            // OK: remove variable data
            delete_node_variabledata(vl_parentID);
            // Just remove nothing all
            xml_node<> * vl_parent_node = vl_node->parent();
            if(vl_parent_node != 0) {
              vl_parent_node->remove_node(vl_node);
            }
          } else {
            // Remove childs from playlistWidgetIDMap; cloneWidgetIDMap
            ACTION_GUIFUNCTIONS("remove_XML - vl_node->document() != &v_doc(DYNAMIC DELETE) - [%s]", vl_parentID);
            // OK: remove variable data
            delete_node_variabledata(vl_parentID);
            // Insert into playlist
            // check if its in playlist -> than change, or insert
            vl_root_node = v_playlist_doc.first_node();
            // If no error
            if(vl_root_node != 0) {
              std::map<std::string, playlist_data>::iterator vl_playlist_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
              if(vl_playlist_map != v_playlistWidgetIDMap.end()) {
                // Change it
                playlist_data vl_playlist =  vl_playlist_map->second;
                vl_node = vl_playlist.playlist_node_ptr;
                vl_node->remove_all_nodes();
                // Clone name and value
                vl_node->name(c_xml_delete_name, v_size_xml_delete_name);
              } else {
                // Append the insert node item
                xml_node<>* vl_node = v_playlist_doc.allocate_node(node_element, c_xml_delete_name);
                char * vl_ptrbuff =  v_playlist_doc.allocate_string(vl_parentID);
                vl_node->append_attribute(v_playlist_doc.allocate_attribute(c_str_id, vl_ptrbuff));
                vl_root_node->append_node(vl_node);
                // playlist widget id map
                playlist_data vl_playlist;
                vl_playlist.playlist_node_ptr = vl_node;
                vl_playlist.idx = v_playlistIdx++;
                // If have erease it
                std::map<string, playlist_data>::iterator vl_node_check_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
                if(vl_node_check_map != v_playlistWidgetIDMap.end()) {
                  v_playlistWidgetIDMap.erase(std::string(vl_parentID));
                }
                v_playlistWidgetIDMap.insert(std::make_pair(std::string(vl_parentID), vl_playlist));
                vl_root_node = v_playlist_doc.first_node();
              }
            } else {
              ACTION_GUIFUNCTIONS("remove_XML - No playlist.root_node - [%s]", vl_parentID);
            }
          }
        } else {
          ACTION_GUIFUNCTIONS("remove_XML - NOT IN  v_widgetIDMap - [%s]:%d", vl_parentID, (int)v_widgetIDMap.size());
          std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.find(vl_parentID);
          if(vl_node_map != v_cloneWidgetIDMap.end()) {
            node_parent vl_node_parent = vl_node_map->second;
            xml_node<> * vl_node =  0; //vl_node_dest;
            // Remove childs from playlistWidgetIDMap; cloneWidgetIDMap
            // OK: remove variable data
            delete_node_variabledata(vl_parentID);
            // Insert into playlist
            // check if its in playlist -> than change, or insert
            vl_root_node = v_playlist_doc.first_node();
            // If no error
            if(vl_root_node != 0) {
              std::map<std::string, playlist_data>::iterator vl_playlist_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
              if(vl_playlist_map != v_playlistWidgetIDMap.end()) {
                // Change it
                playlist_data vl_playlist = vl_playlist_map->second;
                vl_node = vl_playlist.playlist_node_ptr;
                vl_node->remove_all_nodes();
                // Clone name and value
                vl_node->name(c_xml_delete_name, v_size_xml_delete_name);
              } else {
                // Append the insert node item
                xml_node<>* vl_node = v_playlist_doc.allocate_node(node_element, c_xml_delete_name);
                char * vl_ptrbuff =  v_playlist_doc.allocate_string(vl_parentID);
                vl_node->append_attribute(v_playlist_doc.allocate_attribute(c_str_id, vl_ptrbuff));
                vl_root_node->append_node(vl_node);
                // playlist widget id map
                playlist_data vl_playlist;
                vl_playlist.playlist_node_ptr = vl_node;
                vl_playlist.idx = v_playlistIdx++;
                std::map<string, playlist_data>::iterator vl_node_check_map = v_playlistWidgetIDMap.find(std::string(vl_parentID));
                if(vl_node_check_map != v_playlistWidgetIDMap.end()) {
                  v_playlistWidgetIDMap.erase(std::string(vl_parentID));
                }
                v_playlistWidgetIDMap.insert(std::make_pair(std::string(vl_parentID), vl_playlist));
                vl_root_node = v_playlist_doc.first_node();
              }
            } else {
              ACTION_GUIFUNCTIONS("remove_XML - No playlist.root_node - [%s]", vl_parentID);
            }
          }
        }
	  } else {
	    // Not in clone widget ID map
	    vl_ret = false;
	    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: Widget ID not found : {%s}!",  (const char *)vl_parentID);
	  }

    } else {
      // Empty ID
      vl_ret = false;
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: Empty Widget ID {%s}!", vl_parentID);
    }
#ifdef KI_DEBUG_ACTIONS
    std::string vl_xml_out_doc_as_string;
    rapidxml::print(std::back_inserter(vl_xml_out_doc_as_string), v_playlist_doc);
    print_variableData("remove_XML[OUT]");
    ACTION_GUIFUNCTIONS("remove_XML[v_playlist_doc]{%s}",(char *)((const char *)vl_xml_out_doc_as_string.c_str()));
#endif
#ifdef PRINT_USED_MEMORY
    // Document [rapidxml]
    long vl_doc_len = v_doc.doc_memory_size();
    // Playlist document [rapidxml]
    long vl_playlist_doc_len = v_playlist_doc.doc_memory_size();
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"remove_XML[END:MEMORY] - {v_doc=%ld} + {v_playlist_doc=%ld} = %ld - {v_xmlStrs=%ld}",vl_doc_len,vl_playlist_doc_len,(vl_doc_len+vl_playlist_doc_len), v_xmlStrs_length);
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"remove_XML[END:GLOBAL] - {v_variableData=%ld | v_variableDataMap=%ld |v_widgetIDMap=%ld |v_cloneWidgetIDMap=%ld |v_playlistWidgetIDMap=%ld}", (long)v_variableData.size(),(long)v_variableDataMap.size(),(long)v_widgetIDMap.size(),(long)v_cloneWidgetIDMap.size(),(long)v_playlistWidgetIDMap.size());
#endif
    return(vl_ret);
  }
  ///////////////////////////////////////////////////////////
  //
  //  Function: f_EPTF_UIHandler_refreshedXMLforNode
  //
  //  Purpose:
  //    Expand XML for the given node
  //
  //  Parameters:
  //   pl_node_id - *in* <long> - actual node identifier
  //   pl_xulExpanded - *out* *charstring* - the expanded XML string
  //
  //  Return Value:
  //    Returns the result of the process.
  //    pl_xulExpanded - *out* *charstring* - the expanded XML string
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Expand XML for the given node.
  //
  ///////////////////////////////////////////////////////////
  bool refreshed_XML_forNode(long pl_node_id, CHARSTRING& pl_xulExpanded) {
    bool vl_ret = true;
    pl_xulExpanded = CHARSTRING("");
    // root and clone root node                                    // [lifetime: temporary]
    xml_node<> * vl_root_node = v_doc.first_node();
    xml_document<> vl_clone_doc; // Clone document [rapidxml]
    vl_clone_doc.clear();
    xml_node<> * vl_clone_root_node = vl_clone_doc.first_node();
    process_data vl_process_data;
    vl_process_data.ITValuesMapsVector.clear();
    vl_process_data.checkWidgetIDMapLastRun.clear();
    vl_process_data.cloneDocPTR = &vl_clone_doc;
    vl_process_data.waitforIdx = EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers.size_of();
    EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[vl_process_data.waitforIdx] = 0;

    ACTION_GUIFUNCTIONS("refreshed_XML_forNode[START:NODE]->%ld", pl_node_id);
    print_variableData("refreshed_XML_forNode[START]");

    // Find node ID
    std::map<long, int>::iterator vl_data_map = v_variableDataMap.find(pl_node_id);
    // Get the full document refresh
    if(pl_node_id == 0) {
      vl_root_node = v_doc.first_node();
      if(vl_root_node == NULL) {
        vl_ret = false;
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: NO root element in the BASE XML!");
      } else {
        // Reset clone XML data
        reset_clone_XML();
        // copy whole tree into second tree, which will contain metaiterators and the children expanded according the values of the iterator
        vl_clone_root_node = vl_clone_doc.clone_node(vl_root_node);
        vl_clone_doc.append_node(vl_clone_root_node);
        vl_clone_root_node = vl_clone_doc.first_node();
        // Expand XML elements
        expand_XML_elements( vl_clone_root_node, &vl_process_data, 0l);
        // Wait for all data processed
        f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers(vl_process_data.waitforIdx);

        // remove debug and ptr attributes
        vl_clone_root_node = vl_clone_doc.first_node();
        out_process_for_metaiterators(vl_process_data.cloneDocPTR, vl_clone_root_node, (xml_node<>*) 0);
        std::string vl_xml_out_as_string;
        rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_clone_doc);
        pl_xulExpanded = CHARSTRING((char *) vl_xml_out_as_string.c_str());
        ACTION_GUIFUNCTIONS("refreshed_XML_forNode[OUT]{%s}",(char *)((const char *)pl_xulExpanded));
      }
    } else if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
      // Sub tree refresh
      ACTION_GUIFUNCTIONS("refreshed_XML_forNode[vl_data_map->second]->%ld / %ld", (long)vl_data_map->second, (long)v_variableData.size());
      variable_data vl_data = v_variableData[vl_data_map->second];
      /*   v_variableData
       * ---------------------------------------
           xml_node<> * orig_node_ptr;
           char * orig_node_name;
           long local_var_idx;
           char * remote_var_name;
           std::vector<std::pair<string, string> > it_values_list;
       */
      // clear v_variableDataMap and v_variableData child values
      // Delete child nodes variable data and map
      ACTION_GUIFUNCTIONS("refreshed_XML_forNode_1 - vl_data (%s)[%s -> %ld] {%ld : %s}: %d",vl_data.widget_id, vl_data.orig_node_name,(long)(vl_data.orig_node_ptr), vl_data.local_var_idx, vl_data.remote_var_name, (int)vl_data.deleted);

      delete_node_variabledata(vl_data.widget_id);

      xml_node<> * vl_node_ptr = vl_data.orig_node_ptr;
      ACTION_GUIFUNCTIONS("refreshed_XML_forNode_2[vl_node_ptr]->%ld:%ld",(long)vl_node_ptr, (long)vl_data.it_values_list.size());
      action_print_node("refreshed_XML_forNode_2 - vl_data.orig_node_ptr {%s}",vl_data.orig_node_ptr);
      // Set IT Values
      if(init_xml_doc(&vl_clone_doc) == false) {
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The Clone XML doc cannot be loaded!");
      }
      if(0<vl_data.it_values_list.size()) {
        // Print document [rapidxml]
        xml_document<> vl_print_doc;
        vl_print_doc.clear();
        xml_node<> * vl_print_root_node;
        vl_print_root_node = vl_print_doc.clone_node(vl_node_ptr);
        vl_print_doc.append_node(vl_print_root_node);
        std::string vl_xml_proc_as_string;
        rapidxml::print(std::back_inserter(vl_xml_proc_as_string), vl_print_doc);
        int vl_itvalues_map_idx = vl_process_data.ITValuesMapsVector.size();
        vl_process_data.ITValuesMapsVector.push_back(vl_data.it_values_list);
        ACTION_GUIFUNCTIONS("refreshed_XML_forNode_2[vl_xml_proc_as_string]->{%s}",vl_xml_proc_as_string.c_str());
        vl_node_ptr = process_iterator_node(vl_process_data.cloneDocPTR, vl_node_ptr, &(vl_data.it_values_list) , vl_itvalues_map_idx, vl_xml_proc_as_string);
        vl_print_doc.clear();
      }
      xml_node<> * v_clone_node = vl_clone_doc.first_node();
      // clone from original
      copy_node(&vl_clone_doc,vl_node_ptr, v_clone_node, true);

      // Expand XML elements
      expand_XML_elements( v_clone_node, &vl_process_data, 0l);
      // Wait for all data processed
      f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers(vl_process_data.waitforIdx);
      // remove debug and ptr attributes
      out_process_for_metaiterators(vl_process_data.cloneDocPTR, v_clone_node, v_clone_node);

      // Print OUTPUT document [rapidxml]
      xml_document<> vl_print_doc;
      vl_print_doc.clear();
      xml_node<> * vl_print_root_node;
      vl_print_root_node = vl_print_doc.clone_node(v_clone_node);
      vl_print_doc.append_node(vl_print_root_node);
      std::string vl_xml_proc_as_string;
      rapidxml::print(std::back_inserter(vl_xml_proc_as_string), vl_print_doc);

      pl_xulExpanded = CHARSTRING((char *) vl_xml_proc_as_string.c_str());
      vl_print_doc.clear();
    } else {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "ERROR: refreshed_XML_forNode: No original node PTR found [%ld]!", pl_node_id);
      std::string vl_xml_out_as_string;
      rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_clone_doc);
      ACTION_GUIFUNCTIONS("refreshed_XML_forNode[vl_clone_doc]{%s}",(char *)((const char *)vl_xml_out_as_string.c_str()));
    }
    // Cleanup [refreshed_XML_forNode]
    vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
    // For internal maps
    for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
    	vl_process_data.ITValuesMapsVector[vl_idx].clear();
    }
    vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps
    if(vl_process_data.waitforIdx==0){
      f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers__reset();
    }
#ifdef PRINT_USED_MEMORY
    // Document [rapidxml]
    long vl_doc_len = v_doc.doc_memory_size();
    // Clone document [rapidxml]
    long vl_clone_doc_len = vl_clone_doc.doc_memory_size();
    // Playlist document [rapidxml]
    long vl_playlist_doc_len = v_playlist_doc.doc_memory_size();
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"refreshed_XML_forNode[END:MEMORY] - {v_doc=%ld} + {vl_clone_doc=%ld} + {v_playlist_doc=%ld} = %ld - {v_xmlStrs=%ld}",vl_doc_len,vl_clone_doc_len,vl_playlist_doc_len,(vl_doc_len+vl_clone_doc_len+vl_playlist_doc_len), v_xmlStrs_length);
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"refreshed_XML_forNode[END:GLOBAL] - {v_variableData=%ld | v_variableDataMap=%ld |v_widgetIDMap=%ld |v_cloneWidgetIDMap=%ld |v_playlistWidgetIDMap=%ld}", (long)v_variableData.size(),(long)v_variableDataMap.size(),(long)v_widgetIDMap.size(),(long)v_cloneWidgetIDMap.size(),(long)v_playlistWidgetIDMap.size());
#endif
    vl_clone_doc.clear();
    ACTION_GUIFUNCTIONS("refreshed_XML_forNode[OUT]{%s}",(char *)((const char *)pl_xulExpanded));
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: print_all_XML
  //
  //  Purpose:
  //    print all XML
  //
  //  Parameters:
  //   -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    print all XML.
  //
  ///////////////////////////////////////////////////////////
  void print_all_XML(void) {

    std::string vl_xml_out_as_string;

    std::string vl_xml_doc_out_as_string;
    rapidxml::print(std::back_inserter(vl_xml_doc_out_as_string), v_doc);
    ACTION_GUIFUNCTIONS("refreshed_XML_forNode[v_doc]{%s}",(char *)((const char *)vl_xml_doc_out_as_string.c_str()));

    std::string vl_xml_playlist_doc_as_string;
    rapidxml::print(std::back_inserter(vl_xml_playlist_doc_as_string), v_playlist_doc);
    ACTION_GUIFUNCTIONS("refreshed_XML_forNode[v_playlist_doc]{%s}",(char *)((const char *)vl_xml_playlist_doc_as_string.c_str()));
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: append_node_ptrs
  //
  //  Purpose:
  //    Append physical PTRs into entities
  //
  //  Parameters:
  //   pl_doc - *in* <xml_document<> * > - XML document pointer
  //   pl_data - *in* < process_data *> - process data structure index
  //   pl_node - *in | out* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    pl_node - *out* <xml_node<> * > - actual node pointer with physical PTR
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Append physical PTRs into entities "orig_node_ptr" before clone the XML document.
  //
  ///////////////////////////////////////////////////////////
  void append_node_ptrs(xml_document<> * pl_doc, process_data * pl_data, xml_node<> * pl_node){
	bool vl_clearLastrun = false;
    char v_tmpvalPTR[SIZE_OF_PTR+1] = "";  // cache
    // Set PTR buffer
    long vl_ptrlong = (long)pl_node;
    sprintf(v_tmpvalPTR,"%ld",vl_ptrlong);
    char * vl_ptrbuff =  pl_doc->allocate_string(v_tmpvalPTR);
    // allocate & add attribute
    xml_attribute<> * vl_attr = pl_doc->allocate_attribute(c_str_orig_node_ptr, vl_ptrbuff);
    pl_node->append_attribute(vl_attr);
    // insert to widget id cache ->
    char * vl_widgetID = get_attribute(pl_node, (char *)c_str_id);
    if(*vl_widgetID != '\0') {
      // iterator?
      if(is_expandable(pl_node->name(), pl_node->name_size()) == true){
    	// Check the nested existence of iterator id
        std::map<string, int>::iterator vl_node_map = pl_data->checkWidgetIDMapLastRun.find(vl_widgetID);
    	if(vl_node_map != pl_data->checkWidgetIDMapLastRun.end()) {
    	  std::ostringstream vl_error;
    	  vl_error << "EPTF_CLL_UIHandler_GUIFunctions[append_node_ptrs]: Configuration error - an iterator with name " << vl_widgetID <<" already exists in the resolving scope!";
    	  TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED,  vl_error.str().c_str());
    	  cleanup_XML();
    	  f__EPTF__UIHandler__error( CHARSTRING(vl_error.str().c_str()));
    	} else {
          ACTION_GUIFUNCTIONS("append_node_ptrs - checkWidgetIDMapLastRun.insert[%s->%ld]",vl_widgetID,(long)pl_node);
          pl_data->checkWidgetIDMapLastRun.insert(std::make_pair(vl_widgetID,(long) pl_node));  // check widget id map last run
    	  vl_clearLastrun = true;
    	}
      }
      // If have erease it
      std::map<string, xml_node<> *>::iterator vl_node_check_map = v_widgetIDMap.find(std::string(vl_widgetID));
      if(vl_node_check_map != v_widgetIDMap.end()) {
    	  v_widgetIDMap.erase(std::string(vl_widgetID));
      }
      v_widgetIDMap.insert(std::make_pair(std::string(vl_widgetID), pl_node));
      ACTION_GUIFUNCTIONS("append_node_ptrs v_widgetIDMap.insert(%s, %ld)", vl_widgetID, (long)pl_node);
    }
    // go through the two tree
    for( xml_node<> * vl_actode = pl_node->first_node(); vl_actode; vl_actode = vl_actode->next_sibling()){
      append_node_ptrs(pl_doc, pl_data, vl_actode);
    }
    if(vl_clearLastrun == true){
      // Check remove nested Iterator ID -> vl_attrid from check widget id map
      pl_data->checkWidgetIDMapLastRun.erase(vl_widgetID);
    }
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: expand_XML_elements
  //
  //  Purpose:
  //    Expand expandable XML elements
  //
  //  Parameters:
  //   pl_node - *in | out* <xml_node<> * > - actual node pointer
  //   pl_data - *in* < process_data *> - process data structure index
  //   pl_idx - *in* < unsigned long> - expanded index
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Expand expandable XML elements. Get expandables list, get data sources and
  //    expand it.
  //
  ///////////////////////////////////////////////////////////
  void expand_XML_elements( xml_node<> * pl_node, process_data * pl_data, unsigned long pl_idx) {
    vector< xml_node<> * > vl_nodelist;
    vl_nodelist.clear();
    process_data * vl_process_data_ptr = pl_data;
    ACTION_GUIFUNCTIONS("expand_XML_elements - (%ld)*[%s:%ld:%ld]->%ld",(unsigned long)pl_data, pl_node->name(),(unsigned long) pl_node, (unsigned long)pl_data, (unsigned long)vl_nodelist.size());
    // Get first level expandable list
    get_nodes_to_expand(pl_node, &vl_nodelist);
    action_print_node("expand_XML_elements - get_nodes_to_expand {%s}",pl_node);

    // Thru the expandables
    for (vector<xml_node<> *>::iterator vl_expandable = vl_nodelist.begin(); vl_expandable != vl_nodelist.end(); vl_expandable++){
      // Get data source parameters
      char * vl_element;
      char * vl_id;
      char * vl_ptcname;
      char * vl_source;
      EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_params;
      std::ostringstream vl_parameters;
      if(get_data_source( ((xml_node<> * )*vl_expandable), &vl_element, &vl_id, &vl_ptcname, &vl_source)==true){
        ACTION_GUIFUNCTIONS("get_data_source->{element=%s; id=%s; ptcname=%s; source=%s}", vl_element, vl_id, vl_ptcname, vl_source);
        vl_parameters << vl_element << ":" << vl_ptcname << ":" << vl_source << "[";
        vl_params = get_data_source_parameters( ((xml_node<> * )*vl_expandable));
        INTEGER vl_dataParamsSize(vl_params.size_of());
        if(0<vl_dataParamsSize){
         for(int vl_idx=0; vl_idx<vl_dataParamsSize;vl_idx++){
           vl_parameters << (char *)((const char *)vl_params[vl_idx].paramName()) << "=" << (char *)((const char *)vl_params[vl_idx].paramValue());
           ACTION_GUIFUNCTIONS("DATAPARAM->{[%d]: %s = %s}", vl_idx, (char *)((const char *)vl_params[vl_idx].paramName()), (char *)((const char *)vl_params[vl_idx].paramValue()));
         }
        }
      }
      vl_parameters << "]";
      // switch(((xml_node<> * )*vl_expandable)->name())
      char * vl_exp_name = ((xml_node<> * )*vl_expandable)->name();
      size_t vl_exp_name_size = measure(vl_exp_name);
      // case (iterator) OR  case (externalvalue)
      // ----------------------------------------
      if(is_iterator_like(vl_exp_name, vl_exp_name_size) == true){
    	if(vl_process_data_ptr != 0) {
          // Call non blocking Get data
          vl_process_data_ptr->parameters_String = vl_parameters.str();
          EPTF__CLL__DataSource__Definitions::EPTF__DataSource__GetDataHandler vl_dataHandler;
          vl_dataHandler.getDataHandler() = &f__EPTF__UIHandler__expand__current__iterator;

          // put the pointer for the node into the userdata section, will be used in the datahandler function.
          EPTF__CLL__Common__Definitions::EPTF__IntegerList& vl_expandable_ptr = vl_dataHandler.userData();
          vl_expandable_ptr.set_size(3);
          vl_expandable_ptr[0].set_long_long_val( (unsigned long long) pl_data);
          vl_expandable_ptr[1].set_long_long_val( (unsigned long long) ((xml_node<> * )*vl_expandable));
          vl_expandable_ptr[2].set_long_long_val( (unsigned long long) pl_idx);
          ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator-> - (%ld)*[%ld:%ld]", (unsigned long)pl_data,(unsigned long)vl_expandable_ptr[0].get_long_long_val(),(unsigned long)vl_expandable_ptr[1].get_long_long_val());
          // Increase the process counter
          // TODO: JÃ³zsi szerint ez a nÃ¶velÃ©s + a csÃ¶kkentÃ©s nem kell, mert otomata szÃ¡mlÃ¡lja
          ++EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[pl_data->waitforIdx];
          ACTION_GUIFUNCTIONS("expand_XML_elements[f__EPTF__DataSource__getData__nonBlocking:iterator:%ld:%ld:%ld]->(%s;%s;%s)->{%s}", (long)pl_data, (long)((xml_node<> * )*vl_expandable),pl_idx, vl_source, vl_ptcname, vl_element, vl_process_data_ptr->parameters_String.c_str());
          f__EPTF__DataSource__getData__nonBlocking( vl_source, vl_ptcname, vl_element, vl_params, vl_dataHandler );
    	}

      }
      // case (condition)
      // -----------------
      else if( compare_name(vl_exp_name, vl_exp_name_size, c_str_condition, v_size_condition)){
      	if(vl_process_data_ptr != 0){
    	  char * vl_stridsaved = strdup_internal(vl_id);
          ACTION_GUIFUNCTIONS("expand_XML_elements: vl_stridsaved[%s]", vl_stridsaved);

          // Call non blocking Get condition
          EPTF__CLL__DataSource__Definitions::EPTF__DataSource__GetDataHandler vl_dataHandler;
          vl_dataHandler.getDataHandler() = &f__EPTF__UIHandler__expand__current__condition;
          // put the pointer for the node into the userdata section, will be used in the datahandler function.
          EPTF__CLL__Common__Definitions::EPTF__IntegerList& vl_expandable_ptr = vl_dataHandler.userData();
          vl_expandable_ptr.set_size(3);
          vl_expandable_ptr[0].set_long_long_val( (unsigned long long) pl_data );
          vl_expandable_ptr[1].set_long_long_val( (unsigned long long) ((xml_node<> * )*vl_expandable));
          vl_expandable_ptr[2].set_long_long_val( (unsigned long long) vl_stridsaved);
          ACTION_GUIFUNCTIONS("vl_expandable_ptr[getCondition]->[%ld:%ld:%ld]", (unsigned long)vl_expandable_ptr[0].get_long_long_val(),(unsigned long)vl_expandable_ptr[1].get_long_long_val(), (unsigned long)vl_expandable_ptr[2].get_long_long_val());

          // Increase the process counter
          ++EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[pl_data->waitforIdx];
          ACTION_GUIFUNCTIONS("expand_XML_elements[f__EPTF__DataSource__getData__nonBlocking:condition]->(%s;%s;%s)", vl_source, vl_ptcname, vl_element);
          // Call f_EPTF_DataSource_getCondition_nonBlocking
          f__EPTF__DataSource__getCondition__nonBlocking( vl_source, vl_ptcname, vl_element, vl_params, vl_dataHandler );
        }
      } else {
        TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "expand_XML_elements: Unknown expandable [%s]!", vl_exp_name);
      }
    }/* EndFor */
    ACTION_GUIFUNCTIONS("expand_XML_elements - (%ld)*[OUT]->%d", (unsigned long)pl_node, (int)(EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[pl_data->waitforIdx]==0));
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: get_nodes_to_expand
  //
  //  Purpose:
  //    Get expandable list vector
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //   pl_nodeList - *out* <vector< xml_node<>* > * > - expandable node list vector
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Get expandable list vector.
  //
  ///////////////////////////////////////////////////////////
  void get_nodes_to_expand( xml_node<>* pl_node, vector< xml_node<>* > * pl_nodeList)
  {
    if(is_expandable(pl_node->name(), pl_node->name_size()) == true)
    {
      pl_nodeList->push_back(pl_node);
      ACTION_GUIFUNCTIONS("get_nodes_to_expand -> PUSH[ROOT]: %s",pl_node->name() );
    } else {
      // Thru the node list
      for (xml_node<> * vl_act_node = pl_node->first_node(); vl_act_node; vl_act_node = vl_act_node->next_sibling()){
        ACTION_GUIFUNCTIONS("get_nodes_to_expand-> Node: %s",vl_act_node->name() );
        // if expandable save it
        if(is_expandable(vl_act_node->name(), vl_act_node->name_size()) == true)
        {
          pl_nodeList->push_back(vl_act_node);
          ACTION_GUIFUNCTIONS("get_nodes_to_expand-> PUSH[child]: %s",vl_act_node->name() );
        } else {
          get_nodes_to_expand(vl_act_node, pl_nodeList);
        }
      }
    }
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: f__EPTF__UIHandler__expand__current__iterator
  //
  //  Purpose:
  //    Nonblocking Data Handler: Expand the current node iterator, condition and externalvalue.
  //
  //  Parameters:
  //   pl_source *in* *charstring * - the name of the dataSource 'feature'
  //   pl_source *in* *charstring* - the name of the PTC
  //         (dataSource+ptcName should be a unigue id of the data). Default: "" (i.e. PTC name is ignored)
  //   pl_element *in* *charstring* - the type of data
  //   pl_params *in* <EPTF_DataSource_Params> - additional parameters (default: {})
  //   pl_errorCode - *in* *INTEGER&* - error code in the response
  //   pl_remoteDataVarName - *in* *charstring* -  var name on the remote component.
  //           In case errorCode!=0: it contains the error message
  //   pl_ownerCompRef - *in* *COMPONENT&* - reference to the remote component
  //   pl_localDataVarId - *in* *INTEGER&* - var id on the local component
  //                 (contains the current value, not subscribed to remote!)
  //   pl_dataValue - *in* <EPTF_Var_DirectContent> - the value of the data
  //   pl_userData - *in* <EPTF__IntegerList&> -user specific data given at the request
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Nonblocking Data Handler: Expand the current node iterator, condition and externalvalue..
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__expand__current__iterator( const CHARSTRING& pl_source,
                            const CHARSTRING& pl_ptcName,
                            const CHARSTRING& pl_element,
                            const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params& pl_params,
                            const INTEGER& pl_errorCode,
                            const CHARSTRING& pl_remoteDataVarName,
                            const COMPONENT& pl_ownerCompRef,
                            const INTEGER& pl_localDataVarId,
                            const EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent& pl_dataValue,
                            const EPTF__CLL__Common__Definitions::EPTF__IntegerList& pl_userData)
  {
    //pl_dataValue is a CharstringList with the values of the iterator
    // START[0]:
    // --------------------------------------------------------------------------------------------------------------------
    START_MEASURE(0, "f__EPTF__UIHandler__expand__current__iterator: start");
    EPTF__CLL__Common__Definitions::EPTF__CharstringList vl_values;
    long unsigned int vl_values_size;
    const char * * vl_str_values = NULL;
    char vl_tmp_data_bfr[SIZE_OF_MAXDATA];
    unsigned long long vl_wrk_long_ptr = (unsigned long)pl_userData[0].get_long_long_val();
    unsigned long long vl_wrk_long_long = (unsigned long long)pl_userData[1].get_long_long_val();
    long vl_idx = (long)((unsigned long long)pl_userData[2].get_long_long_val());
    process_data * vl_process_data_ptr = (process_data *)vl_wrk_long_ptr;
    xml_node<>* vl_copyNode =  (xml_node<>*)vl_wrk_long_long;
    v_variableDataMapOffsetID++;  // variable data map OFFSET ID
    xml_node<>* vl_IteratorNode = 0;
    long vl_variableDataMapOffsetIDAct = v_variableDataMapOffsetID;
    CHARSTRING vl_tmp_ptr;
    bool vl_pendingError = false;  // Have any error?
    char * vl_origPtr = 0;
    char * vl_node_name = (char *)&c_str_empty[0];
    vl_origPtr = get_attribute(vl_copyNode, (char *) c_str_orig_node_ptr);
    vl_node_name = vl_copyNode->name();
    char * vl_attrid = get_attribute(vl_copyNode,(char *)c_str_id);
    ACTION_GUIFUNCTIONS("->f__EPTF__UIHandler__expand__current__iterator - (%ld)*[%ld:%ld:%ld]{%s}->{%s}",(unsigned long)vl_process_data_ptr,(unsigned long)pl_userData[0].get_long_long_val(),(unsigned long)pl_userData[1].get_long_long_val(),vl_idx, vl_attrid, vl_process_data_ptr->parameters_String.c_str());
    action_print_node("f__EPTF__UIHandler__expand__current__iterator - vl_copyNode {%s}",vl_copyNode);
    std::size_t vl_node_name_size = measure(vl_node_name);
    bool vl_is_externalvalue = compare_name(vl_node_name, vl_node_name_size, c_str_externalvalue, v_size_externalvalue);
    ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - vl_copyNode[%ld]->name(){%s-pl_errorCode:%ld}-vl_is_externalvalue:%d -> %s",(unsigned long)vl_copyNode,vl_node_name, (long)pl_errorCode,(int)vl_is_externalvalue, (char *)((const char *) pl_remoteDataVarName));
    STOP_MEASURE(0);
    // --------------------------------------------------------------------------------------------------------------------
    // STOP[0]:
    // START[1]:
    // --------------------------------------------------------------------------------------------------------------------
    START_MEASURE(1, "f__EPTF__UIHandler__expand__current__iterator: get iterator");

    // change iterator | external value to metaiterator in the copy node.
    if(vl_origPtr!=0){
      vl_IteratorNode = (xml_node<>*) (long *) atol(vl_origPtr);
    }
    if(vl_IteratorNode) {
      set_node_temp_attributes(vl_process_data_ptr->cloneDocPTR, vl_copyNode, vl_variableDataMapOffsetIDAct, (char *)((const char *)pl_remoteDataVarName),(long)pl_localDataVarId);
    }
    if(pl_errorCode != 0 && vl_pendingError == false) {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f_EPTF_DataSource_getData_nonBlocking: Error[%ld] -> {%s}!", (long)pl_errorCode, (char *)((const char *) pl_remoteDataVarName) );
      vl_pendingError = true;
    } else if(*vl_origPtr== '\0'){
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "ERROR: f__EPTF__UIHandler__expand__current__iterator: No original node PTR found [%ld]!", (unsigned long) vl_wrk_long_long);
      vl_pendingError = true;
    } else if(is_iterator_like(vl_node_name, vl_node_name_size) == false) {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f__EPTF__UIHandler__expand__current__iterator: the node not (iterator | externalvalue) instead of: [%s]!", vl_node_name);
      vl_pendingError = true;
    } else {
      // Data type?
      if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_charstringlistVal) {
        vl_values = pl_dataValue.charstringlistVal();
        vl_values_size = vl_values.size_of();
        // externalvalue ...
        if(vl_is_externalvalue == true) {
          // bugfix_artf361869_empty_iterator: no process on empty iterator in externalvalue
          vl_values_size = vl_values.size_of();
          if(1<vl_values_size){
            vl_values_size = 1;
          }
          // vl__dataVarName = (TTCN_Logger::begin_event_log2str(),vl_values.log(),TTCN_Logger::end_event_log2str());
          vl_str_values = new const char *[vl_values_size];
          vl_tmp_ptr = (TTCN_Logger::begin_event_log2str(),vl_values.log(),TTCN_Logger::end_event_log2str());
          *(vl_str_values)= (const char *)vl_tmp_ptr;
        } else {
          vl_str_values = new const char *[vl_values_size];
          for (long unsigned int i = 0; i < vl_values_size; i++){
            *(vl_str_values+i) = (const char *)vl_values[i];
          }
        }
        ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator: %ld->%ld",(long) vl_copyNode, vl_values_size );
      } else {
        // externalvalue ...
        if(vl_is_externalvalue == true) {
          // ALT_charstringlistVal - BASE
          // ALT_intVal
          vl_values_size = 1;
          vl_str_values = new const char *[vl_values_size];
          *(vl_str_values) = NULL;
          if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_intVal) {
            sprintf(vl_tmp_data_bfr,"%ld",(long)pl_dataValue.intVal());
            *(vl_str_values) = (const char *)vl_tmp_data_bfr;
          }
          // ALT_floatVal
          else if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_floatVal) {
            sprintf(vl_tmp_data_bfr,"%f",(float)pl_dataValue.floatVal());
            *(vl_str_values) = (const char *)vl_tmp_data_bfr;
          }
          // ALT_boolVal,
          else if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_boolVal) {
            sprintf(vl_tmp_data_bfr,"%d",(int)pl_dataValue.boolVal());
            *(vl_str_values) = (const char *)vl_tmp_data_bfr;
          }
          // ALT_charstringVal
          else if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_charstringVal) {
            *(vl_str_values) = (const char *)(pl_dataValue.charstringVal());
         }
         // NOT SUPPORTED
         // ----------------------------------
         // ALT_octetstringVal
         // ALT_hexstringVal
         // ALT_bitstringVal
         // ALT_integerlistVal
         // ALT_floatlistVal
         // ALT_statusLEDVal
         // ALT_unknownVal
          else {
            TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f__EPTF__UIHandler__expand__current__iterator: function handles only intVal|floatVal|boolVal|charstringVal directContent for now!!");
            vl_pendingError = true;
          }
          ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator[externalvalue]: {%ld}->%s",vl_values_size, *(vl_str_values));

        } else {
          TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f__EPTF__UIHandler__expand__current__iterator: function handles only CharstringList directContent for now!!");
          vl_pendingError = true;
        }
      }
    }
    STOP_MEASURE(1);
    // --------------------------------------------------------------------------------------------------------------------
    // STOP[1]:

    // If NO Error
    if(vl_pendingError == false) {
      char vl_value_idx [SIZE_OF_LONG];
      char vl_value_count [SIZE_OF_LONG];
      // START[2]:
      // --------------------------------------------------------------------------------------------------------------------
      START_MEASURE(2, "f__EPTF__UIHandler__expand__current__iterator: resubscribe+");
//KI      char * vl_attrid = get_attribute(vl_copyNode,(char *)c_str_id);
      // Put postproc to variable: f_EPTF_UIHandler_updateIterator_PostProc(in integer pl_idx, in EPTF_IntegerList pl_argList)
      //-------------------------- -------------------------------------------------------------------------------------------
      if( *((const char *)pl_remoteDataVarName) != '\0' ) {
        EPTF__CLL__Variable__Definitions::EPTF__Var__GenericFn vl_funcref;
        vl_funcref.funcRef() = &f__EPTF__UIHandler__updateIterator__PostProc;
        {
          EPTF__CLL__Common__Definitions::EPTF__IntegerList& vl_idxlist = vl_funcref.argList();
          vl_idxlist.set_size(1);
          vl_idxlist[0].set_long_long_val( (unsigned long) vl_copyNode);
        }
        // now we are subscribing for every variable. Need to figure out an algorithm to optimalize this.
        ACTION_GUIFUNCTIONS("f__EPTF__Var__resubscribeRemote(%s)",(const char* )pl_remoteDataVarName);
        EPTF__CLL__Variable__Functions::f__EPTF__Var__resubscribeRemote(pl_ownerCompRef, pl_remoteDataVarName, EPTF__CLL__DataSource__Definitions::tsp__EPTF__DataSource__subscriptionMode, pl_localDataVarId, "", -1, cg__EPTF__Var__resubscribeRemoteResp__handler__null);
      }
      ACTION_GUIFUNCTIONS("pl_remoteDataVarName{%s}->[%ld]",(const char* )pl_remoteDataVarName,(long)pl_localDataVarId);

      vector< xml_node<> * > vl_NodesToConnect;
      vl_NodesToConnect.clear();
      int vl_itvalues_map_idx = -1;
      std::ostringstream vl_sstream;

      // Check if first node are param list
      xml_node<> * vl_first_node = vl_copyNode->first_node();
      bool vl_firstIsParam = false;
      bool vl_firstNodeProcessed = false;
      if(vl_first_node!=NULL && compare_name(vl_first_node->name(), vl_first_node->name_size(), c_str_params, v_size_params)) {
        vl_firstIsParam = true;
      }

      ACTION_GUIFUNCTIONS("pl_dataValue{%s}->[%ld]-firstparam(%d)",(const char* )pl_remoteDataVarName, vl_values_size, vl_firstIsParam);
      // node xml string data map
      map<xml_node<> *, std::string> vl_nodeXMLStrDataMap;
      vl_nodeXMLStrDataMap.clear();
      STOP_MEASURE(2);

      // --------------------------------------------------------------------------------------------------------------------
      // STOP[2]:
      // Check nested Iterator ID -> vl_attrid in check widget id map
      std::string vl_widgetID = std::string(vl_attrid);
      std::ostringstream vl_widgetIDLastRunStream;
      vl_widgetIDLastRunStream << vl_widgetID << "_" << vl_idx;
      std::string vl_widgetIDLastRun = vl_widgetIDLastRunStream.str();
      std::ostringstream vl_widgetIDStrStream;
      vl_widgetIDStrStream << "id=\""<< vl_widgetID << "\"";
      std::string vl_widgetIDStr = vl_widgetIDStrStream.str();
      for (long unsigned int i = 0; i < vl_values_size; i++) {
        START_MEASURE(3, "f__EPTF__UIHandler__expand__current__iterator: new_itvalues");
        vl_sstream.clear();
        // START[3]:
        // --------------------------------------------------------------------------------------------------------------------
        ACTION_GUIFUNCTIONS("1 - pl_dataValue[%s->%ld]: %s",vl_attrid,i,(const char *)*(vl_str_values+i));

        // base;
        string vl_tobereplace_base_str = escape_double_quotes(vl_attrid);
        char * vl_value_base = (char *)(*(vl_str_values+i));
        std::ostringstream vl_tobereplace_base;
        vl_tobereplace_base << c_str_replace_sign << vl_tobereplace_base_str << c_str_replace_sign;
        // ::idx
        sprintf(vl_value_idx,"%ld",i);
        std::ostringstream vl_tobereplace_idx;
        vl_tobereplace_idx << c_str_replace_sign << vl_tobereplace_base_str << c_str_replace_idx << c_str_replace_sign;
        // ::count
        sprintf(vl_value_count,"%ld",vl_values_size);
        std::ostringstream vl_tobereplace_count;
        vl_tobereplace_count << c_str_replace_sign << vl_tobereplace_base_str << c_str_replace_count << c_str_replace_sign;
        // ::ref are the variable name but the LOCAL not the remote :)
        // char * vl_value_ref = (char *)((const char *)pl_remoteDataVarName);
        string vl_value_ref_str = escape_double_quotes(((const char *)EPTF__CLL__Variable__Functions::f__EPTF__Var__getName(pl_localDataVarId)));
        std::ostringstream vl_tobereplace_ref;
        vl_tobereplace_ref << c_str_replace_sign << vl_tobereplace_base_str << c_str_replace_ref << c_str_replace_sign;

        map<string, string>  vl_it_values; // itvalues map
        vl_it_values.clear();
        vl_it_values.insert(std::make_pair(vl_tobereplace_base.str(), escape_double_quotes(vl_value_base)));
        vl_it_values.insert(std::make_pair(vl_tobereplace_idx.str(), std::string(vl_value_idx)));
        vl_it_values.insert(std::make_pair(vl_tobereplace_count.str(), std::string(vl_value_count)));
        vl_it_values.insert(std::make_pair(vl_tobereplace_ref.str(), vl_value_ref_str));

        // Save IT Values Map
        vl_itvalues_map_idx = vl_process_data_ptr->ITValuesMapsVector.size();
        vl_process_data_ptr->ITValuesMapsVector.push_back(vl_it_values);
#ifdef KI_DEBUG_ACTIONS
        for(std::map<std::string , std::string>::iterator vl_it_map = vl_it_values.begin(); vl_it_map != vl_it_values.end(); vl_it_map++) {
          vl_sstream << vl_it_map->first << "->" << vl_it_map->second << " | ";
        }
        ACTION_GUIFUNCTIONS("process_iterator_node - vl_process_data_ptr->ITValuesMapsVector[%d] {%s}",vl_itvalues_map_idx, vl_sstream.str().c_str());
#endif

        STOP_MEASURE(3);
        // --------------------------------------------------------------------------------------------------------------------
        // STOP[3]:

        //create "(number of pl_dataValue) * (original number of children under parentNode)" children under vl_copyNode with rapidxml
        vl_first_node = vl_copyNode->first_node();
        for( xml_node<> * vl_pNode = vl_copyNode->first_node(); vl_pNode; vl_pNode=vl_pNode->next_sibling()) {
          START_MEASURE(4, "f__EPTF__UIHandler__expand__current__iterator: xml -> string");
          // START[4]:
          // --------------------------------------------------------------------------------------------------------------------
          std::string vl_xml_proc_as_string;
          std::map<xml_node<> *, std::string>::iterator vl_data_map = vl_nodeXMLStrDataMap.find(vl_pNode);
          if(vl_data_map != vl_nodeXMLStrDataMap.end() && 0<vl_data_map->second.length()) {
            vl_xml_proc_as_string = vl_data_map->second;
          } else {
            // Print document [rapidxml]
            xml_document<> vl_print_doc;
            vl_print_doc.clear();
            xml_node<> * vl_print_root_node;
            vl_print_root_node = vl_print_doc.clone_node(vl_pNode);
            vl_print_doc.append_node(vl_print_root_node);
            rapidxml::print(std::back_inserter(vl_xml_proc_as_string), vl_print_doc);
            vl_print_doc.clear();
            vl_nodeXMLStrDataMap.insert(std::make_pair(vl_pNode, vl_xml_proc_as_string));
          }
          STOP_MEASURE(4);
          // --------------------------------------------------------------------------------------------------------------------
          // STOP[4]:

          START_MEASURE(5, "f__EPTF__UIHandler__expand__current__iterator: process_iterator_node");
          // START[5]:
          // --------------------------------------------------------------------------------------------------------------------
          // If not first node or its not param list or its not processed once
          if(vl_pNode != vl_first_node || vl_firstIsParam == false || vl_firstNodeProcessed == false){
            // process base ::idx | ::count | ::ref on  iterator | externalvalue
            ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - process_iterator_node[XML:PROCESS:START]: %ld:[%s] {%s}",(long)vl_pNode, vl_sstream.str().c_str(), vl_xml_proc_as_string.c_str());
            xml_node<> * vl_pNewNode = process_iterator_node(vl_process_data_ptr->cloneDocPTR, vl_pNode, &vl_it_values, vl_itvalues_map_idx, vl_xml_proc_as_string);
            vl_NodesToConnect.push_back(vl_pNewNode);
            vl_firstNodeProcessed = true;  // First node processed
          } else {
            ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - process_iterator_node[XML:ELSE]: {%s}", vl_xml_proc_as_string.c_str());
          }
          STOP_MEASURE(5);
          // --------------------------------------------------------------------------------------------------------------------
          // STOP[5]:
        }
        vl_it_values.clear(); // itvalues map
      }

      START_MEASURE(6, "f__EPTF__UIHandler__expand__current__iterator: clear");
      // START[6]:
      // --------------------------------------------------------------------------------------------------------------------
      vl_nodeXMLStrDataMap.clear();
      //connect nodes to the copy node, but remove all children first
      //remove all children
      vl_copyNode->remove_all_nodes();
      unsigned long vl_idx = 0l;
      for (vector<xml_node<> *>::iterator vl_expanded = vl_NodesToConnect.begin(); vl_expanded != vl_NodesToConnect.end(); vl_expanded++){
        vl_copyNode->append_node((xml_node<> * )*vl_expanded);
        /* call expand_XMLElements() for every children Node */
        STOP_MEASURE(6);
        ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - expand_XML_elements: (%ld)*{%ld}->%ld", (unsigned long)vl_process_data_ptr, (unsigned long)*vl_expanded, vl_idx);
        action_print_node("f__EPTF__UIHandler__expand__current__iterator - expand_XML_elements - vl_copyNode {%s}",(xml_node<> * )*vl_expanded);
        expand_XML_elements( (xml_node<> * )*vl_expanded, vl_process_data_ptr , vl_idx);
        START_MEASURE(6, "f__EPTF__UIHandler__expand__current__iterator: clear");
        vl_idx++;
      }
      // Free data
      if(vl_str_values != NULL) {
        delete [] vl_str_values;
        STOP_MEASURE(6);
        // --------------------------------------------------------------------------------------------------------------------
        // STOP[6]:
      }

      START_MEASURE(7, "f__EPTF__UIHandler__expand__current__iterator: vl_str_itvalues");
      // START[7]:
      // --------------------------------------------------------------------------------------------------------------------
      // save and update variable data
      // --------------------------------------------------------------------------------
      char vl_szTemp[2048]="<NULL>";

      xml_attribute<> * vl_attr =  vl_copyNode->first_attribute((char *)c_str_orig_itvalues);
      // action_print_node("f__EPTF__UIHandler__expand__current__iterator[vl_copyNode:{%s}", vl_copyNode);
      std::map<std::string, std::string>  vl_full_it_values; // itvalues map
      vl_full_it_values.clear();
      if(vl_attr != NULL) {
        strcpy(vl_szTemp, vl_attr->value());
        std::istringstream vl_itvalue_idxs_stream(vl_attr->value());
        std::string vl_next_string;
        while (std::getline(vl_itvalue_idxs_stream, vl_next_string, ';')) {
          // std::cout << vl_next_string << std::endl;
          int vl_next_idx_value = atoi(vl_next_string.c_str());
          map< string, string > vl_it_values = vl_process_data_ptr->ITValuesMapsVector.at(vl_next_idx_value);
          // For vl_it_values
          for(std::map<std::string, std::string>::iterator vl_itvalue_item = vl_it_values.begin(); vl_itvalue_item != vl_it_values.end(); vl_itvalue_item++) {
            std::string vl_first = vl_itvalue_item->first;
            std::string vl_second = vl_itvalue_item->second;
            vl_full_it_values.insert(std::make_pair(vl_first, vl_second));
          }
        }
      }
#ifdef  KI_DEBUG_ACTIONS
      std::ostringstream vl_sstream_debug;
      for(std::map<std::string , std::string>::iterator vl_it_map = vl_full_it_values.begin(); vl_it_map != vl_full_it_values.end(); vl_it_map++) {
        vl_sstream_debug << vl_it_map->first << "->" << vl_it_map->second << " | ";
      }
      ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - ITVALUES: [%ld:%ld](%s)->{%s} => [%d] {%s}", (long)vl_copyNode, (long)pl_localDataVarId, vl_szTemp, vl_sstream_debug.str().c_str(),vl_itvalues_map_idx, vl_sstream.str().c_str());
#endif

      STOP_MEASURE(7);
      // --------------------------------------------------------------------------------------------------------------------
      // STOP[7]:

      START_MEASURE(8, "f__EPTF__UIHandler__expand__current__iterator: vl_var_data_ptr");
      // START[8]:
      // --------------------------------------------------------------------------------------------------------------------
      // If its valid
      //std::map<xml_node<> *, int>::iterator vl_data_map = v_variableDataMap.find(vl_copyNode);
      char * vl_node_offset_id_str =  get_attribute(vl_copyNode, (char *)c_str_offset_id);
      std::map<long, int>::iterator vl_data_map = v_variableDataMap.end();
      if(vl_node_offset_id_str!=0 && *vl_node_offset_id_str!='\0') {
    	long vl_node_offset_id = atol(vl_node_offset_id_str);
    	if(0l<vl_node_offset_id) {
          std::map<long, int>::iterator vl_data_map = v_variableDataMap.find(vl_node_offset_id);
    	}
      }
      if(vl_data_map != v_variableDataMap.end() && vl_data_map->second<(int)v_variableData.size()) {
        // Existing item
        variable_data * vl_var_data_ptr = &v_variableData[vl_data_map->second];
#ifdef KI_DEBUG_ACTIONS
        // Free buffers first
        if(vl_var_data_ptr->orig_node_name) {
          free(vl_var_data_ptr->orig_node_name);
          vl_var_data_ptr->orig_node_name = 0;
        }
        if(vl_var_data_ptr->remote_var_name) {
          free(vl_var_data_ptr->remote_var_name);
          vl_var_data_ptr->remote_var_name = 0;
        }
#else
        vl_var_data_ptr->orig_node_name = 0;
        vl_var_data_ptr->remote_var_name = 0;
#endif
        vl_var_data_ptr->orig_node_ptr = vl_IteratorNode;
        vl_var_data_ptr->widget_id = vl_attrid;
        vl_var_data_ptr->local_var_idx = (long)pl_localDataVarId;
#ifdef KI_DEBUG_ACTIONS
        vl_var_data_ptr->orig_node_name = strdup_internal(vl_node_name);
        vl_var_data_ptr->remote_var_name = strdup_internal((char *)((const char *)pl_remoteDataVarName));
#else
        vl_var_data_ptr->orig_node_name = (char *)c_str_empty;
        vl_var_data_ptr->remote_var_name = (char *)c_str_empty;           // Remote Variable Name
#endif
        vl_var_data_ptr->it_values_list.clear();
        vl_var_data_ptr->deleted = false;
        vl_var_data_ptr->it_values_list = vl_full_it_values;
        ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - UPDATE - v_variableData(%s)[%s : %ld]{%ld : %s}: %d",vl_var_data_ptr->widget_id,vl_var_data_ptr->orig_node_name,(long)( vl_var_data_ptr->orig_node_ptr),vl_var_data_ptr->local_var_idx, vl_var_data_ptr->remote_var_name, (int)vl_var_data_ptr->deleted);
      } else {
        // New item ->
        variable_data vl_var_data;
        vl_var_data.node_offset_id = vl_variableDataMapOffsetIDAct;
        vl_var_data.orig_node_ptr = vl_IteratorNode;
#ifdef KI_DEBUG_ACTIONS
        vl_var_data.orig_node_name = strdup_internal(vl_node_name);
        vl_var_data.remote_var_name = strdup_internal((char *)((const char *)pl_remoteDataVarName));
#else
        vl_var_data.orig_node_name = (char *)c_str_empty;
        vl_var_data.remote_var_name = (char *)c_str_empty;           // Remote Variable Name
#endif
        vl_var_data.widget_id = vl_attrid;
        vl_var_data.local_var_idx = (long)pl_localDataVarId;
        vl_var_data.it_values_list.clear();
        vl_var_data.deleted = false;
        vl_var_data.it_values_list = vl_full_it_values;

        // Set offset ID
        set_node_offset_id(vl_process_data_ptr->cloneDocPTR, vl_copyNode, vl_variableDataMapOffsetIDAct);
        // Insert variable data map
        v_variableData.push_back(vl_var_data);
        //v_variableDataMap.insert(std::make_pair(vl_copyNode,(v_variableData.size()-1)));
        v_variableDataMap.insert(std::make_pair(vl_var_data.node_offset_id,(v_variableData.size()-1)));
        ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - INSERT - v_variableData(%s)[%s : ID=%ld]{%ld : %s}: %d",vl_var_data.widget_id,vl_var_data.orig_node_name, vl_var_data.node_offset_id, vl_var_data.local_var_idx, vl_var_data.remote_var_name, (int)vl_var_data.deleted);
      }
      // --------------------------------------------------------------------------------------------------------------------
      STOP_MEASURE(8);
      // --------------------------------------------------------------------------------------------------------------------
      // STOP[8]:

    }
    --EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[vl_process_data_ptr->waitforIdx];
    ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__iterator - END - (%ld)*[%ld:%ld]", (unsigned long)vl_process_data_ptr,(unsigned long)pl_userData[0].get_long_long_val(),(unsigned long)pl_userData[1].get_long_long_val());
    action_print_node("f__EPTF__UIHandler__expand__current__iterator - vl_copyNode - END {%s}",vl_copyNode);

    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: set_itvalue_idx
  //
  //  Purpose:
  //    Set ITValues idx into orig_itvalues attribute
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_value_idx - *in* <char *> the saved IT value index in string
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Save ITValues idx into orig_itvalues attribute.
  //
  ///////////////////////////////////////////////////////////
  void set_itvalue_idx(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_node, char * pl_value_idx) {
    // update itvalues idx in attribute
    xml_attribute<> * vl_attrorig =  pl_node->first_attribute((char *)c_str_orig_itvalues);
    char * vl_orig_itvalues = (char *)c_str_empty;
    if(vl_attrorig != NULL){
      vl_orig_itvalues = vl_attrorig->value();
    }
    ACTION_GUIFUNCTIONS("set_itvalue_idx[%ld]iterator:orig:{%s}:[%s]", (unsigned long) pl_node, vl_orig_itvalues, pl_value_idx);
    // If not contain -
    std::string vl_str_act_itvalues(vl_orig_itvalues);
    if(vl_str_act_itvalues.length()==0 || vl_str_act_itvalues.find(pl_value_idx) == std::string::npos){
      vl_str_act_itvalues.append(pl_value_idx);
      ACTION_GUIFUNCTIONS("set_itvalue_idx-pl_clone_doc_ptr[%ld]iterator:orig:{%s}:vl_str_act_itvalues[%s]", (unsigned long) pl_clone_doc_ptr, vl_orig_itvalues, vl_str_act_itvalues.c_str());
      char * vl_attrValuePtr = pl_clone_doc_ptr->allocate_string(vl_str_act_itvalues.c_str());
      ACTION_GUIFUNCTIONS("set_itvalue_idx[%ld]iterator:orig:{%s}:vl_attrValuePtr[%s]", (unsigned long) pl_node, vl_orig_itvalues, vl_attrValuePtr);
      // If have orig
      if(vl_attrorig != NULL){
       pl_node->remove_attribute(vl_attrorig);
      }
      ACTION_GUIFUNCTIONS("set_itvalue_idx[%ld]iterator:orig:{%s}:vl_attrValuePtr[%s]-1", (unsigned long) pl_node, vl_orig_itvalues, vl_attrValuePtr);
      xml_attribute<> * vl_attrnext = pl_clone_doc_ptr->allocate_attribute(c_str_orig_itvalues, vl_attrValuePtr);
      ACTION_GUIFUNCTIONS("set_itvalue_idx[%ld]iterator:orig:{%s}:vl_attrValuePtr[%s]-2", (unsigned long) pl_node, vl_orig_itvalues, vl_attrValuePtr);
      pl_node->append_attribute(vl_attrnext);
      ACTION_GUIFUNCTIONS("set_itvalue_idx[insert]{%s in %s}", pl_value_idx, vl_attrValuePtr);
    } else {
      ACTION_GUIFUNCTIONS("set_itvalue_idx[DUPLICATE]{%s in %s}", pl_value_idx, pl_node->name());
    }
    return;
  }


  ///////////////////////////////////////////////////////////
  //  Function: get_node_iterators_and_playlist
  //
  //  Purpose:
  //    Request node iterators an playlist elements
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_iteratorList - *out* vector< xml_node<>* > * - vector PTR for iterator nodes
  //   pl_playList - *out* vector< playListItem > * - vector for playlist elements
  //
  //  Return Value:
  //    Returns the PTRs of itarators an playlist elements.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Request node iterators an playlist elements.
  //
  ///////////////////////////////////////////////////////////
  void get_node_iterators_and_playlist( xml_node<>* pl_node, vector< xml_node<>* > * pl_iteratorList, vector< playListItem > * pl_playList)
  {
    if(is_iterator_like(pl_node->name(), pl_node->name_size()) == true)
    {
      pl_iteratorList->push_back(pl_node);
      ACTION_GUIFUNCTIONS("get_node_iterators_and_playlist -> PUSH[iterator]: %s",pl_node->name());
    }
    if(pl_playList!=0){
      // check ID and if its in playlist play it ADD / REMOVE
      char * vl_widgetID = get_attribute(pl_node, (char *)c_str_id);
      if(*vl_widgetID != '\0') {
        // check if exist
        std::map<std::string, playlist_data>::iterator vl_playlist_map = v_playlistWidgetIDMap.find(std::string(vl_widgetID));
        if(vl_playlist_map != v_playlistWidgetIDMap.end()){
          playlist_data vl_playlist = vl_playlist_map->second;
          playListItem vl_playlist_out;
          vl_playlist_out.widget_id = vl_widgetID;                            // widget ID
          vl_playlist_out.node_ptr = pl_node;                                 // Node PTR
          vl_playlist_out.playlist_node_ptr = vl_playlist.playlist_node_ptr;  // Playlist Node PTR
          vl_playlist_out.idx = vl_playlist.idx;                              // The sorting idx
          pl_playList->push_back(vl_playlist_out);
          ACTION_GUIFUNCTIONS("get_node_iterators_and_playlist -> PUSH[playlist]: %s",pl_node->name());
        }
      }
    }
    // Thru the node list
    for (xml_node<> * vl_act_node = pl_node->first_node(); vl_act_node; vl_act_node = vl_act_node->next_sibling()){
      get_node_iterators_and_playlist(vl_act_node, pl_iteratorList, pl_playList);
    }
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: process_iterator_node
  //
  //  Purpose:
  //    Process iterator node
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_source - *in* <xml_node<> * > - node pointer
  //   pl_it_values - *in* vector<std::pair<string, string> > * - vector PTR change from | to values
  //   pl_it_idx - *in* <int> the saved IT value index in v_ITValues buffer
  //   pl_xml_as_string *in* <string> processed xml string
  //
  //  Return Value:
  //    Returns the PTR of the result node.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Process iterator node.
  //
  ///////////////////////////////////////////////////////////
  xml_node<>*  process_iterator_node(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_source,map<string, string> * pl_it_values, int pl_it_idx, string pl_xml_as_string)
  {
  xml_node<> * vl_result = NULL;
#ifdef USE_TIME_MEASURE
  start_timer(&v_timer_process_start, &v_tv_process);
#endif
    // Replace Iterator values
    // ==================================================================================
    START_MEASURE(9, "process_iterator_node: replace_all[map]");
    // START[9]:
    // --------------------------------------------------------------------------------------------------------------------
    // Process XML
    // find all %<iteratorvalue>% and replece it from pl_it_values map
    std::ostringstream vl_sstream;
    char * vl_start = (char *) pl_xml_as_string.c_str();  // start ptr
    char * vl_next = strchr(vl_start,'%');                // next item begin ptr
    char * vl_nextend = 0;                                // next item end ptr
    char vl_savedch = '\0';                               // saved character
    while(vl_next!=0) {
      vl_nextend = strchr(vl_next+1,'%');
      if(vl_nextend!=0) {                                 // If have next item
        vl_savedch = *(vl_nextend+1);
        *(vl_nextend+1) = '\0';                           // close end
        // search in pl_it_values map
        std::map<std::string , std::string>::iterator vl_it_map = pl_it_values->find(vl_next);
        if(vl_it_map !=  pl_it_values->end()) {           // If there is replace value
          ACTION_GUIFUNCTIONS("process_iterator_node change: %s -> %s",vl_next,vl_it_map->second.c_str());
          *vl_next = '\0';                                // close begin
          vl_sstream << vl_start<<vl_it_map->second;      // copy previous string + replaced string

          *vl_next = '%';                                 // restore begin
          *(vl_nextend+1) = vl_savedch;                   // restore end
          vl_start = vl_nextend+1;                        // next start ptr
          vl_next = strchr(vl_start,'%');                 // search next begin
        } else {
          *(vl_nextend+1) = vl_savedch;                   // restore end
          vl_next = vl_nextend;                           // next start ptr
        }
      } else {
        vl_next = 0;
      }
    }
    vl_sstream << vl_start;
    pl_xml_as_string = vl_sstream.str();
    ACTION_GUIFUNCTIONS("process_iterator_node -> [pl_xml_as_string]: %s",pl_xml_as_string.c_str());
    STOP_MEASURE(9);

    START_MEASURE(11, "process_iterator_node: parse");
    // START[11]:
    // --------------------------------------------------------------------------------------------------------------------
    // Parse the procesed XML string
    // ==================================================================================
    char * vl_cstr = strdup_internal((const char *)pl_xml_as_string.c_str());
    // Load check, because if its invalid the rapidxml crash
    xml_document<> vl_print_doc;
    vl_print_doc.clear();
    xml_node<> * vl_print_root_node;
    try {
      vl_print_doc.parse<parse_no_entity_translation>(vl_cstr);
      vl_print_root_node = vl_print_doc.first_node();
    } catch (int vl_err) {
      vl_print_root_node = NULL;
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "ERROR: The XML doc cannot be loaded or is not well-formed: {%d}!", vl_err);
    }
    if(vl_print_root_node != NULL) {
      // Finally clone it
      vl_result = pl_clone_doc_ptr->clone_node(vl_print_root_node);
      v_xmlStrs.push_back(vl_cstr);
#ifdef PRINT_USED_MEMORY
      v_xmlStrs_length += strlen(vl_cstr);
#endif
    }
    vl_print_doc.clear();
    STOP_MEASURE(11);

    action_print_node("process_iterator_node - vl_result {%s}",vl_result);

    START_MEASURE(10, "process_iterator_node: itvalues[NOT_USE_ITVALUES_STRING]");
    // set IT values index
    // ==================================================================================
    char vl_value_idx [SIZE_OF_LONG + 10];
    sprintf(vl_value_idx," %d;", pl_it_idx);
    // Set IT Values current node
    vector< xml_node<> * > vl_iteratorlist;
    vl_iteratorlist.clear();
    vector< playListItem > vl_playlist;
    vl_playlist.clear();
    // Get iterator and playlist list
    if(v_playlistWidgetIDMap.empty()==true) {
      get_node_iterators_and_playlist(vl_result, &vl_iteratorlist, 0);
    } else {
      get_node_iterators_and_playlist(vl_result, &vl_iteratorlist, &vl_playlist);
    }
    // Do iterator list
    if(vl_iteratorlist.empty()!=true) {
      for (vector<xml_node<> *>::iterator vl_nextnode = vl_iteratorlist.begin(); vl_nextnode != vl_iteratorlist.end(); vl_nextnode++){
         set_itvalue_idx(pl_clone_doc_ptr, (xml_node<> *)*vl_nextnode, vl_value_idx);
      }
    }
    action_print_node("process_iterator_node - vl_result_1 {%s}",vl_result);
    // Do playlist
    if(vl_playlist.empty()!=true) {
      // sort
      std::sort(vl_playlist.begin(), vl_playlist.end(), playlist_order());
      for (vector<playListItem>::iterator vl_playListItem = vl_playlist.begin(); vl_playListItem != vl_playlist.end(); vl_playListItem++){
        // check ID and if its in playlist play it ADD / REMOVE
        playListItem vl_playListItemAct = (playListItem)*vl_playListItem;
        xml_node<> * vl_nextnode = vl_playListItemAct.node_ptr;
        xml_node<> * vl_play_node = vl_playListItemAct.playlist_node_ptr;
        ACTION_GUIFUNCTIONS("Do playlist[vl_play_node->name()]{%s}->%s",vl_playListItemAct.widget_id, vl_play_node->name());
        // Delete child nodes variable data
        delete_node_variabledata( vl_playListItemAct.widget_id);
        // If delete
        if(compare_name( vl_play_node->name(),vl_play_node->name_size(), c_xml_delete_name, v_size_xml_delete_name) == true) {
          // Just remove nothing all
          xml_node<> * vl_parent_node = (vl_nextnode)->parent();
          if(vl_parent_node != 0) {
            vl_parent_node->remove_node(vl_nextnode);
          }
        } else {
          // Insert item ->
          // -------------------------------------------
          // clone from original
          xml_node<> * vl_parent_node = vl_nextnode;
          // action_print_node("insert_itvalue_idx[PARENT:{%s}", vl_parent_node);
          for (xml_node<> *vl_nextnode_int = vl_play_node->first_node(); vl_nextnode_int; vl_nextnode_int = vl_nextnode_int->next_sibling()) {
            //action_print_node("insert_itvalue_idx[copy->]{%s}", vl_nextnode_int);
            // 2013.11.07. - new append
            xml_node<> * vl_node_source = pl_clone_doc_ptr->clone_node(vl_nextnode_int);
            vl_parent_node->insert_node(0, vl_node_source);
            ACTION_GUIFUNCTIONS("insert_itvalue_idx[copy_X]{%ld | %ld | %ld}",(long)vl_parent_node, (long)0, (long)vl_node_source);
          }
        }
      }
    }

    STOP_MEASURE(10);

#ifdef USE_TIME_MEASURE
    stop_timer(&v_timer_process_start, &v_tv_process, &v_time_process_diff_summ);
#endif
    action_print_node("process_iterator_node - vl_result_2 {%s}",vl_result);
    return(vl_result);
  }

  ///////////////////////////////////////////////////////////
  //  Function: f__EPTF__UIHandler__expand__current__condition
  //
  //  Purpose:
  //    Nonblocking Data Handler: Expand the current node condition.
  //
  //  Parameters:
  //   pl_source *in* *charstring * - the name of the dataSource 'feature'
  //   pl_source *in* *charstring* - the name of the PTC
  //         (dataSource+ptcName should be a unigue id of the data). Default: "" (i.e. PTC name is ignored)
  //   pl_element *in* *charstring* - the type of data
  //   pl_params *in* <EPTF_DataSource_Params> - additional parameters (default: {})
  //   pl_errorCode - *in* *INTEGER&* - error code in the response
  //   pl_remoteDataVarName - *in* *charstring* -  var name on the remote component.
  //           In case errorCode!=0: it contains the error message
  //   pl_ownerCompRef - *in* *COMPONENT&* - reference to the remote component
  //   pl_localDataVarId - *in* *INTEGER&* - var id on the local component
  //                 (contains the current value, not subscribed to remote!)
  //   pl_dataValue - *in* <EPTF_Var_DirectContent> - the value of the data
  //   pl_userData - *in* <EPTF__IntegerList&> -user specific data given at the request
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Nonblocking Data Handler: Expand the current node condition.
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__expand__current__condition ( const CHARSTRING& pl_source,
                            const CHARSTRING& pl_ptcName,
                            const CHARSTRING& pl_element,
                            const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params& pl_params,
                            const INTEGER& pl_errorCode,
                            const CHARSTRING& pl_remoteDataVarName,
                            const COMPONENT& pl_ownerCompRef,
                            const INTEGER& pl_localDataVarId,
                            const EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent& pl_dataValue,
                            const EPTF__CLL__Common__Definitions::EPTF__IntegerList& pl_userData)
  {
    bool vl_conditionresult = false;
    unsigned long long vl_wrk_long_ptr = (unsigned long)pl_userData[0].get_long_long_val();
    unsigned long long vl_wrk_long_long = (unsigned long long)pl_userData[1].get_long_long_val();
    process_data * vl_process_data_ptr = (process_data *)vl_wrk_long_ptr;
    xml_node<>* vl_copyNode =  (xml_node<>*)vl_wrk_long_long;
    bool vl_pendingError = false;  // Have any error?
    char * vl_origPtr = 0;
    vl_wrk_long_long = (unsigned long long)pl_userData[2].get_long_long_val();
    char * vl_condition_id_saved = (char *)vl_wrk_long_long;
    vl_origPtr = get_attribute(vl_copyNode, (char *) c_str_orig_node_ptr);
    ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__condition: [%ld:{%s}]->%d",(unsigned long) vl_copyNode, vl_condition_id_saved, vl_conditionresult );
    if(pl_errorCode != 0 && vl_pendingError == false) {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f_EPTF_DataSource_getCondition_nonBlocking: Error[%ld] -> {%s}!", (long)pl_errorCode, (char *)((const char *) pl_remoteDataVarName) );
      vl_pendingError = true;
    } else if(*vl_origPtr == '\0'){
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "ERROR: f__EPTF__UIHandler__expand__current__condition: No original node PTR found [%ld]!", (unsigned long) vl_wrk_long_long);
      vl_pendingError = true;
    } else {
      xml_node<>* vl_IteratorNode = (xml_node<>*) (long *) atol(vl_origPtr);
      if(vl_IteratorNode) {
        //change condition to metaiterator in the copy node.
        set_node_temp_attributes(vl_process_data_ptr->cloneDocPTR, vl_copyNode, (long)vl_copyNode, (char *)((const char *)pl_remoteDataVarName),(long)pl_localDataVarId);
      }
      // pl_dataValue is a boolean with the values of the condition
      // Data type
      if (pl_dataValue.get_selection() == EPTF__Var__DirectContent::ALT_boolVal) {
        vl_conditionresult = pl_dataValue.boolVal();
        ACTION_GUIFUNCTIONS("f__EPTF__UIHandler__expand__current__condition: [%ld]->%d",(unsigned long) vl_copyNode, vl_conditionresult );
      } else {
        TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "f__EPTF__UIHandler__expand__current__condition: function handles only bool directContent for now!!");
        vl_pendingError = true;
      }
    }
    // If NO Error
    if(vl_pendingError == false && vl_condition_id_saved != 0) {
      xml_node<> * vl_pNewNode = process_condition_node(vl_process_data_ptr->cloneDocPTR, vl_copyNode, vl_conditionresult, vl_condition_id_saved);
      free(vl_condition_id_saved);	// Free saved
      // No any additional wrk needed
      if(vl_pNewNode != NULL) {
      }
      // Expand inside
      expand_XML_elements( vl_copyNode, vl_process_data_ptr, 0l);
    }
    // ===================================================================================================================

    --EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[vl_process_data_ptr->waitforIdx];
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: process_condition_node
  //
  //  Purpose:
  //    Process condition node
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_source - *in* <xml_node<> * > - node pointer
  //   pl_condition - *in* <bool > - condition value
  //   vl_conditionid - *in* <const char * > - condition ID
  //
  //  Return Value:
  //    Returns the PTR of the result node.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Process condition node.
  //
  ///////////////////////////////////////////////////////////
  xml_node<>*  process_condition_node(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_source, bool pl_condition, char * vl_conditionid )
  {
    xml_node<> * vl_result = NULL;
    vector< xml_node<> * > vl_NodesToRemove;
    vl_NodesToRemove.clear();
    // Process child nodes and attributes
    for (xml_node<> *vl_child = pl_source->first_node(); vl_child; vl_child = vl_child->next_sibling()){
      bool vl_processed = false;
      ACTION_GUIFUNCTIONS("process_condition_node-> next]: [%s]", vl_child->name());
      // If 'insertif' process it
      if(compare_name( vl_child->name(),vl_child->name_size(), c_str_insertif, v_size_insertif) == true){
        char * vl_id = get_attribute( vl_child, (char *)c_str_id);
        ACTION_GUIFUNCTIONS("process_condition_node-> ID]: [%s]", vl_id );
        if(compare(vl_conditionid, measure(vl_conditionid),vl_id,measure(vl_id))) {
          char * vl_negatePtr = get_attribute(vl_child, (char *) c_str_negate);
          ACTION_GUIFUNCTIONS("process_condition_node-> set_node_temp_attributes->]: [%s]", vl_id );
          set_node_temp_attributes(pl_clone_doc_ptr, vl_child, (long)vl_child, (char *)c_str_TODO, 0l);
          bool vl_negate = compare(vl_negatePtr,measure(vl_negatePtr),c_str_true, v_size_true);
          // If true than nothing
          if((pl_condition == true) ^ (vl_negate == true)){
            ACTION_GUIFUNCTIONS("process_condition_node-> PROCESS]: [%s]-> %d", vl_id, (int) pl_condition );
          } else {
            ACTION_GUIFUNCTIONS("process_condition_node-> ELSE]: [%s]-> %d", vl_id, (int) pl_condition );
            vl_processed = true;
            vl_NodesToRemove.push_back(vl_child);
          }
        }
      }
      // If not processed call for children
      if(vl_processed == false) {
        // If not insertif ->
        process_condition_node(pl_clone_doc_ptr, vl_child, pl_condition, vl_conditionid);
      }
    }
    for (vector<xml_node<> *>::iterator vl_remove = vl_NodesToRemove.begin(); vl_remove != vl_NodesToRemove.end(); vl_remove++){
      ACTION_GUIFUNCTIONS("process_condition_node-> REMOVE]: [%s]", ((xml_node<> * )*vl_remove)->name());
      ((xml_node<> * )*vl_remove)->remove_all_nodes();
    }

    return(vl_result);
  }

  //=========================================================================
  // SUBROUTINES
  //=========================================================================

  ///////////////////////////////////////////////////////////
  //  Function: is_valid_XML
  //
  //  Purpose:
  //    Validate the XML with the given XSD
  //
  //  Parameters:
  //   pl_xml_str - *in* <char * > - input XML string
  //   pl_schema_filename - *in* <char * > - input schema file name
  //
  //  Return Value:
  //    Returns the result of validation.
  //      - VALID_XML                         : XML valid
  //      - ERROR_INVALID_XML                 : XML invalid with XSD parser
  //      - ERROR_XML_LOAD                    : the doc cannot be loaded or is not well-formed
  //      - ERROR_XML_ROOT_ERROR              : the root element are not 'Widgets'
  //      - ERROR_XSD_LOAD                    : the schema cannot be loaded or is not well-formed
  //      - ERROR_XSD_CREATE_PARSER_CTX       : unable to create a parser context for the schema
  //      - ERROR_XSD_SCHEMA                  : the schema itself is not valid
  //      - ERROR_XSD_CREATE_VALIDATION_CTX   : unable to create a validation context for the schema
  //
  //  Errors:
  //    - is_valid_XML: The XML doc cannot be loaded or is not well-formed: {<xml string>}!
  //    - is_valid_XML: The root element in the xml is '<act root>' instead of 'Widgets'!
  //    - is_valid_XML: The the schema cannot be loaded or is not well-formed[<schema filename>]!
  //    - is_valid_XML: The schema patch [<xsd:element name="Widgets" />] invalid!
  //    - is_valid_XML: Unable to create a parser context for the schema[<schema filename>]!
  //    - is_valid_XML: The schema itself is not valid[<schema filename>]!
  //    - is_valid_XML: unable to create a validation context for the schema[<schema filename>]!
  //    - is_valid_XML: Invalid XML {<xml string>} with schema [<schema filename>] - message: <error_message>!
  //
  //  Detailed Comments:
  //    Validate the XML with the given XSD.
  //
  ///////////////////////////////////////////////////////////
  int is_valid_XML(char * pl_xml_str, const char * pl_schema_filename)
  {
    int vl_ret = VALID_XML;
    xmlInitParser();
    ACTION_GUIFUNCTIONS("is_valid_XML[pl_xml_str]->%s", pl_xml_str);
    xmlDocPtr vl_doc = xmlParseMemory(pl_xml_str, measure(pl_xml_str));
    xmlDocPtr vl_schema_doc = NULL;
    xmlSchemaPtr vl_schema = NULL;
    xmlSchemaValidCtxtPtr vl_valid_ctxt = NULL;
    xmlSchemaParserCtxtPtr vl_parser_ctxt = NULL;
    if(vl_doc == NULL) {
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: The XML doc cannot be loaded or is not well-formed: {%s}!", pl_xml_str);
      /* the doc cannot be loaded or is not well-formed */
      vl_ret = ERROR_XML_LOAD;
    } else {
      xmlNode * vl_root_element = NULL;
      /*Get the root element node */
      vl_root_element = xmlDocGetRootElement(vl_doc);
      if (xmlStrcmp(vl_root_element->name, (const xmlChar *) c_xml_root_name)) {
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: The root element in the xml is '%s' instead of '%s'!",  (const char *)vl_root_element->name, c_xml_root_name);
        /* the root element are not 'Widgets' */
        vl_ret = ERROR_XML_ROOT_ERROR;
      } else {
        ACTION_GUIFUNCTIONS("is_valid_XML[pl_schema_filename]->%s", pl_schema_filename);
        vl_schema_doc = xmlReadFile(pl_schema_filename, NULL, XML_PARSE_NONET);
        if (vl_schema_doc == NULL) {
          /* the schema cannot be loaded or is not well-formed */
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: The the schema cannot be loaded or is not well-formed[%s]!", pl_schema_filename);
          vl_ret = ERROR_XSD_LOAD;
        } else {
          vl_root_element = xmlDocGetRootElement(vl_schema_doc);
          // Patch into -> <xsd:element name="Widgets" />
          xmlNodePtr vl_newnode = xmlNewTextChild (vl_root_element, (xmlNs *)NULL, (xmlChar *)c_xml_element, NULL);
          xmlAttrPtr vl_newattr = NULL;
          if(vl_newnode) {
            vl_newattr = xmlNewProp (vl_newnode, (const xmlChar *)c_str_name, (const xmlChar *)c_xml_root_name);
          }
          if(vl_newnode == NULL || vl_newattr == NULL) {
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: The schema patch [<xsd:%s %s=\"%s\" />] invalid!", c_xml_element, c_str_name, c_xml_root_name);
          }
          vl_parser_ctxt = xmlSchemaNewDocParserCtxt(vl_schema_doc);
          if (vl_parser_ctxt == NULL) {
            /* unable to create a parser context for the schema */
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: Unable to create a parser context for the schema[%s]!", pl_schema_filename);
            vl_ret = ERROR_XSD_CREATE_PARSER_CTX;
          } else {
            vl_schema = xmlSchemaParse(vl_parser_ctxt);
            if (vl_schema == NULL) {
              /* the schema itself is not valid */
              TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: The schema itself is not valid[%s]!", pl_schema_filename);
              vl_ret = ERROR_XSD_SCHEMA;
            } else {
              vl_valid_ctxt = xmlSchemaNewValidCtxt(vl_schema);
              if (vl_valid_ctxt == NULL) {
                /* unable to create a validation context for the schema */
                TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: unable to create a validation context for the schema[%s]!", pl_schema_filename);
                vl_ret = ERROR_XSD_CREATE_VALIDATION_CTX;
              } else {
                int vl_is_valid = (xmlSchemaValidateDoc(vl_valid_ctxt, vl_doc) == 0);
                /* force the return value to be non-negative on success */
                vl_ret = vl_is_valid ? VALID_XML : ERROR_INVALID_XML;
                if(vl_ret == ERROR_INVALID_XML){
                  char * vl_errorstr = (char *)&c_str_empty[0];
                  xmlErrorPtr vl_error = xmlGetLastError();
                  if(vl_error != NULL && vl_error->message != NULL) {
                    vl_errorstr = vl_error->message;
                  }
                  TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"ERROR: is_valid_XML: Invalid XML {%s} with schema [%s] - message: %s!", pl_xml_str, pl_schema_filename, vl_errorstr);
                }
              }
            }
          }
        }
      }
    }
    if(vl_valid_ctxt != NULL) {xmlSchemaFreeValidCtxt(vl_valid_ctxt);};
    if(vl_schema != NULL) {xmlSchemaFree(vl_schema);};
    if(vl_parser_ctxt != NULL) {xmlSchemaFreeParserCtxt(vl_parser_ctxt);};
    if(vl_schema_doc != NULL) {xmlFreeDoc(vl_schema_doc);};
    if(vl_doc != NULL) {xmlFreeDoc(vl_doc);};
    xmlCleanupParser();
    return (vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: measure
  //
  //  Purpose:
  //    Measure string length
  //
  //  Parameters:
  //   pl_ptr - *in* <const char * > - input string
  //
  //  Return Value:
  //    Returns the string length.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Measure string length.
  //
  ///////////////////////////////////////////////////////////
  std::size_t measure(const char * pl_ptr)
  {
    return(internal::measure(pl_ptr));
  }

  ///////////////////////////////////////////////////////////
  //  Function: compare
  //
  //  Purpose:
  //    Compare strings
  //
  //  Parameters:
  //   pl_p1 - *in* <const char * > - input string 1
  //   pl_size1 - *in* <size_t> - input string 1 length
  //   pl_p2 - *in* <const char * > - input string 2
  //   pl_size2 - *in* <size_t> - input string 2 length
  //
  //  Return Value:
  //    Returns the result of compare ==true if equal.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Compare two string.
  //
  ///////////////////////////////////////////////////////////
  bool compare(const char * pl_p1, std::size_t pl_size1, const char * pl_p2, std::size_t pl_size2)
  {
    return(internal::compare(pl_p1, pl_size1, pl_p2, pl_size2, v_case_sensitive));
  }

  ///////////////////////////////////////////////////////////
  //  Function: compare name
  //
  //  Purpose:
  //    Compare strings
  //
  //  Parameters:
  //   pl_p1 - *in* <const char * > - input string 1 with namespace
  //   pl_size1 - *in* <size_t> - input string 1 length
  //   pl_p2 - *in* <const char * > - input string 2
  //   pl_size2 - *in* <size_t> - input string 2 length
  //
  //  Return Value:
  //    Returns the result of compare_name ==true if equal.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Compare two string without namespace <xxx:yyy /> tag.
  //
  ///////////////////////////////////////////////////////////
  bool compare_name(const char * pl_p1, std::size_t pl_size1, const char * pl_p2, std::size_t pl_size2)
  {
    bool vl_ret = false;
#ifdef CLEAR_XML_NAMESPACE_ELEMENTS
    vl_ret = internal::compare(pl_p1, pl_size1, pl_p2, pl_size2, v_case_sensitive);
#else
    int vl_idxFrom = pl_size1-pl_size2;
    if(0<=vl_idxFrom){
      vl_ret = internal::compare((pl_p1+vl_idxFrom), pl_size2, pl_p2, pl_size2, v_case_sensitive);
      ACTION_GUIFUNCTIONS("compare_name(%s->%s):%d",(pl_p1+vl_idxFrom), pl_p2, vl_ret );
    }
#endif
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: is_expandable
  //
  //  Purpose:
  //    The current node expandable?
  //
  //  Parameters:
  //   pl_name - *in* <const char * > - input entity name
  //   pl_name_size - *in* <size_t> - input name string length
  //
  //  Return Value:
  //    Returns ==true if the node name expandable.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   The current node expandable?
  //
  ///////////////////////////////////////////////////////////
  bool is_expandable(const char * pl_name, std::size_t pl_name_size)
  {
    bool vl_bret = false;
    if (pl_name){
      if (pl_name_size == 0) {
        pl_name_size = measure(pl_name);
      }
      if(compare_name(pl_name, pl_name_size, c_str_iterator, v_size_iterator) ||
         compare_name(pl_name, pl_name_size, c_str_externalvalue, v_size_externalvalue) ||
         compare_name(pl_name, pl_name_size, c_str_condition, v_size_condition)){
           vl_bret = true;
      }
    }
    return(vl_bret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: is_iterator_like
  //
  //  Purpose:
  //    The current node iterator OR externalvalue?
  //
  //  Parameters:
  //   pl_name - *in* <const char * > - input entity name
  //   pl_name_size - *in* <size_t> - input name string length
  //
  //  Return Value:
  //    Returns ==true if the node name expandable.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   The current node iterator OR externalvalue?
  //
  ///////////////////////////////////////////////////////////
  bool is_iterator_like(const char * pl_name, std::size_t pl_name_size)
  {
    bool vl_bret = false;
    if (pl_name){
      if (pl_name_size == 0) {
        pl_name_size = measure(pl_name);
      }
      if(compare_name(pl_name, pl_name_size, c_str_iterator, v_size_iterator) ||
         compare_name(pl_name, pl_name_size, c_str_externalvalue, v_size_externalvalue)){
           vl_bret = true;
      }
    }
    return(vl_bret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: get_data_source
  //
  //  Purpose:
  //    Get the data source
  //
  //  Parameters:
  //   pl_expandable - *in* <xml_node<> * > - node pointer
  //   pl_element_out - *out* <char ** > - element pointer out
  //   pl_id_out - *out* <char ** > - id pointer out
  //   pl_ptcname_out - *out* <char ** > - ptc pointer out
  //   pl_source_out - *out* <char ** > - source pointer out
  //
  //  Return Value:
  //    Returns ==true if no any error.
  //
  //  Errors:
  //    - get_data_parameters: unknown attribute [<attribute name>=<attribute value>]!
  //    - get_data_parameters: invalid call!");
  //
  //  Detailed Comments:
  //   Get the data source.
  //
  ///////////////////////////////////////////////////////////
  bool get_data_source( xml_node<>*  pl_expandable,
                        char ** pl_element_out, char ** pl_id_out, char ** pl_ptcname_out, char ** pl_source_out)
  {
    bool vl_bret = false;
    if(pl_expandable && pl_element_out && pl_id_out && pl_ptcname_out && pl_source_out){
      *pl_element_out = (char *)&c_str_empty;
      *pl_id_out = (char *)&c_str_empty;
      *pl_ptcname_out = (char *)&c_str_empty;
      *pl_source_out = (char *)&c_str_empty;
      for (xml_attribute<> *vl_attr = ((xml_node<> * )pl_expandable)->first_attribute(); vl_attr; vl_attr = vl_attr->next_attribute())
      {
        std::size_t vl_size_attrname = measure(vl_attr->name());
        vl_bret = true;
        if(compare(vl_attr->name(), vl_size_attrname, c_str_element, v_size_element)){
          *pl_element_out = vl_attr->value();
        } else if(compare(vl_attr->name(), vl_size_attrname, c_str_id, v_size_id)){
          *pl_id_out = vl_attr->value();
        } else if(compare(vl_attr->name(), vl_size_attrname, c_str_ptcname, v_size_ptcname)){
          *pl_ptcname_out = vl_attr->value();
        } else if(compare(vl_attr->name(), vl_size_attrname, c_str_source, v_size_source)){
          *pl_source_out = vl_attr->value();
        } else if(compare(vl_attr->name(), vl_size_attrname, c_str_orig_node_ptr, v_size_orig_node_ptr)){
        } else {
          vl_bret = false;
          for(unsigned int vl_idx=0; vl_idx< (sizeof(v_temp_attributes) / sizeof(const char *)) && vl_bret == false; vl_idx++) {
            vl_bret = compare(vl_attr->name(), vl_size_attrname, v_temp_attributes[vl_idx], *(v_temp_attributes_size[vl_idx]));
          }
          if(vl_bret == false) {
            TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "get_data_parameters: unknown attribute [%s=%s]!", vl_attr->name(), vl_attr->value());
          }
        }
      }
    } else {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "get_data_parameters: invalid call!");
    }
    return(vl_bret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: get_attribute
  //
  //  Purpose:
  //    Get the requested attribute
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_attributeName - *in* <char * > - attribute name
  //
  //  Return Value:
  //    Returns the PTR of the requested attribute or
  //            PTR of c_str_empty=="" if not found.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the requested node attribute.
  //
  ///////////////////////////////////////////////////////////
  char * get_attribute( xml_node<>*  pl_node, char * pl_attributeName)
  {
    char * vl_attributeValue = (char *) c_str_empty;
    if(pl_node && pl_attributeName){
      xml_attribute<> * vl_attr =  ((xml_node<> * )pl_node)->first_attribute(pl_attributeName);
      if(vl_attr != NULL){
        vl_attributeValue = vl_attr->value();
      }
    } else {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "get_attribute invalid call[%s]!", pl_attributeName);
    }
    return vl_attributeValue;
  }

  ///////////////////////////////////////////////////////////
  //  Function: copy_node
  //
  //  Purpose:
  //    Clone and copy xml nodes and attributes
  //
  //  Parameters:
  //   pl_doc - *in* <xml_document<> *> - the rapidxml document
  //   pl_node_src - *in* <xml_node<> * > - source node pointer
  //   pl_node_dest - *in* <xml_node<> * > - destination node pointer
  //   pl_reset_orig - *in* <bool > - reset the original name | attributes
  //
  //  Return Value:
  //    Returns the result of clone process.-
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Clone and copy xml nodes and attributes.
  //
  ///////////////////////////////////////////////////////////
  bool copy_node( xml_document<> * pl_doc, xml_node<>* pl_node_src, xml_node<>* pl_node_dest,  bool pl_reset_orig) {
    bool vl_ret = false;
    // Reset if requested
    if(pl_reset_orig == true) {
      pl_node_dest->remove_all_nodes();
      pl_node_dest->remove_all_attributes();

      // Clone name and value
      pl_node_dest->name(pl_node_src->name(), pl_node_src->name_size());
      pl_node_dest->value(pl_node_src->value(), pl_node_src->value_size());
    }

    // Clone and add child nodes and attributes
    for (xml_node<> *child = pl_node_src->first_node(); child; child = child->next_sibling()) {
      pl_node_dest->append_node(pl_doc->clone_node(child));
    }
    for (xml_attribute<> *attr = pl_node_src->first_attribute(); attr; attr = attr->next_attribute()) {
      pl_node_dest->append_attribute(pl_doc->allocate_attribute(attr->name(), attr->value(), attr->name_size(), attr->value_size()));
    }
    if(compare_name(pl_node_src->name(), pl_node_src->name_size(), pl_node_dest->name(), pl_node_dest->name_size())==true) {
      vl_ret = true;
    }
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: get_metaiterator_itvalues
  //
  //  Purpose:
  //    Get the metaiterator itvalues
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - node pointer
  //
  //  Return Value:
  //    Returns the PTR of the requested itvalues or
  //            PTR of c_str_empty=="" if not found.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the metaiterator itvalues.
  //
  ///////////////////////////////////////////////////////////
  const char * get_metaiterator_itvalues( xml_node<>*  pl_node )
  {
    char * vl_attrITValues = (char *) c_str_empty;
    xml_node<>* vl_Node = pl_node;
    while(vl_Node->parent()) {
      if(compare_name(vl_Node->name(), vl_Node->name_size(),c_str_metaiterator, v_size_metaiterator) == true)
      {
        vl_attrITValues = get_attribute(vl_Node, (char *) c_str_orig_itvalues);
        break;
      } else {
        vl_Node = vl_Node->parent();
      }
    }
    return vl_attrITValues;
  }

  ///////////////////////////////////////////////////////////
  //  Function: get_data_source_parameters
  //
  //  Purpose:
  //    Get the data source parameters
  //
  //  Parameters:
  //   pl_expandable - *in* <xml_node<> * > - node pointer
  //
  //  Return Value:
  //    Returns the requested EPTF_DataSource_Params.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the data source parameters.
  //
  ///////////////////////////////////////////////////////////
  EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params get_data_source_parameters( xml_node<> *  pl_expandable)
  {
    EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_params(NULL_VALUE);
    // If have params
    xml_node<> * vl_next_node = pl_expandable->first_node();
    if(vl_next_node){
        if(vl_next_node && compare_name(vl_next_node->name(), measure(vl_next_node->name()), c_str_params, v_size_params)){
          // Collect parameters
        for (xml_node<> * data_node = vl_next_node->first_node(); data_node; data_node = data_node->next_sibling()){
          if(compare_name(data_node->name(), data_node->name_size(),c_str_dataparam, v_size_dataparam) == true)
          {
            char * vl_name = 0;
            char * vl_value = 0;
            // On attriutes while not found
            for (xml_attribute<> *vl_attr = data_node->first_attribute(); (vl_attr && (vl_name==0 || vl_value==0) ) ; vl_attr = vl_attr->next_attribute())
            {
              size_t vl_attr_size = measure(vl_attr->name());
              if(compare(vl_attr->name(), vl_attr_size,c_str_name, v_size_name) == true){
                vl_name = (char *)vl_attr->value();
              } else if(compare(vl_attr->name(), vl_attr_size,c_str_value, v_size_value) == true){
                vl_value = (char *)vl_attr->value();
              }
            }
            // if found
            if(vl_name != 0 && vl_value != 0){
              INTEGER vl_dataParamsSize(vl_params.size_of());
              vl_params[vl_dataParamsSize].paramName() = vl_name;
              vl_params[vl_dataParamsSize].paramValue() = vl_value;
            }
          }
        }
      }
    }
    return(vl_params);
  }

  ///////////////////////////////////////////////////////////
  //  Function: set_node_temp_attributes
  //
  //  Purpose:
  //    Set new node attributes.
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_node_id - *in* <long> - node ID
  //   pl_remote_name - *in* <char *> - remote data var name
  //   pl_local_id - *in* <long> - local data var ID
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Set new node attributes (orig_node_name;
  //   DEBUG_remote_data_varname; DEBUG_local_data_varid),
  //   set name to 'metaiterator'.
  //
  ///////////////////////////////////////////////////////////
  void set_node_temp_attributes(xml_document<> * pl_clone_doc_ptr,  xml_node<> * pl_node, long pl_node_id, char * pl_remote_name, long pl_local_id)
  {
	char v_tmpvalLONG[SIZE_OF_LONG+1] = "";  // cache
	char v_tmpvalue_metadata[2*SIZE_OF_LONG+1] = ""; // cache
    sprintf(v_tmpvalLONG,"%ld",pl_local_id);
    // append original name attribute [orig_node_name]
    char * vl_orig_node_name =  pl_node->name();
    char * vl_attrValuePtr = pl_clone_doc_ptr->allocate_string(vl_orig_node_name);
    xml_attribute<> *vl_attr = pl_clone_doc_ptr->allocate_attribute(c_str_orig_node_name, vl_attrValuePtr);
    pl_node->append_attribute(vl_attr);
    // metadata = <node_ptr>_<localdatavarid>
    sprintf(v_tmpvalue_metadata,"%ld_%s", pl_node_id, v_tmpvalLONG);
    vl_attrValuePtr = pl_clone_doc_ptr->allocate_string((char *)((const char *)v_tmpvalue_metadata));
    vl_attr = pl_clone_doc_ptr->allocate_attribute(c_str_metadata, vl_attrValuePtr);
    pl_node->append_attribute(vl_attr);
    // set: name->metaiterator
    pl_node->name(c_str_metaiterator);
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: set_node_offset_id
  //
  //  Purpose:
  //    Set new node attributes.
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_offset_id - *in* <long> - offset ID
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Set new node attributes (orig_node_name;
  //   DEBUG_remote_data_varname; DEBUG_local_data_varid),
  //   set name to 'metaiterator'.
  //
  ///////////////////////////////////////////////////////////
  void set_node_offset_id(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_node, long pl_offset_id)
  {
	char v_tmpvalLONG[SIZE_OF_LONG+1] = "";  // cache
    sprintf(v_tmpvalLONG,"%ld",pl_offset_id);
    char * vl_attrValuePtr = pl_clone_doc_ptr->allocate_string((char *)((const char *)v_tmpvalLONG));
    xml_attribute<> *vl_attr = pl_clone_doc_ptr->allocate_attribute(c_str_offset_id, vl_attrValuePtr);
    pl_node->append_attribute(vl_attr);
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: out_process_for_metaiterators
  //
  //  Purpose:
  //    Output process for iterator elements
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //   pl_parentiterator - *in* <xml_node<> * > - parent iterator node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Remove condition metaiterators, the iterator and
  //    externalvalue tags rename for iterator
  //    Remove debug attributes (orig_node_name;
  //    DEBUG_remote_data_varname; DEBUG_local_data_varid).
  //
  ///////////////////////////////////////////////////////////
  void out_process_for_metaiterators(xml_document<> * pl_clone_doc_ptr, xml_node<>* pl_node, xml_node<>* pl_parentiterator) {
    ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> Node: [%s]->%ld, %ld",pl_node->name(), (long)pl_node, (long)pl_parentiterator);
    if(pl_node->parent()==NULL) {
      return;
    }
    xml_node<>* vl_parentiterator = pl_parentiterator;
    /* 2013.09.27: */
    // If iterator node
    if(compare_name( pl_node->name(), pl_node->name_size(), c_str_iterator, v_size_iterator)) {
       // Processed
       return;
    } else
    // If metaiterator node
    if(compare_name( pl_node->name(), pl_node->name_size(), c_str_metaiterator, v_size_metaiterator)) {
       xml_node<> * vl_parent_node = pl_node->parent();
       xml_node<> * vl_first_node = pl_node->first_node();
       pl_node->name(c_str_iterator);     // Set name to iterator
       // If first node param list, remove it
       if(vl_first_node!=NULL && compare_name( vl_first_node->name(), vl_first_node->name_size(), c_str_params, v_size_params)) {
         ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> Remove: [%s]->%s", pl_node->name(), vl_first_node->name());
         pl_node->remove_first_node();
       }
       char * vl_orig_node_name = get_attribute(pl_node, (char *)c_str_orig_node_name);
       std::size_t vl_orig_node_name_size = measure(vl_orig_node_name);
       // If the meta iterator iterator or externalvalue =>
       // rename to iterator; next parent this; insert into clone widget id map;
       // than call recursive all node list
       // ----------------------------------------------------------------------
       if( 0<vl_orig_node_name_size && (is_iterator_like(vl_orig_node_name, vl_orig_node_name_size) == true )) {
         vl_parentiterator = pl_node;       // Next parent iterator this
         // Thru the node list
         for (xml_node<> * vl_act_node = pl_node->first_node(); vl_act_node; ) {
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> ITERATOR: [%s]->%ld, %ld",vl_act_node->name(), (long)vl_act_node, (long)vl_parentiterator);
           out_process_for_metaiterators(pl_clone_doc_ptr, vl_act_node, vl_parentiterator);
           if(vl_act_node->parent()==NULL){
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> BREAK(ITERATOR): [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
             vl_act_node = NULL;
           } else {
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> NEXT(ITERATOR): [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
             vl_act_node = vl_act_node->next_sibling();
           }
         }
         // Get Orig node PTR
         char * vl_origPtr = get_attribute(pl_node, (char *) c_str_orig_node_ptr);
         xml_node<>* vl_origNode = (xml_node<>*) (long *) atol(vl_origPtr);
         // Remove temp attributes
         remove_nodes_temp_attributes(pl_node);
         // insert to clone widget id map ->
         char * vl_widgetID = get_attribute(pl_node, (char *)c_str_id);
         if(0<measure(vl_widgetID)) {
           node_parent vl_node_parent;
           vl_node_parent.orig_node_ptr = vl_origNode;                    // Original Node PTR
           vl_node_parent.iterator_like = false;                          // The Item NOT iterator or externalvalue
           std::ostringstream vl_itteretor_id;
           vl_itteretor_id << vl_widgetID << "_" << v_iterator_idx++;
           vl_node_parent.node_offset_id = 0l;
           vl_node_parent.parent_offset_node_id = 0l;
           char * vl_node_offset_id_str =  get_attribute(pl_node, (char *)c_str_offset_id);
           long vl_node_offset_id;
           if(vl_node_offset_id_str!=0 && *vl_node_offset_id_str!='\0') {
         	vl_node_offset_id = atol(vl_node_offset_id_str);
         	if(0l<vl_node_offset_id) {
         		vl_node_parent.node_offset_id = vl_node_offset_id;
         	}
           } else {
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> [%s]: <>", pl_node->name());
           }
           // get parent id
           if(vl_parentiterator!=0) {
        	 char * vl_parent_node_offset_id_str =  get_attribute(vl_parentiterator, (char *)c_str_offset_id);
             if(vl_parent_node_offset_id_str!=0 && *vl_parent_node_offset_id_str!='\0') {
         	   vl_node_offset_id = atol(vl_parent_node_offset_id_str);
         	   if(0l<vl_node_offset_id) {
         		 vl_node_parent.parent_offset_node_id = vl_node_offset_id;
         	   }
             }
           }
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators[ITERATOR]-> [%ld]-> %ld={%s}", vl_node_parent.parent_offset_node_id,vl_node_parent.node_offset_id,vl_node_offset_id_str);
           // If have erease it
           std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.find(vl_itteretor_id.str());
           if(vl_node_map != v_cloneWidgetIDMap.end()) {
        	 v_cloneWidgetIDMap.erase(vl_itteretor_id.str());
           }
           v_cloneWidgetIDMap.insert(std::make_pair(vl_itteretor_id.str(), vl_node_parent));
           ACTION_GUIFUNCTIONS("process_for_clonemap[metaiterator]-> [%s:%ld]: WIDGETID[%s:%s]->%d", pl_node->name(), (long)vl_parentiterator, vl_widgetID, vl_itteretor_id.str().c_str(), (int)v_cloneWidgetIDMap.size());
         }
       } else {
         // CONDITION: Remove the condition item;
         // --------------------------------------------------------------------
         xml_node<> * vl_next_processed_node = 0;
         // Move if required
         xml_node<> * vl_prev_node = pl_node->next_sibling();
         xml_node<> * vl_act_node = pl_node->first_node();
         // If have internal: Thru the node list -> append to parent
         if(vl_act_node!=0) {
           for (; vl_act_node !=0 ; vl_act_node = vl_act_node->next_sibling()) {
             xml_node<> * vl_clone_act_node = pl_clone_doc_ptr->clone_node( vl_act_node );
             vl_parent_node->insert_node(vl_prev_node, vl_clone_act_node);
             // If have internal, the first internal
             vl_next_processed_node = vl_clone_act_node;
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> Parent insert: [%s]->%s(%ld : %ld, %ld -> %ld)", vl_parent_node->name(), vl_act_node->name(), (long)vl_parent_node, (long)vl_prev_node, (long)vl_act_node, (long)vl_clone_act_node);
             vl_prev_node = vl_clone_act_node->next_sibling();
           }
         } else {
           // If no internal the next sibling node
           vl_next_processed_node = pl_node->next_sibling();
         }
         ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> Remove from parent: [%s]->%s(%ld)", vl_parent_node->name(), pl_node->name(),(long)pl_node);
         vl_parent_node->remove_node(pl_node);
         // Thru the parent node list -> process it
         for (xml_node<> * vl_act_node = /* 2013.09.27: vl_next_processed_node */vl_parent_node->first_node(); vl_act_node; ) {
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> COND: [%s]->%ld, %ld",vl_act_node->name(), (long)vl_act_node, (long)vl_parentiterator);
           out_process_for_metaiterators(pl_clone_doc_ptr, vl_act_node, vl_parentiterator);
           if(vl_act_node->parent()==NULL){
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> BREAK[COND]: [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
             vl_act_node = NULL;
           } else {
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> NEXT[COND]: [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
             vl_act_node = vl_act_node->next_sibling();
           }
         }
       }
     } else {
       // NO ITERATOR & CONDITION
       // --------------------------------------------------------------------
       // Get Orig node PTR
       char * vl_origPtr = get_attribute(pl_node, (char *) c_str_orig_node_ptr);
       // If not removed already
       if(vl_origPtr!= 0 && *vl_origPtr!='\0') {
         xml_node<>* vl_origNode = (xml_node<>*) (long *) atol(vl_origPtr);
         // Remove temp attributes
         remove_nodes_temp_attributes(pl_node);
         // insert to clone widget id map ->
         char * vl_widgetID = get_attribute(pl_node, (char *)c_str_id);
         if(0<measure(vl_widgetID)) {
           node_parent vl_node_parent;
           vl_node_parent.orig_node_ptr = vl_origNode;                    // Original Node PTR
           vl_node_parent.iterator_like = false;                          // The Item NOT iterator or externalvalue
           vl_node_parent.node_offset_id = 0l;
           vl_node_parent.parent_offset_node_id = 0l;
           char * vl_node_offset_id_str =  get_attribute(pl_node, (char *)c_str_offset_id);
           long vl_node_offset_id;
           if(vl_node_offset_id_str!=0 && *vl_node_offset_id_str!='\0') {
        	  vl_node_offset_id= atol(vl_node_offset_id_str);
       	      if(0l<vl_node_offset_id) {
       		    vl_node_parent.node_offset_id = vl_node_offset_id;
       	      }
           } else {
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> [%s]: <>", pl_node->name());
           }
           // get parent id
           if(vl_parentiterator!=0) {
        	 char * vl_parent_node_offset_id_str =  get_attribute(vl_parentiterator, (char *)c_str_offset_id);
             if(vl_parent_node_offset_id_str!=0 && *vl_parent_node_offset_id_str!='\0') {
       	       vl_node_offset_id = atol(vl_parent_node_offset_id_str);
       	         if(0l<vl_node_offset_id) {
       		       vl_node_parent.parent_offset_node_id = vl_node_offset_id;
       	         }
             }
           }
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> [%ld]-> %ld={%s}", vl_node_parent.parent_offset_node_id,vl_node_parent.node_offset_id,vl_node_offset_id_str);
           // If have erease it
           std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.find(std::string(vl_widgetID));
           if(vl_node_map != v_cloneWidgetIDMap.end()) {
      	     v_cloneWidgetIDMap.erase(std::string(vl_widgetID));
           } else {
             ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> widgetID : [%s]->%ld",pl_node->name(), (long)pl_node);
           }
           v_cloneWidgetIDMap.insert(std::make_pair(std::string(vl_widgetID), vl_node_parent));
           ACTION_GUIFUNCTIONS("process_for_clonemap[normal]-> [%s:%ld]: WIDGETID[%s:%ld]->%d", pl_node->name(), (long)vl_parentiterator, vl_widgetID,(long)vl_node_parent.orig_node_ptr, (int)v_cloneWidgetIDMap.size());
         }
       } else {
         ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> NO Orig node PTR : [%s]->%ld",pl_node->name(), (long)pl_node);
       }
       // Thru the node list
       for (xml_node<> * vl_act_node = pl_node->first_node(); vl_act_node; ) {
         ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> : [%s]->%ld, %ld",vl_act_node->name(), (long)vl_act_node, (long)vl_parentiterator);
         out_process_for_metaiterators(pl_clone_doc_ptr, vl_act_node, vl_parentiterator);
         if(vl_act_node->parent()==NULL){
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> BREAK: [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
           vl_act_node = NULL;
         } else {
           ACTION_GUIFUNCTIONS("out_process_for_metaiterators-> NEXT: [%s]->%ld", vl_act_node->name(),(long)vl_act_node);
           vl_act_node = vl_act_node->next_sibling();
         }
       }
     }
     return;
  }


  ///////////////////////////////////////////////////////////
  //  Function: remove_nodes_temp_attributes
  //
  //  Purpose:
  //    Remove debug attributes
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Remove debug attributes (orig_node_name;
  //    DEBUG_remote_data_varname; DEBUG_local_data_varid).
  //
  ///////////////////////////////////////////////////////////
  void remove_nodes_temp_attributes( xml_node<>* pl_node)
  {
    ACTION_GUIFUNCTIONS("remove_nodes_temp_attributes-> Node: [%s]->%ld",pl_node->name(), (long)pl_node);
    // Not metaiterator just remove the debug attributes
    xml_attribute<> * vl_saved_attributes[(sizeof(v_temp_attributes) / sizeof(const char *))+1];
    unsigned int vl_change_count = 0;
    // Clear buffer
    for(unsigned int vl_idx = 0; vl_idx<sizeof(v_temp_attributes) / sizeof(const char *); vl_idx++) {
      vl_saved_attributes[vl_idx] = NULL;
    }
    // Thru the attribute list
    for (xml_attribute<> *vl_attr = ((xml_node<> * )pl_node)->first_attribute(); vl_attr && vl_change_count<sizeof(v_temp_attributes) / sizeof(const char *); vl_attr = vl_attr->next_attribute()) {
      std::size_t vl_size_attrname = measure(vl_attr->name());
      bool vl_found = false;
      for(unsigned int vl_idx=0; vl_found==false && vl_idx< sizeof(v_temp_attributes) / sizeof(const char *); vl_idx++) {
        // Remove if Temporary
        if(compare(vl_attr->name(), vl_size_attrname, v_temp_attributes[vl_idx], *(v_temp_attributes_size[vl_idx]))) {
          NO_ACTION_GUIFUNCTIONS("remove_nodes_temp_attributes-> PUSH]: [%s]-> %ld",vl_attr->name(), (long) vl_attr );
          vl_saved_attributes[vl_change_count] = vl_attr;
          vl_change_count++;
          vl_found = true;
        }
      }
    }
    // Thru the buffered attributes
    for(unsigned int vl_idx = 0; vl_idx<sizeof(v_temp_attributes) / sizeof(const char *) && vl_saved_attributes[vl_idx] != NULL; vl_idx++) {
      pl_node->remove_attribute(vl_saved_attributes[vl_idx]);
      NO_ACTION_GUIFUNCTIONS("remove_nodes_temp_attributes-> Remove: [%ld]->", (long) vl_saved_attributes[vl_idx] );
    }
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: delete_node_variabledata
  //
  //  Purpose:
  //    Delete child node variable data and map
  //
  //  Parameters:
  //   pl_node_id - *in* <const char *> - pl_widget_id id
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Delete child node map.
  //
  ///////////////////////////////////////////////////////////
  void delete_node_variabledata(const char * pl_widget_id){
    // insert to widget id cache ->
	if(pl_widget_id!=0 && *pl_widget_id != '\0') {
	  // A tree-t kellene tÃ¶rÃ¶lni ******************************* KI ********************************************
	  // widget id map for nodes
	  v_widgetIDMap.erase(std::string(pl_widget_id));
	  // clone widget id map for nodes
	  v_cloneWidgetIDMap.erase(std::string(pl_widget_id));
	}
	ACTION_GUIFUNCTIONS("delete_node_variabledata-> widgetID: [%s]",pl_widget_id);
	// VariableDatÃ¡t is kellene tÃ¶rÃ¶lni ******************************* KI ********************************************
    return;
  }

#ifdef CLEAR_XML_NAMESPACE_ELEMENTS
  ///////////////////////////////////////////////////////////
  //  Function: clear_namespace_elements
  //
  //  Purpose:
  //    Clear namespace elements
  //
  //  Parameters:
  //   pl_xmlstr - *in* <char * > - xml string ptr
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Clear namespace elements: <ns: | </ns:
  //
  ///////////////////////////////////////////////////////////
  void replaceAll(string& str, const string& from, const string& to) {
      if(from.empty())
          return;
      string wsRet;
      wsRet.reserve(str.length());
      size_t start_pos = 0, pos;
      while((pos = str.find(from, start_pos)) != string::npos) {
          wsRet += str.substr(start_pos, pos - start_pos);
          wsRet += to;
          pos += from.length();
          start_pos = pos;
      }
      wsRet += str.substr(start_pos);
      str.swap(wsRet); // faster than str = wsRet;
  }
  void clear_namespace_elements( char * pl_xmlstr) {
	ACTION_GUIFUNCTIONS("clear_namespace_elements[pl_xmlstr]:{%s}",pl_xmlstr);
    char * vl_ptrnamespace_start = strchr(pl_xmlstr,'<');
    char * vl_ptrnamespace_end = 0; // default NO namespace element
    // Move to start
    if(vl_ptrnamespace_start!=0 && pl_xmlstr!=vl_ptrnamespace_start){
      strcpy(pl_xmlstr,vl_ptrnamespace_start);
      vl_ptrnamespace_start = pl_xmlstr;
    } else {
      vl_ptrnamespace_start = pl_xmlstr;
    }
    // search namespace
    if(*vl_ptrnamespace_start == '<'){
      vl_ptrnamespace_end = strstr(pl_xmlstr,":Widgets");
      if(vl_ptrnamespace_end!=0) {
        vl_ptrnamespace_start = vl_ptrnamespace_end;
    	while(pl_xmlstr<vl_ptrnamespace_start && *vl_ptrnamespace_start!='<') {
          vl_ptrnamespace_start--;
    	}
    	if(*vl_ptrnamespace_start!='<'){
          vl_ptrnamespace_end = 0;
    	}
      } else {
        vl_ptrnamespace_start = strstr(pl_xmlstr,"<?xml");
        // If have XML tag -> next xml tag
        if(vl_ptrnamespace_start!=0 && vl_ptrnamespace_start==pl_xmlstr) {
          vl_ptrnamespace_start = strchr(pl_xmlstr+1,'<');
        } else {
          vl_ptrnamespace_start = strchr(pl_xmlstr,'<');
        }
        vl_ptrnamespace_end = vl_ptrnamespace_start;
        while(vl_ptrnamespace_end!=0 && *vl_ptrnamespace_end!='\0' && *vl_ptrnamespace_end!=':') {
          vl_ptrnamespace_end++;
          if(*vl_ptrnamespace_end == ' ' || *vl_ptrnamespace_end == '>') {
            vl_ptrnamespace_end = 0;
          }
        }
      }
    } else {
      vl_ptrnamespace_end = 0;
    }
    ACTION_GUIFUNCTIONS("clear_namespace_elements[%ld ; %ld]",(long)vl_ptrnamespace_start, (long)vl_ptrnamespace_end);
    // Search namespace
    if(vl_ptrnamespace_end != 0) {
      // Have namespace tag
      if(pl_xmlstr <= vl_ptrnamespace_start) {
        vl_ptrnamespace_start++;
        *vl_ptrnamespace_end = '\0';
        std::string vl_namespace_str(vl_ptrnamespace_start);
        NO_ACTION_GUIFUNCTIONS("clear_namespace_elements - namespace{%s}",vl_namespace_str.c_str());
        // <namespace:
        std::ostringstream vl_str_start_ns_tag;
        vl_str_start_ns_tag << "<" << vl_namespace_str << ":";
        // </namespace:
        std::ostringstream vl_str_end_ns_tag;
        vl_str_end_ns_tag << "</" << vl_namespace_str << ":";
        // xmlns:namespace=
        std::ostringstream vl_str_ns_tag;
        vl_str_ns_tag << "xmlns:" << vl_namespace_str << "=";
        *vl_ptrnamespace_end = ':';

        std::string vl_xml_str(pl_xmlstr);
        std::string vl_start_xml_tag_start("<");
        std::string vl_end_xml_tag_start("</");
        std::string vl_start_ns_tag("xmlns=");
        // Clear namespace starts <NS: -> <
        replaceAll(vl_xml_str,vl_str_start_ns_tag.str(),vl_start_xml_tag_start);
        // Clear namespace ends </NS: -> </
        replaceAll(vl_xml_str,vl_str_end_ns_tag.str(),vl_end_xml_tag_start);
        // Clear namespace xmlns:NS= -> xmlns=
        replaceAll(vl_xml_str,vl_str_ns_tag.str(),vl_start_ns_tag);
        // Copy back
        strcpy(pl_xmlstr,vl_xml_str.c_str());
      }
    } else if(vl_ptrnamespace_end!=0) {
      ACTION_GUIFUNCTIONS("clear_namespace_elements[vl_ptrnamespace_end-1] (%s}", vl_ptrnamespace_end-1);
    } else {
      ACTION_GUIFUNCTIONS("clear_namespace_elements[ELSE]");
    }
    ACTION_GUIFUNCTIONS("clear_namespace_elements[OUT]:{%s}",pl_xmlstr);
    return;
  }
#endif

  ///////////////////////////////////////////////////////////
  //  Function: no_action_in_guifunctions
  //
  //  Purpose:
  //    NO action in GUIFunctions
  //
  //  Parameters:
  //   pl_txt - *in* <char * > - string ptr
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    NO action in GUIFunctions.
  //
  ///////////////////////////////////////////////////////////
  void no_action_in_guifunctions(const char * pl_txt) {
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: action_print_node
  //
  //  Purpose:
  //    Action print this node
  //
  //  Parameters:
  //   pl_txt - *in* <char * > - string ptr
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    ction print this node.
  //
  ///////////////////////////////////////////////////////////
  void action_print_node( const char * pl_txt, xml_node<>* pl_node) {
#ifdef  KI_DEBUG_ACTIONS
    // Print document [rapidxml]
    xml_document<> vl_print_doc;
    vl_print_doc.clear();
    xml_node<> * vl_print_root_node;
    vl_print_root_node = vl_print_doc.clone_node(pl_node);
    vl_print_doc.append_node(vl_print_root_node);
    std::string vl_xml_proc_as_string;
    rapidxml::print(std::back_inserter(vl_xml_proc_as_string), vl_print_doc);
    ACTION_GUIFUNCTIONS(pl_txt,(char *) vl_xml_proc_as_string.c_str());
    vl_print_doc.clear();
#endif
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: escape_double_quotes
  //
  //  Purpose:
  //    Escape " - double quote characters
  //
  //  Parameters:
  //   pl_txt - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    Returns the escaped string.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Escape " - double quote characters.
  //
  ///////////////////////////////////////////////////////////
  string escape_double_quotes(const char * pl_txt) {
    std::string vl_ret("");
    vl_ret.reserve(MAX_RESERVED_ESCAPE);
    const char * pl_txt_next = pl_txt;
    const char * pl_txt_from = pl_txt;
    while(*pl_txt_next != '\0'){
      if( *pl_txt_next == '\"' ) {
        vl_ret.append(pl_txt_from, pl_txt_next-pl_txt_from);
        vl_ret.append("&quot;");
        pl_txt_from = pl_txt_next+1;
      }
      if( *pl_txt_next == '\'' ) {
        vl_ret.append(pl_txt_from, pl_txt_next-pl_txt_from);
        vl_ret.append("&apos;");
        pl_txt_from = pl_txt_next+1;
      }
      pl_txt_next++;
    }
    vl_ret.append(pl_txt_from);
    ACTION_GUIFUNCTIONS("escape_double_quotes(%s}-{%s}", pl_txt,vl_ret.c_str());
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: parse_it_values_map
  //
  //  Purpose:
  //    Parse and build the it values map
  //
  //  Parameters:
  //   pl_txt - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    Returns the it values map.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Parse and build the it values map.
  //
  ///////////////////////////////////////////////////////////
  map<string, string> parse_it_values_map(const char * pl_txt) {
    map<string, string> vl_ret;
    vl_ret.clear();
    ACTION_GUIFUNCTIONS("parse_it_values_map[START](%s}", pl_txt);
    // First " for FROM
    const char * vl_start = strchr(pl_txt,'\"');
    // If have it value
    while(vl_start!=0) {
      bool vl_found = false;
      std::string vl_str_from("");
      vl_str_from.reserve(MAX_RESERVED_PARSE);
      vl_start++;
      const char * vl_end_from = strchr(vl_start,'\"');
      while(vl_end_from !=0 && vl_found == false) {
        // escaped "
        if(*(vl_end_from+1)=='\"') {
          vl_str_from.append(vl_start,(vl_end_from-vl_start+1));
          vl_start = vl_end_from+2;
          vl_end_from = strchr(vl_start,'\"');
        } else {
          vl_str_from.append(vl_start,(vl_end_from-vl_start));
           vl_start = vl_end_from+1;
           vl_found = true;
        }
      }
      // If happy ending
      if(vl_end_from !=0) {
        // Search TO
        vl_start = strchr(vl_end_from,':');
        if(vl_start!=0) {
          vl_start = strchr(vl_start+1,'\"');
        }
        if(vl_start!=0) {
          vl_found = false;
          std::string vl_str_to("");
          vl_str_to.reserve(MAX_RESERVED_PARSE);
          vl_start++;
          const char * vl_end_to = strchr(vl_start,'\"');
          while(vl_end_to !=0 && vl_found == false) {
            // escaped "
            if(*(vl_end_from+1)=='\"') {
              vl_str_to.append(vl_start,(vl_end_to-vl_start+1));
              vl_start = vl_end_to+2;
              vl_end_to = strchr(vl_start,'\"');
            } else {
              vl_str_to.append(vl_start,(vl_end_to-vl_start));
               vl_start = vl_end_to+1;
               vl_found = true;
            }
          }
          if(vl_end_to !=0) {
            // Its found -> insert into the MAP
            ACTION_GUIFUNCTIONS("parse_it_values_map[insert](%s}-{%s}", vl_str_from.c_str(),vl_str_to.c_str());
            vl_ret.insert(std::make_pair(vl_str_from, vl_str_to));
            // Search NEXT item
            vl_start = strchr(vl_end_to,',');
            if(vl_start!=0) {
              vl_start = strchr(vl_start+1,'\"');
            }
          } else {
            vl_start = 0; // stop
          }
        } else {
          vl_start = 0; // stop
        }
      } else {
        vl_start = 0; // stop
      }
    }
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: strdup_internal
  //
  //  Purpose:
  //    duplicate a string with malloc
  //
  //  Parameters:
  //    pl_instr - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    the new memory block address
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Duplicate a string with malloc.
  //
  ///////////////////////////////////////////////////////////
  char * strdup_internal(const char * pl_instr) {
    char * vl_ret = (char *)malloc(measure(pl_instr)+1);
    if(vl_ret!=0){
      strcpy(vl_ret,pl_instr);
    } else {
      TTCN_Logger::log(TTCN_Logger::ERROR_UNQUALIFIED, "strdup_internal: Error[not enough memory] -> {%s}!", pl_instr);
    }
    return(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: init_xml_doc
  //
  //  Purpose:
  //    Load playlist XML document
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    - the result
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Load playlist XML document.
  //
  ///////////////////////////////////////////////////////////
   bool init_xml_doc(xml_document<> * vl_docptr) {
     bool vl_ret = true;
     vl_docptr->clear();
     // root node
     xml_node<>* vl_root = vl_docptr->allocate_node(node_element, c_xml_root_name);
     vl_docptr->append_node(vl_root);
     vl_ret = (vl_docptr->first_node() != 0);
     return(vl_ret);
   }

   ///////////////////////////////////////////////////////////
   //  Function: clear_variable_data
   //
   //  Purpose:
   //    Clear variable data tables
   //
   //  Parameters:
   //    -
   //
   //  Return Value:
   //    - the result
   //
   //  Errors:
   //    -
   //
   //  Detailed Comments:
   //    Clear variable data tables.
   //
   ///////////////////////////////////////////////////////////
   void clear_variable_data(void) {
     // For variable data
	 if(0<v_variableData.size()){
       ACTION_GUIFUNCTIONS("clear_variable_data - widget_id)[orig_node_name -> vl_idx : orig_node_ptr] {local_var_idx : remote_var_name}: deleted");
	 }
     for(unsigned int vl_idx=0; vl_idx< v_variableData.size(); vl_idx++){
       // If not deleted
       if(v_variableData[vl_idx].deleted != true) {
#ifdef KI_DEBUG_ACTIONS
         ACTION_GUIFUNCTIONS("clear_variable_data - v_variableData(%s)[%s -> %d # %ld]->{%ld : %s}: %d",
        		 v_variableData[vl_idx].widget_id,
        		 v_variableData[vl_idx].orig_node_name,
        		 vl_idx,
        		 (long)( v_variableData[vl_idx].orig_node_ptr),
        		 (long)v_variableData[vl_idx].local_var_idx,
        		 v_variableData[vl_idx].remote_var_name,
        		 (int)v_variableData[vl_idx].deleted);
         if(v_variableData[vl_idx].orig_node_name) {
           free(v_variableData[vl_idx].orig_node_name);
         }
         if(v_variableData[vl_idx].remote_var_name) {
           free(v_variableData[vl_idx].remote_var_name);
         }
#endif
         v_variableData[vl_idx].it_values_list.clear();
       } else {
         ACTION_GUIFUNCTIONS("clear_variable_data - v_variableData(DELETED)(%s)[ -> %d : %ld : %ld]",v_variableData[vl_idx].widget_id,vl_idx,(long)( v_variableData[vl_idx].node_offset_id),(long)( v_variableData[vl_idx].orig_node_ptr));
       }
     }
     v_variableData.clear();  // For variable data
     v_variableDataMap.clear(); // variable data map
     return;
   }

//=========================================================================
// TIME DEBUG Functions
//=========================================================================
  void reset_timer(void) {
#ifdef USE_TIME_MEASURE
    v_time_full_diff_summ = 0.0;
    v_time_process_diff_summ = 0.0;
    for(int vl_idx=0;vl_idx<MB_OF_MEASURES;vl_idx++) {
      v_time_measure_diff_summ[vl_idx] = 0.0;
      strcpy(&v_time_measure_str[vl_idx][0],"-");
    }
#endif
    return;
  }
  void start_timer(clock_t * pl_timer_start_ptr, struct timeval * pl_tv)
  {
    *pl_timer_start_ptr = clock();  /* get current time  */
    gettimeofday(pl_tv,NULL);
    return;
  }
  void stop_timer(clock_t * pl_timer_start_ptr, struct timeval * pl_tv, double * pl_time_diff_summ_ptr)
  {
    char vl_time_str[512];
    clock_t timer_end;
    long time_diff;
    timer_end = clock();  /* get current time  */
    struct timeval vl_tv_end;
    gettimeofday(&vl_tv_end,NULL);
    time_diff = ( ((vl_tv_end.tv_sec - pl_tv->tv_sec)*1000000) + (vl_tv_end.tv_usec - pl_tv->tv_usec) );
    *pl_time_diff_summ_ptr += (double)(((double)time_diff)/100);
    sprintf(vl_time_str,"{%ld -> summ=%f}",time_diff, *pl_time_diff_summ_ptr);
    return;
  }

  void print_variableData(const char * pl_id) {
#ifdef KI_DEBUG_ACTIONS
/*
    // For v_cloneWidgetIDMap
    ACTION_GUIFUNCTIONS_PERMANENT("%s - v_cloneWidgetIDMap[first]->node_offset_id : parent_offset_node_id : iterator_like", pl_id);
    for(std::map<string, node_parent>::iterator vl_node_map = v_cloneWidgetIDMap.begin(); vl_node_map != v_cloneWidgetIDMap.end(); vl_node_map++) {
      node_parent vl_node_parent = vl_node_map->second;
      ACTION_GUIFUNCTIONS_PERMANENT("%s - v_cloneWidgetIDMap[%s]->%ld : %ld : %d", pl_id, vl_node_map->first.c_str(), vl_node_parent.node_offset_id, vl_node_parent.parent_offset_node_id, (int)vl_node_parent.iterator_like );
    }
    ACTION_GUIFUNCTIONS_PERMANENT("%s - v_variableData(widget_id)[orig_node_name -> vl_idx : orig_node_ptr]{local_var_idx : remote_var_name}: deleted {vl_sstream}", pl_id);
    for(unsigned int vl_idx=0; vl_idx< v_variableData.size(); vl_idx++){
      std::ostringstream vl_sstream;
      for(std::map<std::string , std::string>::iterator vl_it_map = v_variableData[vl_idx].it_values_list.begin(); vl_it_map != v_variableData[vl_idx].it_values_list.end(); vl_it_map++) {
        vl_sstream << vl_it_map->first << "->" << vl_it_map->second << " | ";
      }
      ACTION_GUIFUNCTIONS_PERMANENT("%s - v_variableData(%s)[%s -> %d : %ld]{%ld : %s}: %d {%s}",pl_id,v_variableData[vl_idx].widget_id,v_variableData[vl_idx].orig_node_name,vl_idx,(long)( v_variableData[vl_idx].orig_node_ptr), v_variableData[vl_idx].local_var_idx, v_variableData[vl_idx].remote_var_name, (int)v_variableData[vl_idx].deleted, vl_sstream.str().c_str());
    }
    // For map<xml_node<> *, int> v_variableDataMap
    ACTION_GUIFUNCTIONS_PERMANENT("%s - v_variableDataMap[node_offset_id-><idx>] - v_variableDataMap.size()=%ld", pl_id, (long)v_variableDataMap.size());
    for(std::map<long, int>::iterator vl_node_map = v_variableDataMap.begin(); vl_node_map != v_variableDataMap.end(); vl_node_map++) {
      ACTION_GUIFUNCTIONS_PERMANENT("%s - v_variableDataMap[%ld->%d]", pl_id, vl_node_map->first, vl_node_map->second);
    }
    // For v_playlistWidgetIDMap
    for(std::map<string, playlist_data>::iterator vl_node_map = v_playlistWidgetIDMap.begin(); vl_node_map != v_playlistWidgetIDMap.end(); vl_node_map++) {
      ACTION_GUIFUNCTIONS_PERMANENT("%s - v_playlistWidgetIDMap[%s]", pl_id, vl_node_map->first.c_str());
    }
*/
#endif
    return;
  }


//=========================================================================
// TESTS
//=========================================================================

  // test_init_XML() - pass
  // ----------------------------------------------------------------------
  std::string test_init_XML(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_init_XML->" );
      init_XML();
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_init_XML() PASS" );
      return vl_returnString;
  }

  // test_reset_XML() - pass
  // ----------------------------------------------------------------------
  std::string test_reset_XML(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_reset_XML->" );
      reset_XML();
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_reset_XML() PASS" );
      return vl_returnString;
  }

  // test_expand_XML() - pass
  // ----------------------------------------------------------------------
  std::string test_expand_XML(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_XML->" );
      init_XML();

      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<Widgets><iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator></Widgets>";

      CHARSTRING vl_expandedXML;
      expand_XML( CHARSTRING(vl_cstr),CHARSTRING(""), vl_expandedXML);


      CHARSTRING vl_result("<Widgets>*\
*<iterator element=\"EntityGroups\" id=\"EG\" source=\"ExecCtrl\" metadata=\"*\">*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox/>*<hbox/>*<hbox/>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox/>*<hbox/>*<hbox/>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox/>*<hbox/>*<hbox/>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox/>*<hbox/>*<hbox/>*\
*</iterator>*\
*</hbox>*\
*</iterator>*\
*</Widgets>*");

      if (!CHARSTRING_template(STRING_PATTERN,vl_result ).match(vl_expandedXML)) {
          is_NOK = true;
          vl_returnString.append("test_expand_XML() FAIL: '");
          vl_returnString.append( (const char *)vl_expandedXML);
          vl_returnString.append("'");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_XML() PASS" );
      }
      return vl_returnString;
  }

  // test_refreshed_XML_forNode() - pass
  // ----------------------------------------------------------------------
  std::string test_refreshed_XML_forNode(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode" );
      init_XML();

      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<Widgets>\
                          <iterator element='EntityGroups' id='EG' source='ExecCtrl'>\
                            <hbox>\
                              <iterator id='LGen' source='ExecCtrl' element='LGenList'>\
                                <hbox><label id='label_%EG%_%LGen%'></label></hbox>\
                              </iterator>\
                            </hbox>\
                            </iterator>\
                       </Widgets>";
      CHARSTRING vl_expandedXML;
      CHARSTRING vl_xulExpanded_1("");
      CHARSTRING vl_xulExpanded_1_2_1("");

      expand_XML( CHARSTRING(vl_cstr),CHARSTRING(""), vl_expandedXML);

      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode:vl_expandedXML:{%s}", (char *)(const char *)vl_expandedXML );
      vl_doc.parse<0>((char *)(const char *)vl_expandedXML);

      xml_node<> * vl_node = vl_doc.first_node();
      xml_node<> * vl_node_1 = vl_node->first_node();
      xml_node<> * vl_node_1_1 = vl_node_1->first_node();
      xml_node<> * vl_node_1_2 = vl_node_1_1->next_sibling();
      xml_node<> * vl_node_1_2_1 = vl_node_1_2->first_node();
      // Get internal
      char * vl_metadata_1_2_1 = get_attribute(vl_node_1_2_1, (char *)c_str_metadata);
      ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode_vl_metadata_1_2_1[1]");
      char * vl_sep = strchr(vl_metadata_1_2_1, '_');
      if(vl_sep!=0){
        *vl_sep = '\0';
        long vl_node_id_1_2_1 = atol(vl_metadata_1_2_1);
        ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode_vl_metadata_1_2_1[2]->%ld",vl_node_id_1_2_1);
        bool vl_result_1_2_1 = refreshed_XML_forNode(vl_node_id_1_2_1, vl_xulExpanded_1_2_1);
        if(vl_result_1_2_1==false) {
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode:refreshed_XML_forNode(vl_node_id_1_2_1)[%d]=%s", (int)vl_result_1_2_1, (char *)(const char *)vl_xulExpanded_1_2_1 );
        }
      }
      // Get full
      ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode_vl_metadata_1[1]");
      char * vl_metadata_1 = get_attribute(vl_node_1, (char *)c_str_metadata);
      vl_sep = strchr(vl_metadata_1, '_');
      if(vl_sep!=0){
        *vl_sep = '\0';
        long vl_node_id_1 = atol(vl_metadata_1);
        ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode_vl_metadata_1[2]->%ld",vl_node_id_1);
        bool vl_result_1 = refreshed_XML_forNode(vl_node_id_1, vl_xulExpanded_1);
        if(vl_result_1==false) {
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode:refreshed_XML_forNode(vl_node_id_1)[%d]=%s",(int)vl_result_1, (char *)(const char *)vl_xulExpanded_1 );
        }
      }

      ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode[1]");
      CHARSTRING vl_result_1("*<iterator element=\"EntityGroups\" id=\"EG\" source=\"ExecCtrl\" metadata=\"*\">*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox>*\
*<label id=\"label_EG1_LGen1\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG1_LGen2\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG1_LGen3\"/>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox>*\
*<label id=\"label_EG2_LGen1\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG2_LGen2\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG2_LGen3\"/>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox>*\
*<label id=\"label_EG3_LGen1\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG3_LGen2\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG3_LGen3\"/>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox>*\
*<label id=\"label_EG4_LGen1\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG4_LGen2\"/>*\
*</hbox>*\
*<hbox>*\
*<label id=\"label_EG4_LGen3\"/>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*</iterator>*");

      CHARSTRING vl_result_1_2_1("*<iterator id=\"LGen\" source=\"ExecCtrl\" element=\"LGenList\" metadata=\"*\">*\
*<hbox>*<label id=\"label_EG2_LGen1\"/>*</hbox>*\
*<hbox>*<label id=\"label_EG2_LGen2\"/>*</hbox>*\
*<hbox>*<label id=\"label_EG2_LGen3\"/>*</hbox>*</iterator>*");

      ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode[2]");
      if ((!(CHARSTRING_template(vl_result_1).match(vl_xulExpanded_1)))) {
        is_NOK = true;
      }

      ACTION_GUIFUNCTIONS("test_refreshed_XML_forNode[3]");
      if (!CHARSTRING_template(STRING_PATTERN,vl_result_1 ).match(vl_xulExpanded_1)) {
        is_NOK = true;
        vl_returnString.append("test_refreshed_XML_forNode() FAIL,  vl_result_1!='");
        vl_returnString.append(vl_xulExpanded_1);
        vl_returnString.append("'");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      } else {
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode:vl_xulExpanded_1 - OK!");
        if (!CHARSTRING_template(STRING_PATTERN,vl_result_1_2_1 ).match(vl_xulExpanded_1_2_1)) {
          is_NOK = true;
          vl_returnString.append("test_refreshed_XML_forNode() FAIL, vl_result_1_2_1!='");
          vl_returnString.append(vl_xulExpanded_1_2_1);
          vl_returnString.append("'");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        } else {
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode:vl_xulExpanded_1_2_1 - OK!");
        }
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_refreshed_XML_forNode() PASS{%s}", vl_returnString.c_str());
      }
      vl_doc.clear();
      print_variableData((char *)"test_refreshed_XML_forNode:END");
      return vl_returnString;
  }

  // test_append_node_ptrs() - pass
  // ----------------------------------------------------------------------
  std::string test_append_node_ptrs(){
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_append_node_ptrs->" );
      std::string vl_returnString("");
      bool is_NOK = false;
      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator>";
      xml_document<> vl_doc;
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      process_data vl_process_data;
      vl_process_data.ITValuesMapsVector.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();
      vl_process_data.cloneDocPTR = &vl_doc;

      //void append_node_ptrs(xml_document<> * pl_doc,  xml_node<> * pl_node){
      append_node_ptrs(&vl_doc, &vl_process_data, vl_node);

      char myString[29];
      sprintf(myString,"%lu",(long) vl_node);
      if (strcmp(vl_node->first_attribute(c_str_orig_node_ptr)->value(),myString) != 0 ){
        is_NOK = true;
        vl_returnString.append("test_append_node_ptrs() FAIL, node does not contain correct pointers ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      vl_node = vl_node->first_node();
      sprintf(myString,"%lu",(long) vl_node);
      if (strcmp(vl_node->first_attribute(c_str_orig_node_ptr)->value(),myString) != 0 ){
        is_NOK = true;
        vl_returnString.append("test_append_node_ptrs() FAIL, node does not contain correct pointers ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      vl_node = vl_node->first_node();
      sprintf(myString,"%lu",(long) vl_node);
      if (strcmp(vl_node->first_attribute(c_str_orig_node_ptr)->value(),myString) != 0 ){
        is_NOK = true;
        vl_returnString.append("test_append_node_ptrs() FAIL, node does not contain correct pointers ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      vl_node = vl_node->first_node();
      sprintf(myString,"%lu",(long) vl_node);
      if (strcmp(vl_node->first_attribute(c_str_orig_node_ptr)->value(),myString) != 0 ){
        is_NOK = true;
        vl_returnString.append("test_append_node_ptrs() FAIL, node does not contain correct pointers ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_append_node_ptrs() PASS" );
      }

      return vl_returnString;
  }

  // test_expand_XML_elements() - pass
  // ----------------------------------------------------------------------
  std::string test_expand_XML_elements(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_XML_elements->" );
      init_XML();
      bool is_NOK = false;
      xml_document<> vl_doc;
      xml_document<> vl_clone_doc;
      vl_clone_doc.clear();
      process_data vl_process_data;
      vl_process_data.ITValuesMapsVector.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();
      vl_process_data.cloneDocPTR = &vl_clone_doc;
      vl_process_data.waitforIdx = EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers.size_of();
      EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__UIHandler__GUIFunctions__nofRunning__processExternalData__handlers[vl_process_data.waitforIdx] = 0;

      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();
      append_node_ptrs(&vl_doc, &vl_process_data, pl_node);

      xml_node<> * vl_clone_root_node = vl_clone_doc.clone_node(pl_node);
      xml_document<> * v_act_clone_doc_ptr = &vl_clone_doc;
      v_act_clone_doc_ptr->append_node(vl_clone_root_node);
      vl_clone_root_node = v_act_clone_doc_ptr->first_node();


      expand_XML_elements( vl_clone_root_node , &vl_process_data, 0l);
      f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers(vl_process_data.waitforIdx);

      std::string vl_xml_out_as_string;
      rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_clone_doc);
      CHARSTRING vl_expandedXML = CHARSTRING((char *) vl_xml_out_as_string.c_str());

      CHARSTRING vl_result("*<metaiterator element=?EntityGroups? id=?EG? source=?ExecCtrl? *>*\
*<hbox orig_node_ptr=*>*\
*<metaiterator *>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*</metaiterator>*\
*</hbox>*\
*<hbox orig_node_ptr=*>*\
*<metaiterator *>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*</metaiterator>*\
*</hbox>*\
*<hbox orig_node_ptr=*>*\
*<metaiterator *>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*</metaiterator>*\
*</hbox>*\
*<hbox orig_node_ptr=*>*\
*<metaiterator *>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*<hbox orig_node_ptr=*/>*\
*</metaiterator>*\
*</hbox>*\
*</metaiterator>*");

      if (!CHARSTRING_template(STRING_PATTERN,vl_result ).match(vl_expandedXML)) {
          is_NOK = true;
          vl_returnString.append("test_expand_XML_elements() FAIL,  '");
          vl_returnString.append(vl_expandedXML);
          vl_returnString.append("'");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_xml_out_as_string.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_XML_elements() PASS" );
      }
      vl_clone_doc.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
      // For internal maps
      for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
        vl_process_data.ITValuesMapsVector[vl_idx].clear();
      }
      vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps
      return vl_returnString;
  }

  // test_get_nodes_to_expand() - pass
  // ----------------------------------------------------------------------
  std::string test_get_nodes_to_expand(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_nodes_to_expand->" );
      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\
                                      <hbox> \
                                        <iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                          <hbox> \
                                            <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
                                              <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                              <hbox> </hbox> \
                                            </iterator> \
                                          </hbox> \
                                        </iterator>\
                                        <hbox> \
                                          <iterator id='YX_Depends_EC_GEZA' source='ExecCtrl' element='XY'> \
                                            <hbox> </hbox> \
                                          </iterator> \
                                        </hbox> \
                                      </hbox> \
                                    </Widgets>";

      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      vector< xml_node<>* > vl_nodeList;
      vl_nodeList.clear();
      get_nodes_to_expand( vl_node, &vl_nodeList);

      if(vl_nodeList.size() != 2){
        is_NOK = true;
        vl_returnString.append("test_get_nodes_to_expand() FAIL, Number of expandable nodes is wrong: %d", vl_nodeList.size());
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      for (vector<xml_node<> *>::iterator vl_expandable = vl_nodeList.begin(); vl_expandable != vl_nodeList.end(); vl_expandable++){
         char * vl_geza(((xml_node<> * )*vl_expandable)->first_attribute("id")->value());
         if( !compare_name("EG", measure("EG"), vl_geza, measure(vl_geza)) && !compare_name("YX_Depends_EC_GEZA", measure("YX_Depends_EC_GEZA"), vl_geza, measure(vl_geza))){
           is_NOK = true;
           vl_returnString.append("test_get_nodes_to_expand() FAIL, Iterator's IDs are wrong: ");
           vl_returnString.append(vl_geza);
           TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
         }
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_nodes_to_expand() PASS" );
      }

      return vl_returnString;
  }

  // test_expand_current_iterator() - pass
  // ----------------------------------------------------------------------
  std::string test_expand_current_iterator(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_current_iterator->" );
      bool is_NOK = false;

      xml_document<> vl_doc;
      process_data vl_process_data;
      vl_process_data.ITValuesMapsVector.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();
      vl_process_data.cloneDocPTR = &vl_doc;

      char vl_cstr[] = "<Widgets>\
                          <externalvalue element='EntityGroups' id='EG' source='ExecCtrl'> \
                            <params> <dataparam name='EntityGroup' value='EG'></dataparam></params> \
                            <hbox> \
                              <label id='X_Depends_EC_%EG::idx%' value='%EG%'/> \
                              <textbox id='tb_%EG%' value='%EG%'/> \
                            </hbox> \
                          </externalvalue>\
                        </Widgets>";

      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      append_node_ptrs(&vl_doc, &vl_process_data, vl_node);

      const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_params;
      EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent vl_dataValue;
      vl_dataValue.charstringVal() = CHARSTRING("eg_test");
      EPTF__CLL__Common__Definitions::EPTF__IntegerList vl_userData;
      vl_userData.set_size(1);
      long vl_ptrlong = (long) vl_node->first_node();
      vl_userData[0].set_long_long_val((long) vl_ptrlong );
      vl_userData[1].set_long_long_val((long) &vl_process_data );
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_current_iterator[vl_userData]->%ld | userdata: %d", (long)vl_userData[0].get_long_long_val(), (int)sizeof(vl_userData[0]));

      /*  Check whether the it values are replaced correctly */
      f__EPTF__UIHandler__expand__current__iterator( CHARSTRING(""),  //  pl_source
                                    CHARSTRING (""), // pl_ptcName
                                    CHARSTRING (""), // pl_element,
                                    vl_params,
                                    INTEGER(0),      // pl_errorCode,
                                    CHARSTRING(""),  // pl_remoteDataVarName,
                                    COMPONENT(),     // pl_ownerCompRef,
                                    INTEGER(1),      // pl_localDataVarId,
                                    vl_dataValue,
                                    vl_userData);

      std::ostringstream ss;
      ss << vl_doc;
      std::string result_xml = ss.str();

      vl_doc.clear();
      if(result_xml.find("eg_test")==std::string::npos){
        is_NOK = true;
        vl_returnString.append("test_expand_current_iterator() FAIL, iterator value does not appear in the result");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, result_xml.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_current_iterator() PASS" );
      }
      vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
      // For internal maps
      for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
      	vl_process_data.ITValuesMapsVector[vl_idx].clear();
      }
      vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps
      return vl_returnString;
  }

  // test_process_iterator_node() - pass
  // ----------------------------------------------------------------------
  std::string test_process_iterator_node(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_process_iterator_node->" );

      bool is_NOK = false;
      xml_document<> vl_doc;
      process_data vl_process_data;
      vl_process_data.ITValuesMapsVector.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();
      vl_process_data.cloneDocPTR = &vl_doc;
      char vl_cstr[] = "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\
                                      <hbox> \
                                        <iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                          <hbox> \
                                            <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
                                              <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                              <hbox> \
                                                <label id='label_inner_%EG::ref%'>%EG%</label> \
                                              </hbox> \
                                            </iterator> \
                                          </hbox> \
                                        </iterator>\
                                      </hbox> \
                                      <hbox> \
                                        <iterator id='YX_Depends_EC_GEZA_%EG%' source='ExecCtrl' element='XY'> \
                                          <hbox>AA</hbox> \
                                        </iterator> \
                                      </hbox> \
                                    </Widgets>";

      char vl_cstr_node[] = "<hbox>\
          <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
          <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
          <hbox> \
            <label id='label_inner_%EG::ref%'>%EG%</label> \
          </hbox> \
        </iterator>\
       </hbox>";

      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      vl_node = vl_node->first_node();
      xml_node<> * vl_parent_node = vl_node->first_node();
      xml_node<> * vl_act_node = vl_parent_node->first_node();

      map<string, string>  vl_it_values; // itvalues map
      vl_it_values.clear();
      vl_it_values.insert(std::make_pair(string("%EG%"), string("EG1")));
      vl_it_values.insert(std::make_pair(string("%EG::idx%"), string("1")));
      vl_it_values.insert(std::make_pair(string("%EG::count%"), string("4")));
      vl_it_values.insert(std::make_pair(string("%EG::ref%"), string("EG1ref")));

      // Set IT Values
      if(0<vl_it_values.size()) {
        // Save IT Values Map
        int vl_itvalues_map_idx = vl_process_data.ITValuesMapsVector.size();
        vl_process_data.ITValuesMapsVector.push_back(vl_it_values);
        vl_node = process_iterator_node(&vl_doc, vl_act_node, &vl_it_values, vl_itvalues_map_idx, string(vl_cstr_node) );
        // For internal maps
        for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
          vl_process_data.ITValuesMapsVector[vl_idx].clear();
        }
        vl_process_data.ITValuesMapsVector.clear();
        vl_parent_node->remove_all_nodes();
        vl_parent_node->append_node(vl_node);
      }

      vl_it_values.clear();
      std::string vl_xml_out_as_string;
      rapidxml::print(std::back_inserter(vl_xml_out_as_string), vl_doc);

      CHARSTRING vl_expectedXML("<Widgets xmlns=\"http://ttcn.ericsson.se/protocolModules/xtdp/xtdl\">*\
*<hbox>*\
*<iterator element=\"EntityGroups\" id=\"EG\" source=\"ExecCtrl\">*\
*<hbox>*\
*<iterator id=\"X_Depends_EC_1\" source=\"ExecCtrl\" element=\"X_Depends_EG\" orig_itvalues=\" 17;\">*\
*<params>*\
*<dataparam name=\"EntityGroup\" value=\"EG1\"/>*\
*</params>*\
*<hbox>*\
*<label id=\"label_inner_EG1ref\">EG1</label>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*</iterator>*\
*</hbox>*\
*<hbox>*\
*<iterator id=\"YX_Depends_EC_GEZA_%EG%*\" source=\"ExecCtrl\" element=\"XY\">*\
*<hbox>AA</hbox>*\
*</iterator>*\
*</hbox>*\
</Widgets>*");
      /* %EG%

      */


      if(!CHARSTRING_template(STRING_PATTERN,vl_expectedXML ).match(vl_xml_out_as_string.c_str())){
        is_NOK = true;
        vl_returnString.append("test_process_iterator_node() FAIL, the xml returned is not as expected! '");
        vl_returnString.append(vl_xml_out_as_string);
        vl_returnString.append("' != {");
        vl_returnString.append(vl_expectedXML);
        vl_returnString.append("}");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_process_iterator_node() PASS" );
      }
      vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
      // For internal maps
      for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
      	vl_process_data.ITValuesMapsVector[vl_idx].clear();
      }
      vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps

      return vl_returnString;
  }

  // test_expand_current_condition() - pass
  // ----------------------------------------------------------------------
  std::string test_expand_current_condition(){
	  char v_tmpvalPTR[SIZE_OF_PTR+1] = "";  // cache
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_current_condition->" );
      bool is_NOK = false;

      char vl_cstr[] = "<hbox id='LGen'>\
             <condition element='isUnpair' id='Geza' source='ExecCtrl'>\
               <params> <dataparam name='Param' value='Geza' /> </params>\
               <insertif id='Geza'>\
                 <textbox id='Geza' readonly='true' value='Geza' />\
               </insertif>\
             </condition>\
           </hbox>";

      xml_document<> vl_doc;
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      process_data vl_process_data;
      vl_process_data.ITValuesMapsVector.clear();
      vl_process_data.checkWidgetIDMapLastRun.clear();
      vl_process_data.cloneDocPTR = &vl_doc;

      append_node_ptrs(&vl_doc, &vl_process_data, vl_node);


      const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_params;
      EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent vl_dataValue;
      vl_dataValue.boolVal() = BOOLEAN(true);
      EPTF__CLL__Common__Definitions::EPTF__IntegerList vl_userData;
      char * vl_saved_id = strdup_internal("Geza");
      vl_userData.set_size(3);

      long vl_ptrlong = (long) vl_node->first_node();
      sprintf(v_tmpvalPTR,"%ld",vl_ptrlong);
      // LONG CONVERSION -> SET:A.set_long_long_val(V), GET:(unsigned long int)(A.get_long_long_val())
      vl_userData[0].set_long_long_val((long) vl_ptrlong );
      vl_userData[1].set_long_long_val((long) vl_saved_id );
      vl_userData[2].set_long_long_val((long) &vl_process_data );

      f__EPTF__UIHandler__expand__current__condition ( CHARSTRING(""),  //not used
                                 CHARSTRING(""),  //not used
                                 CHARSTRING(""),  //not used
                                 vl_params,       //not used
                                 INTEGER(0),      //0
                                 CHARSTRING(""),  //temp attributumnak adja be
                                 COMPONENT(),     //not used
                                 INTEGER(0),      //temp attributumnak adja be
                                 vl_dataValue,    //directcontent true-false
                                 vl_userData);    //pointer nodera


      std::ostringstream ss;
      ss << vl_doc;
      std::string result_xml = ss.str();

      vl_doc.clear();
      if(result_xml.find("textbox")==std::string::npos){
        is_NOK = true;
        vl_returnString.append("test_expand_current_condition() FAIL, textbox does not appear in the condition's body, but condition is true ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }  else {
        char vl_cstrclear[] = "<hbox id='LGen'>\
               <condition element='isUnpair' id='Geza' source='ExecCtrl'>\
                 <params> <dataparam name='Param' value='Geza' /> </params>\
                 <insertif id='Geza'>\
                   <textbox id='Geza' readonly='true' value='Geza' />\
                 </insertif>\
               </condition>\
             </hbox>";
        xml_document<> vl_doc;
        vl_doc.parse<0>(vl_cstrclear);
        xml_node<> * vl_node = vl_doc.first_node();
        append_node_ptrs(&vl_doc, &vl_process_data, vl_node);
        char * vl_saved_id = strdup_internal("Geza");
        vl_dataValue.boolVal() = BOOLEAN(false);
        vl_ptrlong = (long) vl_node->first_node();
        vl_userData[0].set_long_long_val((long) vl_ptrlong );
        vl_userData[1].set_long_long_val((long) vl_saved_id );
        vl_userData[2].set_long_long_val((long) &vl_process_data );


        f__EPTF__UIHandler__expand__current__condition ( CHARSTRING(""),  //not used
                                   CHARSTRING(""),  //not used
                                   CHARSTRING(""),  //not used
                                   vl_params,       //not used
                                   INTEGER(0),      //0
                                   CHARSTRING(""),  //temp attributumnak adja be
                                   COMPONENT(),     //not used
                                   INTEGER(0),      //temp attributumnak adja be
                                   vl_dataValue,    //directcontent true-false
                                   vl_userData);    //pointer nodera


        std::ostringstream ssclear;
        ssclear << vl_doc;
        std::string result_xmlclear = ssclear.str();
        vl_doc.clear();

        if(result_xmlclear.find("textbox")!=std::string::npos){
          is_NOK = true;
          vl_returnString.append("test_expand_current_condition() FAIL, textbox not appear in the condition's body, but condition is false ");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        } else {
          char vl_cstrnegate[] = "<hbox id='LGen'>\
                 <condition element='isUnpair' id='Geza' source='ExecCtrl'>\
                   <params> <dataparam name='Param' value='Geza' /> </params>\
                   <insertif id='Geza' negate='true'>\
                     <textbox id='Geza' readonly='true' value='Geza' />\
                   </insertif>\
                 </condition>\
               </hbox>";
          xml_document<> vl_doc;
          vl_doc.parse<0>(vl_cstrnegate);
          xml_node<> * vl_node = vl_doc.first_node();
          append_node_ptrs(&vl_doc, &vl_process_data, vl_node);

          char * vl_saved_id = strdup_internal("Geza");
          vl_dataValue.boolVal() = BOOLEAN(true);
          vl_ptrlong = (long) vl_node->first_node();
          vl_userData[0].set_long_long_val((long) vl_ptrlong );
          vl_userData[1].set_long_long_val((long) vl_saved_id );
          vl_userData[2].set_long_long_val((long) &vl_process_data );

          f__EPTF__UIHandler__expand__current__condition ( CHARSTRING(""),  //not used
                                     CHARSTRING(""),  //not used
                                     CHARSTRING(""),  //not used
                                     vl_params,       //not used
                                     INTEGER(0),      //0
                                     CHARSTRING(""),  //temp attributumnak adja be
                                     COMPONENT(),     //not used
                                     INTEGER(0),      //temp attributumnak adja be
                                     vl_dataValue,    //directcontent true-false
                                     vl_userData);    //pointer nodera


          std::ostringstream ssnegate;
          ssnegate << vl_doc;
          std::string result_xmlnegate = ssnegate.str();
          vl_doc.clear();

          if(result_xmlnegate.find("textbox")!=std::string::npos){
            is_NOK = true;
            vl_returnString.append("test_expand_current_condition() FAIL, textbox not appear in the condition's body, but condition is true and negate=true ");
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
          }
        }

      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_expand_current_condition() PASS" );
      }
      vl_process_data.checkWidgetIDMapLastRun.clear();  // check widget id map
      // For internal maps
      for(unsigned int vl_idx=0; vl_idx< vl_process_data.ITValuesMapsVector.size(); vl_idx++){
      	vl_process_data.ITValuesMapsVector[vl_idx].clear();
      }
      vl_process_data.ITValuesMapsVector.clear();     // For ITValues Maps
      return vl_returnString;
  }

  // test_process_condition_node() - pass
  // ----------------------------------------------------------------------
  std::string test_process_condition_node(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_process_condition_node->" );
      bool is_NOK = false;

      char vl_cstr[] = "<hbox id='LGen'>\
             <condition element='isUnpair' id='Geza' source='ExecCtrl'>\
               <params> <dataparam name='Param' value='Geza' /> </params>\
               <insertif id='Geza'>\
                 <textbox id='Geza' readonly='true' value='Geza' />\
               </insertif>\
             </condition>\
           </hbox>";
      xml_document<> vl_doc;
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();

      char vl_cstrFalse[1000];
      strcpy(vl_cstrFalse,(const char *) "<hbox id='LGen'>\
             <condition element='isUnpair' id='Geza' source='ExecCtrl'>\
               <params> <dataparam name='Param' value='Geza' /> </params>\
               <insertif id='Geza'>\
                 <textbox id='Geza' readonly='true' value='Geza' />\
               </insertif>\
             </condition>\
           </hbox>" );
      xml_document<> vl_docFalse;
      vl_docFalse.parse<0>(vl_cstrFalse);
      xml_node<> * vl_nodeFalse = vl_docFalse.first_node();

      process_condition_node(&vl_doc, vl_node, true, (char *) "Geza");
      process_condition_node(&vl_doc, vl_nodeFalse, false, (char *) "Geza");

      std::ostringstream ss;
      ss << vl_doc;
      std::string result_xml = ss.str();

      std::ostringstream ssFalse;
      ssFalse << vl_docFalse;
      std::string result_xmlFalse = ssFalse.str();

      if(result_xml.find("textbox")==std::string::npos){
        is_NOK = true;
        vl_returnString.append("test_process_condition_node() FAIL, textbox does not appear in the condition's body, but condition is true ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(result_xmlFalse.find("textbox")!=std::string::npos){
        is_NOK = true;
        vl_returnString.append("test_process_condition_node() FAIL, textbox appears in the condition's body, but condition is false ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_process_condition_node() PASS" );
      }

      return vl_returnString;
  }


  // ----------------------------------------------------------------------
  // SUBROUTINES
  // ----------------------------------------------------------------------

  // test_is_valid_XML() - pass
  // ----------------------------------------------------------------------
  std::string test_is_valid_XML(){
      std::string vl_returnString("");
      bool is_NOK = false;
      // Validate the XML
      const char * vl_XSD_filepath = (const char *)EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__Private__CT_component_v__EPTF__UIHandler__WidgetFunctions__XTDP__XSD__filepath;
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED,"is_valid_XML[vl_XSD_filepath] (%s}", vl_XSD_filepath);
      // If XSD file path are set
      if(vl_XSD_filepath != 0 &&  *vl_XSD_filepath != '\0'){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_is_valid_XML->" );
        char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator>";
        int vl_return = is_valid_XML(vl_cstr, vl_XSD_filepath);

        if(vl_return != ERROR_XML_ROOT_ERROR)
        {
          is_NOK = true;
          vl_returnString.append("test_is_valid_XML() FAIL, XML is wrong, but validator valids it. ");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        }

        //Now with a valid xml
        char vl_cstr2[] = "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'><window><iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator></window></Widgets>";
        vl_return = is_valid_XML(vl_cstr2, vl_XSD_filepath);

        if(vl_return != VALID_XML)
        {
          is_NOK = true;
          vl_returnString.append("test_is_valid_XML() FAIL, XML is valid, but validator thinks differently. ");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        }

        vl_return = is_valid_XML(vl_cstr2, "XTDL.xsd_notexists");
        if(vl_return != ERROR_XSD_LOAD)
        {
          is_NOK = true;
          vl_returnString.append("test_is_valid_XML() FAIL, XSD not present, but validator tells differently ");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        }
      }
      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_is_valid_XML(%s) PASS", vl_XSD_filepath );
      }

      return vl_returnString;
  }

  // test_measure() - pass
  // ----------------------------------------------------------------------
  std::string test_measure(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_measure->" );
      bool is_NOK = false;

      std::size_t vl_size = measure("Geza");

      if(vl_size != 4){
        is_NOK = true;
        vl_returnString.append("test_measure() FAIL, Geza size is not 4!");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_measure() PASS" );
      }

      return vl_returnString;
  }

  // test_compare() - pass
  // ----------------------------------------------------------------------
  std::string test_compare(){
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_compare->" );
      std::string vl_returnString("");
      bool is_NOK = false;

      bool vl_true = compare("Geza", strlen("Geza"), "Geza", strlen("Geza"));
      bool vl_false = compare("Geza", strlen("Geza"), "Geza2", strlen("Geza2"));

      if(vl_false != false){
        is_NOK = true;
        vl_returnString.append("test_compare() FAIL, vl_false should be false, but it's not");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(vl_true != true){
        is_NOK = true;
        vl_returnString.append("test_compare() FAIL, vl_true should be true, but it's not ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_compare() PASS" );
      }

      return vl_returnString;
  }


  // test_is_expandable() - pass
  // ----------------------------------------------------------------------
  std::string test_is_expandable(){
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_is_expandable->" );

      std::string vl_returnString("");
      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'><hbox> <iterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();

      if(is_expandable(pl_node->name(), pl_node->name_size()) == false)
      {
        is_NOK = true;
        vl_returnString.append("test_is_expandable() FAIL, iterator is not expandable according to the function");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      pl_node = pl_node->first_node();
      if(is_expandable(pl_node->name(), pl_node->name_size()) == true)
      {
        is_NOK = true;
        vl_returnString.append("test_is_expandable() FAIL, hbox is expandable according to the function ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_is_expandable() PASS" );
      }

      return vl_returnString;
  }

  // test_get_data_source() - pass
  // ----------------------------------------------------------------------
  std::string test_get_data_source(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_data_source->" );

      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<Widgets xmlns='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>\
                                      <hbox> \
                                        <iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                          <hbox> \
                                            <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG' ptcname='ptc1'> \
                                              <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                              <hbox> \
                                                <label id='label_inner_%EG::ref%'>%EG%</label> \
                                              </hbox> \
                                            </iterator> \
                                            <label id='label_outer_%EG%'> </label> \
                                          </hbox> \
                                        </iterator>\
                                      </hbox> \
                                    </Widgets>";

      vl_doc.parse<0>(vl_cstr);
      xml_node<> * vl_node = vl_doc.first_node();
      vl_node = vl_node->first_node();
      vl_node = vl_node->first_node();
      vl_node = vl_node->first_node();
      vl_node = vl_node->first_node();

      char * vl_element;
      char * vl_id;
      char * vl_ptcname;
      char * vl_source;

      if(get_data_source( vl_node, &vl_element, &vl_id, &vl_ptcname, &vl_source)==true){
          if( !compare(vl_id, measure(vl_id),"X_Depends_EC_%EG::idx%", measure("X_Depends_EC_%EG::idx%")) ||
              !compare(vl_source,measure(vl_source),"ExecCtrl", measure("ExecCtrl")) ||
              !compare(vl_element,measure(vl_element),"X_Depends_EG", measure("X_Depends_EG")) ||
              !compare(vl_ptcname,measure(vl_ptcname),"ptc1", measure("ptc1"))){
            is_NOK = true;
            vl_returnString.append("test_get_data_source() FAIL, returns ok, but the following data are not as expected! '");
            vl_returnString.append("element=");
            vl_returnString.append(vl_element);
            vl_returnString.append("id=");
            vl_returnString.append(vl_id);
            vl_returnString.append("ptcname=");
            vl_returnString.append(vl_ptcname);
            vl_returnString.append("source=");
            vl_returnString.append(vl_source);
            vl_returnString.append("'");
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
          }
      } else {
          is_NOK = true;
          vl_returnString.append("test_get_data_source() FAIL, returns false, but the xml node is correct" );
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      vl_node = vl_node->first_node();
      if(get_data_source( vl_node, &vl_element, &vl_id, &vl_ptcname, &vl_source)==true){
            is_NOK = true;
            vl_returnString.append("test_get_data_source() FAIL, returns true, but the xml node is not correct '");
            vl_returnString.append("element=");
            vl_returnString.append(vl_element);
            vl_returnString.append("id=");
            vl_returnString.append(vl_id);
            vl_returnString.append("ptcname=");
            vl_returnString.append(vl_ptcname);
            vl_returnString.append("source=");
            vl_returnString.append(vl_source);
            vl_returnString.append("'");
            TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_data_source() PASS" );
      }
      return vl_returnString;
  }

  // test_get_attribute() - pass
  // ----------------------------------------------------------------------
  std::string test_get_attribute(){
      std::string vl_returnString("");

      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_attribute->" );
      bool is_NOK = false;
      xml_document<> vl_doc;
      xml_node<> * pl_node = vl_doc.allocate_node(node_element, "GEZA_NODENAME");
      vl_doc.append_node(pl_node);
      xml_attribute<> *vl_attr = vl_doc.allocate_attribute("id", "GEZA_ID");
      pl_node->append_attribute(vl_attr);
      xml_attribute<> *vl_attr2 = vl_doc.allocate_attribute("name", "GEZA_NAME");
      pl_node->append_attribute(vl_attr2);
      xml_attribute<> *vl_attr3 = vl_doc.allocate_attribute("value", "GEZA_VALUE");
      pl_node->append_attribute(vl_attr3);

      char * vl_return = get_attribute( pl_node, (char *)c_str_id);
      if(strcmp(vl_return,"GEZA_ID")){
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_attribute() FAIL, the id shouldn't be : " );
          is_NOK = true;
          vl_returnString.append("test_get_attribute() FAIL, the id shouldn't be ");
          vl_returnString.append(vl_return);
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      vl_return = get_attribute( pl_node, (char *)c_str_name);
      if(strcmp(vl_return,"GEZA_NAME")){
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_attribute() FAIL, the name shouldn't be " );
          vl_returnString.append("test_get_attribute() FAIL, the name shouldn't be ");
          vl_returnString.append(vl_return);
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
          is_NOK = true;
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_attribute() PASS" );
      }

      return vl_returnString;
  }

  // test_get_metaiterator_itvalues() - pass
  // ----------------------------------------------------------------------
  std::string test_get_metaiterator_itvalues(){
      std::string vl_returnString("");

      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_metaiterator_itvalues->" );
      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<metaiterator element='EntityGroups' id='EG' source='ExecCtrl' orig_itvalues='%EG%=EG1;%LGEN%=LGEN2'><hbox> <metaiterator id='LGen' source='ExecCtrl' element='LGenList'> <hbox> </hbox> </iterator> </hbox> </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();
      pl_node = pl_node->first_node();

      char * vl_return = (char *) get_metaiterator_itvalues( pl_node );
      if(strcmp(vl_return,"%EG%=EG1;%LGEN%=LGEN2")){
          is_NOK = true;
          vl_returnString.append("test_get_metaiterator_itvalues() FAIL, the ITvalues shouldn't be '");
          vl_returnString.append(vl_return);
          vl_returnString.append("'");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_metaiterator_itvalues() PASS" );
      }
      return vl_returnString;
  }

  // test_get_data_source_parameters() - pass
  // ----------------------------------------------------------------------
  std::string test_get_data_source_parameters(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_data_source_parameters->" );

      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                      <hbox> \
                                        <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
                                          <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                          <hbox> </hbox> \
                                        </iterator> \
                                      </hbox> \
                                    </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();

      EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_return = get_data_source_parameters(pl_node);
      if( vl_return.size_of() > 0)
      {
        is_NOK = true;
        vl_returnString.append("get_data_source_parameters() FAIL, iterator has no datasource parameters");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      pl_node = pl_node->first_node();
      pl_node = pl_node->first_node();

      EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params vl_return2 = get_data_source_parameters(pl_node);

      if( vl_return2.size_of() == 0)
      {
        is_NOK = true;
        vl_returnString.append("get_data_source_parameters() FAIL, iterator has datasource parameters but function does not say so");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      } else {
        if(strcmp(vl_return2[0].paramName(),"EntityGroup") != 0 || strcmp(vl_return2[0].paramValue(),"%EG%") != 0 ){
          is_NOK = true;
          vl_returnString.append("get_data_source_parameters() FAIL, paramname is wrong :'");
          vl_returnString.append(vl_return2[0].paramName());
          vl_returnString.append("'");
          TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
        }
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_get_data_source_parameters() PASS" );
      }

      return vl_returnString;
  }

  // test_set_node_temp_attributes() - pass
  // ----------------------------------------------------------------------
  std::string test_set_node_temp_attributes(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_set_node_temp_attributes->" );
      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                      <hbox> \
                                        <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
                                          <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                          <hbox> </hbox> \
                                        </iterator> \
                                      </hbox> \
                                    </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();

      //void set_node_temp_attributes( xml_node<> * pl_node, char * pl_remote_name, long pl_local_id)
      set_node_temp_attributes(&vl_doc, pl_node, (long)pl_node, (char *) "remotename", 1234567);

      std::ostringstream ss;
      ss << vl_doc;
      std::string result_xml = ss.str();
      //TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, result_xml.c_str() );

      if(result_xml.find(c_str_orig_node_name)==std::string::npos){
        is_NOK = true;
        vl_returnString.append("set_node_temp_attributes() FAIL, Temp attributes does not appear in the xml {");
        vl_returnString.append(result_xml.c_str());
        vl_returnString.append("}");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_set_node_temp_attributes() PASS" );
      }

      return vl_returnString;
  }

  // test_remove_nodes_temp_attributes() - pass
  // ----------------------------------------------------------------------
  std::string test_remove_nodes_temp_attributes(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_remove_nodes_temp_attributes->" );
      bool is_NOK = false;
      xml_document<> vl_doc;

      char vl_cstr[] = "<iterator element='EntityGroups' id='EG' source='ExecCtrl'> \
                                      <hbox> \
                                        <iterator id='X_Depends_EC_%EG::idx%' source='ExecCtrl' element='X_Depends_EG'> \
                                          <params> <dataparam name='EntityGroup' value='%EG%'></dataparam> </params> \
                                          <hbox> </hbox> \
                                        </iterator> \
                                      </hbox> \
                                    </iterator>";
      vl_doc.parse<0>(vl_cstr);
      xml_node<> * pl_node = vl_doc.first_node();

      set_node_temp_attributes(&vl_doc, pl_node, (long)pl_node, (char *) "remotename", 1234567);
      remove_nodes_temp_attributes( pl_node );

      std::ostringstream ss;
      ss << vl_doc;
      std::string result_xml = ss.str();

      if(result_xml.find("DEBUG")!=std::string::npos){
        is_NOK = true;
        vl_returnString.append("test_remove_node_temp_attributes() FAIL, DEBUG does appear in the xml");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_remove_nodes_temp_attributes() PASS" );
      }

      return vl_returnString;
  }

  // test_compare_name() - pass
  // ----------------------------------------------------------------------
  std::string test_compare_name(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_compare_name->" );
      bool is_NOK = false;

      bool vl_true = compare_name("Geza", strlen("Geza"), "Geza", strlen("Geza"));
      bool vl_false = compare_name("Geza", strlen("Geza"), "Geza2", strlen("Geza2"));

      if(vl_false != false){
        is_NOK = true;
        vl_returnString.append("test_compare_name() FAIL, vl_false should be false, but it's not");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(vl_true != true){
        is_NOK = true;
        vl_returnString.append("test_compare_name() FAIL, vl_true should be true, but it's not ");
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }

      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_compare_name() PASS" );
      }

      return vl_returnString;
  }

  // test_clear_namespace_elements() - pass
  // ----------------------------------------------------------------------
  std::string test_clear_namespace_elements(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_clear_namespace_elements->" );
      bool is_NOK = false;
#ifdef CLEAR_XML_NAMESPACE_ELEMENTS
      char vl_xml[] = "<xul:Widgets><xul:hbox id='LGen'>\
             <xul:condition element='isUnpair' id='Geza' source='ExecCtrl'>\
               <xul::params> <ns:dataparam name='Param' value='Geza' /> </xul:params>\
               <xul:insertif id='Geza'>\
                 <xul:textbox id='Geza' readonly='true' value='Geza' />\
               </xul:insertif>\
             </xul:condition>\
           </xul:hbox></xul:Widgets>";

      clear_namespace_elements(vl_xml);

      if(strstr(vl_xml,"<xul:")!=0) {
        is_NOK = true;
        vl_returnString.append("test_clear_namespace_elements() FAIL: ");
        vl_returnString.append(vl_xml);
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
      }
#endif
      if(is_NOK != true){
        TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_clear_namespace_elements() PASS" );
      }

      return vl_returnString;
  }

  // test_escape_double_quotes() - pass
  // ----------------------------------------------------------------------
  std::string test_escape_double_quotes(){
    std::string vl_returnString("");
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_escape_double_quotes->" );
    bool is_NOK = false;
    const char vl_instr[] = "\"{double}\" '{single}'";
    char vl_outrequested[] = "&quot;{double}&quot; &apos;{single}&apos;";
    std::string vl_strout = escape_double_quotes(vl_instr);
    if(strcmp(vl_strout.c_str(),vl_outrequested)!=0) {
      is_NOK = true;
      vl_returnString.append("test_escape_double_quotes() FAIL: [");
      vl_returnString.append(vl_strout);
      vl_returnString.append("] != {");
      vl_returnString.append(vl_outrequested);
      vl_returnString.append("}");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, vl_returnString.c_str() );
    }
    if(is_NOK != true){
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_escape_double_quotes() PASS [%s]", vl_strout.c_str() );
   }
   return vl_returnString;
  }

  // test_cleanup_XML() - pass
  // ----------------------------------------------------------------------
  std::string test_cleanup_XML(){
      std::string vl_returnString("");
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_cleanup_XML->" );
      init_XML();
      cleanup_XML();
      TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "test_cleanup_XML() PASS" );
      return vl_returnString;
  }


}// namespace
