///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_UIHandlerCLI_CommandDefinitions
// 
//  Purpose:
//    This module contains definitions of UIHandler CLI management.
// 
//  Module depends on:
//    <EPTF_CLL_UIHandler_MsgDefinitions>
//    <EPTF_CLL_UIHandler_Definitions>
//    <EPTF_CLL_Variable_Definitions>
// 
//  Current Owner:
//    Laszlo Skumat (ELSZSKU)
// 
//  Last Review Date:
//    2007-xx-xx
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////////
module EPTF_CLL_UIHandlerCLI_CommandDefinitions
// [.objid{ itu_t(0) identified_organization(4) etsi(0) 
// identified_organization(127) ericsson(5) testing(0)
// <put further nodes here if needed>}]
{

//const charstring c_UIHandler_CLI_assignExt := "[ \t]#(,)[\"]#(1,1)(*)[^\\]#(1,1)\"[ \t]#(,):=[ \t]#(,)([^ \t]#(1,))[ \t]#(,)"; // pattern: "  any_word1  :=    any_word2" 
const charstring c_UIHandler_CLI_assignExt := "[ \t]#(,)([^ ]#(1,))[ \t]#(,):=[ \t]#(,)([^ ]#(1,))[ \t]#(,)"; // pattern: "  any_word1  :=    any_word2" 
const charstring c_UIHandler_CLI_specifyExt := "[ \t]#(,)([^ ]#(1,))[ \t]#(,)";                     // pattern: "  any_word "
const charstring c_UIHandler_CLI_WS := "[ \t]#(,)" // whitespace
///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_getContent
//
// Purpose:
//    Template to get the content of a variable.
//  Detailed Comments:
//    The content of the variable will be printed out on the telnet port once.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_getContentCommand := "[Gg][Ee][Tt]";
const charstring c_UIHandler_CLI_getContentCommandMask := c_UIHandler_CLI_WS&c_UIHandler_CLI_getContentCommand&c_UIHandler_CLI_specifyExt;
template charstring t_UIHandler_CLI_getContent := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_getContentCommand}{c_UIHandler_CLI_specifyExt}";
template charstring t_UIHandler_CLI_getContentFailed := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_getContentCommand}*";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_setContent
//
// Purpose:
//    Template to set the content of a variable.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_setContentCommand := "[Ss][Ee][Tt]";
const charstring c_UIHandler_CLI_setContentCommandMask := c_UIHandler_CLI_WS&c_UIHandler_CLI_setContentCommand&c_UIHandler_CLI_assignExt; 
template charstring t_UIHandler_CLI_setContent := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_setContentCommand}{c_UIHandler_CLI_assignExt}";
template charstring t_UIHandler_CLI_setContentFailed := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_setContentCommand}*";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_displayContent
//
// Purpose:
//    Template to periodically display the content of a variable.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_displayContentCommand := "[Dd][Ii][Ss][Pp][Ll][Aa][Yy]";
const charstring c_UIHandler_CLI_displayContentCommandMask := c_UIHandler_CLI_WS&c_UIHandler_CLI_displayContentCommand&c_UIHandler_CLI_specifyExt;
template charstring t_UIHandler_CLI_displayContent := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_displayContentCommand}{c_UIHandler_CLI_specifyExt}";
template charstring t_UIHandler_CLI_displayContentFailed := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_displayContentCommand}*";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_hideContent
//
// Purpose:
//    Template to stop the periodical display the content of a variable.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_hideContentCommand := "[Hh][Ii][Dd][Ee]";
const charstring c_UIHandler_CLI_hideContentCommandMask := c_UIHandler_CLI_WS&c_UIHandler_CLI_hideContentCommand&c_UIHandler_CLI_specifyExt;
template charstring t_UIHandler_CLI_hideContent := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_hideContentCommand}{c_UIHandler_CLI_specifyExt}";
template charstring t_UIHandler_CLI_hideAll := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_hideContentCommand} #(,)";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_initGui
//
// Purpose:
//    Orders the UIHandler to connect to the runtime GUI.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_initGui := "[Ii][Nn][Ii][Tt][ ][Gg][Uu][Ii]";
template charstring t_UIHandler_CLI_initGui := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_initGui}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_closeGui
//
// Purpose:
//    Orders the UIHandler to disconnect from the runtime GUI.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_closeGui := "[Cc][Ll][Oo][Ss][Ee][ ][Gg][Uu][Ii]";
template charstring t_UIHandler_CLI_closeGui := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_closeGui}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_quit
//
// Purpose:
//    Closes the telnet connection.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_quit := "[Qq][Uu][Ii][Tt]";
template charstring t_UIHandler_CLI_quit := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_quit}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_stop
//
// Purpose:
//    Stops the test execution.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_stop := "[Ss][Tt][Oo][Pp]";
template charstring t_UIHandler_CLI_stop := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_stop}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_help
//
// Purpose:
//    Displays a help screen.
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_help := "[Hh][Ee][Ll][Pp]";
template charstring t_UIHandler_CLI_help := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_help}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_ls
//
// Purpose:
//    Lists all variables
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_ls := "[Ll][Ss]";
template charstring t_UIHandler_CLI_ls := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_ls}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_lsReadonly
//
// Purpose:
//    Lists variables that are read-only
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_lsReadonly := "[Ll][Ss][Rr][Ee][Aa][Dd][Oo][Nn][Ll][Yy]";
template charstring t_UIHandler_CLI_lsReadonly := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_lsReadonly}{c_UIHandler_CLI_WS}";

///////////////////////////////////////////////////////////
// Template: t_UIHandler_CLI_lsWritable
//
// Purpose:
//    Lists variables that are writable
///////////////////////////////////////////////////////////
const charstring c_UIHandler_CLI_lsWritable := "[Ll][Ss][Ww][Rr][Ii][Tt][Aa][Bb][Ll][Ee]";
template charstring t_UIHandler_CLI_lsWritable := pattern "{c_UIHandler_CLI_WS}{c_UIHandler_CLI_lsWritable}{c_UIHandler_CLI_WS}";

}  // end of module
