<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:titansim='http://ttcn.ericsson.se/protocolModules/xtdp/xtdl'>
  <xsl:output method="html" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes" />
  <xsl:strip-space elements="*"/>
  <xsl:template match="text()" />
  <xsl:variable name="newline">
    <xsl:text>
    </xsl:text>
  </xsl:variable>

  <xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
  <xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>

  <xsl:template match="/">
    <xsl:choose>
      <xsl:when test="/titansim:Widgets">
        <xsl:apply-templates select="/titansim:Widgets/*"/>
      </xsl:when>
      <xsl:when test="/window">
        <html>
          <xsl:value-of select="$newline" />
          <xsl:variable name="window_label">
            <xsl:value-of select="/window/@title"/>
          </xsl:variable>
          <xsl:variable name="window_help" select="@help"/>
          <xsl:variable name="window_about" select="@about"/>
          <xsl:variable name="id">
            <xsl:value-of select="/window/@id"/>
          </xsl:variable>
          <head>
            <xsl:value-of select="$newline" />
            <meta http-equiv="pragma" content="no-cache" />
            <xsl:value-of select="$newline" />
            <meta http-equiv="Set-Cookie" content="guiVersion=0"/>
            <link href="main.css" type="text/css" rel="STYLESHEET" media="screen, print"/>
            <xsl:value-of select="$newline" />
            <!-- jquery ajax scripts to refresh -->
            <script src="jquery-3.5.1.min.js" type="text/javascript"/>
            <script src="jquery.flot.min.js" type="text/javascript"/>
            <script src="jquery.flot.hiddengraphs.js" type="text/javascript"/>
            <script src="jquery.flot.navigate.min.js" type="text/javascript"/>
            <script src="jquery.flot.time.js" type="text/javascript"/>
            <script src="html2canvas.js" type="text/javascript"/>
            <script src="htmlbrowser.js" type="text/javascript"/>
            <title>
              <xsl:value-of select="$window_label"/>
            </title>
          </head>
          <xsl:value-of select="$newline" />
          <body>
            <xsl:value-of select="$newline" />
            <div id="page_holder_id" class="page_holder">
              <xsl:value-of select="$newline" />
              <div id="main_holder_id" class="main_holder">
                <xsl:value-of select="$newline" />
                <img src="empty.png" id="header_placeholder" width="10" height="0"/>
                <form target="_top">
                  <xsl:value-of select="$newline" />
                  <div class="header">
                    <xsl:value-of select="$newline" />
                    <table class="header">
                      <xsl:value-of select="$newline" />
                      <tr>
                        <xsl:value-of select="$newline" />
                        <td class="logo">
                          <a class="logo" href="http://www.ericsson.se" target="_blank">
                            <img class="logoImage" src="ericsson_logo_top.png" alt=""/>
                          </a>
                        </td>
                        <td class="title">
                          <a class="logo" href="">
                            <xsl:value-of select="$window_label"/>
                          </a>
                        </td>
                        <xsl:value-of select="$newline" />
                        <td class="print">
                          <div class="print">
                            <ul class="print_button">
                              <xsl:choose>
                                <xsl:when test="$window_about != ''">
                                  <li>
                                    <a class="print" href="javascript:aboutpage('{$window_about}');" title="About">
                                      <img src="about.png" alt="About" id="about_button" width="16" height="16"/>
                                    </a>
                                  </li>
                                </xsl:when>
                              </xsl:choose>
                              <xsl:choose>
                                <xsl:when test="$window_help != ''">
                                  <li>
                                    <a class="print" href="javascript:helppage('{$window_help}');" title="Help">
                                      <img src="help.png" alt="Help" id="help_button" width="16" height="16"/>
                                    </a>
                                  </li>
                                </xsl:when>
                              </xsl:choose>
                              <li>
                                <a class="print" href="javascript:printpage();" title="Print">
                                  <img src="print.gif" alt="Print" id="print_button" width="16" height="16"/>
                                </a>
                              </li>
                              <li>
                                <a class="print" href="/complete_gui.xml" title="Save">
                                  <img src="save.gif" alt="Save" id="save_button" width="16" height="16"/>
                                </a>
                              </li>
                              <li>
                                <div id="full_size_id" class="full_size">
                                  <a class="print" href="javascript:full_size();" title="Maximize">
                                    <img src="full_size.gif" alt="Maximize" width="24" height="16"/>
                                  </a>
                                </div>
                                <div id="orig_size_id" class="orig_size">
                                  <a class="print" href="javascript:orig_size();" title="Restore">
                                    <img src="orig_size.gif" alt="Restore" width="24" height="16"/>
                                  </a>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </td>
                      </tr>
                      <xsl:value-of select="$newline" />
                    </table>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                  <div class="print_subtitle" id="browsergui_print_subtitle">
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                  <div class="main_block">
                    <xsl:value-of select="$newline" />
                    <xsl:apply-templates select="/window/*"/>
                  </div>
                  <input id="ajaxReloadVersionCount" type="hidden" value="" name="tab" />
                  <input id="browserguiTabID" type="hidden" value="{$id}" />
                  <xsl:value-of select="$newline" />
                </form>
                <xsl:value-of select="$newline" />
                <img src="empty.png" id="footer_placeholder" width="10" height="0"/>
              </div>
              <xsl:value-of select="$newline" />
            </div>
            <xsl:value-of select="$newline" />
          </body>
          <xsl:value-of select="$newline" />
        </html>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="id">
          <xsl:value-of select="titansim:tabpage/@id"/>
        </xsl:variable>
        <div class="tabpage" id="{$id}">
          <xsl:value-of select="$newline" />
          <input id="ajaxTabVersionCount_{$id}" type="hidden" value="" />
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="titansim:tabpage/*"/>
        </div>
        <xsl:value-of select="$newline" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="titansim:window">
    <!-- target: Browser -->
    <html>
      <xsl:value-of select="$newline" />
      <xsl:variable name="window_label">
        <xsl:value-of select="@title"/>
      </xsl:variable>
      <xsl:variable name="window_help" select="@help"/>
      <xsl:variable name="window_about" select="@about"/>
      <head>
        <xsl:value-of select="$newline" />
        <meta http-equiv="pragma" content="no-cache" />
        <xsl:value-of select="$newline" />
        <meta http-equiv="Set-Cookie" content="guiVersion=0"/>
        <link href="main.css" type="text/css" rel="STYLESHEET" media="screen, print"/>
        <xsl:value-of select="$newline" />
        <!-- jquery ajax scripts to refresh -->
        <script src="jquery-3.5.1.min.js" type="text/javascript"/>
        <script src="jquery.flot.min.js" type="text/javascript"/>
        <script src="jquery.flot.hiddengraphs.js" type="text/javascript"/>
        <script src="jquery.flot.navigate.min.js" type="text/javascript"/>
        <script src="jquery.flot.time.js" type="text/javascript"/>
        <script src="html2canvas.js" type="text/javascript"/>
        <script src="htmlbrowser.js" type="text/javascript"/>
        <title>
          <xsl:value-of select="$window_label"/>
        </title>
      </head>
      <xsl:value-of select="$newline" />
      <body>
        <xsl:value-of select="$newline" />
        <div id="page_holder_id" class="page_holder">
          <xsl:value-of select="$newline" />
          <div id="main_holder_id" class="main_holder">
            <xsl:value-of select="$newline" />
            <img src="empty.png" id="header_placeholder" width="10" height="0"/>
            <form target="_top">
              <xsl:value-of select="$newline" />
              <div class="header">
                <xsl:value-of select="$newline" />
                <table class="header">
                  <xsl:value-of select="$newline" />
                  <tr>
                    <xsl:value-of select="$newline" />
                    <td class="logo">
                      <a class="logo" href="http://www.ericsson.se" target="_blank">
                        <img class="logoImage" src="ericsson_logo_top.png" alt=""/>
                      </a>
                    </td>
                    <td class="title">
                      <a class="logo" href="">
                        <xsl:value-of select="$window_label"/>
                      </a>
                    </td>
                    <xsl:value-of select="$newline" />
                    <td class="print">
                      <div class="print">
                        <ul class="print_button">
                          <xsl:choose>
                            <xsl:when test="$window_about != ''">
                              <li>
                                <a class="print" href="javascript:aboutpage('{$window_about}');" title="About">
                                  <img src="about.png" alt="About" id="about_button" width="16" height="16"/>
                                </a>
                              </li>
                            </xsl:when>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="$window_help != ''">
                              <li>
                                <a class="print" href="javascript:helppage('{$window_help}');" title="Help">
                                  <img src="help.png" alt="Help" id="help_button" width="16" height="16"/>
                                </a>
                              </li>
                            </xsl:when>
                          </xsl:choose>
                          <li>
                            <a class="print" href="javascript:printpage();" title="Print">
                              <img src="print.gif" alt="Print" id="print_button" width="16" height="16"/>
                            </a>
                          </li>
                          <li>
                            <a class="print" href="/complete_gui.xml" title="Save">
                              <img src="save.gif" alt="Save" id="save_button" width="16" height="16"/>
                            </a>
                          </li>
                          <li>
                            <div id="full_size_id" class="full_size">
                              <a class="print" href="javascript:full_size();" title="Maximize">
                                <img src="full_size.gif" alt="Maximize" width="24" height="16"/>
                              </a>
                            </div>
                            <div id="orig_size_id" class="orig_size">
                              <a class="print" href="javascript:orig_size();" title="Restore">
                                <img src="orig_size.gif" alt="Restore" width="24" height="16"/>
                              </a>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </td>
                  </tr>
                  <xsl:value-of select="$newline" />
                </table>
                <xsl:value-of select="$newline" />
              </div>
              <xsl:value-of select="$newline" />
              <div class="print_subtitle" id="browsergui_print_subtitle">
                <xsl:value-of select="$newline" />
              </div>
              <xsl:value-of select="$newline" />
              <div class="main_block">
                <xsl:value-of select="$newline" />
                <xsl:apply-templates select="*"/>
              </div>
              <input id="ajaxReloadVersionCount" type="hidden" value="" name="main" />
              <xsl:value-of select="$newline" />
            </form>
            <xsl:value-of select="$newline" />
            <img src="empty.png" id="footer_placeholder" width="10" height="0"/>
          </div>
          <xsl:value-of select="$newline" />
        </div>
        <xsl:value-of select="$newline" />
      </body>
      <xsl:value-of select="$newline" />
    </html>
  </xsl:template>


  <!-- TABPAGES -->
  <xsl:template match="titansim:tabpages">
    <xsl:variable name="id">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="layout">
      <xsl:value-of select="@layout"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($layout,'LISTCARD')">
        <div class="expand_collapse_left">
          <a id="expandCollapseImg{$id}" onclick="javascript:toggleTabpages('{$id}');">&#160;&#160;</a>
        </div>
        <table class="vert">
          <xsl:value-of select="$newline" />
          <tr>
            <td class="menu">
              <div id="expand_collapse_data{$id}" class="expand_collapse_data">
                <xsl:value-of select="$newline" />
                <div class="vert_menu" id="menu_{$id}">
                  <xsl:value-of select="$newline" />
                  <xsl:choose>
                    <xsl:when test="$disabled_txt='disabled'">
                      <ul class="vert_menu {$customclass}" disabled="disabled">
                        <xsl:value-of select="$newline" />
                        <xsl:for-each select="titansim:tabpage">
                          <xsl:variable name="tab_id">
                            <xsl:value-of select="@id"/>
                          </xsl:variable>
                          <xsl:variable name="tab_label">
                            <xsl:value-of select="@label"/>
                          </xsl:variable>
                          <xsl:variable name="tab_orientation">
                            <xsl:value-of select="@orientation"/>
                          </xsl:variable>
                          <xsl:variable name="tab_howmanychildren">
                            <xsl:value-of select="count(child::*)"/>
                          </xsl:variable>
                          <xsl:variable name="tab_disabledongui">
                            <xsl:value-of select="@disabledongui"/>
                          </xsl:variable>
                          <xsl:variable name="tab_customclass">
                            <xsl:value-of select="@customclass"/>
                          </xsl:variable>
                          <xsl:choose>
                            <xsl:when test="$tab_howmanychildren > 0">
                              <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                                <span>
                                  <xsl:value-of select="$tab_label"/>
                                </span>
                              </li>
                              <xsl:value-of select="$newline" />
                            </xsl:when>
                            <xsl:otherwise>
                              <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                                <span>
                                  <xsl:value-of select="$tab_label"/>
                                </span>
                              </li>
                              <xsl:value-of select="$newline" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>
                      </ul>
                      <xsl:value-of select="$newline" />
                    </xsl:when>
                    <xsl:otherwise>
                      <ul class="vert_menu {$customclass}">
                        <xsl:value-of select="$newline" />
                        <xsl:for-each select="titansim:tabpage">
                          <xsl:variable name="tab_id">
                            <xsl:value-of select="@id"/>
                          </xsl:variable>
                          <xsl:variable name="tab_label">
                            <xsl:value-of select="@label"/>
                          </xsl:variable>
                          <xsl:variable name="tab_orientation">
                            <xsl:value-of select="@orientation"/>
                          </xsl:variable>
                          <xsl:variable name="tab_howmanychildren">
                            <xsl:value-of select="count(child::*)"/>
                          </xsl:variable>
                          <xsl:variable name="tab_disabledongui">
                            <xsl:value-of select="@disabledongui"/>
                          </xsl:variable>
                          <xsl:variable name="tab_customclass">
                            <xsl:value-of select="@customclass"/>
                          </xsl:variable>
                          <xsl:choose>
                            <xsl:when test="$tab_disabledongui='true'">
                              <xsl:choose>
                                <xsl:when test="$tab_howmanychildren > 0">
                                  <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                                    <span>
                                      <xsl:value-of select="$tab_label"/>
                                    </span>
                                  </li>
                                  <xsl:value-of select="$newline" />
                                </xsl:when>
                                <xsl:otherwise>
                                  <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                                    <span>
                                      <xsl:value-of select="$tab_label"/>
                                    </span>
                                  </li>
                                  <xsl:value-of select="$newline" />
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="$tab_howmanychildren > 0">
                                  <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                                    <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}" href="/?tabrequest_{$tab_id}" onClick="setTab('{$id}','{$tab_id}'); return true;">
                                      <xsl:value-of select="$tab_label"/>
                                    </a>
                                  </li>
                                  <xsl:value-of select="$newline" />
                                </xsl:when>
                                <xsl:otherwise>
                                  <li class="{$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                                    <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}" href="/?tabrequest_{$tab_id}" onClick="setTab('{$id}','{$tab_id}'); return true;">
                                      <xsl:value-of select="$tab_label"/>
                                    </a>
                                  </li>
                                  <xsl:value-of select="$newline" />
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:for-each>
                      </ul>
                      <xsl:value-of select="$newline" />
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
              </div>
            </td>
            <xsl:value-of select="$newline" />
            <td class="data">
              <div class="vert_data" id="data_{$id}">
                <xsl:value-of select="$newline" />
                <xsl:apply-templates select="*"/>
              </div>
            </td>
            <xsl:value-of select="$newline" />
          </tr>
          <xsl:value-of select="$newline" />
        </table>
        <xsl:value-of select="$newline" />
      </xsl:when>
      <xsl:when test="contains($layout,'CARD')">
        <div class="hidetabs_menu" id="menu_{$id}">
          <xsl:value-of select="$newline" />
          <xsl:choose>
            <xsl:when test="$disabled_txt='disabled'">
              <ul class="hidetabs_menu {$customclass}" disabled="disabled">
                <xsl:value-of select="$newline" />
                <xsl:for-each select="titansim:tabpage">
                  <xsl:variable name="tab_id">
                    <xsl:value-of select="@id"/>
                  </xsl:variable>
                  <xsl:variable name="tab_label">
                    <xsl:value-of select="@label"/>
                  </xsl:variable>
                  <xsl:variable name="tab_orientation">
                    <xsl:value-of select="@orientation"/>
                  </xsl:variable>
                  <xsl:variable name="tab_howmanychildren">
                    <xsl:value-of select="count(child::*)"/>
                  </xsl:variable>
                  <xsl:variable name="tab_disabledongui">
                    <xsl:value-of select="@disabledongui"/>
                  </xsl:variable>
                  <xsl:variable name="tab_customclass">
                    <xsl:value-of select="@customclass"/>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="$tab_howmanychildren > 0">
                      <li class="hidetabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                        <span>
                          <xsl:value-of select="$tab_label"/>
                        </span>
                      </li>
                      <xsl:value-of select="$newline" />
                    </xsl:when>
                    <xsl:otherwise>
                      <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                        <span>
                          <xsl:value-of select="$tab_label"/>
                        </span>
                      </li>
                      <xsl:value-of select="$newline" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </ul>
              <xsl:value-of select="$newline" />
            </xsl:when>
            <xsl:otherwise>
              <ul class="hidetabs_menu {$customclass}">
                <xsl:value-of select="$newline" />
                <xsl:for-each select="titansim:tabpage">
                  <xsl:variable name="tab_id">
                    <xsl:value-of select="@id"/>
                  </xsl:variable>
                  <xsl:variable name="tab_label">
                    <xsl:value-of select="@label"/>
                  </xsl:variable>
                  <xsl:variable name="tab_orientation">
                    <xsl:value-of select="@orientation"/>
                  </xsl:variable>
                  <xsl:variable name="tab_howmanychildren">
                    <xsl:value-of select="count(child::*)"/>
                  </xsl:variable>
                  <xsl:variable name="tab_disabledongui">
                    <xsl:value-of select="@disabledongui"/>
                  </xsl:variable>
                  <xsl:variable name="tab_customclass">
                    <xsl:value-of select="@customclass"/>
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="$tab_disabledongui='true'">
                      <xsl:choose>
                        <xsl:when test="$tab_howmanychildren > 0">
                          <li class="hidetabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                            <span>
                              <xsl:value-of select="$tab_label"/>
                            </span>
                          </li>
                          <xsl:value-of select="$newline" />
                        </xsl:when>
                        <xsl:otherwise>
                          <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                            <span>
                              <xsl:value-of select="$tab_label"/>
                            </span>
                          </li>
                          <xsl:value-of select="$newline" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="$tab_howmanychildren > 0">
                          <li class="hidetabpage_selected {$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                            <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}">
                              <xsl:value-of select="$tab_label"/>
                            </a>
                          </li>
                          <xsl:value-of select="$newline" />
                        </xsl:when>
                        <xsl:otherwise>
                          <li class="{$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                            <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}">
                              <xsl:value-of select="$tab_label"/>
                            </a>
                          </li>
                          <xsl:value-of select="$newline" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </ul>
              <xsl:value-of select="$newline" />
            </xsl:otherwise>
          </xsl:choose>
        </div>
        <xsl:value-of select="$newline" />
        <div class="tabs_data" id="data_{$id}">
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="*"/>
        </div>
        <xsl:value-of select="$newline" />
      </xsl:when>
      <xsl:otherwise>
        <div class="tabs_menu" id="menu_{$id}">
          <xsl:value-of select="$newline" />
          <div class="expand_collapse_up">
            <a id="expandCollapseImg{$id}" onclick="javascript:toggleTabpages('{$id}');">&#160;&#160;</a>
          </div>
          <xsl:value-of select="$newline" />
          <div id="expand_collapse_data{$id}" class="expand_collapse_data">
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <ul class="tabs_menu menu_disabled {$customclass}" disabled="disabled">
                  <xsl:value-of select="$newline" />
                  <xsl:for-each select="titansim:tabpage">
                    <xsl:variable name="tab_id">
                      <xsl:value-of select="@id"/>
                    </xsl:variable>
                    <xsl:variable name="tab_label">
                      <xsl:value-of select="@label"/>
                    </xsl:variable>
                    <xsl:variable name="tab_orientation">
                      <xsl:value-of select="@orientation"/>
                    </xsl:variable>
                    <xsl:variable name="tab_howmanychildren">
                      <xsl:value-of select="count(child::*)"/>
                    </xsl:variable>
                    <xsl:variable name="tab_disabledongui">
                      <xsl:value-of select="@disabledongui"/>
                    </xsl:variable>
                    <xsl:variable name="tab_customclass">
                      <xsl:value-of select="@customclass"/>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="$tab_howmanychildren > 0">
                        <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                          <span>
                            <xsl:value-of select="$tab_label"/>
                          </span>
                        </li>
                        <xsl:value-of select="$newline" />
                      </xsl:when>
                      <xsl:otherwise>
                        <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                          <span>
                            <xsl:value-of select="$tab_label"/>
                          </span>
                        </li>
                        <xsl:value-of select="$newline" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </ul>
              </xsl:when>
              <xsl:otherwise>
                <ul class="tabs_menu {$customclass}">
                  <xsl:value-of select="$newline" />
                  <xsl:for-each select="titansim:tabpage">
                    <xsl:variable name="tab_id">
                      <xsl:value-of select="@id"/>
                    </xsl:variable>
                    <xsl:variable name="tab_label">
                      <xsl:value-of select="@label"/>
                    </xsl:variable>
                    <xsl:variable name="tab_orientation">
                      <xsl:value-of select="@orientation"/>
                    </xsl:variable>
                    <xsl:variable name="tab_howmanychildren">
                      <xsl:value-of select="count(child::*)"/>
                    </xsl:variable>
                    <xsl:variable name="tab_disabledongui">
                      <xsl:value-of select="@disabledongui"/>
                    </xsl:variable>
                    <xsl:variable name="tab_customclass">
                      <xsl:value-of select="@customclass"/>
                    </xsl:variable>
                    <xsl:choose>
                      <xsl:when test="$tab_disabledongui='true'">
                        <xsl:choose>
                          <xsl:when test="$tab_howmanychildren > 0">
                            <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" disabled="disabled">
                              <span>
                                <xsl:value-of select="$tab_label"/>
                              </span>
                            </li>
                            <xsl:value-of select="$newline" />
                          </xsl:when>
                          <xsl:otherwise>
                            <li class="{$tab_customclass}" title="{$tab_label}" disabled="disabled">
                              <span>
                                <xsl:value-of select="$tab_label"/>
                              </span>
                            </li>
                            <xsl:value-of select="$newline" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="$tab_howmanychildren > 0">
                            <li class="tabpage_selected {$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                              <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}" href="/?tabrequest_{$tab_id}" onClick="setTab('{$id}','{$tab_id}'); return true;">
                                <xsl:value-of select="$tab_label"/>
                              </a>
                            </li>
                            <xsl:value-of select="$newline" />
                          </xsl:when>
                          <xsl:otherwise>
                            <li class="{$tab_customclass}" title="{$tab_label}" id="listitem_{$tab_id}">
                              <a class="tabset" title="{$tab_label}" id="tabpage_{$tab_id}" parenttab="{$id}" href="/?tabrequest_{$tab_id}" onClick="setTab('{$id}','{$tab_id}'); return true;">
                                <xsl:value-of select="$tab_label"/>
                              </a>
                            </li>
                            <xsl:value-of select="$newline" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </ul>
              </xsl:otherwise>
            </xsl:choose>
          </div>
          <xsl:value-of select="$newline" />
        </div>
        <xsl:value-of select="$newline" />
        <div class="tabs_data {$customclass}" id="data_{$id}">
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="*"/>
        </div>
        <xsl:value-of select="$newline" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- TABPAGE -->
  <xsl:template match="titansim:tabpage">
    <!-- Ha en a masodik url vagyok, akkor a parent activeTabID erteke kell legyen az url variable erteke -->
    <xsl:choose>
      <xsl:when test="count(preceding-sibling::*)=0">
        <xsl:variable name="id">
          <xsl:value-of select="@id"/>
        </xsl:variable>
        <xsl:variable name="tooltip">
          <xsl:value-of select="@tooltiptext"/>
        </xsl:variable>
        <xsl:variable name="label">
          <xsl:value-of select="@label"/>
        </xsl:variable>
        <xsl:variable name="orientation">
          <xsl:value-of select="@orientation"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="*">
            <!-- element have child elements -->
            <div class="tabpage" id="{$id}" title="{$tooltip}">
              <xsl:value-of select="$newline" />
              <input id="ajaxTabVersionCount_{$id}" type="hidden" value="" />
              <xsl:value-of select="$newline" />
              <xsl:apply-templates select="*"/>
            </div>
            <xsl:value-of select="$newline" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="id">
          <xsl:value-of select="@id"/>
        </xsl:variable>
        <xsl:variable name="tooltip">
          <xsl:value-of select="@tooltiptext"/>
        </xsl:variable>
        <xsl:variable name="label">
          <xsl:value-of select="@label"/>
        </xsl:variable>
        <xsl:variable name="orientation">
          <xsl:value-of select="@orientation"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="*">
            <!-- element have child elements -->
            <div class="tabpage" title="{$tooltip}" id="{$id}">
              <xsl:value-of select="$newline" />
              <input id="ajaxTabVersionCount_{$id}" type="hidden" value="" />
              <xsl:value-of select="$newline" />
              <xsl:apply-templates select="*"/>
            </div>
            <xsl:value-of select="$newline" />
          </xsl:when>
          <xsl:otherwise>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- HBOX -->
  <!-- <xsl:value-of select="$orientation" />
vertical az apa, akkor widthet kell allitani, horizontal akkor heigthot - HEIGHT-et soha nem all�tunk, mert azt rendezi a SCROLL bar
<div style="width: {$flexperc}%;">
-->
  <xsl:template match="titansim:hbox">
    <xsl:param name="orientation"/>
    <xsl:variable name="id">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="ownorient">
      <xsl:value-of select="@orientation"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="fixedposition_mixed">
      <xsl:value-of select="@fixedposition"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_class">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>hbox_disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>hbox_enabled</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="fixedposition">
      <xsl:value-of select="translate($fixedposition_mixed,$ucletters,$lcletters)"/>
    </xsl:variable>
    <xsl:variable name="fixedposition_class">
      <xsl:choose>
        <xsl:when test="$fixedposition='topleft'">
          <xsl:text>fixed_box_top_left</xsl:text>
        </xsl:when>
        <xsl:when test="$fixedposition='toprigt'">
          <xsl:text>fixed_box_top_right</xsl:text>
        </xsl:when>
        <xsl:when test="$fixedposition='bottomleft'">
          <xsl:text>fixed_box_bottom_left</xsl:text>
        </xsl:when>
        <xsl:when test="$fixedposition='bottomrigt'">
          <xsl:text>fixed_box_bottom_right</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$fixedposition_class!=''">
        <div class="{$fixedposition_class}">
          <xsl:choose>
            <xsl:when test="($flexperc > 0)">
              <xsl:choose>
                <xsl:when test="contains($orientation,'horizontal')">
                  <div class="hbox_hor {$disabled_class} {$customclass}" style="width: {$flexperc}%;" id="hbox_{$id}">
                    <xsl:value-of select="$newline" />
                    <xsl:apply-templates select="*">
                      <xsl:with-param name="orientation" select="$ownorient"/>
                    </xsl:apply-templates>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div class="hbox_vert {$disabled_class} {$customclass}" id="hbox_{$id}" title="">
                    <xsl:value-of select="$newline" />
                    <xsl:apply-templates select="*">
                      <xsl:with-param name="orientation" select="$ownorient"/>
                    </xsl:apply-templates>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="contains($orientation,'horizontal')">
                  <div class="hbox_hor {$disabled_class} {$customclass}" id="hbox_{$id}">
                    <xsl:value-of select="$newline" />
                    <xsl:apply-templates select="*">
                      <xsl:with-param name="orientation" select="$ownorient"/>
                    </xsl:apply-templates>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="*">
                      <!-- element have child elements -->
                      <div class="hbox_vert {$disabled_class} {$customclass}" id="hbox_{$id}" title="">
                        <xsl:value-of select="$newline" />
                        <xsl:apply-templates select="*">
                          <xsl:with-param name="orientation" select="$ownorient"/>
                        </xsl:apply-templates>
                        <xsl:value-of select="$newline" />
                      </div>
                      <xsl:value-of select="$newline" />
                    </xsl:when>
                    <xsl:otherwise>
                      <!-- NO child elements -->
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="($flexperc > 0)">
            <xsl:choose>
              <xsl:when test="contains($orientation,'horizontal')">
                <div class="hbox_hor {$disabled_class} {$customclass}" id="hbox_{$id}"  style="display: table-cell; width: {$flexperc}%;">
                  <xsl:value-of select="$newline" />
                  <xsl:apply-templates select="*">
                    <xsl:with-param name="orientation" select="$ownorient"/>
                  </xsl:apply-templates>
                  <xsl:value-of select="$newline" />
                </div>
                <xsl:value-of select="$newline" />
              </xsl:when>
              <xsl:otherwise>
                <div class="hbox_vert {$disabled_class} {$customclass}" id="hbox_{$id}">
                  <xsl:value-of select="$newline" />
                  <xsl:apply-templates select="*">
                    <xsl:with-param name="orientation" select="$ownorient"/>
                  </xsl:apply-templates>
                  <xsl:value-of select="$newline" />
                </div>
                <xsl:value-of select="$newline" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="contains($orientation,'horizontal')">
                <div class="hbox_hor {$disabled_class} {$customclass}" id="hbox_{$id}" style="display: table-cell;">
                  <xsl:value-of select="$newline" />
                  <xsl:apply-templates select="*">
                    <xsl:with-param name="orientation" select="$ownorient"/>
                  </xsl:apply-templates>
                  <xsl:value-of select="$newline" />
                </div>
                <xsl:value-of select="$newline" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="*">
                    <!-- element have child elements -->
                    <div class="hbox_vert {$disabled_class} {$customclass}" id="hbox_{$id}" title="" style="display: table; width: 100%;">
                      <xsl:value-of select="$newline" />
                      <xsl:apply-templates select="*">
                        <xsl:with-param name="orientation" select="$ownorient"/>
                      </xsl:apply-templates>
                      <xsl:value-of select="$newline" />
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <!-- NO child elements -->
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <!-- TREE -->
  <xsl:template match="titansim:tree">
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_class">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>tree_disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>tree_enabled</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$flexperc > 0">
        <xsl:choose>
          <xsl:when test="contains($parentorient,'horizontal')">
            <div class="hbox_td {$disabled_class}" style="width: {$flexperc}%;">
              <table class="tree_hor {$disabled_class} {$customclass}" style="width: 100%;">
                <xsl:value-of select="$newline" />
                <tr>
                  <xsl:value-of select="$newline" />
                  <xsl:apply-templates select="titansim:treecols"/>
                </tr>
                <xsl:value-of select="$newline" />
                <xsl:apply-templates select="titansim:treechildren"/>
              </table>
            </div>
            <xsl:value-of select="$newline" />
          </xsl:when>
          <xsl:otherwise>
            <table class="tree_vert {$disabled_class} {$customclass}">
              <xsl:value-of select="$newline" />
              <tr>
                <xsl:value-of select="$newline" />
                <xsl:apply-templates select="titansim:treecols"/>
              </tr>
              <xsl:value-of select="$newline" />
              <xsl:apply-templates select="titansim:treechildren"/>
            </table>
            <xsl:value-of select="$newline" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <table class="tree {$disabled_class} {$customclass}">
          <xsl:value-of select="$newline" />
          <tr>
            <xsl:value-of select="$newline" />
            <xsl:apply-templates select="titansim:treecols"/>
          </tr>
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="titansim:treechildren"/>
        </table>
        <xsl:value-of select="$newline" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="titansim:treecols">
    <xsl:apply-templates select="titansim:treecol"/>
  </xsl:template>

  <xsl:template match="titansim:treecol">
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="label">
      <xsl:value-of select="@label"/>
    </xsl:variable>
    <xsl:variable name="align">
      <xsl:value-of select="@align"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$flexperc > 0">
        <td style="width: {$flexperc}%;" class="{$customclass}">
          <xsl:value-of select="$newline" />
          <xsl:choose>
            <xsl:when test="string-length(@tooltiptext) > 0">
              <input class="treecol" style="text-align:{$align};" type="text" readonly="readonly" disabled="disabled" title="{$tooltip}"  value="{$label}" />
              <xsl:value-of select="$newline" />
            </xsl:when>
            <xsl:otherwise>
              <input class="treecol" style="text-align:{$align};" type="text" readonly="readonly" disabled="disabled" title="{$label}" value="{$label}" />
              <xsl:value-of select="$newline" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <xsl:value-of select="$newline" />
      </xsl:when>
      <xsl:otherwise>
        <td class="{$customclass}">
          <xsl:value-of select="$newline" />
          <xsl:choose>
            <xsl:when test="string-length(@tooltiptext) > 0">
              <input class="treecol" style="text-align:{$align};" type="text" readonly="readonly" disabled="disabled" title="{$tooltip}" value="{$label}" />
              <xsl:value-of select="$newline" />
            </xsl:when>
            <xsl:otherwise>
              <input class="treecol" style="text-align:{$align};" type="text" readonly="readonly" disabled="disabled" title="{$label}" align="{$align}" value="{$label}" />
              <xsl:value-of select="$newline" />
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <xsl:value-of select="$newline" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="titansim:treechildren">
    <xsl:apply-templates select="titansim:treeitem"/>
  </xsl:template>

  <xsl:template match="titansim:treeitem">
    <xsl:apply-templates select="titansim:treerow"/>
  </xsl:template>

  <xsl:template match="titansim:treerow">
    <xsl:variable name="treecolnum">
      <xsl:value-of select="count(./../../../titansim:treecols/titansim:treecol)"/>
    </xsl:variable>
    <xsl:variable name="treecellnum">
      <xsl:value-of select="count(./titansim:treecell)"/>
    </xsl:variable>
    <!--
  <xsl:for-each select="./../../../titansim:treecols/titansim:treecol">
    xxx $treecolnum: <xsl:value-of select="$treecolnum" />
    yyy $treecellnum: <xsl:value-of select="$treecellnum" />
  </xsl:for-each>
  -->
    <tr>
      <xsl:value-of select="$newline" />
      <xsl:apply-templates select="titansim:treecell"/>
      <xsl:if test="not($treecolnum = $treecellnum)">
        <xsl:call-template name="putnumberoftreecell">
          <xsl:with-param name="index" select="$treecellnum + 1" />
          <xsl:with-param name="total" select="$treecolnum" />
        </xsl:call-template>
      </xsl:if>
    </tr>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <xsl:template name="putnumberoftreecell">
    <xsl:param name="index" select="1" />
    <xsl:param name="total" select="10" />

    <td>
      <xsl:value-of select="$newline" />
      <input type="text" style="text-align: right;" disabled="disabled" readonly="readonly" class="textbox_read"/>
    </td>

    <xsl:if test="not($index = $total)">
      <xsl:call-template name="putnumberoftreecell">
        <xsl:with-param name="index" select="$index + 1" />
        <xsl:with-param name="total" select="$total" />
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template match="titansim:treecell">
    <td>
      <xsl:value-of select="$newline" />
      <xsl:variable name="treecollid">
        <xsl:value-of select="position()"/>
      </xsl:variable>
      <xsl:variable name="tooltip">
        <xsl:value-of select="@tooltiptext"/>
      </xsl:variable>
      <xsl:variable name="label">
        <xsl:value-of select="@label"/>
      </xsl:variable>
      <xsl:variable name="align">
        <xsl:value-of select="@align"/>
      </xsl:variable>
      <xsl:variable name="idje">
        <xsl:value-of select="@id"/>
      </xsl:variable>
      <xsl:variable name="treeid">
        <xsl:value-of select="./../../../../@id"/>
      </xsl:variable>
      <xsl:variable name="treerowid">
        <xsl:value-of select="count(./../preceding-sibling::titansim:treerow)"/>
      </xsl:variable>
      <xsl:variable name="treecollidminus1">
        <xsl:value-of select="position()-1"/>
      </xsl:variable>
      <xsl:variable name="treecellcustomclass">
        <xsl:value-of select="@customclass"/>
      </xsl:variable>
      <xsl:for-each select="./../../../../titansim:treecols/titansim:treecol">
        <xsl:if test="position() = $treecollid">
          <xsl:variable name="treecell_align">
            <xsl:choose>
              <xsl:when test="$align!=''">
                <xsl:value-of select="$align" />
              </xsl:when>
              <xsl:when test="@treecellalign!=''">
                <xsl:value-of select="@treecellalign" />
              </xsl:when>
              <xsl:when test="@align!=''">
                <xsl:value-of select="@align" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:text/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:variable name="widgetType">
            <xsl:value-of select="@widgetType"/>
          </xsl:variable>
          <xsl:variable name="disabledongui">
            <xsl:value-of select="@disabledongui"/>
          </xsl:variable>
          <xsl:variable name="customclass">
            <xsl:choose>
              <xsl:when test="$treecellcustomclass!=''">
                <xsl:value-of select="$treecellcustomclass" />
              </xsl:when>
              <xsl:when test="@customclass!=''">
                <xsl:value-of select="@customclass" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:text/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:variable name="editable">
            <xsl:value-of select="@editable"/>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="contains($label,'[led:blue]')">
              <xsl:variable name="led_label">
                <xsl:value-of select="substring-after($label,'[led:blue]')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$idje = ''">
                  <div id="bg_{$treeid}.{$treerowid}.{$treecollidminus1}" title="{$tooltip}" class="led_blue {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_blue_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div id="bg_{$idje}" title="{$tooltip}" class="led_blue {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_blue_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$idje}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($label,'[led:black]')">
              <xsl:variable name="led_label">
                <xsl:value-of select="substring-after($label,'[led:black]')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$idje = ''">
                  <div id="bg_{$treeid}.{$treerowid}.{$treecollidminus1}" title="{$tooltip}" class="led_black {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_black_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div id="bg_{$idje}" title="{$tooltip}" class="led_black {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_black_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$idje}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($label,'[led:red]')">
              <xsl:variable name="led_label">
                <xsl:value-of select="substring-after($label,'[led:red]')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$idje = ''">
                  <div id="bg_{$treeid}.{$treerowid}.{$treecollidminus1}" title="{$tooltip}" class="led_red {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_red_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div id="bg_{$idje}" title="{$tooltip}" class="led_red {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_red_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$idje}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($label,'[led:green]')">
              <xsl:variable name="led_label">
                <xsl:value-of select="substring-after($label,'[led:green]')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$idje = ''">
                  <div id="bg_{$treeid}.{$treerowid}.{$treecollidminus1}" title="{$tooltip}" class="led_green {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_green_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div id="bg_{$idje}" title="{$tooltip}" class="led_green {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_green_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$idje}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($label,'[led:yellow]')">
              <xsl:variable name="led_label">
                <xsl:value-of select="substring-after($label,'[led:yellow]')"/>
              </xsl:variable>
              <xsl:choose>
                <xsl:when test="$idje = ''">
                  <div id="bg_{$treeid}.{$treerowid}.{$treecollidminus1}" title="{$tooltip}" class="led_yellow {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_yellow_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <div id="bg_{$idje}" title="{$tooltip}" class="led_yellow {$customclass}">
                    <xsl:value-of select="$newline" />
                    <img src="led_yellow_16x16.png"/>
                    <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" id="{$idje}" value="{$led_label}" readonly="readonly" class="led_text"/>
                    <xsl:value-of select="$newline" />
                  </div>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($widgetType,'checkBox')">
              <xsl:choose>
                <xsl:when test="contains($label,'true')">
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" checked=""/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$idje}" id="{$idje}" checked=""/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:when test="contains($label,'false')">
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$idje}" id="{$idje}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($widgetType,'pushButton')">
              <xsl:choose>
                <xsl:when test="$disabledongui='true' or contains($editable,'false')">
                  <span class="greyButton" style="text-align:{$treecell_align};">
                    <button class="{$customclass}" title="{$tooltip}" disabled="disabled" id="{$idje}" type="button" name="{$idje}">
                      <xsl:value-of select="$label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                  <xsl:value-of select="$newline" />
                </xsl:when>
                <xsl:otherwise>
                  <span class="blueButton" style="text-align:{$treecell_align};">
                    <button class="{$customclass}" title="{$tooltip}" id="{$idje}" type="button" name="{$idje}">
                      <xsl:value-of select="$label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                  <xsl:value-of select="$newline" />
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($widgetType, 'toggleButton')">
              <xsl:choose>
                <xsl:when test="contains($label,'true')">
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" checked=""/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$idje}" id="{$idje}" checked=""/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:when test="contains($label,'false')">
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="checkbox" style="text-align:{$treecell_align};" class="{$customclass}" title="{$tooltip}" name="{$idje}" id="{$idje}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="contains($widgetType, 'integerField') or contains($widgetType, 'floatField')">
              <xsl:choose>
                <xsl:when test="$treecell_align=''">
                  <xsl:choose>
                    <xsl:when test="$disabledongui='true' or $editable='false'">
                      <xsl:choose>
                        <xsl:when test="$idje = ''">
                          <input type="text" style="text-align: right;" title="{$tooltip}" disabled="disabled" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <input type="text" style="text-align: right;" title="{$tooltip}" disabled="disabled" name="{$idje}" id="{$idje}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="$idje = ''">
                          <input type="text" style="text-align: right;" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" class="textbox_editable {$customclass}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <input type="text" style="text-align:right;" title="{$tooltip}" name="{$idje}" id="{$idje}" value="{$label}" class="textbox_editable {$customclass}" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$disabledongui='true' or $editable='false'">
                      <xsl:choose>
                        <xsl:when test="$idje = ''">
                          <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" disabled="disabled" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" disabled="disabled" name="{$idje}" id="{$idje}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:choose>
                        <xsl:when test="$idje = ''">
                          <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" class="textbox_editable {$customclass}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" name="{$idje}" id="{$idje}" value="{$label}" class="textbox_editable {$customclass}" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:choose>
                <xsl:when test="$disabledongui='true' or $editable='false'">
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" disabled="disabled" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" disabled="disabled" name="{$idje}" id="{$idje}" value="{$label}" readonly="readonly" class="textbox_read {$customclass}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$idje = ''">
                      <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" name="{$treeid}.{$treerowid}.{$treecollidminus1}" id="{$treeid}.{$treerowid}.{$treecollidminus1}" value="{$label}" class="textbox_editable {$customclass}"/>
                    </xsl:when>
                    <xsl:otherwise>
                      <input type="text" style="text-align:{$treecell_align};" title="{$tooltip}" name="{$idje}" id="{$idje}" value="{$label}" class="textbox_editable {$customclass}"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:if>
      </xsl:for-each>

    </td>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <!-- TEXTBOX -->
  <xsl:template match="titansim:textbox">
    <xsl:variable name="textboxId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="textboxValue">
      <xsl:value-of select="@value"/>
    </xsl:variable>
    <xsl:variable name="multiline">
      <xsl:value-of select="@multiline"/>
    </xsl:variable>
    <xsl:variable name="rows">
      <xsl:value-of select="@rows"/>
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 93)" />
    </xsl:variable>
    <xsl:variable name="colsflex">
      <xsl:value-of select="$flexperc * 2"/>
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="textlength">
      <xsl:value-of select="floor(string-length($textboxValue)* 58 div 10) + 10"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="focusafterchange">
      <xsl:value-of select="@focusafterchange"/>
    </xsl:variable>
    <xsl:variable name="readonly">
      <xsl:value-of select="@readonly"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$flexperc > 0">
        <xsl:choose>
          <xsl:when test="contains($parentorient,'horizontal')">
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <xsl:choose>
                  <xsl:when test="contains($multiline,'true')">
                    <xsl:choose>
                      <xsl:when test="$focusafterchange=''">
                        <div class="textbox_holder" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                          <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                          <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="textbox_holder" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                      <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}"/>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($multiline,'true')">
                    <xsl:choose>
                      <xsl:when test="$focusafterchange=''">
                        <div class="textbox_holder" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                          <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                          <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="textbox_holder" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                      <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}"/>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <xsl:choose>
                  <xsl:when test="contains($multiline,'true')">
                    <xsl:choose>
                      <xsl:when test="$focusafterchange=''">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$textboxValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}">
                          <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: 100%;"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($multiline,'true')">
                    <xsl:choose>
                      <xsl:when test="$focusafterchange=''">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$textboxValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}">
                          <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                            <xsl:value-of select="$textboxValue"/>
                          </textarea>
                        </div>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}"  style="width: 100%;"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$disabled_txt='disabled'">
            <xsl:choose>
              <xsl:when test="contains($multiline,'true')">
                <xsl:choose>
                  <xsl:when test="$focusafterchange=''">
                    <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                      <xsl:value-of select="$textboxValue"/>
                    </textarea>
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}">
                      <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                        <xsl:value-of select="$textboxValue"/>
                      </textarea>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="contains($multiline,'true')">
                <xsl:choose>
                  <xsl:when test="$focusafterchange=''">
                    <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                      <xsl:value-of select="$textboxValue"/>
                    </textarea>
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="focusafterchange_{$focusafterchange}" focusafterchange="{$focusafterchange}">
                      <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                        <xsl:value-of select="$textboxValue"/>
                      </textarea>
                    </div>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- BUTTON -->
  <xsl:template match="titansim:button">
    <xsl:variable name="buttonId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="buttonValue">
      <xsl:value-of select="@value"/>
    </xsl:variable>
    <xsl:variable name="buttonDisabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="label">
      <xsl:value-of select="@label"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or contains($buttonDisabled,'true')">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="string-length(@tooltiptext) > 0">
        <xsl:choose>
          <xsl:when test="$flexperc > 0">
            <xsl:choose>
              <xsl:when test="contains($parentorient,'horizontal')">
                <xsl:choose>
                  <xsl:when test="$disabled_txt='disabled'">
                    <div class="button button_hor" style="width: {$flexperc}%;">
                      <span class="greyButton">
                        <button class="{$customclass}" title="{$tooltip}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="button button_hor" style="width: {$flexperc}%;">
                      <span class="blueButton">
                        <button class="{$customclass}" title="{$tooltip}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$disabled_txt='disabled'">
                    <div class="button">
                      <span class="greyButton">
                        <button class="{$customclass}" title="{$tooltip}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="button">
                      <span class="blueButton">
                        <button class="{$customclass}" title="{$tooltip}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <div class="button">
                  <span class="greyButton">
                    <button class="{$customclass}" title="{$tooltip}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                      <xsl:value-of select="current()/@label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                </div>
                <xsl:value-of select="$newline" />
              </xsl:when>
              <xsl:otherwise>
                <div class="button">
                  <span class="blueButton">
                    <button class="{$customclass}" title="{$tooltip}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                      <xsl:value-of select="current()/@label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                </div>
                <xsl:value-of select="$newline" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$flexperc > 0">
            <xsl:choose>
              <xsl:when test="contains($parentorient,'horizontal')">
                <xsl:choose>
                  <xsl:when test="$disabled_txt='disabled'">
                    <div  class="button button_hor" style="width: {$flexperc}%;">
                      <span class="greyButton">
                        <button class="{$customclass}" title="{$label}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="button button_hor" style="width: {$flexperc}%;">
                      <span class="blueButton">
                        <button class="{$customclass}" title="{$label}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$disabled_txt='disabled'">
                    <div class="button">
                      <span class="greyButton">
                        <button class="{$customclass}" title="{$label}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="button">
                      <span class="blueButton">
                        <button class="{$customclass}" title="{$label}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                          <xsl:value-of select="current()/@label"/>
                          <xsl:call-template name="insertImage">
                            <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                          </xsl:call-template>
                        </button>
                      </span>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <div class="button">
                  <span class="greyButton">
                    <button class="{$customclass}" title="{$label}" disabled="disabled" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                      <xsl:value-of select="current()/@label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                </div>
                <xsl:value-of select="$newline" />
              </xsl:when>
              <xsl:otherwise>
                <div class="button">
                  <span class="blueButton">
                    <button class="{$customclass}" title="{$label}" id="{$buttonId}" type="button" name="{$buttonId}" value="{$buttonValue}">
                      <xsl:value-of select="current()/@label"/>
                      <xsl:call-template name="insertImage">
                        <xsl:with-param name="ima_id">"<xsl:value-of select="current()/@imageid"/>"</xsl:with-param>
                      </xsl:call-template>
                    </button>
                  </span>
                </div>
                <xsl:value-of select="$newline" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- INSERTIMAGE -->
  <xsl:template name="insertImage">
    <xsl:param name="ima_id" />
    <xsl:choose>
      <xsl:when test="string-length($ima_id) > 2">
        <img src="{$ima_id}.png" height="16" width="16" alt=""/>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <!-- SPACER -->
  <xsl:template match="titansim:spacer">
    <xsl:text> </xsl:text>
  </xsl:template>

  <!-- LABEL -->
  <xsl:template match="titansim:label">
    <xsl:param name="orientation" />
    <xsl:variable name="labelId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 93)" />
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="value">
      <xsl:value-of select="@value"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_class">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>label_disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>label_enabled</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <!-- dynamic updatable label: textbox  -->
      <xsl:when test="contains($customclass,'HasExternalData')">

        <xsl:choose>
          <xsl:when test="string-length(@tooltiptext) > 0">
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$tooltip}" style="width: {$flexperc}%;">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox" title="{$tooltip}"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$tooltip}"  style="width: 100%;">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox" title="{$tooltip}"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$tooltip}">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox" title="{$tooltip}"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$tooltip}">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox" title="{$tooltip}"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$value}" style="width: {$flexperc}%;">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$value}"  style="width: 100%;">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$value}">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$value}">
                      <input type="text" name="{$labelId}" id="{$labelId}" value="{$value}" readonly="readonly" class="label_textbox"/>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:when>
      <xsl:otherwise>

        <xsl:choose>
          <xsl:when test="string-length(@tooltiptext) > 0">
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$tooltip}" style="width: {$flexperc}%;">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$tooltip}"  style="width: 100%;">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$tooltip}">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$tooltip}">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$value}" style="width: {$flexperc}%;">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$value}"  style="width: 100%;">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <div class="label {$disabled_class} {$customclass}" title="{$value}">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:when>
                  <xsl:otherwise>
                    <div class="label_title {$disabled_class} {$customclass}" title="{$value}">
                      <label class="">
                        <xsl:value-of select="@value"/>
                      </label>
                    </div>
                    <xsl:value-of select="$newline" />
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>

      </xsl:otherwise>
    </xsl:choose>

  </xsl:template>

  <!-- MENULIST -->
  <xsl:template match="titansim:menulist">
    <xsl:variable name="menulistId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="label">
      <xsl:value-of select="@label"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="readonly">
      <xsl:value-of select="@readonly"/>
    </xsl:variable>
    <xsl:variable name="editable">
      <xsl:value-of select="@editable"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true' or translate($readonly,'TRUE','true')='true'">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$disabled_txt='disabled'">
        <select name="{$menulistId}" id="{$menulistId}" size="1" title="{$tooltip}" disabled="disabled" class="{$customclass}">
          <option value="{$label}">
            <xsl:value-of select="@label"/>
          </option>
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="titansim:menupopup"/>
        </select>
      </xsl:when>
      <xsl:otherwise>
        <select name="{$menulistId}" id="{$menulistId}" size="1" title="{$tooltip}" class="{$customclass}">
          <option value="{$label}">
            <xsl:value-of select="@label"/>
          </option>
          <xsl:value-of select="$newline" />
          <xsl:apply-templates select="titansim:menupopup"/>
        </select>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- MENUPOPUP -->
  <xsl:template match="titansim:menupopup">
    <xsl:apply-templates select="titansim:menuitem"/>
  </xsl:template>

  <!-- MENUITEM -->
  <xsl:template match="titansim:menuitem">
    <xsl:variable name="menuitemId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="selected">
      <xsl:value-of select="@selected"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="translate(@selected,'TRUE','true')='true'">
        <option selected="selected" value="{$menuitemId}">
          <xsl:value-of select="@label"/>
        </option>
        <xsl:value-of select="$newline" />
      </xsl:when>
      <xsl:otherwise>
        <option value="{$menuitemId}">
          <xsl:value-of select="@label"/>
        </option>
        <xsl:value-of select="$newline" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- LISTBOX -->
  <xsl:template match="titansim:listbox">
    <xsl:variable name="listboxId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="size">
      <xsl:value-of select="@rows"/>
    </xsl:variable>
    <xsl:variable name="type">
      <xsl:value-of select="@seltype"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$disabled_txt='disabled'">
        <xsl:choose>
          <xsl:when test="contains($type, 'multiple')">
            <xsl:choose>
              <xsl:when test="($size &lt; 3)">
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="3" multiple="multiple" title="{$tooltip}" disabled="disabled">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:when>
              <xsl:otherwise>
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="{$size}" multiple="multiple" title="{$tooltip}" disabled="disabled">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="($size &lt; 3)">
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="3" title="{$tooltip}" disabled="disabled">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:when>
              <xsl:otherwise>
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="{$size}" title="{$tooltip}" disabled="disabled">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="contains($type, 'multiple')">
            <xsl:choose>
              <xsl:when test="($size &lt; 3)">
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="3" multiple="multiple" title="{$tooltip}">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:when>
              <xsl:otherwise>
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="{$size}" multiple="multiple" title="{$tooltip}">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="($size &lt; 3)">
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="3" title="{$tooltip}">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:when>
              <xsl:otherwise>
                <select class="listbox {$customclass}" name="{$listboxId}" id="{$listboxId}" size="{$size}" title="{$tooltip}">
                  <xsl:apply-templates select="titansim:listitem"/>
                </select>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- LISTITEM -->
  <xsl:template match="titansim:listitem">
    <xsl:variable name="listitemId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <option value="{$listitemId}">
      <xsl:value-of select="@label"/>
    </option>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <!-- DISTRIBUTION CHART -->
  <xsl:template match="titansim:distributionchart">
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="chartId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="chartTitle">
      <xsl:value-of select="@title"/>
    </xsl:variable>
    <xsl:variable name="xAxisTitle">
      <xsl:value-of select="@axisXLabel"/>
    </xsl:variable>
    <xsl:variable name="yAxisTitle">
      <xsl:value-of select="@axisYLabel"/>
    </xsl:variable>
    <xsl:variable name="chartId_without_point" select="translate($chartId,'.','_')"/>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_class">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>distributionchart_disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>distributionchart_enabled</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($parentorient,'horizontal')">
        <xsl:choose>
          <xsl:when test="($flexperc > 90)">
            <div id="{$chartId_without_point}_draw" class="hbox_distributionchart {$customclass}" title="{$tooltip}" style="width: 100%">
              <img src="loading_100.gif"/>
            </div>
          </xsl:when>
          <xsl:otherwise>
            <div id="{$chartId_without_point}_draw" class="hbox_distributionchart {$customclass}" title="{$tooltip}" style="width: {$flexperc}%">
              <img src="loading_100.gif"/>
            </div>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <div id="{$chartId_without_point}_draw" class="hbox_distributionchart {$customclass}" title="{$tooltip}" style="width: 100%">
          <img src="loading_100.gif"/>
        </div>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="string-length(@tooltiptext) > 0">
        <div id="{$chartId_without_point}" class="distributionchart" title="{$tooltip}" xaxis="{$xAxisTitle}" yaxis="{$yAxisTitle}">
          <xsl:apply-templates select="titansim:valuelist"/>
          <xsl:apply-templates select="titansim:intervallimits"/>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <div id="{$chartId_without_point}" class="distributionchart" title="{$chartTitle}" xaxis="{$xAxisTitle}" yaxis="{$yAxisTitle}">
          <xsl:apply-templates select="titansim:valuelist"/>
          <xsl:apply-templates select="titansim:intervallimits"/>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- VALUELIST -->
  <xsl:template match="titansim:valuelist">
    <xsl:variable name="valuelistId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="legend">
      <xsl:value-of select="@legend"/>
    </xsl:variable>
    <xsl:variable name="valueListColor">
      <xsl:value-of select="@color"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>

    <input class='distributionchart_valuelist' id="{$valuelistId}" type="hidden" name="{$legend}" color="{$valueListColor}" title="{$tooltip}">
      <xsl:apply-templates select="titansim:value"/>
      <xsl:for-each select="titansim:value">
        <value class='distributionchart_value' id="{$valuelistId}_value">
          <xsl:value-of select="."/>
        </value>
      </xsl:for-each>
    </input>
  </xsl:template>

  <!-- INTERVALLIMITS -->
  <xsl:template match="titansim:intervallimits">
    <xsl:variable name="limitstId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <select class="intervallimits" type="hidden" id="{$limitstId}">
      <xsl:for-each select="titansim:value">
        <xsl:variable name="limit">
          <xsl:value-of select="."/>
        </xsl:variable>
        <option>
          <xsl:value-of select="."/>
        </option>
      </xsl:for-each>
    </select>
  </xsl:template>

  <!-- CHART -->
  <xsl:template match="titansim:chart">
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="chartId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="chartTitle">
      <xsl:value-of select="@title"/>
    </xsl:variable>
    <xsl:variable name="chartId_without_point" select="translate($chartId,'.','_')"/>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>
    <xsl:variable name="disabled_class">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>chart_disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>chart_enabled</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="contains($parentorient,'horizontal')">
        <xsl:choose>
          <xsl:when test="($flexperc > 90)">
            <div id="{$chartId_without_point}_draw" class="hbox_chart {$customclass}" title="{$tooltip}">
              <img src="loading_100.gif"/>
            </div>
          </xsl:when>
          <xsl:otherwise>
            <div id="{$chartId_without_point}_draw" class="hbox_chart {$customclass}" title="{$tooltip}" style="width: {$flexperc}%">
              <img src="loading_100.gif"/>
            </div>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <div id="{$chartId_without_point}_draw" class="hbox_chart {$customclass}" title="{$tooltip}">
          <img src="loading_100.gif"/>
        </div>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="string-length(@tooltiptext) > 0">
        <div id="{$chartId_without_point}" class="chart" title="{$tooltip}">
          <xsl:apply-templates select="titansim:trace"/>
        </div>
      </xsl:when>
      <xsl:otherwise>
        <div id="{$chartId_without_point}" class="chart" title="{$chartTitle}">
          <xsl:apply-templates select="titansim:trace"/>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- TRACE -->
  <xsl:template match="titansim:trace">
    <xsl:variable name="traceId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="traceName">
      <xsl:value-of select="@name"/>
    </xsl:variable>
    <xsl:variable name="traceColor">
      <xsl:value-of select="@color"/>
    </xsl:variable>
    <xsl:variable name="traceMaxPoints">
      <xsl:value-of select="@maxPoints"/>
    </xsl:variable>
    <xsl:variable name="tracePhysicalUnitX">
      <xsl:value-of select="@physicalUnitX"/>
    </xsl:variable>
    <xsl:variable name="tracePhysicalUnitY">
      <xsl:value-of select="@physicalUnitY"/>
    </xsl:variable>
    <input id="{$traceId}" type="hidden" name="{$traceName}" color="{$traceColor}" maxPoints="{$traceMaxPoints}" physicalUnitX="{$tracePhysicalUnitX}" physicalUnitY="{$tracePhysicalUnitY}"/>
  </xsl:template>

  <!-- NUMERICAL WIDGET -->
  <xsl:template match="titansim:numericalwidget">
    <xsl:variable name="textboxId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="textboxValue">
      <xsl:value-of select="@value"/>
    </xsl:variable>
    <xsl:variable name="readonly">
      <xsl:value-of select="@readonly"/>
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="multiline">
      <xsl:value-of select="@multiline"/>
    </xsl:variable>
    <xsl:variable name="rows">
      <xsl:value-of select="@rows"/>
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="colsflex">
      <xsl:value-of select="$flexperc * 2"/>
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="textlength">
      <xsl:value-of select="floor(string-length($textboxValue)* 58 div 10) + 10"/>
    </xsl:variable>
    <xsl:variable name="tooltip">
      <xsl:value-of select="@tooltiptext"/>
    </xsl:variable>
    <xsl:variable name="spinner">
      <xsl:value-of select="@spinner"/>
    </xsl:variable>
    <xsl:variable name="minvalue">
      <xsl:value-of select="@minvalue"/>
    </xsl:variable>
    <xsl:variable name="maxvalue">
      <xsl:value-of select="@maxvalue"/>
    </xsl:variable>
    <xsl:variable name="stepsize">
      <xsl:value-of select="@stepsize"/>
    </xsl:variable>
    <xsl:variable name="widgetType">
      <xsl:value-of select="@widgetType"/>
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="contains($widgetType,'integerField')">
        <xsl:variable name="truncedValue">
          <xsl:value-of select="substring-before($textboxValue,'.')"/>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="translate($spinner,'TRUE','true')='true'">
            <xsl:choose>
              <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                <!--  integerField and spinner and readonly -->
                <table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td class="noborder" rowspan="2">
                      <xsl:choose>
                        <xsl:when test="$flexperc > 0">
                          <xsl:choose>
                            <xsl:when test="contains($parentorient,'horizontal')">
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$truncedValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$truncedValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="contains($multiline,'true')">
                              <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                <xsl:value-of select="$truncedValue"/>
                              </textarea>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="small">
                      <span id="upButtonSpan_{$textboxId}" class="upButtonReadOnly">
                        <a disabled="disabled" onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'true');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="small">
                      <span id="downButtonSpan_{$textboxId}" class="downButtonReadOnly">
                        <a disabled="disabled" onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'false');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                </table>
              </xsl:when>
              <xsl:otherwise>
                <!--  integerField and spinner and editable -->
                <table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td class="noborder" rowspan="2">
                      <xsl:choose>
                        <xsl:when test="$flexperc > 0">
                          <xsl:choose>
                            <xsl:when test="contains($parentorient,'horizontal')">
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$truncedValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$truncedValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="contains($multiline,'true')">
                              <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                <xsl:value-of select="$truncedValue"/>
                              </textarea>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="small">
                      <span id="upButtonSpan_{$textboxId}" class="upButton">
                        <a onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'true');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="small">
                      <span id="downButtonSpan_{$textboxId}" class="downButton">
                        <a onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'false');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                </table>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <!--  integerField and no spinner -->
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <xsl:choose>
                      <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$truncedValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$truncedValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$truncedValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$truncedValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                    <xsl:choose>
                      <xsl:when test="contains($multiline,'true')">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$truncedValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="contains($multiline,'true')">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$truncedValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$truncedValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <!--  floatField  -->
        <xsl:choose>
          <xsl:when test="translate($spinner,'TRUE','true')='true'">
            <!--  floatField and spinner -->
            <xsl:choose>
              <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                <!--  floatField and spinner and readonly -->
                <table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td class="noborder" rowspan="2">
                      <xsl:choose>
                        <xsl:when test="$flexperc > 0">
                          <xsl:choose>
                            <xsl:when test="contains($parentorient,'horizontal')">
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$textboxValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$textboxValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="contains($multiline,'true')">
                              <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                                <xsl:value-of select="$textboxValue"/>
                              </textarea>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="small">
                      <span id="upButtonSpan_{$textboxId}" class="upButtonReadOnly">
                        <a disabled="disabled" onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'true');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="small">
                      <span id="downButtonSpan_{$textboxId}" class="downButtonReadOnly">
                        <a disabled="disabled" onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'false');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                </table>
              </xsl:when>
              <xsl:otherwise>
                <!--  floatField and spinner and editable-->
                <table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td class="noborder" rowspan="2">
                      <xsl:choose>
                        <xsl:when test="$flexperc > 0">
                          <xsl:choose>
                            <xsl:when test="contains($parentorient,'horizontal')">
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$textboxValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:choose>
                                <xsl:when test="contains($multiline,'true')">
                                  <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                    <xsl:value-of select="$textboxValue"/>
                                  </textarea>
                                </xsl:when>
                                <xsl:otherwise>
                                  <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:choose>
                            <xsl:when test="contains($multiline,'true')">
                              <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                                <xsl:value-of select="$textboxValue"/>
                              </textarea>
                            </xsl:when>
                            <xsl:otherwise>
                              <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td class="small">
                      <span id="upButtonSpan_{$textboxId}" class="upButton">
                        <a onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'true');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td class="small">
                      <span id="downButtonSpan_{$textboxId}" class="downButton">
                        <a onclick="javascript:changeNumericalWidget('{$textboxId}','{$minvalue}','{$maxvalue}','{$stepsize}','{$widgetType}', 'false');">&#160;&#160;&#160;&#160;</a>
                      </span>
                    </td>
                  </tr>
                </table>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <!--  floatField and no spinner -->
            <xsl:choose>
              <xsl:when test="$flexperc > 0">
                <xsl:choose>
                  <xsl:when test="contains($parentorient,'horizontal')">
                    <xsl:choose>
                      <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$textboxValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="{$colsflex}" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$textboxValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$textboxValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:choose>
                          <xsl:when test="contains($multiline,'true')">
                            <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                              <xsl:value-of select="$textboxValue"/>
                            </textarea>
                          </xsl:when>
                          <xsl:otherwise>
                            <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="$disabledongui='true' or translate($readonly,'TRUE','true')='true'">
                    <xsl:choose>
                      <xsl:when test="contains($multiline,'true')">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$textboxValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" readonly="readonly" class="textbox_read {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:choose>
                      <xsl:when test="contains($multiline,'true')">
                        <textarea rows="{$rows}" cols="200" name="{$textboxId}" id="{$textboxId}" class="textbox_editable {$customclass}" title="{$tooltip}">
                          <xsl:value-of select="$textboxValue"/>
                        </textarea>
                      </xsl:when>
                      <xsl:otherwise>
                        <input type="text" name="{$textboxId}" id="{$textboxId}" value="{$textboxValue}" class="textbox_editable {$customclass}" title="{$tooltip}" style="width: {$textlength}px;"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- HTMLCODE -->
  <xsl:template match="titansim:htmlcode">
    <xsl:variable name="htmlCodeId">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <xsl:variable name="newline">
      <xsl:text>
      </xsl:text>
    </xsl:variable>
    <xsl:variable name="htmlCodeValueWithNewLine">
      <xsl:value-of select="./titansim:value"/>
    </xsl:variable>
    <xsl:variable name="htmlCodeValue">
      <xsl:value-of select="translate($htmlCodeValueWithNewLine,$newline,'')" />
    </xsl:variable>
    <xsl:variable name="disabledongui">
      <xsl:value-of select="@disabledongui"/>
    </xsl:variable>
    <xsl:variable name="disabled">
      <xsl:value-of select="@disabled"/>
    </xsl:variable>
    <xsl:variable name="parentorient">
      <xsl:value-of select="../@orientation" />
    </xsl:variable>
    <xsl:variable name="sumflex">
      <xsl:value-of select="sum(../node()/@flex)"/>
    </xsl:variable>
    <xsl:variable name="flexperc">
      <xsl:value-of select="floor((@flex div $sumflex) * 100)" />
    </xsl:variable>
    <xsl:variable name="customclass">
      <xsl:value-of select="@customclass"/>
    </xsl:variable>

    <xsl:variable name="disabled_txt">
      <xsl:choose>
        <xsl:when test="$disabledongui='true' or $disabled='true'">
          <xsl:text>disabled</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>


    <xsl:choose>
      <xsl:when test="$flexperc > 0">
        <xsl:choose>
          <xsl:when test="contains($parentorient,'horizontal')">
            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <div id="{$htmlCodeId}" class="htmlcode {$customclass}" disabled="disabled" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                </div>
              </xsl:when>
              <xsl:otherwise>
                <div id="{$htmlCodeId}" class="htmlcode {$customclass}" style="width: {$flexperc}%; FLOAT: left; display: table-cell;">
                </div>
                <script type="text/javascript">
               document.getElementById('<xsl:copy-of select="$htmlCodeId"/>').innerHTML = '<xsl:copy-of select="$htmlCodeValue"/>';
                </script>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:when>
          <xsl:otherwise>

            <xsl:choose>
              <xsl:when test="$disabled_txt='disabled'">
                <div id="{$htmlCodeId}" class="htmlcode {$customclass}" disabled="disabled">
                </div>
              </xsl:when>
              <xsl:otherwise>
                <div id="{$htmlCodeId}" class="htmlcode {$customclass}">
                </div>
                <script type="text/javascript">
               document.getElementById('<xsl:copy-of select="$htmlCodeId"/>').innerHTML = '<xsl:copy-of select="$htmlCodeValue"/>';
                </script>
              </xsl:otherwise>
            </xsl:choose>

          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="$disabled_txt='disabled'">
            <div id="{$htmlCodeId}" class="htmlcode {$customclass}" disabled="disabled">
            </div>
          </xsl:when>
          <xsl:otherwise>
            <div id="{$htmlCodeId}" class="htmlcode {$customclass}">
            </div>
            <script type="text/javascript">
               document.getElementById('<xsl:copy-of select="$htmlCodeId"/>').innerHTML = '<xsl:copy-of select="$htmlCodeValue"/>';
            </script>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>


  </xsl:template>

  <!-- =========================================== NOT USED =========================================== -->

  <xsl:template match="titansim:tab">
    <xsl:variable name="tabid">
      <xsl:value-of select="@id"/>
    </xsl:variable>
    <button name="{$tabid}">
      <xsl:value-of select="@label"/>
    </button>
    <xsl:value-of select="$newline" />
  </xsl:template>

  <xsl:template match="titansim:tabpanels">
    <xsl:apply-templates select="*"/>
  </xsl:template>

  <xsl:template match="titansim:tabpanel">
    <xsl:apply-templates select="*"/>
  </xsl:template>

</xsl:stylesheet>
