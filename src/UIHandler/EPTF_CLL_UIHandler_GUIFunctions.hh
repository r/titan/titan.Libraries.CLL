///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

#include "Universal_charstring.hh"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include "EPTF_CLL_DataSource_Definitions.hh"
#include "rapidxml.hh"
#include <libxml/parser.h>
#include <libxml/xmlschemas.h>
#include <map>
#include "EPTF_CLL_DataSource_Definitions.hh"
#include "EPTF_CLL_Variable_Definitions.hh"
#include "EPTF_CLL_Common_Definitions.hh"
#include <string.h>

using namespace rapidxml;
using namespace std;

//TODO use class instead of namespace
namespace EPTF__CLL__UIHandler__WidgetFunctions {

/* process Data structure */
struct process_data {
  vector< map< string, string > > ITValuesMapsVector;  // For ITValues Maps
  map<string,int> checkWidgetIDMapLastRun;             // check widget id map for nodes in clone_doc
  xml_document<> * cloneDocPTR;                        // local clone doc PTR
  int waitforIdx;                                      // for f__EPTF__UIHandler__GUIFunctions__waitForProcessExternalData__handlers
  std::string parameters_String;                       // concatenated data parametes string
};

/* Varibale Data structure */
struct variable_data {
  long node_offset_id;                                      // Node OFFSET ID
  xml_node<> * orig_node_ptr;                               // Original Node PTR
  char * orig_node_name;                                    // Original Node Name
  char * widget_id;                                           // Node ID
  long local_var_idx;                                       // Local Variable Index
  char * remote_var_name;                                   // Remote Variable Name
  map<string, string>  it_values_list;                      // IT Values Vector
  bool deleted;                                             // The Item Deleted
};

/* widget node, parent nod structure */
struct node_parent {
  long node_offset_id;                                      // Node OFFSET ID
  xml_node<> * orig_node_ptr;                               // Original Node PTR
  long parent_offset_node_id;                               // Parent OFFSET ID
  bool iterator_like;                                       // The Item iterator or externalvalue
};

/* playlist data structure */
struct playlist_data {
  xml_node<> * playlist_node_ptr;                           // Playlist Node PTR
  int idx;                                                  // The sorting idx
};

/* playlist class */
struct playListItem {
  char * widget_id;                                         // widget ID
  long node_offset_id;                                      // Node OFFSET ID
  xml_node<> * node_ptr;                                    // Node PTR
  xml_node<> * playlist_node_ptr;                           // Playlist Node PTR
  int idx;                                                  // The sorting idx
};

struct playlist_order
{
    bool operator()(playListItem const & a, playListItem const & b) const
    {
        return a.idx < b.idx;
    }
};

//=========================================================================
// External Variables [TTCN-3]
//=========================================================================
// EPTF_CLL_UIHandler_Definitions:
// -------------------------------
// v_UIHandler_GUIFunctions_nofRunning_processExternalData_handlers
// - number of running processes counter used waiting for all process done
// -------------------------------
// v_EPTF_UIHandler_WidgetFunctions_XTDP_XSD_filepath
// - validator XSD path if it its not  empty: "" this is the validator XSD path.
//   if empty, the validate process is not necessary.

//=========================================================================
// External Functions [TTCN-3]
//=========================================================================
// EPTF_CLL_UIHandler_WidgetFunctions:
// -----------------------------------
// f_EPTF_UIHandler_GUIFunctions_waitForProcessExternalData_handlers
// - wait for all non blocking data request process done
// -----------------------------------
// f_EPTF_DataSource_getData_nonBlocking
// - non blocking get data function

//=========================================================================
// Definitions
//=========================================================================
/* Defined buffer sizes */
/* -------------------- */
#define SIZE_OF_PTR                       64   /* Node PTR variable text representation */
#define SIZE_OF_BOOL                      4    /* Bool variable text representation */
#define SIZE_OF_LONG                      16   /* Long variable text representation */
#define SIZE_OF_FLOAT                     128  /* Float variable text representation */
#define SIZE_OF_MAXDATA                   128  /* Maximal not string data length */

/* XML Validator Codes */
/* ------------------- */
#define VALID_XML                         1  /* XML valid */
#define ERROR_INVALID_XML                 0  /* XML invalid with XSD parser */
#define ERROR_XML_LOAD                   -1  /* the doc cannot be loaded or is not well-formed */
#define ERROR_XML_ROOT_ERROR             -2  /* the root element are not 'Widgets' */
#define ERROR_XSD_LOAD                   -3  /* the schema cannot be loaded or is not well-formed */
#define ERROR_XSD_CREATE_PARSER_CTX      -4  /* unable to create a parser context for the schema */
#define ERROR_XSD_SCHEMA                 -5  /* the schema itself is not valid */
#define ERROR_XSD_CREATE_VALIDATION_CTX  -6 /* unable to create a validation context for the schema */

//=========================================================================
// Functions
//=========================================================================

  ///////////////////////////////////////////////////////////
  //  Function: init_XML
  //
  //  Purpose:
  //    Initializes the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Initialize the parser variables.
  //
  ///////////////////////////////////////////////////////////
  void init_XML(void);

  ///////////////////////////////////////////////////////////
  //  Function: cleanup_XML
  //
  //  Purpose:
  //    cleanup the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    cleanup the XML parser variables.
  //
  ///////////////////////////////////////////////////////////
  void cleanup_XML(void);

  ///////////////////////////////////////////////////////////
  //  Function: reset_XML
  //
  //  Purpose:
  //    reset the full XML document
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    reset the full XML document.
  //
  ///////////////////////////////////////////////////////////
  void reset_XML(void);

  ///////////////////////////////////////////////////////////
  //  Function: reset_clone_XML
  //
  //  Purpose:
  //    reset the clone XML document
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    reset the clone XML document.
  //
  ///////////////////////////////////////////////////////////
  void reset_clone_XML(void);

  ///////////////////////////////////////////////////////////
  //  Function: expand_XML
  //
  //  Purpose:
  //    Expand XML conditional, iterator, external value elements
  //
  //  Parameters:
  //   pl_xml - *in* *charstring* - the input xul XML
  //   pl_parentWidgetID - *in* *charstring* - the parent widget ID
  //   pl_expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Return Value:
  //    Returns the result of validation and process.
  //    pl_expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Errors:
  //    - The XML invalid[<error code>] : {<xml string>}
  //    - NO root element in the XML : {<xml string>}
  //    - is_valid_XML: The XML doc cannot be loaded or is not well-formed: {<xml string>}!
  //    - is_valid_XML: The root element in the xml is '<act root>' instead of 'Widgets'!
  //    - is_valid_XML: The the schema cannot be loaded or is not well-formed[<schema filename>]!
  //    - is_valid_XML: The schema patch [<xsd:element name="Widgets" />] invalid!
  //    - is_valid_XML: Unable to create a parser context for the schema[<schema filename>]!
  //    - is_valid_XML: The schema itself is not valid[<schema filename>]!
  //    - is_valid_XML: unable to create a validation context for the schema[<schema filename>]!
  //    - is_valid_XML: Invalid XML {<xml string>} with schema [<schema filename>] - message: <error_message>!
  //    - get_data_parameters: unknown attribute [<attribute name>=<attribute value>]!
  //    - get_data_parameters: invalid call!");
  //
  //  Detailed Comments:
  //    Load XML string into rapidXML document. Validate it if
  //    v_EPTF_UIHandler_WidgetFunctions_XTDP_XSD_filepath XSD path are set.
  //    Append physical PTRs into entities "orig_node_ptr" Clone the XML document
  //    than expand all iterator, condition and externalvalue elements using
  //    f_EPTF_DataSource_getData_nonBlocking external function.
  //
  ///////////////////////////////////////////////////////////
  bool expand_XML(const CHARSTRING& pl_xml, const CHARSTRING& pl_parentWidgetID, CHARSTRING& pl_expandedXML);

  ///////////////////////////////////////////////////////////
  //  Function: remove_XML
  //
  //  Purpose:
  //    Remove XML elements from active document
  //
  //  Parameters:
  //   pl_removeWidgetID - *in* *charstring* - the removed widget ID
  //
  //  Return Value:
  //    Returns the result of remove process.
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Remove XML elements from active document.
  //
  ///////////////////////////////////////////////////////////
  bool remove_XML(const CHARSTRING& pl_removeWidgetID);

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_refreshedXMLforNode
  //
  //  Purpose:
  //    Expand XML for the given node
  //
  //  Parameters:
  //   pl_node_id - *in* <long> - actual node identifier
  //   pl_xulExpanded - *out* *charstring* - the expanded XML string
  //
  //  Return Value:
  //    Returns the result of the process.
  //    pl_xulExpanded - *out* *charstring* - the expanded XML string
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Expand XML for the given node.
  //
  ///////////////////////////////////////////////////////////
  bool refreshed_XML_forNode(long pl_node_id, CHARSTRING& pl_xulExpanded);

  ///////////////////////////////////////////////////////////
  //  Function: print_all_XML
  //
  //  Purpose:
  //    print all XML
  //
  //  Parameters:
  //   -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    print all XML.
  //
  ///////////////////////////////////////////////////////////
  void print_all_XML(void);

  ///////////////////////////////////////////////////////////
  //  Function: append_node_ptrs
  //
  //  Purpose:
  //    Append physical PTRs into entities
  //
  //  Parameters:
  //   pl_doc - *in* <xml_document<> * > - XML document pointer
  //   pl_data - *in* < process_data *> - process data structure index
  //   pl_node - *in | out* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    pl_node - *out* <xml_node<> * > - actual node pointer with physical PTR
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Append physical PTRs into entities "orig_node_ptr" before clone the XML document.
  //
  ///////////////////////////////////////////////////////////
  void append_node_ptrs(xml_document<> * pl_doc, process_data * pl_data, xml_node<> * pl_node);

  ///////////////////////////////////////////////////////////
  //  Function: expand_XML_elements
  //
  //  Purpose:
  //    Expand expandable XML elements
  //
  //  Parameters:
  //   pl_node - *in | out* <xml_node<> * > - actual node pointer
  //   pl_data - *in* < process_data *> - process data structure index
  //   pl_idx - *in* < unsigned long> - expanded index
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Expand expandable XML elements. Get expandables list, get data sources and
  //    expand it.
  //
  ///////////////////////////////////////////////////////////
  void expand_XML_elements( xml_node<> * pl_node, process_data * pl_data, unsigned long pl_idx);

  ///////////////////////////////////////////////////////////
  //  Function: get_nodes_to_expand
  //
  //  Purpose:
  //    Get expandable list vector
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //   pl_nodeList - *out* <vector< xml_node<>* > * > - expandable node list vector
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Get expandable list vector.
  //
  ///////////////////////////////////////////////////////////
  void get_nodes_to_expand( xml_node<>* pl_node, vector< xml_node<>* > * pl_nodeList );

  ///////////////////////////////////////////////////////////
  //  Function: expand_current_iterator
  //
  //  Purpose:
  //    Nonblocking Data Handler: Expand the current node iterator and external value.
  //
  //  Parameters:
  //   pl_source *in* *charstring * - the name of the dataSource 'feature'
  //   pl_source *in* *charstring* - the name of the PTC
  //         (dataSource+ptcName should be a unigue id of the data). Default: "" (i.e. PTC name is ignored)
  //   pl_element *in* *charstring* - the type of data
  //   pl_params *in* <EPTF_DataSource_Params> - additional parameters (default: {})
  //   pl_errorCode - *in* *INTEGER&* - error code in the response
  //   pl_remoteDataVarName - *in* *charstring* -  var name on the remote component.
  //           In case errorCode!=0: it contains the error message
  //   pl_ownerCompRef - *in* *COMPONENT&* - reference to the remote component
  //   pl_localDataVarId - *in* *INTEGER&* - var id on the local component
  //                 (contains the current value, not subscribed to remote!)
  //   pl_dataValue - *in* <EPTF_Var_DirectContent> - the value of the data
  //   pl_userData - *in* <EPTF__IntegerList&> -user specific data given at the request
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Nonblocking Data Handler: Expand the current node iterator and external value.
  //
  ///////////////////////////////////////////////////////////
  void expand_current_iterator ( const CHARSTRING& pl_source,
                            const CHARSTRING& pl_ptcName,
                            const CHARSTRING& pl_element,
                            const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params& pl_params,
                            const INTEGER& pl_errorCode,
                            const CHARSTRING& pl_remoteDataVarName,
                            const COMPONENT& pl_ownerCompRef,
                            const INTEGER& pl_localDataVarId,
                            const EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent& pl_dataValue,
                            const EPTF__CLL__Common__Definitions::EPTF__IntegerList& pl_userData);


   ///////////////////////////////////////////////////////////
   //  Function: set_itvalue_idx
   //
   //  Purpose:
   //    Set ITValues idx into orig_itvalues attribute
   //
   //  Parameters:
   //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
   //   pl_node - *in* <xml_node<> * > - node pointer
   //   pl_value_idx - *in* <char *> the saved IT value index in string
   //
   //  Return Value:
   //    -
   //
   //  Errors:
   //    -
   //
   //  Detailed Comments:
   //   Save ITValues idx into orig_itvalues attribute.
   //
   ///////////////////////////////////////////////////////////
   void set_itvalue_idx(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_node, char * pl_value_idx);

   ///////////////////////////////////////////////////////////
   //  Function: get_node_iterators_and_playlist
   //
   //  Purpose:
   //    Request node iterators an playlist elements
   //
   //  Parameters:
   //   pl_node - *in* <xml_node<> * > - node pointer
   //   pl_iteratorList - *out* vector< xml_node<>* > * - vector PTR for iterator nodes
   //   pl_playList - *out* vector< playListItem > * - vector for playlist elements
   //
   //  Return Value:
   //    Returns the PTRs of itarators an playlist elements.
   //
   //  Errors:
   //    -
   //
   //  Detailed Comments:
   //   Request node iterators an playlist elements.
   //
   ///////////////////////////////////////////////////////////
   void get_node_iterators_and_playlist( xml_node<>* pl_node, vector< xml_node<>* > * pl_iteratorList, vector< playListItem > * pl_playList);

  ///////////////////////////////////////////////////////////
  //  Function: process_iterator_node
  //
  //  Purpose:
  //    Process iterator node
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_source - *in* <xml_node<> * > - node pointer
  //   pl_it_values - *in* vector<std::pair<string, string> > * - vector PTR change from | to values
  //   pl_it_idx - *in* <int> the saved IT value index in v_ITValues buffer
  //   pl_xml_as_string *in* <string> processed xml string
  //
  //  Return Value:
  //    Returns the PTR of the result node.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Process iterator node.
  //
  ///////////////////////////////////////////////////////////
  xml_node<>*  process_iterator_node(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_source,map<string, string> * pl_it_values, int pl_it_idx, string pl_xml_as_string);


  ///////////////////////////////////////////////////////////
  //  Function: f__EPTF__UIHandler__expand__current__condition
  //
  //  Purpose:
  //    Nonblocking Data Handler: Expand the current node condition.
  //
  //  Parameters:
  //   pl_source *in* *charstring * - the name of the dataSource 'feature'
  //   pl_source *in* *charstring* - the name of the PTC
  //         (dataSource+ptcName should be a unigue id of the data). Default: "" (i.e. PTC name is ignored)
  //   pl_element *in* *charstring* - the type of data
  //   pl_params *in* <EPTF_DataSource_Params> - additional parameters (default: {})
  //   pl_errorCode - *in* *INTEGER&* - error code in the response
  //   pl_remoteDataVarName - *in* *charstring* -  var name on the remote component.
  //           In case errorCode!=0: it contains the error message
  //   pl_ownerCompRef - *in* *COMPONENT&* - reference to the remote component
  //   pl_localDataVarId - *in* *INTEGER&* - var id on the local component
  //                 (contains the current value, not subscribed to remote!)
  //   pl_dataValue - *in* <EPTF_Var_DirectContent> - the value of the data
  //   pl_userData - *in* <EPTF__IntegerList&> -user specific data given at the request
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Nonblocking Data Handler: Expand the current node condition.
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__expand__current__condition ( const CHARSTRING& pl_source,
                            const CHARSTRING& pl_ptcName,
                            const CHARSTRING& pl_element,
                            const EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params& pl_params,
                            const INTEGER& pl_errorCode,
                            const CHARSTRING& pl_remoteDataVarName,
                            const COMPONENT& pl_ownerCompRef,
                            const INTEGER& pl_localDataVarId,
                            const EPTF__CLL__Variable__Definitions::EPTF__Var__DirectContent& pl_dataValue,
                            const EPTF__CLL__Common__Definitions::EPTF__IntegerList& pl_userData);

  ///////////////////////////////////////////////////////////
  //  Function: process_condition_node
  //
  //  Purpose:
  //    Process condition node
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_source - *in* <xml_node<> * > - node pointer
  //   pl_condition - *in* <bool > - condition value
  //   vl_conditionid - *in* <const char * > - condition ID
  //
  //  Return Value:
  //    Returns the PTR of the result node.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Process condition node.
  //
  ///////////////////////////////////////////////////////////
  xml_node<>*  process_condition_node(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_source, bool pl_condition, char * vl_conditionid );

  //=========================================================================
  // SUBROUTINES
  //=========================================================================

  ///////////////////////////////////////////////////////////
  //  Function: is_valid_XML
  //
  //  Purpose:
  //    Validate the XML with the given XSD
  //
  //  Parameters:
  //   pl_xml_str - *in* <char * > - input XML string
  //   pl_schema_filename - *in* <char * > - input schema file name
  //
  //  Return Value:
  //    Returns the result of validation.
  //      - VALID_XML                         : XML valid
  //      - ERROR_INVALID_XML                 : XML invalid with XSD parser
  //      - ERROR_XML_LOAD                    : the doc cannot be loaded or is not well-formed
  //      - ERROR_XML_ROOT_ERROR              : the root element are not 'Widgets'
  //      - ERROR_XSD_LOAD                    : the schema cannot be loaded or is not well-formed
  //      - ERROR_XSD_CREATE_PARSER_CTX       : unable to create a parser context for the schema
  //      - ERROR_XSD_SCHEMA                  : the schema itself is not valid
  //      - ERROR_XSD_CREATE_VALIDATION_CTX   : unable to create a validation context for the schema
  //
  //  Errors:
  //    - is_valid_XML: The XML doc cannot be loaded or is not well-formed: {<xml string>}!
  //    - is_valid_XML: The root element in the xml is '<act root>' instead of 'Widgets'!
  //    - is_valid_XML: The the schema cannot be loaded or is not well-formed[<schema filename>]!
  //    - is_valid_XML: The schema patch [<xsd:element name="Widgets" />] invalid!
  //    - is_valid_XML: Unable to create a parser context for the schema[<schema filename>]!
  //    - is_valid_XML: The schema itself is not valid[<schema filename>]!
  //    - is_valid_XML: unable to create a validation context for the schema[<schema filename>]!
  //    - is_valid_XML: Invalid XML {<xml string>} with schema [<schema filename>] - message: <error_message>!
  //
  //  Detailed Comments:
  //    Validate the XML with the given XSD.
  //
  ///////////////////////////////////////////////////////////
  int is_valid_XML(char * pl_xml_str, const char * pl_schema_filename);

  ///////////////////////////////////////////////////////////
  //  Function: measure
  //
  //  Purpose:
  //    Measure string length
  //
  //  Parameters:
  //   pl_ptr - *in* <const char * > - input string
  //
  //  Return Value:
  //    Returns the string length.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Measure string length.
  //
  ///////////////////////////////////////////////////////////
  std::size_t measure(const char * pl_ptr);

  ///////////////////////////////////////////////////////////
  //  Function: compare
  //
  //  Purpose:
  //    Compare strings
  //
  //  Parameters:
  //   pl_p1 - *in* <const char * > - input string 1
  //   pl_size1 - *in* <size_t> - input string 1 length
  //   pl_p2 - *in* <const char * > - input string 2
  //   pl_size2 - *in* <size_t> - input string 2 length
  //
  //  Return Value:
  //    Returns the result of compare ==true if equal.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Compare two string.
  //
  ///////////////////////////////////////////////////////////
  bool compare(const char * pl_p1, std::size_t pl_size1, const char * pl_p2, std::size_t pl_size2);

  ///////////////////////////////////////////////////////////
  //  Function: compare name
  //
  //  Purpose:
  //    Compare strings
  //
  //  Parameters:
  //   pl_p1 - *in* <const char * > - input string 1
  //   pl_size1 - *in* <size_t> - input string 1 length
  //   pl_p2 - *in* <const char * > - input string 2
  //   pl_size2 - *in* <size_t> - input string 2 length
  //
  //  Return Value:
  //    Returns the result of compare ==true if equal.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Compare two string without namespace <xxx:yyy /> tag.
  //
  ///////////////////////////////////////////////////////////
  bool compare_name(const char * pl_p1, std::size_t pl_size1, const char * pl_p2, std::size_t pl_size2);

  ///////////////////////////////////////////////////////////
  //  Function: is_expandable
  //
  //  Purpose:
  //    The current node expandable?
  //
  //  Parameters:
  //   pl_name - *in* <const char * > - input entity name
  //   pl_name_size - *in* <size_t> - input name string length
  //
  //  Return Value:
  //    Returns ==true if the node name expandable.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   The current node expandable?
  //
  ///////////////////////////////////////////////////////////

  bool is_expandable(const char * pl_name, std::size_t pl_name_size);

  ///////////////////////////////////////////////////////////
  //  Function: is_iterator_like
  //
  //  Purpose:
  //    The current node iterator OR externalvalue?
  //
  //  Parameters:
  //   pl_name - *in* <const char * > - input entity name
  //   pl_name_size - *in* <size_t> - input name string length
  //
  //  Return Value:
  //    Returns ==true if the node name expandable.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   The current node iterator OR externalvalue?
  //
  ///////////////////////////////////////////////////////////
  bool is_iterator_like(const char * pl_name, std::size_t pl_name_size);

  ///////////////////////////////////////////////////////////
  //  Function: get_data_source
  //
  //  Purpose:
  //    Get the data source
  //
  //  Parameters:
  //   pl_expandable - *in* <xml_node<> * > - node pointer
  //   pl_element_out - *out* <char ** > - element pointer out
  //   pl_id_out - *out* <char ** > - id pointer out
  //   pl_ptcname_out - *out* <char ** > - ptc pointer out
  //   pl_source_out - *out* <char ** > - source pointer out
  //
  //  Return Value:
  //    Returns ==true if no any error.
  //
  //  Errors:
  //    - get_data_parameters: unknown attribute [<attribute name>=<attribute value>]!
  //    - get_data_parameters: invalid call!");
  //
  //  Detailed Comments:
  //   Get the data source.
  //
  ///////////////////////////////////////////////////////////
  bool get_data_source( xml_node<> *  pl_expandable,
          char * * pl_element_out, char * * pl_id_out, char * * pl_ptcname_out, char * * pl_source_out);

  ///////////////////////////////////////////////////////////
  //  Function: get_attribute
  //
  //  Purpose:
  //    Get the requested attribute
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_attributeName - *in* <char * > - attribute name
  //
  //  Return Value:
  //    Returns the PTR of the requested attribute or
  //            PTR of c_str_empty=="" if not found.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the requested node attribute.
  //
  ///////////////////////////////////////////////////////////
  char * get_attribute( xml_node<>*  pl_node, char * pl_attributeName);


  ///////////////////////////////////////////////////////////
  //  Function: copy_node
  //
  //  Purpose:
  //    Clone and copy xml nodes and attributes
  //
  //  Parameters:
  //   pl_doc - *in* <xml_document<> *> - the rapidxml document
  //   pl_node_src - *in* <xml_node<> * > - source node pointer
  //   pl_node_dest - *in* <xml_node<> * > - destination node pointer
  //   pl_reset_orig - *in* <bool > - reset the original name | attributes
  //
  //  Return Value:
  //    Returns the result of clone process.-
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Clone and copy xml nodes and attributes.
  //
  ///////////////////////////////////////////////////////////
  bool copy_node( xml_document<> * pl_doc, xml_node<>* pl_node_src, xml_node<>* pl_node_dest,  bool pl_reset_orig);


  ///////////////////////////////////////////////////////////
  //  Function: get_metaiterator_itvalues
  //
  //  Purpose:
  //    Get the metaiterator itvalues
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - node pointer
  //
  //  Return Value:
  //    Returns the PTR of the requested itvalues or
  //            PTR of c_str_empty=="" if not found.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the metaiterator itvalues.
  //
  ///////////////////////////////////////////////////////////
  const char * get_metaiterator_itvalues( xml_node<>*  pl_node );

  ///////////////////////////////////////////////////////////
  //  Function: get_data_source_parameters
  //
  //  Purpose:
  //    Get the data source parameters
  //
  //  Parameters:
  //   pl_expandable - *in* <xml_node<> * > - node pointer
  //
  //  Return Value:
  //    Returns the requested EPTF_DataSource_Params.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Get the data source parameters.
  //
  ///////////////////////////////////////////////////////////
  EPTF__CLL__DataSource__Definitions::EPTF__DataSource__Params get_data_source_parameters( xml_node<> *  pl_expandable);

  ///////////////////////////////////////////////////////////
  //  Function: set_node_temp_attributes
  //
  //  Purpose:
  //    Set new node attributes.
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_node_id - *in* <long> - node ID
  //   pl_remote_name - *in* <char *> - remote data var name
  //   pl_local_id - *in* <long> - local data var ID
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Set new node attributes (orig_node_name;
  //   DEBUG_remote_data_varname; DEBUG_local_data_varid),
  //   set name to 'metaiterator'.
  //
  ///////////////////////////////////////////////////////////
  void set_node_temp_attributes(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_node, long pl_node_id, char * pl_remote_name, long pl_local_id);

  ///////////////////////////////////////////////////////////
  //  Function: set_node_offset_id
  //
  //  Purpose:
  //    Set new node attributes.
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - node pointer
  //   pl_offset_id - *in* <long> - offset ID
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //   Set new node attributes (orig_node_name;
  //   DEBUG_remote_data_varname; DEBUG_local_data_varid),
  //   set name to 'metaiterator'.
  //
  ///////////////////////////////////////////////////////////
  void set_node_offset_id(xml_document<> * pl_clone_doc_ptr, xml_node<> * pl_node, long pl_offset_id);

  ///////////////////////////////////////////////////////////
  //  Function: out_process_for_metaiterators
  //
  //  Purpose:
  //    Output process for itterator elements
  //
  //  Parameters:
  //   pl_clone_doc_ptr - *in* <xml_document<> * > - document pointer
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //   pl_parentiterator - *in* <xml_node<> * > - parent iterator node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Remove condition metaiterators, the iterator and
  //    externalvalue tags rename for iterator
  //    Remove debug attributes (orig_node_name;
  //    DEBUG_remote_data_varname; DEBUG_local_data_varid).
  //
  ///////////////////////////////////////////////////////////
  void out_process_for_metaiterators(xml_document<> * pl_clone_doc_ptr, xml_node<>* pl_node, xml_node<>* pl_parentiterator);

  ///////////////////////////////////////////////////////////
  //  Function: remove_nodes_temp_attributes
  //
  //  Purpose:
  //    Remove debug attributes
  //
  //  Parameters:
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Remove debug attributes (orig_node_name;
  //    DEBUG_remote_data_varname; DEBUG_local_data_varid).
  //
  ///////////////////////////////////////////////////////////
  void remove_nodes_temp_attributes( xml_node<>* pl_node);

  ///////////////////////////////////////////////////////////
  //  Function: delete_node_variabledata
  //
  //  Purpose:
  //    Delete child node variable data and map
  //
  //  Parameters:
  //   pl_node_id - *in* <const char *> - pl_widget_id id
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Delete child node map.
  //
  ///////////////////////////////////////////////////////////
  void delete_node_variabledata(const char * pl_widget_id);

  ///////////////////////////////////////////////////////////
  //  Function: clear_namespace_elements
  //
  //  Purpose:
  //    Clear namespace elements
  //
  //  Parameters:
  //   pl_xmlstr - *in* <char * > - xml string ptr
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Clear namespace elements: <ns: | </ns:
  //
  ///////////////////////////////////////////////////////////
  void clear_namespace_elements( char * pl_xmlstr);


  ///////////////////////////////////////////////////////////
  //  Function: no_action_in_guifunctions
  //
  //  Purpose:
  //    NO action in GUIFunctions
  //
  //  Parameters:
  //   pl_txt - *in* <char * > - string ptr
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    NO action in GUIFunctions.
  //
  ///////////////////////////////////////////////////////////
  void no_action_in_guifunctions(const char * pl_txt);

  ///////////////////////////////////////////////////////////
  //  Function: action_print_node
  //
  //  Purpose:
  //    Action print this node
  //
  //  Parameters:
  //   pl_txt - *in* <char * > - string ptr
  //   pl_node - *in* <xml_node<> * > - actual node pointer
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    ction print this node.
  //
  ///////////////////////////////////////////////////////////
  void action_print_node( const char * pl_txt, xml_node<>* pl_node);

  ///////////////////////////////////////////////////////////
  //  Function: escape_double_quotes
  //
  //  Purpose:
  //    Escape " - double quote characters
  //
  //  Parameters:
  //   pl_txt - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    Returns the escaped string.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Escape " - double quote characters.
  //
  ///////////////////////////////////////////////////////////
  string escape_double_quotes(const char * pl_txt);

  ///////////////////////////////////////////////////////////
  //  Function: parse_it_values_map
  //
  //  Purpose:
  //    Parse and build the it values map
  //
  //  Parameters:
  //   pl_txt - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    Returns the it values map.
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Parse and build the it values map.
  //
  ///////////////////////////////////////////////////////////
  map<string, string> parse_it_values_map(const char * pl_txt);


  ///////////////////////////////////////////////////////////
  //  Function: strdup_internal
  //
  //  Purpose:
  //    duplicate a string with malloc
  //
  //  Parameters:
  //    pl_instr - *in* <const char * > - string ptr
  //
  //  Return Value:
  //    the new memory block address
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Duplicate a string with malloc.
  //
  ///////////////////////////////////////////////////////////
   char * strdup_internal(const char * pl_instr);


   ///////////////////////////////////////////////////////////
   //  Function: init_xml_doc
   //
   //  Purpose:
   //    Load playlist XML document
   //
   //  Parameters:
   //    -
   //
   //  Return Value:
   //    - the result
   //
   //  Errors:
   //    -
   //
   //  Detailed Comments:
   //    Load playlist XML document.
   //
   ///////////////////////////////////////////////////////////
    bool init_xml_doc(xml_document<> * vl_docptr);

    ///////////////////////////////////////////////////////////
    //  Function: clear_variable_data
    //
    //  Purpose:
    //    Clear variable data tables
    //
    //  Parameters:
    //    -
    //
    //  Return Value:
    //    - the result
    //
    //  Errors:
    //    -
    //
    //  Detailed Comments:
    //    Clear variable data tables.
    //
    ///////////////////////////////////////////////////////////
    void clear_variable_data(void);

//=========================================================================
// TIME DEBUG Functions
//=========================================================================
  void reset_timer(void);
  void start_timer(clock_t * timer_start_ptr, struct timeval * pl_tv);
  void stop_timer(clock_t * timer_start_ptr, struct timeval * pl_tv, double * time_diff_summ_ptr);
  void print_variableData(const char * pl_ident);

//=========================================================================
// TESTS
//=========================================================================
  std::string test_init_XML();
  std::string test_cleanup_XML();
  std::string test_reset_XML();
  std::string test_expand_XML();
  std::string test_refreshed_XML_forNode();
  std::string test_append_node_ptrs();
  std::string test_expand_XML_elements();
  std::string test_get_nodes_to_expand();
  std::string test_expand_current_iterator();
  std::string test_process_iterator_node();
  std::string test_expand_current_condition ();
  std::string test_process_condition_node();

  std::string test_is_valid_XML();
  std::string test_measure();
  std::string test_compare();
  std::string test_is_expandable();
  std::string test_get_data_source();
  std::string test_get_attribute();
  std::string test_get_metaiterator_itvalues();
  std::string test_get_data_source_parameters();
  std::string test_set_node_temp_attributes();
  std::string test_remove_nodes_temp_attributes();
  std::string test_compare_name();
  std::string test_clear_namespace_elements();
  std::string test_escape_double_quotes();

}// namespace
