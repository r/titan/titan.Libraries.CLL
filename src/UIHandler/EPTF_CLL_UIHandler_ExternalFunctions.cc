///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

#include "EPTF_CLL_UIHandler_WidgetFunctions.hh"
#include "EPTF_CLL_UIHandler_GUIFunctions.hh"

namespace EPTF__CLL__UIHandler__WidgetFunctions {

  CHARSTRING f__EPTF__UIHandler__simulation__encode__directContentDB(const EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__simulation__directContentDB& pl__DB) {
    Text_Buf tb;
    pl__DB.encode_text(tb);
    return CHARSTRING(tb.get_len(),tb.get_data());
  }

  void f__EPTF__UIHandler__simulation__decode__directContentDB(const CHARSTRING& pl__dbStr,
      EPTF__CLL__UIHandler__Definitions::EPTF__UIHandler__simulation__directContentDB& pl__DB) {
    Text_Buf tb;
    tb.push_raw(pl__dbStr.lengthof(), pl__dbStr);
    pl__DB.decode_text(tb);
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_init_XML
  //
  //  Purpose:
  //    Initializes the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    Initialize the parser variables.
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__init__XML(void) {
    init_XML();
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_cleanup_XML
  //
  //  Purpose:
  //    cleanup the XML parser
  //
  //  Parameters:
  //    -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //    -
  //
  //  Detailed Comments:
  //    cleanup the XML parser variables.
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__cleanup__XML(void) {
    cleanup_XML();
    return;
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_expandXML
  //
  //  Purpose:
  //    Expand XML conditional, iterator, external value elements
  //
  //  Parameters:
  //   pl__expandedXML - *out* *charstring* - the expanded XML string
  //   pl__xul - *in* *charstring* - the input xul XML
  //   pl__parentWidgetId - *in* *charstring* - the parent widget ID
  //
  //  Return Value:
  //    Returns the result of validation and process.
  //    pl__expandedXML - *out* *charstring* - the expanded XML string
  //
  //  Errors:
  //    - The XML invalid[<error code>] : {<xml string>}
  //    - NO root element in the XML : {<xml string>}
  //    - is_valid_XML: The XML doc cannot be loaded or is not well-formed: {<xml string>}!
  //    - is_valid_XML: The root element in the xml is '<act root>' instead of 'Widgets'!
  //    - is_valid_XML: The the schema cannot be loaded or is not well-formed[<schema filename>]!
  //    - is_valid_XML: The schema patch [<xsd:element name="Widgets" />] invalid!
  //    - is_valid_XML: Unable to create a parser context for the schema[<schema filename>]!
  //    - is_valid_XML: The schema itself is not valid[<schema filename>]!
  //    - is_valid_XML: unable to create a validation context for the schema[<schema filename>]!
  //    - is_valid_XML: Invalid XML {<xml string>} with schema [<schema filename>] - message: <error_message>!
  //    - get_data_parameters: unknown attribute [<attribute name>=<attribute value>]!
  //    - get_data_parameters: invalid call!");
  //
  //  Detailed Comments:
  //    Load XML string into rapidXML document. Validate it if
  //    v_EPTF_UIHandler_WidgetFunctions_XTDP_XSD_filepath XSD path are set.
  //    Append physical PTRs into entities "orig_node_ptr" Clone the XML document
  //    than expand all iterator, condition and externalvalue elements using
  //    f_EPTF_DataSource_getData_nonBlocking external function.
  //
  ///////////////////////////////////////////////////////////
  BOOLEAN f__EPTF__UIHandler__expandXML(CHARSTRING& pl__expandedXML, const CHARSTRING& pl__xul, const CHARSTRING& pl__parentWidgetId = "" )
  {
    bool vl_ret = expand_XML(pl__xul, pl__parentWidgetId, pl__expandedXML);
    return BOOLEAN(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_removeXML
  //
  //  Purpose:
  //    Remove XML elements from active document
  //
  //  Parameters:
  //   pl_removeWidgetID - *in* *charstring* - the removed widget ID
  //
  //  Return Value:
  //    Returns the result of remove process.
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Remove XML elements from active document.
  //
  ///////////////////////////////////////////////////////////
  BOOLEAN f__EPTF__UIHandler__removeXML(const CHARSTRING& pl_removeWidgetID)
  {
    bool vl_ret = remove_XML(pl_removeWidgetID);
    return BOOLEAN(vl_ret);
  }

  CHARSTRING f__EPTF__UIHandler__GUIFunctions__Tests(){
    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "f__EPTF__UIHandler__GUIFunctions__Tests starts" );

    std::string s("");

    s.append(test_init_XML());                         //pass
    s.append(test_reset_XML());                        //pass
    s.append(test_expand_XML());                       //pass
//    s.append(test_refreshed_XML_forNode());            //pass
    s.append(test_append_node_ptrs());                 //pass
    s.append(test_expand_XML_elements());              //pass
    s.append(test_get_nodes_to_expand());              //pass
//    s.append(test_expand_current_iterator());          //pass
//    s.append(test_process_iterator_node());            //pass
//    s.append(test_expand_current_condition ());        //pass
//    s.append(test_process_condition_node());           //pass
    // SUBROUTINES
    s.append(test_is_valid_XML());                     //pass
    s.append(test_measure());                          //pass
    s.append(test_compare());                          //pass
    s.append(test_is_expandable());                    //pass
    s.append(test_get_data_source());                  //pass
    s.append(test_get_attribute());                    //pass
    s.append(test_get_metaiterator_itvalues());        //pass
    s.append(test_get_data_source_parameters());       //pass
    s.append(test_set_node_temp_attributes());         //pass
    s.append(test_remove_nodes_temp_attributes());     //pass
    s.append(test_compare_name());                     //pass
    s.append(test_clear_namespace_elements());         //pass
    s.append(test_escape_double_quotes());             //pass
    // Cleanup
    s.append(test_cleanup_XML());                      //pass

    TTCN_Logger::log(TTCN_Logger::USER_UNQUALIFIED, "f__EPTF__UIHandler__GUIFunctions__Tests ends" );
    return(CHARSTRING(s.c_str()));
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_refreshedXMLforNode
  //
  //  Purpose:
  //    Expand XML for the given node
  //
  //  Parameters:
  //   pl__nodePointer - *in* <int> - actual node pointer
  //   pl__xulExpanded - *out* *CHARSTRING* - the expanded XML string
  //
  //  Return Value:
  //    pl_xulExpanded - *out* *CHARSTRING* - the expanded XML string
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    Expand XML for the given node.
  //
  ///////////////////////////////////////////////////////////
  BOOLEAN f__EPTF__UIHandler__refreshedXMLforNode( const INTEGER& pl__nodePointer, CHARSTRING& pl__xulExpanded) {
    bool vl_ret = refreshed_XML_forNode((long) pl__nodePointer.get_long_long_val(), pl__xulExpanded);
    return BOOLEAN(vl_ret);
  }

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_UIHandler_printAllXML
  //
  //  Purpose:
  //    print all XML
  //
  //  Parameters:
  //   -
  //
  //  Return Value:
  //    -
  //
  //  Errors:
  //
  //  Detailed Comments:
  //    print all XML.
  //
  ///////////////////////////////////////////////////////////
  void f__EPTF__UIHandler__printAllXML(void) {
    print_all_XML();
    return;
  }


}// namespace
