///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_HostAdmin_Definitions
//
//  Purpose:
//   This module contains data type definitions for EPTF HostAdmin.
//
//  Module Parameters:
//    tsp_EPTF_HostAdmin_loggingComponentMask - *charstring* - component-type mask for logging, default value: "EPTF_HostAdmin"
//    tsp_EPTF_HostAdmin_pidList - <EPTF_HostAdmin_pidsOnHostList> - list of PIDs of processes to monitor (cpu and mem usage) per host
//    tsp_EPTF_HostAdmin_getPidListCmd - <EPTF_HostAdmin_getPidsCmdsOnHostList> - list of commands for getting PIDs of processes to monitor, eg {"ps -u egbotat | grep nedit | awk '{print $1}'"} per host
//    tsp_EPTF_HostAdmin_sortBy - <EPTF_HostAdmin_PDUSortBy> - sort option for process statistics if HostAdminServer is used
// 
//  Current Owner:
//    Gabor Tatarka (egbotat)
//    Bence molnar (ebenmol)
//
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//    <EPTF_CLL_Common_Definitions>
//    <EPTF_CLL_DataSource_Definitions>
//    <EPTF_CLL_HostAdmin_BaseDefinitions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_RBT_Definitions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_StatMeasure_Definitions>
//
//  Last Review Date:
//    2008-02-14
//
//  Detailed Comments:
// 
///////////////////////////////////////////////////////////
module EPTF_CLL_HostAdmin_Definitions
{

//=========================================================================
// Imports
//=======================================================================
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Common_Definitions all;
import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_HostAdmin_BaseDefinitions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_RBT_Definitions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_StatMeasure_Definitions all;

modulepar charstring tsp_EPTF_HostAdmin_loggingComponentMask := "EPTF_HostAdmin";
modulepar EPTF_HostAdmin_pidsOnHostList tsp_EPTF_HostAdmin_pidList := {};
modulepar EPTF_HostAdmin_getPidsCmdsOnHostList tsp_EPTF_HostAdmin_getProcessPidListCmds := {};//depricated - not used
modulepar EPTF_HostAdmin_PDUSortBy tsp_EPTF_HostAdmin_sortBy := CpuLoad;
//=========================================================================
// Component Types
//=========================================================================

/////////////////////////////////////////////////////////////////////////
//  Component: EPTF_HostAdminServerBase_CT
//
//  Purpose:
//    Component type for refering the HostAdminServer
/////////////////////////////////////////////////////////////////////////
type component EPTF_HostAdminServerBase_CT { }; // used to refer the server from this module, the server extends this

/////////////////////////////////////////////////////////////////////////
//  Component: EPTF_HostAdmin_CT
//
//  Purpose:
//    Component type for EPTF_HostAdmin
//
//  Detailed Comments:
//    Extends EPTF_HostAdmin_Base_CT, EPTF_Var_CT, EPTF_Logging_CT, EPTF_DataSourceClient_CT, EPTF_RBT_CT, EPTF_StatMeasure_CT
/////////////////////////////////////////////////////////////////////////
type component EPTF_HostAdmin_CT extends EPTF_HostAdmin_Base_CT, EPTF_Var_CT, EPTF_Logging_CT, EPTF_DataSourceClient_CT, EPTF_RBT_CT, EPTF_StatMeasure_CT
{
  private var integer v_EPTF_HostAdmin_hostName_k := -1;
  private var integer v_EPTF_HostAdmin_ipv4Addr_k := -1;
  private var integer v_EPTF_HostAdmin_ipv6Addr_k := -1;
  private var integer v_EPTF_HostAdmin_testerHostLoad_k := -1;
  private var integer v_EPTF_HostAdmin_minHostLoad_statKey := -1;
  private var integer v_EPTF_HostAdmin_minHostLoad_k := -1;
  private var integer v_EPTF_HostAdmin_maxHostLoad_statKey := -1;
  private var integer v_EPTF_HostAdmin_maxHostLoad_k := -1;
  private var integer v_EPTF_HostAdmin_avgHostLoad_statKey := -1;
  private var integer v_EPTF_HostAdmin_avgHostLoad_k := -1;
  private var integer v_EPTF_HostAdmin_numCPUs_k := -1;
  private var EPTF_IntegerList v_EPTF_HostAdmin_CPULoads_k := {};
  private var integer v_EPTF_HostAdmin_physicalMemory_k := -1;
  private var integer v_EPTF_HostAdmin_freeMemory_k := -1;

  private var EPTF_HostAdmin_ProcessStatDB v_EPTF_HostAdmin_processStatDB := c_init_EPTF_HostAdmin_ProcessStatDB;
  private var charstring v_EPTF_HostAdmin_hostName := "";
  private var charstring v_EPTF_HostAdmin_ipv4Addr := "";
  private var charstring v_EPTF_HostAdmin_ipv6Addr := "";

  private var boolean v_EPTF_HostAdmin_initialized := false;
  // logging
  private var integer v_HostAdmin_loggingMaskId := c_EPTF_Logging_invalidMaskId;

  private port EPTF_HostAdmin_Sync_PT Sync_PCO;
  private var EPTF_HostAdminServerBase_CT v_EPTF_HostAdmin_serverRef := null
  private var EPTF_HostAdmin_PDUSortBy v_EPTF_HostAdmin_sortBy := tsp_EPTF_HostAdmin_sortBy;
  private var EPTF_Base_ComponentInfoList v_EPTF_HostAdmin_compInfoList := {};
  private var EPTF_IntegerList v_EPTF_HostAdmin_processNameOrder := {};
}

//=========================================================================
// Types
//=========================================================================
type record EPTF_HostAdmin_ProcessStatDB {
  integer nofProcesses,
  integer nofProcesses_k,
  EPTF_HostAdmin_ProcessStatList processes
}

type record EPTF_HostAdmin_ProcessStat {
  integer idx,
  integer pid_k,
  integer name_k,
  integer selfName_k,
  integer cpu_k,
  integer mem_k
}

type record of EPTF_HostAdmin_ProcessStat EPTF_HostAdmin_ProcessStatList;

type record EPTF_HostAdmin_pidsOnHost {
  charstring hostName,
  EPTF_IntegerList pids
}
type record of EPTF_HostAdmin_pidsOnHost EPTF_HostAdmin_pidsOnHostList;

type record EPTF_HostAdmin_getPidsCmdsOnHost {
  charstring hostName,
  EPTF_CharstringList cmds
}
type record of EPTF_HostAdmin_getPidsCmdsOnHost EPTF_HostAdmin_getPidsCmdsOnHostList;

//=========================================================================
// Server <-> Client PDUs
//=========================================================================
type record EPTF_HostAdmin_PDUProcessStat {
  charstring  selfName,
  integer     pid,
  float       cpuLoad,
  integer     memUsage
}

type record of EPTF_HostAdmin_PDUProcessStat EPTF_HostAdmin_PDUProcessStatList;

type record EPTF_HostAdmin_PDUHostStat {
  charstring                        hostName,
  charstring                        ipv4Addr,
  charstring                        ipv6Addr,
  integer                           physicalMem,
  integer                           freeMem,
  float                             cpuLoad,
  float                             cpuMin,
  float                             cpuMax,
  float                             cpuAvg,
  EPTF_FloatList                    perCpuLoad,
  EPTF_HostAdmin_PDUProcessStatList processes,
  EPTF_IntegerList                  processOrder
}

type record EPTF_HostAdmin_PDUBye { }

///////////////////////////////////////////////////////////
// Type: EPTF_HostAdmin_PDUSortBy
// 
// Purpose:
//  Enumerated type for sort options of process statistics if used with HostAdminServer
//
// Elements:
//  { SelfName (0), CpuLoad, MemUsage, DontSort, max_SortBy }
///////////////////////////////////////////////////////////
// sent by server to client, client does the sorting before sending the process stats
type enumerated EPTF_HostAdmin_PDUSortBy {
  SelfName (0),
  CpuLoad,
  MemUsage,
  DontSort,

  max_SortBy
}

type record of EPTF_HostAdmin_PDUSortBy EPTF_HostAdmin_PDUSortByList;

const EPTF_HostAdmin_PDUSortByList c_EPTF_HostAdmin_PDUSortByList := {
  SelfName,
  CpuLoad,
  MemUsage,
  DontSort
}

const EPTF_CharstringList c_EPTF_HostAdmin_PDUSortByStr := {
  "EPTF SelfName",
  "CPU Load",
  "Memory Usage",
  "Unsorted"
}

type union EPTF_HostAdmin_PDU
{
  EPTF_HostAdmin_PDUHostStat hostStat,
  EPTF_HostAdmin_PDUBye bye,
  EPTF_HostAdmin_PDUSortBy sortBy
}

type port EPTF_HostAdmin_Sync_PT message {
  inout EPTF_HostAdmin_PDU
} with { extension "internal" }

//=========================================================================
// Constants
//=========================================================================
const EPTF_HostAdmin_ProcessStatDB c_init_EPTF_HostAdmin_ProcessStatDB :=
{
  nofProcesses := 0,
  nofProcesses_k := -1,
  processes := {}
}

/////////////////////////////////////////////////////////////////////////
// Constants: c_EPTF_HostAdmin_testerHostLoad_name
//
// Purpose:
//    EPTF variable name constants
//
// Other values:
//    - c_EPTF_HostAdmin_hostName_name
//    - c_EPTF_HostAdmin_ipv4Addr_name
//    - c_EPTF_HostAdmin_ipv6Addr_name
//    - c_EPTF_HostAdmin_testerHostLoad_name
//    - c_EPTF_HostAdmin_minHostLoad_name
//    - c_EPTF_HostAdmin_maxHostLoad_name
//    - c_EPTF_HostAdmin_avgHostLoad_name
//    - c_EPTF_HostAdmin_resetMinMaxAvgCPULoad_name
//    - c_EPTF_HostAdmin_numCPUs_name
//    - c_EPTF_HostAdmin_CPULoads_name
//    - c_EPTF_HostAdmin_physicalMemory_name
//    - c_EPTF_HostAdmin_freeMemory_name
//    - c_EPTF_HostAdmin_nofProcessStats
//    - c_EPTF_HostAdmin_processPrefix
//    - c_EPTF_HostAdmin_processPID
//    - c_EPTF_HostAdmin_processCpuLoad
//    - c_EPTF_HostAdmin_processMemUsage
//    - c_EPTF_HostAdmin_processName
//    - c_EPTF_HostAdmin_processSelfName
/////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_HostAdmin_hostName_name := "hostName";
const charstring c_EPTF_HostAdmin_ipv4Addr_name := "ipv4Addr";
const charstring c_EPTF_HostAdmin_ipv6Addr_name := "ipv6Addr";
const charstring c_EPTF_HostAdmin_testerHostLoad_name := "testerHostLoad";
const charstring c_EPTF_HostAdmin_minHostLoad_name := "minHostLoad";
const charstring c_EPTF_HostAdmin_maxHostLoad_name := "maxHostLoad";
const charstring c_EPTF_HostAdmin_avgHostLoad_name := "avgHostLoad";
const charstring c_EPTF_HostAdmin_resetMinMaxAvgCPULoad_name := "resetMinMaxAvgCPULoad";
const charstring c_EPTF_HostAdmin_numCPUs_name := "numCPUs";
const charstring c_EPTF_HostAdmin_CPULoads_name := "CPULoads";
const charstring c_EPTF_HostAdmin_physicalMemory_name := "physicalMemory";
const charstring c_EPTF_HostAdmin_freeMemory_name := "freeMemory";
const charstring c_EPTF_HostAdmin_nofProcessStats := "nofProcessStats";
const charstring c_EPTF_HostAdmin_processPrefix := "process";
const charstring c_EPTF_HostAdmin_processPID := "PID";
const charstring c_EPTF_HostAdmin_processCpuLoad := "CPU";
const charstring c_EPTF_HostAdmin_processMemUsage := "MEM";
const charstring c_EPTF_HostAdmin_processName := "Name";
const charstring c_EPTF_HostAdmin_processSelfName := "SelfName";

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_HostAdmin_loggingEventClasses
// 
//  Purpose:
//    list of logging event class names used on the HostAdmin
// 
//  Detailed Comments:
//    <EPTF_Logging_EventClassPrefixList> { "Warning", "Debug" }
///////////////////////////////////////////////////////////
const EPTF_Logging_EventClassPrefixList c_EPTF_HostAdmin_loggingEventClasses := { "Warning", "Debug" };

///////////////////////////////////////////////////////////
//  Constant: c_EPTF_HostAdmin_loggingClassIdx_Warning
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *0*
///////////////////////////////////////////////////////////
const integer c_EPTF_HostAdmin_loggingClassIdx_Warning := 0;
///////////////////////////////////////////////////////////
//  Constant: c_EPTF_HostAdmin_loggingClassIdx_Debug
// 
//  Purpose:
//    logging class index for Error
// 
//  Detailed Comments:
//    *1*
///////////////////////////////////////////////////////////
const integer c_EPTF_HostAdmin_loggingClassIdx_Debug := 1;

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_DataSource_sourceId
//
// Purpose:
//   This constant defines the identifier which should be written into the datasource 
//   requests in order to tell the DataSource server that the requests belong to HostAdmin,
//   so send it to the HostAdmin in order to process.
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_DataSource_sourceId := "HostAdmin";

///////////////////////////////////////////////////////////////////////////////
// Constant: c_EPTF_HostAdmin_dataElement_Help
//
// Purpose:
//   This data element returns the help information about
//   all dataElements supported.
//   ValueType: charstring
//
// Parameters:
//   - <c_EPTF_DataSource_paramNameHelpFormat> - the format of the output
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_EPTF_HostAdmin_dataElement_help := c_EPTF_DataSource_dataElement_Help

const EPTF_DataSource_Help_DataElement c_EPTF_HostAdmin_Help_dataElement_help := c_EPTF_DataSource_Help_dataElement_genericHelp;

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_iteratorVar_prefix
//
// Purpose:
//  The names of the variables, which are made to represent the iterators, 
//	are made with the use of this constant.
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_iteratorVar_prefix := "HostAdmin_iterator_";


///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_iteratorProcesses
//
// Purpose:
//   This iterator lists the processes on a given host.
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_iteratorProcesses := "Processes"

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_iteratorProcesses := {
  name := c_HostAdmin_iteratorProcesses,
  valueType := charstringlistType,
  typeDescriptor := {
    isListOf := true,
    typeName := "Process"
  },
  description := "This iterator lists the processes on a given host."
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_iteratorProcesses_varName
//
// Purpose:
//	The Processes iterator's variable name.
//
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_iteratorProcesses_varName := "Processes"

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_iteratorCPUs
//
// Purpose:
//   This iterator lists CPUs on a given host.
//
// Parameters:
//   -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_iteratorCPUs := "CPUs"

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_iteratorCPUs := {
  name := c_HostAdmin_iteratorCPUs,
  valueType := charstringlistType,
  typeDescriptor := {
    isListOf := true,
    typeName := "CPU"
  },
  description := " This iterator lists the CPUs on a given host."
}


///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_iteratorCPUs_varName
//
// Purpose:
//	The CPUs iterator's variable name.
//
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_iteratorCPUs_varName := "CPUs"


///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementCPULoad
//
// Purpose:
//   This data element returns the CPULoad of a CPU, type float
//
// Parameters:
//	c_HostAdmin_paramNameCPU_ID - *integer* - cpu id (0..c_HostAdmin_dataElementHostNumCPUs)
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementCPULoad := "CPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementCPULoad := {
  name := c_HostAdmin_dataElementCPULoad,
  valueType := floatType,
  description := "This data element returns the CPULoad of a CPU",
  parameters := {
    {
      name := c_HostAdmin_paramNameCPU_ID,
      typeDescriptor := {
        reference := {
          isIndexInListOf := true,
          typeName := c_HostAdmin_Help_iteratorCPUs.typeDescriptor.typeName
        }
      },
      description := "Cpu id. Valid values: 0.." &c_HostAdmin_dataElementHostNumCPUs,
      exampleValue := c_HostAdmin_dataElementHostNumCPUs
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementHostName
//
// Purpose:
//   This data element returns the name of the host, type charsting
//
// Parameters:
//  -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementHostName := "HostName";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementHostName := {
  name := c_HostAdmin_dataElementHostName,
  valueType := charstringType,
  description := "This data element returns the name of the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementIPv4Addr
//
// Purpose:
//   This data element returns the IPv4 address of the host, type charsting
//
// Parameters:
//  -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementIPv4Addr := "IPv4Addr";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementIPv4Addr := {
  name := c_HostAdmin_dataElementIPv4Addr,
  valueType := charstringType,
  description := "This data element returns the IPv4 address of the host, type charsting"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementIPv6Addr
//
// Purpose:
//   This data element returns the IPv6 address of the host, type charsting
//
// Parameters:
//  -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementIPv6Addr := "IPv6Addr";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementIPv6Addr := {
  name := c_HostAdmin_dataElementIPv6Addr,
  valueType := charstringType,
  description := "This data element returns the IPv6 address of the host, type charsting"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementHostCPULoad
//
// Purpose:
//   This data element returns the CPU load of the host, type float
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementHostCPULoad := "HostCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementHostCPULoad := {
  name := c_HostAdmin_dataElementHostCPULoad,
  valueType := floatType,
  description := "This data element returns the CPU load of the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementMinCPULoad
//
// Purpose:
//   This data element returns the minimal CPU load of the host, type float
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementMinCPULoad := "MinCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementMinCPULoad := {
  name := c_HostAdmin_dataElementMinCPULoad,
  valueType := floatType,
  description := "This data element returns the minimal CPU load of the host, type float"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementMaxCPULoad
//
// Purpose:
//   This data element returns the maximal CPU load of the host, type float
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementMaxCPULoad := "MaxCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementMaxCPULoad := {
  name := c_HostAdmin_dataElementMaxCPULoad,
  valueType := floatType,
  description := "This data element returns the maximal CPU load of the host, type float"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementAvgCPULoad
//
// Purpose:
//   This data element returns the average CPU load of the host, type float
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementAvgCPULoad := "AvgCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementAvgCPULoad := {
  name := c_HostAdmin_dataElementAvgCPULoad,
  valueType := floatType,
  description := "This data element returns the average CPU load of the host, type float"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementResetMinMaxAvgCPULoad
//
// Purpose:
//   This data element defines a button to reset the min/max/average CPU load of the host, type integer
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementResetMinMaxAvgCPULoad := "ResetMinMaxAvgCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementResetMinMaxAvgCPULoad := {
  name := c_HostAdmin_dataElementResetMinMaxAvgCPULoad,
  valueType := intType,
  description := " This data element defines a button to reset the min/max/average CPU load of the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementHostNumCPUs
//
// Purpose:
//   This data element returns the number of CPUs on the host, type integer
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementHostNumCPUs := "HostNumCPUs";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementHostNumCPUs := {
  name := c_HostAdmin_dataElementHostNumCPUs,
  valueType := intType,
  description := "This data element returns the number of CPUs on the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementFreeMemory
//
// Purpose:
//   This data element returns the free memory on the host, type integer
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementHostFreeMemory := "HostFreeMemory";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementHostFreeMemory := {
  name := c_HostAdmin_dataElementHostFreeMemory,
  valueType := intType,
  description := "This data element returns the free memory on the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementPhysicalMemory
//
// Purpose:
//   This data element returns the physical memory available on the host, type integer
//
// Parameters:
//	 -
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementHostPhysicalMemory := "HostPhysicalMemory";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementHostPhysicalMemory := {
  name := c_HostAdmin_dataElementHostPhysicalMemory,
  valueType := intType,
  description := "This data element returns the physical memory available on the host"
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementProcName
//
// Purpose:
//   This data element returns the name of the given process (=selfName), type charstring
//
// Parameters:
//   c_HostAdmin_paramNameProcID	  - process id
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementProcName := "ProcName";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementProcName := {
  name := c_HostAdmin_dataElementProcName,
  valueType := charstringType,
  description := "This data element returns the name of the given process (=selfName)",
  parameters := {
    {
      name := c_HostAdmin_paramNameProcID,
      typeDescriptor := {
        reference := {
          typeName := c_HostAdmin_Help_iteratorProcesses.typeDescriptor.typeName
        }
      },
      description := "process id",
      exampleValue := "1"
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementProcCPULoad
//
// Purpose:
//   This data element returns the CPU load of the given process, type float
//
// Parameters:
//   c_HostAdmin_paramNameProcID	  - process id
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementProcCPULoad := "ProcCPULoad";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementProcCPULoad := {
  name := c_HostAdmin_dataElementProcCPULoad,
  valueType := floatType,
  description := "This data element returns the CPU load of the process",
  parameters := {
    {
      name := c_HostAdmin_paramNameProcID,
      typeDescriptor := {
        reference := {
          typeName := c_HostAdmin_Help_iteratorProcesses.typeDescriptor.typeName
        }
      },
      description := "process id",
      exampleValue := "1"
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_dataElementProcMEMUsage
//
// Purpose:
//   This data element returns the memory usage of the given process, type integer
//
// Parameters:
//   c_HostAdmin_paramNameProcID	  - process id
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_dataElementProcMEMUsage := "ProcMEMUsage";

const EPTF_DataSource_Help_DataElement c_HostAdmin_Help_dataElementProcMEMUsage := {
  name := c_HostAdmin_dataElementProcMEMUsage,
  valueType := intType,
  description := "This data element returns the memory usage of the given process",
  parameters := {
    {
      name := c_HostAdmin_paramNameProcID,
      typeDescriptor := {
        reference := {
          typeName := c_HostAdmin_Help_iteratorProcesses.typeDescriptor.typeName
        }
      },
      description := "process id",
      exampleValue := "1"
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_paramNameCPU_ID
//
// Purpose:
//   Specifies a CPU by its number
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_paramNameCPU_ID := "CPU_ID"

///////////////////////////////////////////////////////////////////////////////
// Constant: c_HostAdmin_paramNameProcID
//
// Purpose:
//   Specifies a process by its ID
//
///////////////////////////////////////////////////////////////////////////////
const charstring c_HostAdmin_paramNameProcID := "ProcID"


// the datasource help database for HostAdmin
const EPTF_DataSource_Help_DataElementChildren c_EPTF_HostAdmin_help := {
  {
    dataElement := c_EPTF_HostAdmin_Help_dataElement_help
  },
  {
    dataElement := c_HostAdmin_Help_iteratorProcesses
  },
  {
    dataElement := c_HostAdmin_Help_dataElementProcName
  },
  {
    dataElement := c_HostAdmin_Help_dataElementProcCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementProcMEMUsage
  },
  {
    dataElement := c_HostAdmin_Help_iteratorCPUs
  },
  {
    dataElement := c_HostAdmin_Help_dataElementCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementHostNumCPUs
  },
  {
    dataElement := c_HostAdmin_Help_dataElementHostPhysicalMemory
  },
  {
    dataElement := c_HostAdmin_Help_dataElementHostFreeMemory
  },
  {
    dataElement := c_HostAdmin_Help_dataElementHostName
  },
  {
    dataElement := c_HostAdmin_Help_dataElementIPv4Addr
  },
  {
    dataElement := c_HostAdmin_Help_dataElementIPv6Addr
  },
  {
    dataElement := c_HostAdmin_Help_dataElementHostCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementMinCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementMaxCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementAvgCPULoad
  },
  {
    dataElement := c_HostAdmin_Help_dataElementResetMinMaxAvgCPULoad
  }    
}
  

} // module

