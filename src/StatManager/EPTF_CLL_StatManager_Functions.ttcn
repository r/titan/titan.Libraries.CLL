///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  Module: EPTF_CLL_StatManager_Functions
// 
//  Purpose:
//    This module contains function definitions for TTCN-3 EPTF StatManager 
//    implementation.
// 
//  Module Parameters:
//    
//
//  Module depends on:
//    <EPTF_CLL_DataSource_Definitions>
//    <EPTF_CLL_DataSource_Functions>
//    <EPTF_CLL_DataSourceClient_Functions>
//    <EPTF_CLL_StatManager_Definitions>
//    <EPTF_CLL_StatMeasure_Definitions>
//    <EPTF_CLL_StatMeasure_Functions>
//    <EPTF_CLL_Logging_Definitions>
//    <EPTF_CLL_Logging_Functions>
//    <EPTF_CLL_Base_Functions>
//    <EPTF_CLL_HashMapStr2Int_Functions>
//    <EPTF_CLL_Variable_Definitions>
//    <EPTF_CLL_Variable_Functions>
//
//  Public functions:
//    <f_EPTF_StatManager_init_CT>    
// 
//  Current Owner:
//    Balazs Lugossy (EBALLUG)
// 
//  Last Review Date:
//    
//
//  Detailed Comments:
//    This module contains function definitions for TTCN-3 EPTF StatManager 
//    implementation.
// 
///////////////////////////////////////////////////////////////////////////////

module EPTF_CLL_StatManager_Functions {

import from EPTF_CLL_DataSource_Definitions all;
import from EPTF_CLL_DataSource_Functions all;
import from EPTF_CLL_DataSourceClient_Functions all;
import from EPTF_CLL_StatManager_Definitions all;
import from EPTF_CLL_StatMeasure_Definitions all;
import from EPTF_CLL_StatMeasure_Functions all;
import from EPTF_CLL_Logging_Definitions all;
import from EPTF_CLL_Logging_Functions all;
import from EPTF_CLL_Base_Functions all;
import from EPTF_CLL_HashMapStr2Int_Functions all;
import from EPTF_CLL_Variable_Definitions all;
import from EPTF_CLL_Variable_Functions all;
import from EPTF_CLL_Common_Definitions all;

group ModulePars{
  modulepar boolean tsp_debug_EPTF_StatManager_Functions := false;
}//ModulePars


///////////////////////////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_init_CT
// 
//  Purpose:
//    Initializes the EPTF_StatManager_CT component
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    This function should be called before using the <EPTF_StatManager_CT>
//    component.
//
///////////////////////////////////////////////////////////////////////////////
public function f_EPTF_StatManager_init_CT(
  in charstring pl_selfName,
  in EPTF_DataSource_CT pl_dataSourceCompRef := null) 
runs on EPTF_StatManager_CT 
{
  if (v_EPTF_StatManager_initialized) {
    return; // already initialized
  }

  f_EPTF_Logging_init_CT(pl_selfName);
  v_StatManager_loggingMaskId := f_EPTF_Logging_registerComponentMasks(tsp_EPTF_StatManager_loggingComponentMask, c_EPTF_StatManager_loggingEventClasses, EPTF_Logging_CLL);
  if(tsp_debug_EPTF_StatManager_Functions) {
    f_EPTF_Logging_enableLocalMask(v_StatManager_loggingMaskId, c_EPTF_StatManager_loggingClassIdx_Debug);
  } else {
    f_EPTF_Logging_disableLocalMask(v_StatManager_loggingMaskId, c_EPTF_StatManager_loggingClassIdx_Debug);
  }
  //f_EPTF_Var_init_CT(pl_selfName);

  if (pl_dataSourceCompRef == null or pl_dataSourceCompRef == self) {
    pl_dataSourceCompRef := self;
    f_EPTF_DataSource_init_CT(pl_selfName);
  }
  f_EPTF_DataSourceClient_init_CT(pl_selfName, pl_dataSourceCompRef);
  f_EPTF_StatMeasure_init_CT(pl_selfName);
  f_EPTF_str2int_HashMap_Init(pl_selfName);

  v_EPTF_StatManager_limitStatVarsHashMap := f_EPTF_str2int_HashMap_New("StatManager_LimitStatVars");

  f_EPTF_DataSourceClient_registerData(c_StatManager_DataSource_sourceId, pl_selfName, refers(f_EPTF_StatManager_DSProcessData));
  f_EPTF_DataSourceClient_registerDataValue(c_StatManager_DataSource_sourceId, pl_selfName, refers(f_EPTF_StatManager_DSProcessDataValue));
  f_EPTF_DataSourceClient_sendReady(c_StatManager_DataSource_sourceId, f_EPTF_Base_selfName());

  v_EPTF_StatManager_initialized := true;

  f_EPTF_Base_registerCleanup(refers(f_EPTF_StatManager_cleanup_CT));

  f_EPTF_StatManager_debug(
    "---- EPTF_StatManager INIT "&pl_selfName&" DONE ----");
}

///////////////////////////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_cleanup_CT
// 
//  Purpose:
//    This function should be called before the <EPTF_StatManager_CT> component
//    shuts down.
//
//  Parameters:
//    -
//
//  Return Value:
//    -
//
//  Errors:
//    -
//
//  Detailed Comments:
//    The function is registered by <f_EPTF_Base_RegisterCleanup> as a 
//    cleanup function in the init function for the <EPTF_StatManager_CT> 
//    component.
//    This function makes it possible to be able to shut down the
//    <EPTF_StatManager_CT> component gracefully.
//    
///////////////////////////////////////////////////////////////////////////////
private function f_EPTF_StatManager_cleanup_CT() 
runs on EPTF_StatManager_CT 
{
  if (v_EPTF_StatManager_initialized == false) {
    return;
  }
  f_EPTF_str2int_HashMap_DeleteById(v_EPTF_StatManager_limitStatVarsHashMap);
  v_EPTF_StatManager_limitStatVarsHashMap := -1;
  v_EPTF_StatManager_initialized := false;
  f_EPTF_StatManager_debug("----- STATMANAGER CLEANUP DONE -------");
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_debug
// 
//  Purpose:
//    Function to log a debug message from StatManager feature.
//
//  Parameters:
//    - pl_message - *in* *charstring* - the message to log
//
//  Return Value:
//    -
//
//  Errors & assertions:
//    - 
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_StatManager_debug(in @lazy charstring pl_message)
runs on EPTF_StatManager_CT
{
  f_EPTF_Logging_debugV2(pl_message, v_StatManager_loggingMaskId, {c_EPTF_StatManager_loggingClassIdx_Debug});
}

///////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_warning
// 
//  Purpose:
//    Function to log a warning message from StatManager feature.
//
//  Parameters:
//    - pl_message - *in* *charstring* - the message to log
//
//  Return Value:
//    -
//
//  Errors & assertions:
//    - 
//
//  Detailed Comments:
//    -
//
///////////////////////////////////////////////////////////
private function f_EPTF_StatManager_warning(in @lazy charstring pl_message)
runs on EPTF_StatManager_CT
{
  f_EPTF_Logging_warningV2(pl_message, v_StatManager_loggingMaskId, {c_EPTF_StatManager_loggingClassIdx_Warning});
}

///////////////////////////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_getOrCreateLimitStat
// 
//  Purpose:
//    Creates a limit statistic from the provided variable then a variable to this statistic thus it is possible
//    from the custom GUI descriptor to query the color of a LED depending on the value of the original variable
//
//  Parameters:
//    pl_varIdx - *in* *integer* - the index of variable for which the limits statistic will be created
//    pl_limitsPar - *in* *EPTF_StatMeasure_LimitsPar*  - the parameters for the limits statistic
//
//  Return Value:
//    integer - The index of the variable that holds the actual state of the LED
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////////////////////
private function f_EPTF_StatManager_getOrCreateLimitStat(
  in integer pl_varId,
  in EPTF_StatMeasure_LimitsPar pl_limitsPar)
runs on EPTF_StatManager_CT return integer
{
  var EPTF_StatMeasure_StatParams vl_limitPar;
  vl_limitPar.limits := pl_limitsPar;
  var integer vl_statId := -1;  
  var integer vl_limitStatVarId := -1;
  var charstring vl_hashKey:= log2str(pl_varId) & log2str(pl_limitsPar);

  if (f_EPTF_str2int_HashMap_Find(v_EPTF_StatManager_limitStatVarsHashMap, vl_hashKey, vl_limitStatVarId))
  {
    return vl_limitStatVarId;
  }

  vl_statId := f_EPTF_StatMeasure_newStatWithParams(pl_varId, vl_limitPar);
  vl_limitStatVarId := f_EPTF_StatMeasure_createVarFromStat(vl_statId);

  f_EPTF_str2int_HashMap_Insert(v_EPTF_StatManager_limitStatVarsHashMap, vl_hashKey, vl_limitStatVarId);

  f_EPTF_Var_refreshContent(pl_varId); // calculate initial value of the stat
  return vl_limitStatVarId;
}

private function f_EPTF_StatManager_gosCalcFn(in integer pl_idx, in EPTF_IntegerList pl_argList, in EPTF_IntegerList pl_nonVarArgList, inout EPTF_Var_DirectContent pl_retVal) runs on EPTF_StatManager_CT {
  pl_retVal := {floatVal := 0.0}
  var integer vl_nofSucc,vl_nofFail,vl_nofTimeout,vl_nofError;
  vl_nofSucc := f_EPTF_Var_getIntValue(pl_argList[0]);
  vl_nofFail := f_EPTF_Var_getIntValue(pl_argList[1]);
  vl_nofTimeout := f_EPTF_Var_getIntValue(pl_argList[2]);
  vl_nofError := f_EPTF_Var_getIntValue(pl_argList[3]);
  if (vl_nofSucc>0) {
    pl_retVal.floatVal := 100.0*int2float(vl_nofSucc)/int2float(vl_nofSucc+vl_nofFail+vl_nofTimeout+vl_nofError);
  }
}

///////////////////////////////////////////////////////////////////////////////
//  Function: f_EPTF_StatManager_getOrCreateGoSStat
// 
//  Purpose:
//    Creates a GoS statistics from the provided variable names then a variable to this statistic thus it is possible
//    from the custom GUI descriptor to query the value
//
//  Parameters:
//    pl_gosPar - *in* *EPTF_StatManager_GoSPar*  - the parameters for the gos statistic
//
//  Return Value:
//    integer - The index of the variable that holds the actual value of the GoS statistics
//
//  Errors:
//    -
//
//  Detailed Comments:
//
///////////////////////////////////////////////////////////////////////////////
private function f_EPTF_StatManager_getOrCreateGoSStat(
  in EPTF_StatManager_GoSPar pl_gosPar)
runs on EPTF_StatManager_CT return integer
{
  var integer vl_gosStatVarId := -1;
  var charstring vl_hashKey:= "GoS_"&log2str(pl_gosPar.nofSuccVarId);

  if (f_EPTF_str2int_HashMap_Find(v_EPTF_StatManager_limitStatVarsHashMap, vl_hashKey, vl_gosStatVarId))
  {
    return vl_gosStatVarId;
  }

  f_EPTF_Var_newFloat(vl_hashKey,0.123,vl_gosStatVarId)

  f_EPTF_Var_subscribeLocal(vl_gosStatVarId,
    {{
      refers(f_EPTF_StatManager_gosCalcFn),
      {pl_gosPar.nofSuccVarId,pl_gosPar.nofFailVarId,pl_gosPar.nofTimeoutVarId,pl_gosPar.nofErrorVarId},
      {}
    }}
  );
  
  f_EPTF_str2int_HashMap_Insert(v_EPTF_StatManager_limitStatVarsHashMap, vl_hashKey, vl_gosStatVarId);

  return vl_gosStatVarId;
}

private function f_EPTF_StatManager_DSProcessData(
  out charstring pl_dataVarName,
  in charstring pl_source,
  in charstring pl_ptcName,
  in charstring pl_element,
  in EPTF_DataSource_Params pl_params)
runs on EPTF_StatManager_CT return integer
{
  var EPTF_StatMeasure_LimitsPar vl_limitspar:={-1, led_black, false, {}};
  var integer vl_i:=0, vl_size:=0, vl_varId:=-1;

  select(pl_element){
    case(c_StatManager_dataElementGoS) {
      var EPTF_StatManager_GoSPar vl_gosPar := {-1,-1,-1,-1}

      vl_size := sizeof(pl_params);

      for(vl_i:=0; vl_i<vl_size; vl_i:=vl_i+1){
        select(pl_params[vl_i].paramName){
          case(c_StatManager_paramName_SuccessVarId){
            if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_gosPar.nofSuccVarId)){
                pl_dataVarName := log2str(%definitionId& ": Wrong "&c_StatManager_paramName_SuccessVarId&" parameter was given! No GoS statistic will be returned!")
                f_EPTF_StatManager_warning(pl_dataVarName);
                return -1;
            }
          }
          case(c_StatManager_paramName_FailVarId){
            if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_gosPar.nofFailVarId)){
                pl_dataVarName := log2str(%definitionId& ": Wrong "&c_StatManager_paramName_FailVarId&" parameter was given! No GoS statistic will be returned!")
                f_EPTF_StatManager_warning(pl_dataVarName);
                return -1;
            }
          }
          case(c_StatManager_paramName_TimeoutVarId){
            if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_gosPar.nofTimeoutVarId)){
                pl_dataVarName := log2str(%definitionId& ": Wrong "&c_StatManager_paramName_TimeoutVarId&" parameter was given! No GoS statistic will be returned!")
                f_EPTF_StatManager_warning(pl_dataVarName);
                return -1;
            }
          }
          case(c_StatManager_paramName_ErrorVarId){
            if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_gosPar.nofErrorVarId)){
                pl_dataVarName := log2str(%definitionId& ": Wrong "&c_StatManager_paramName_ErrorVarId&" parameter was given! No GoS statistic will be returned!")
                f_EPTF_StatManager_warning(pl_dataVarName);
                return -1;
            }
          } case else {
             pl_dataVarName := log2str(%definitionId& ": The parameter name "& pl_params[vl_i].paramName & " is not valid!");
             f_EPTF_StatManager_warning(pl_dataVarName);
             return -1;
          }
        }
      }
        
      var integer vl_idx := f_EPTF_StatManager_getOrCreateGoSStat(vl_gosPar);
      if (vl_idx == -1) {
        return -1;
      }
      pl_dataVarName :=  f_EPTF_Var_getName(vl_idx);
      return 0;

    }
    case(c_StatManager_dataElementLEDlimit){
      vl_size := sizeof(pl_params);
      
    for(vl_i:=0; vl_i<vl_size; vl_i:=vl_i+1){
      select(pl_params[vl_i].paramName){
        case(c_StatManager_paramName_blackLimit){
          if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
              vl_limitspar.limits[sizeof(vl_limitspar.limits)].blackLimit := str2float(pl_params[vl_i].paramValue);
            }                     
        }
        
        case(c_StatManager_paramName_blueLimit){
          if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
              vl_limitspar.limits[sizeof(vl_limitspar.limits)].blueLimit := str2float(pl_params[vl_i].paramValue);
            }                     
        }
        
        case(c_StatManager_paramName_redLimit){
          if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
              vl_limitspar.limits[sizeof(vl_limitspar.limits)].redLimit := str2float(pl_params[vl_i].paramValue);
            }                     
        }
        
        case(c_StatManager_paramName_yellowLimit){
          if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
              vl_limitspar.limits[sizeof(vl_limitspar.limits)].yellowLimit := str2float(pl_params[vl_i].paramValue);
            }                     
        }
        
        case(c_StatManager_paramName_greenLimit){
          if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
              vl_limitspar.limits[sizeof(vl_limitspar.limits)].greenLimit := str2float(pl_params[vl_i].paramValue);
            }                     
        }
        
        case(c_StatManager_paramName_defaultColor){
          if(not f_EPTF_StatManager_checkColor(pl_params[vl_i].paramValue, vl_limitspar)){
              f_EPTF_StatManager_warning(%definitionId& ": Setting '"&c_StatManager_paramName_defaultColor&"' was unsuccessful! Using default color: black");
              vl_limitspar.defaultColor := led_black;
          }
        }
        
        case(c_StatManager_paramName_enableValueInLEDText){
          if(not f_EPTF_StatManager_checkEnabledLEDText(pl_params[vl_i].paramValue, vl_limitspar)){
              f_EPTF_StatManager_warning(%definitionId& ": Setting '"&c_StatManager_paramName_enableValueInLEDText&"' was unsuccessful! Setting it to false");
              vl_limitspar.enableValueInLEDText:=false;
          }
        }
        
        case(c_StatManager_paramName_VarId){
          if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_varId)){
              f_EPTF_StatManager_warning(%definitionId& ": Wrong "&c_StatManager_paramName_VarId&" parameter was given! No limit statistic will be returned!");
              return -1;
          }
        }
        
        case(c_StatManager_paramName_referenceVarId){
          if(not f_EPTF_StatManager_checkvarId(pl_params[vl_i].paramValue, vl_limitspar.referenceVarId)){
              f_EPTF_StatManager_warning(%definitionId& ": Wrong "&c_StatManager_paramName_referenceVarId&" parameter was given! Setting to -1!");
              vl_limitspar.referenceVarId:=-1;
          }
        }
        case else{
          f_EPTF_StatManager_warning(%definitionId& pl_params[vl_i].paramName & " is not a valid parameter name!");
        }
      }
    }
    
    if(vl_varId == -1){
      f_EPTF_StatManager_warning(%definitionId& ": No "&c_StatManager_paramName_VarId&" parameter was given! No limit statistic will be returned!");
        return -1
    }    

    var integer vl_idx := f_EPTF_StatManager_getOrCreateLimitStat(vl_varId, vl_limitspar);
      if (vl_idx == -1) {
        return -1;
      }
      pl_dataVarName :=  f_EPTF_Var_getName(vl_idx);
      return 0;

    }
    case else {  //error, no rule for that
      pl_dataVarName := "";
      f_EPTF_StatManager_warning(%definitionId& ": unhandled element: "& pl_element);
      return -1;
    }    
  }
}

  ///////////////////////////////////////////////////////////
  //  Function: f_EPTF_StatManager_DSProcessDataValue
  // 
  //  Purpose:
  //    Processes the incoming DataValue requests - iterators and external data elements - and gives back the value.
  //    It should be registered in the LoadRegulator_init_CT. Type function fcb_EPTF_DataSourceClient_dataValueHandler
  //
  //  Parameters:
  //    *out *<EPTF_Var_DirectContent>* pl_dataValue* - the value of the data or the iterator result
  //    *in charstring pl_source* - the name of the data source 'feature'
  //    *in charstring pl_ptcName* - the name of the ptc (ID of the PTC)  
  //    *in charstring pl_element* - the name of the data element
  //    *in* <EPTF_DataSource_Params> *pl_params* - the parameters
  //      of the data for the dataElement
  //
  //  Return Value:
  //    integer - error code (0 of OK, non zero if unsuccessful: e.g. invalid parameters given in pl_params)
  //
  //  Detailed Comments: 
  //    -
  //
  ///////////////////////////////////////////////////////////
  private function f_EPTF_StatManager_DSProcessDataValue(out EPTF_Var_DirectContent pl_dataValue,
                                                in charstring pl_source,
                                                in charstring pl_ptcName,
                                                in charstring pl_element,
                                                in EPTF_DataSource_Params pl_params)
  runs on EPTF_StatManager_CT return integer{   
    var integer vl_errorCode := -1;
    pl_dataValue := {unknownVal := {omit}}; // set it to invalid
    select( pl_element )
    {
      case(c_EPTF_DataSource_dataElement_Help) {
        vl_errorCode := f_EPTF_DataSource_handleHelp(pl_dataValue,pl_source,pl_params,c_EPTF_StatManager_help);
      }
      case(c_StatManager_dataElementLEDlimit) {
        var integer vl_size := sizeof(pl_params);
        var EPTF_StatMeasure_LimitsPar vl_limitspar:={-1, led_black, false, {}};
        var boolean vl_valueSet := false;
        var float vl_value := 0.0;
        var float vl_refValue := 0.0;
        
        for(var integer vl_i:=0; vl_i<vl_size; vl_i:=vl_i+1){
          select(pl_params[vl_i].paramName){
            case(c_StatManager_paramName_blackLimit){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_limitspar.limits[sizeof(vl_limitspar.limits)].blackLimit := str2float(pl_params[vl_i].paramValue);
              }                     
            }
          
            case(c_StatManager_paramName_blueLimit){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_limitspar.limits[sizeof(vl_limitspar.limits)].blueLimit := str2float(pl_params[vl_i].paramValue);
              }                     
            }
            
            case(c_StatManager_paramName_redLimit){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_limitspar.limits[sizeof(vl_limitspar.limits)].redLimit := str2float(pl_params[vl_i].paramValue);
              }                     
            }
            
            case(c_StatManager_paramName_yellowLimit){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_limitspar.limits[sizeof(vl_limitspar.limits)].yellowLimit := str2float(pl_params[vl_i].paramValue);
              }                     
            }
            
            case(c_StatManager_paramName_greenLimit){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_limitspar.limits[sizeof(vl_limitspar.limits)].greenLimit := str2float(pl_params[vl_i].paramValue);
              }                     
            }
            
            case(c_StatManager_paramName_defaultColor){
              if(not f_EPTF_StatManager_checkColor(pl_params[vl_i].paramValue, vl_limitspar)){
                f_EPTF_StatManager_warning(%definitionId& ": Setting '"&c_StatManager_paramName_defaultColor&"' was unsuccessful! Using default color: black");
                vl_limitspar.defaultColor := led_black;
              }
            }
            
            case(c_StatManager_paramName_enableValueInLEDText){
              if(not f_EPTF_StatManager_checkEnabledLEDText(pl_params[vl_i].paramValue, vl_limitspar)){
                f_EPTF_StatManager_warning(%definitionId& ": Setting '"&c_StatManager_paramName_enableValueInLEDText&"' was unsuccessful! Setting it to false");
                vl_limitspar.enableValueInLEDText:=false;
              }
            }
            
            case(c_StatManager_paramName_value){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_value := str2float(pl_params[vl_i].paramValue);
                vl_valueSet := true;
              }
            }
            
            case(c_StatManager_paramName_referenceValue){
              if(f_EPTF_StatManager_checkLimit(pl_params[vl_i].paramValue)){
                vl_refValue := str2float(pl_params[vl_i].paramValue);
              }
            }
            
            case else{
              f_EPTF_StatManager_warning(%definitionId& pl_params[vl_i].paramName & " is not a valid parameter name!");
            }
          }
        }
        
        if (vl_valueSet) {
          f_EPTF_StatMeasure_bubblesort_LimitList(vl_limitspar.limits);
          pl_dataValue.statusLEDVal := f_EPTF_StatMeasure_calculateValue_limits(
            pl_value := vl_value,
            pl_limits := vl_limitspar.limits,
            pl_referenceValue := vl_refValue,
            pl_defaultLED := {vl_limitspar.defaultColor, ""},
            pl_enableValueInLEDText := vl_limitspar.enableValueInLEDText
          );
          vl_errorCode := 0;
        } else {
          pl_dataValue.charstringVal := "No value was given in the parameters";
          vl_errorCode := 5;
        }
      }
      case else
      {
      }
    }
    return vl_errorCode;
  }  

private function f_EPTF_StatManager_checkLimit(in charstring pl_lim) runs on EPTF_StatManager_CT return boolean{  
  if(match(pl_lim, pattern c_EPTF_StatManager_floatNumber)){
    return true;
    
    
  }else{
    f_EPTF_StatManager_warning(%definitionId& ": " & pl_lim & " is not a legal float number!");
    return false;
  }  
}

private function f_EPTF_StatManager_checkColor(in charstring pl_color, inout EPTF_StatMeasure_LimitsPar pl_limitsPar) runs on EPTF_StatManager_CT return boolean{
  select(pl_color){
    case("blue"){pl_limitsPar.defaultColor:=led_blue}
    case("black"){pl_limitsPar.defaultColor:=led_black}
    case("yellow"){pl_limitsPar.defaultColor:=led_yellow}
    case("green"){pl_limitsPar.defaultColor:=led_green}
    case("red"){pl_limitsPar.defaultColor:=led_red}
    case else{
      f_EPTF_StatManager_warning(%definitionId& ": " & pl_color & " is not a legal LED color!");
      return false;}
  }
  return true;
}

private function f_EPTF_StatManager_checkEnabledLEDText(in charstring pl_enabled, inout EPTF_StatMeasure_LimitsPar pl_limitsPar) runs on EPTF_StatManager_CT return boolean{
  select(pl_enabled){
    case("yes"){pl_limitsPar.enableValueInLEDText:=true}
    case("no"){pl_limitsPar.enableValueInLEDText:=false}
    case else{
      f_EPTF_StatManager_warning(%definitionId& ": " & pl_enabled & " is not a legal parameter value for: '"&c_StatManager_paramName_enableValueInLEDText&"'!");
      return false}  
  }
  return true;
}

private function f_EPTF_StatManager_checkvarId(in charstring pl_varId, out integer pl_IvarId) runs on EPTF_StatManager_CT return boolean{
  pl_IvarId := f_EPTF_Var_getId(pl_varId);
    
  if(pl_IvarId>-1)
  {   
    return true
  }else{    
    return false
  }
}

} // module
