///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_CLL_ExecCtrlTimeProfile_Definitions
// 
//  Purpose:
//    This module contains type definitions for EPTF_CLL_ExecCtrl_TimeProfile implementation.
// 
//  Module depends on:
//    <EPTF_CLL_Common_Definitions>
// 
//  Module Parameters:
//    tsp_EPTF_ExecCtrl_TimeProfileDescrList - <EPTF_ExecCtrl_TimeProfileDescrList> - the time profile descriptors
//    tsp_EPTF_ExecCtrl_TimeProfileList - <tsp_EPTF_ExecCtrl_TimeProfileList> - the time profiles
//    tsp_EPTF_ExecCtrl_TimeProfile2TcList - <EPTF_ExecCtrl_TimeProfile2TcList> - traffic case to time profile assignment
//
//  Current Owner:
//    ethjgi
// 
//  Last Review Date:
//    2008-03-10
//
//  Detailed Comments:
//    This module contains type definitions for EPTF_CLL_ExecCtrl_TimeProfile implementation.
//
///////////////////////////////////////////////////////////////

module EPTF_CLL_ExecCtrlTimeProfile_Definitions {

import from EPTF_CLL_Common_Definitions all;

modulepar EPTF_ExecCtrl_TimeProfileDescrList tsp_EPTF_ExecCtrl_TimeProfileDescrList := {};
modulepar EPTF_ExecCtrl_TimeProfileList tsp_EPTF_ExecCtrl_TimeProfileList := {};
modulepar EPTF_ExecCtrl_TimeProfile2TcList tsp_EPTF_ExecCtrl_TimeProfile2TcList := {};

// time dependent targetValue (e.g. CPS):

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfile_TCName
//
// Purpose:
//   Type for identify a traffic case by names
//
// Elements:
//   - eGrpName - *charstring* - the name of entity group
//   - scName - *charstring* - the name of scenario
//   - tcName - *charstring* - the name of traffic case
//       
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TimeProfile_TCName {  
  charstring eGrpName,
  charstring scName,
  charstring tcName
}


// this defined the DB:

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileItem
//
// Purpose:
//   Defines an item in the time profile
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TimeProfileItem {
  float time, // time elapsed since the profile was started (in seconds)
  float targetValue   // targetValue to use from that time
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileItemList
//
// Purpose:
//   Defines the time profile as a list of <EPTF_ExecCtrl_TimeProfileItem>
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TimeProfileItem EPTF_ExecCtrl_TimeProfileItemList;

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TcTimeProfileItem
//
// Purpose:
//   Time profile data type for a traffic case
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TcTimeProfileItem  {
  EPTF_IntegerList timeProfileIdList, // ids in EPTF_ExecCtrl_TimeProfileDescrList
  integer timeProfileIdx, // id in EPTF_ExecCtrl_TimeProfileList
  EPTF_IntegerList eventIdList  // id of the scheduled event for these time profiles
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TcTimeProfile
//
// Purpose:
//   List of traffic case time profile items <EPTF_ExecCtrl_TcTimeProfileItem>
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TcTimeProfileItem EPTF_ExecCtrl_TcTimeProfile; // per tc

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_ScTimeProfileItem
//
// Purpose:
//   List of traffic case time profiles
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_ScTimeProfileItem {
  EPTF_ExecCtrl_TcTimeProfile tcTimeProfile
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_ScTimeProfile
//
// Purpose:
//   Database of time profile data for scenarios
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_ScTimeProfileItem EPTF_ExecCtrl_ScTimeProfile; // timeprofile per sc: scTimeProfile[<scidx>].tcTimeProfile[<tcidx>].timeProfileIdList

// this defines the tsp:

// defines a time profile:
///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileDescr
//
// Purpose:
//   Type to define a time profile
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TimeProfileDescr {
  charstring name,
  EPTF_ExecCtrl_TimeProfileItemList timeProfileData // the profile data
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileDescrList
//
// Purpose:
//   List of time profiles <EPTF_ExecCtrl_TimeProfileDescr>
//   This type can be used to define all time profiles
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TimeProfileDescr EPTF_ExecCtrl_TimeProfileDescrList; // list of time profiles


///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileData
//
// Purpose:
//   Time profile data for a traffic case
//
///////////////////////////////////////////////////////////
type record  EPTF_ExecCtrl_TimeProfileData {
  charstring timeProfileDescrName, // the name of the time profile descriptor
  float startTime,            // the time when the profile should be activated (with respect to start of the execution) (in seconds)
  float periodOfRepetition,   // the period that the timeProfileData should be repeated with
  integer nofRepetition       // number of times the timeProfileData should be repeated (-1: infinity, 0: executed only once (never repeated), 1: executed two times (repeated once), etc.)
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileDataList
//
// Purpose:
//   List of time profile traffic case data <EPTF_ExecCtrl_TimeProfileData>
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TimeProfileData EPTF_ExecCtrl_TimeProfileDataList;

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfile
//
// Purpose:
//   Time profile definition
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TimeProfile {
  charstring name,                                    // name of the time profile
  EPTF_ExecCtrl_TimeProfileDataList timeProfileList   // the list of time profiles that should be used
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfileList
//
// Purpose:
//   List of time profile traffic case data <EPTF_ExecCtrl_TimeProfile>
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TimeProfile EPTF_ExecCtrl_TimeProfileList;

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfile2Tc
//
// Purpose:
//   Time profile data for a traffic case
//
///////////////////////////////////////////////////////////
type record EPTF_ExecCtrl_TimeProfile2Tc {
  EPTF_ExecCtrl_TimeProfile_TCName tcId, // id of the traffic case
  charstring timeProfileName // the name of time profiles that should be used for this TC
}

///////////////////////////////////////////////////////////
// Type: EPTF_ExecCtrl_TimeProfile2TcList
//
// Purpose:
//   List of traffic case time profiles <EPTF_ExecCtrl_TimeProfile2Tc>
//   This type can be used to define the time profiles for all traffic cases
//
///////////////////////////////////////////////////////////
type record of EPTF_ExecCtrl_TimeProfile2Tc EPTF_ExecCtrl_TimeProfile2TcList; // defines which timeProfile belongs to which tc



} // module EPTF_CLL_ExecCtrlTimeProfile_Definitions
