///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
///////////////////////////////////////////////////////////////////////////////
#include <deque>
#include <string>
#include <sstream>

#if (__GNUC__ < 4)//earlier than gcc 4.0
 #include <ext/hash_map>
 #if (__GNUC__ == 3 && __GNUC_MINOR__ >=1) //gcc [3.1 , 4.0[
  using __gnu_cxx::hash_map;
  using __gnu_cxx::hash;
  #define hash_ns __gnu_cxx::hash_map 
 #else //earlier than gcc 3.1
  using std::hash_map;
  using std::hash; 
  #define hash_ns std::hash_map
 #endif
#else //from gcc 4.0
 #if (__GNUC__ == 4 && __GNUC_MINOR__ < 3 ) //gcc [4.0 , 4.3[
  #include <ext/hash_map>
  using __gnu_cxx::hash_map;
  using __gnu_cxx::hash; 
  #define hash_ns __gnu_cxx::hash_map
 #else //from gcc 4.3
  #include <tr1/unordered_map>
  using std::tr1::unordered_map;  
  #define hash_ns std::tr1::unordered_map
 #endif
#endif

#include "EPTF_CLL_DsRestAPI_Timeline_Functions.hh"

struct TimelineEntry {
  int tp;
  std::deque<int> x;
  std::deque<std::string> y;
};

typedef hash_ns<std::string, TimelineEntry> Timeline;

Timeline * timeline = NULL;

void EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__MapCleanup() {
  if (timeline != NULL) {
    delete timeline;
    timeline = NULL;
  }
}

void EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__MapInit() {
  EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__MapCleanup();
  timeline = new Timeline;
}

void EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__InitMeasurement(const CHARSTRING& pl__key, const INTEGER& pl__maxpoints) {
  std::string key(pl__key);
  std::deque<int> x;
  std::deque<std::string> y;
  TimelineEntry entry = {
    0, x, y
  };
  std::pair<std::string, TimelineEntry> toInsert(key, entry);
  timeline->insert(toInsert);
}

void EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__AddMeasurement(const CHARSTRING& pl__key, const INTEGER& pl__time, const CHARSTRING& pl__content, const INTEGER& tp, const INTEGER& pl__maxpoints) {
  std::string key(pl__key);
  Timeline::iterator item = timeline->find(key);
  if (item != timeline->end()) {
    
    TimelineEntry& entry = item->second;
    
    if (entry.x.size() > (unsigned int) pl__maxpoints - 1 && pl__maxpoints != 0) {
      entry.x.pop_front();
      entry.y.pop_front();
    }
    
    entry.x.push_back(pl__time);
    entry.y.push_back((const char*) pl__content);
    entry.tp = tp;
  } else {
    std::deque<int> x(1, pl__time);
    std::deque<std::string> y(1, std::string(pl__content));
    TimelineEntry entry = {
      tp, x, y
    };
    std::pair<std::string, TimelineEntry> toInsert(key, entry);
    timeline->insert(toInsert);
  }
}

int getFirstIndex(const TimelineEntry& pl_entry, const int& pl__since) {
  int lastIndex = pl_entry.x.size();
  for (int i = lastIndex - 1; i >= 0; --i) {
      if (pl_entry.x[i] < pl__since) {
          return i + 1;
      }
  }
  return 0;
}

void getTimelineAsString(CHARSTRING& pl_timeline, const TimelineEntry& pl_entry, const int& pl_firstIndex = 0) {
  int size = pl_entry.x.size();
  std::stringstream ss;
  ss << "{\"tp\":" << pl_entry.tp << ",\"x\":[";
  if (pl_firstIndex < size) {
      ss << pl_entry.x[pl_firstIndex];
  }
  
  for (int i = pl_firstIndex + 1; i < size; ++i) {
      ss << "," << pl_entry.x[i];
  }
  ss << "],\"y\":[";
  if (pl_firstIndex < size) {
      ss << "\"" << pl_entry.y[pl_firstIndex] << "\"";
  }
  for (int i = pl_firstIndex + 1; i < size; ++i) {
      ss << ",\"" << pl_entry.y[i] << "\"";
  }
  ss << "]}";
  
  pl_timeline = ss.str().c_str();
}

BOOLEAN EPTF__CLL__DsRestAPI__Timeline__Functions::ef__EPTF__DsRestAPI__Timeline__GetTimeline(const CHARSTRING& pl__key, const INTEGER& pl__since, CHARSTRING& pl__timeline) {
  std::string key(pl__key);
  Timeline::const_iterator item = timeline->find(key);
  if (item != timeline->end()) {
    const TimelineEntry& entry = item->second;
    if (entry.x.size() > 0) {
      if (pl__since < 0) {
        getTimelineAsString(pl__timeline, entry);
      } else {
        getTimelineAsString(pl__timeline, entry, getFirstIndex(entry, pl__since));
      }
    } else {
      pl__timeline = "{\"tp\":0,\"x\":[],\"y\":[]}";
    }
    return true;
  } else {
    return false;
  }
}