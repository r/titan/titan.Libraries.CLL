///////////////////////////////////////////////////////////////////////////////
//                                                                           //
// Copyright (c) 2000-2019 Ericsson Telecom AB                               //
//                                                                           //
// All rights reserved. This program and the accompanying materials          //
// are made available under the terms of the Eclipse Public License v2.0     //
// which accompanies this distribution, and is available at                  //
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html                                 //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////
#include "EPTF_CLL_NameService_Definitions.hh"

///////////////////////////////////////////////////////////
//  Function: f__EPTF__NS__Client__upcast
// 
//  Purpose:
//    Implementation of the external function <f_EPTF_NS_Client_upcast>.
///////////////////////////////////////////////////////////
EPTF__CLL__Base__Definitions::EPTF__Base__CT EPTF__CLL__NameService__Definitions::f__EPTF__NS__Client__upcast(const EPTF__CLL__NameService__Definitions::EPTF__NS__Client__CT& pl__compRef) {
  return EPTF__CLL__Base__Definitions::EPTF__Base__CT((component)pl__compRef);
}

///////////////////////////////////////////////////////////
//  Function: f__EPTF__NS__Client__downcast
// 
//  Purpose:
//    Implementation of the external function <f_EPTF_NS_Client_downcast>.
///////////////////////////////////////////////////////////
EPTF__CLL__NameService__Definitions::EPTF__NS__Client__CT EPTF__CLL__NameService__Definitions::f__EPTF__NS__Client__downcast(const EPTF__CLL__Base__Definitions::EPTF__Base__CT& pl__compRef) {
  return EPTF__CLL__NameService__Definitions::EPTF__NS__Client__CT((component)pl__compRef);
}
